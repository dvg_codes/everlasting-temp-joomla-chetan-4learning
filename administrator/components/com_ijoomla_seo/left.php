<?php 
defined('_JEXEC') or die('Restricted access'); 
?>
	<link rel="StyleSheet" href="components/com_ijoomla_seo/javascript/dtree.css" type="text/css" />
	<script type="text/javascript" src="components/com_ijoomla_seo/javascript/dtree.js"></script>
	
	<table width="100%" cellspacing='3' cellpadding="4" >
	<tr><td valign='top' width="160" >
	<table width='100%' cellpadding='5' height='100%' class='menu_table'>
		<tr>
			<td>

        <a href="javascript: d.openAll();"><?php echo JText::_('OPEN_ALL', true) ?></a> | <a href="javascript: d.closeAll();"><?php echo JText::_('CLOSE_ALL', true )?></a><br />
	<br />
	<script type="text/javascript">
		d = new dTree('d');
		d.add( 0, -1, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_COMPONENT_TITLE"); ?>', 'index.php?option=com_ijoomla_seo', '', '', 'components/com_ijoomla_seo/images/icons/small/ijoomla.png' ) ;
        d.add( 749, 0, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_GURU_COURSE") ; ?>', 'http://www.ijoomla.com/redirect/seo/course.htm' , '','components/com_ijoomla_seo/images/icons/guru.png' ,'components/com_ijoomla_seo/images/icons/guru.png' ,'components/com_ijoomla_seo/images/icons/guru.png' ) ;        
		d.add( 750, 0, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_LM_SETTINGS"); ?>', 'index.php?option=com_ijoomla_seo&controller=config', '', '', 'components/com_ijoomla_seo/images/icons/small/settings.png', 'components/com_ijoomla_seo/images/icons/small/settings.png' ) ;

        d.add( 800, 0, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_MANAGERS") ; ?>', '' , '','components/com_ijoomla_seo/images/icons/small/readers.png' ,'components/com_ijoomla_seo/images/icons/small/readers.png' ,'components/com_ijoomla_seo/images/icons/small/readers.png' ) ;
	    d.add( 801, 800, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_METATAGS") ; ?>', 'index.php?option=com_ijoomla_seo&controller=menus&choosemain=1' ,'' ,'' ,'components/com_ijoomla_seo/images/icons/small/readers.png' ) ;
		d.add( 802, 800, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_KEYWORDS") ; ?>', 'index.php?option=com_ijoomla_seo&controller=keysmenus&choosemain=1', '', '', 'components/com_ijoomla_seo/images/icons/small/keywords_s.png') ;
        d.add( 803, 800, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_PAGES") ; ?>', 'index.php?option=com_ijoomla_seo&controller=pages', '', '', 'components/com_ijoomla_seo/images/icons/small/content.png' ) ;

		d.add( 804, 0, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_REDIRECTS") ; ?>', '', '', '', 
'components/com_ijoomla_seo/images/icons/small/redirects_s.png', 'components/com_ijoomla_seo/images/icons/small/redirects_s.png' ) ;
        d.add( 805, 804, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_LIST") ; ?>', 'index.php?option=com_ijoomla_seo&controller=redirect', '', '', 'components/com_ijoomla_seo/images/icons/small/content.png' ) ;
		d.add( 806, 804, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_CATEGORIES") ; ?>', 'index.php?option=com_ijoomla_seo&controller=redirectcategory', '', '', 'components/com_ijoomla_seo/images/icons/small/content.png' ) ;

		d.add( 807, 0, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_INTERNAL_LINKS") ; ?>', '', '', '', 
'components/com_ijoomla_seo/images/icons/small/internal_s.png','components/com_ijoomla_seo/images/icons/small/internal_s.png' ) ;
		d.add( 808, 807, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_INTERNAL_LIST") ; ?>', 'index.php?option=com_ijoomla_seo&controller=ilinks', '', '', 'components/com_ijoomla_seo/images/icons/small/content.png' ) ;
		d.add( 809, 807, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_CATEGORIES") ; ?>', 'index.php?option=com_ijoomla_seo&controller=ilinkscategory', '', '', 'components/com_ijoomla_seo/images/icons/small/content.png' ) ;							

		d.add( 810, 0, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_LANGUAGES") ; ?>', 'index.php?option=com_ijoomla_seo&controller=language&id=english.ijoomla_seo&hidemainmenu=1', '', '', 'components/com_ijoomla_seo/images/icons/small/language.png' ) ;

        d.add( 811, 0  , '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_DOCUMENTATION") ; ?>', '', '', '','components/com_ijoomla_seo/images/icons/small/support.png','components/com_ijoomla_seo/images/icons/small/support.png'  ) ;
        d.add( 815, 811, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_UPGRADE") ; ?>', 'http://www.ijoomla.com/redirect/general/latestversion.htm', '<?php echo JText::_("COM_IJOOMLA_SEO_LATEST_VERSION");?>', '_blank', 'components/com_ijoomla_seo/images/icons/small/latest_version.png' );
        d.add( 814, 811, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_VIDEO_TUTORIALS") ; ?>', 'http://www.ijoomla.com/redirect/seo/videos.htm', '<?php echo JText::_("COM_IJOOMLA_SEO_VIDEO_TUTORIALS");?>', '_blank', 'components/com_ijoomla_seo/images/icon_video.gif' );        
		d.add( 817, 811, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_FORUMS") ; ?>', 'http://www.ijoomla.com/redirect/seo/forum.htm', '<?php echo JText::_("COM_IJOOMLA_SEO_FORUMS");?>', '_blank', 'components/com_ijoomla_seo/images/icons/small/forum.png' );
		d.add( 818, 811, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_SUPPORT_HELP") ; ?>', 'http://www.ijoomla.com/redirect/general/support.htm', '<?php echo JText::_("COM_IJOOMLA_SEO_SUPPORT_HELP");?>', '_blank', 'components/com_ijoomla_seo/images/icons/small/support.png');
		d.add( 816, 811, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_CONTACT_US") ; ?>', 'http://www.ijoomla.com/redirect/general/contact.htm', '<?php echo JText::_("COM_IJOOMLA_SEO_CONTACT_US"); ?>', '_blank', 'components/com_ijoomla_seo/images/icons/small/contact.png' );
        d.add( 812, 811, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_FAQ") ; ?>', 'http://www.ijoomla.com/redirect/seo/faq.htm', '<?php echo JText::_("COM_IJOOMLA_SEO_FAQ");?>', '_blank', 'components/com_ijoomla_seo/images/icons/small/help.png' ) ;
		d.add( 815, 811, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_OTHER_COMPONENTS") ; ?>', 'http://www.ijoomla.com/redirect/general/othercomponents.htm', '<?php echo JText::_("COM_IJOOMLA_SEO_OTHER_COMPONENTS"); ?>', '_blank', 'components/com_ijoomla_seo/images/icons/small/other_components.png' );
	    d.add( 819, 811, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_TEMPLATES") ; ?>', 'http://www.ijoomla.com/redirect/general/templates.htm', '<?php echo JText::_("COM_IJOOMLA_SEO_TEMPLATES");?>', '_blank', 'components/com_ijoomla_seo/images/icons/small/templates.png' );
		d.add( 813, 811, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_IJOOMLA_WEBSITE") ; ?>', 'http://www.ijoomla.com', '<?php echo JText::_("COM_IJOOMLA_SEO_IJOOMLA_WEBSITE");?>', '_blank', 'components/com_ijoomla_seo/images/icons/small/ijoomla.png' );		

        d.add( 820, 0, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_ABOUT") ; ?>', 'index.php?option=com_ijoomla_seo&controller=about', '', '', 'components/com_ijoomla_seo/images/icons/small/about.png', 'components/com_ijoomla_seo/images/icons/small/about.png' );
        d.add( 821, 0, '&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_RATE_US") ; ?>', 'http://www.ijoomla.com/redirect/seo/rate.htm', '<?php echo JText::_("COM_IJOOMLA_SEO_RATE_US"); ?>', '_blank', 'components/com_ijoomla_seo/images/icons/small/contact.png' );
		document.write( d );
	</script>
			  </td></tr></table>
	</td><td valign='top' align='left'>