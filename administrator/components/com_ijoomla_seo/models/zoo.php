<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.utilities.date' );

jimport('joomla.application.component.modellist');

class iJoomla_SeoModelZoo extends JModelList{

	protected $_context = 'com_ijoomla_seo.zoo';
	private $total=0;

	function populateState(){
		// Initialize variables.
		$app = JFactory::getApplication('administrator');
		// Load the list state.
		$this->setState('list.start', $app->getUserStateFromRequest($this->_context . '.list.start', 'limitstart', 0, 'int'));
		$this->setState('list.limit', $app->getUserStateFromRequest($this->_context . '.list.limit', 'limit', $app->getCfg('list_limit', 25) , 'int'));
		$this->setState('selected', JRequest::getVar('cid', array()));
		$catid = $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		$this->setState('filter.catid', $catid);
	}
	
	function getPagination(){		
		if ($this->total == 0) { return false; }		
		$pagination=parent::getPagination();
		$pagination->total = $this->total;
		if($pagination->total%$pagination->limit>0){
			$nr_pages=intval($pagination->total/$pagination->limit)+1;
		}	
		else{
			$nr_pages=intval($pagination->total/$pagination->limit);
		}	
		$pagination->set('pages.total', $nr_pages);
		$pagination->set('pages.stop', $nr_pages);		
		return $pagination;
	}

	function existsZoo() {
		$db = &JFactory::getDBO();
		$sql = "SHOW TABLES";
		$db->setQuery($sql);
		$tables = $db->loadResultArray();
		$config =& JFactory::getConfig();
		if (!in_array($config->getValue( 'config.dbprefix' ) . "zoo_category", $tables)) { return false; } else { return true; }
	}	
	
	function getItems(){
		$config = new JConfig();
		$app		= JFactory::getApplication('administrator');
		$limistart	= $app->getUserStateFromRequest($this->context.'.list.start', 'limitstart');
		$limit		= $app->getUserStateFromRequest($this->context.'.list.limit', 'limit', $config->list_limit);
				
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$zoo_option = JRequest::getVar("zoo", "1");
		if($zoo_option == "1"){
			$this->sincronizeZooItemsWithSeo();		
			$query = $this->getListQueryItems();			
		}
		else{
			$this->sincronizeZooCategoriesWithSeo();
			$query = $this->getListQueryCategory();
		}
		$db->setQuery($query);
		$db->query();
		$result	= $db->loadObjectList();
		$this->total = count($result);
		
		$db->setQuery($query, $limistart, $limit);
		$db->query();
		$result	= $db->loadObjectList();			
		return $result;
	}
	
	function getCategs() {
		$db = &JFactory::getDBO();
		$search = JRequest::getVar("zoo");
		// only query the db if we select "items"
		if ($search == "1") {
			$sql = "SELECT id, name FROM #__zoo_category";
			$db->setQuery($sql);
			return $db->loadObjectList();
		}
		return false;
	}	
	
	function getListQueryItems(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
				
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','any','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
					
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_state = "";
			$filter_search = "";		
			$filter_missing = JRequest::getVar("value", "", "get");
			$this->setState('filter.missing', $filter_missing, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
		}	
							
		$where=" 1=1 ";		
				
		switch ($filter_missing){
			case "1":
				$where.= " and mt.titletag = '' ";
				break;
			case "2":
				$where.= ' and l.params like (\'%"metadata.keywords":""%\') ';
				break;
			case "3":
				$where.= ' and l.params like (\'%"metadata.description":""%\') ';				
				break;
			case "4":
				$where.= ' and ( l.params like (\'%"metadata.keywords":""%\') or l.params like (\'%"metadata.description":""%\') ) ';
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.=" and l.state =1 ";
				break;
			case "2":
				$where.=" and l.state =0 ";
				break;				
			default:
				break;
		}
		
		if($filter_search != ""){ 
			$where.=" and (l.name like '%".addslashes($filter_search)."%' or l.params like '%\"".addslashes($filter_search)."\"%') ";
		}
		
		if ((JRequest::getInt('zoo') == 1) && JRequest::getInt('itemcats_zoo') > 0) {
			$where .= " AND k.category_id = '" . JRequest::getInt('itemcats_zoo') . "' ";
		}		
				
		/*
		$query->clear();
		$query->select('l.id, l.name, l.params, mt.titletag');
		$query->from('#__zoo_item l');
		$query->join('LEFT', "`#__ijseo_metags` AS mt ON mt.id = l.id and mt.mtype='zoo_items'");
		//JOIN xc43m_zoo_category_item AS k ON l.id = k.item_id
		$query->join('LEFT', "`#__zoo_category_item` AS k ON l.id = k.item_id ")
		$query->where($where);
		*/
		
		$query = "
			SELECT l.id, l.name, l.params, mt.titletag
			FROM #__zoo_item AS l
			LEFT JOIN `#__ijseo_metags` AS mt ON mt.id = l.id AND mt.mtype = 'zoo_items' 
			LEFT JOIN `#__zoo_category_item` AS k ON l.id = k.item_id
			WHERE {$where}
			group by l.id
		";
		
		//echo (string)$query . "<br />";
		return $query;
	}
	
	function getListQueryCategory(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','any','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
					
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_state = "";
			$filter_search = "";
			$filter_missing = JRequest::getVar("value", "", "get");
			$this->setState('filter.missing', $filter_missing, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
		}	
							
		$where=" 1=1 ";		
					
		switch ($filter_missing){
			case "1":
				$where.= " and mt.titletag = '' ";
				break;
			case "2":
				$where.= " and mt.metakey = '' ";
				break;
			case "3":
				$where.= " and mt.metadesc = ''";
				break;
			case "4":
				$where.= " and (mt.metakey = '' or mt.metadesc = '' or mt.titletag = '') ";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.=" and c.published=1 ";
				break;
			case "2":
				$where.=" and c.published=0 ";
				break;				
			default:
				break;
		}
		
		if($filter_search!=""){ 
			$where.=" and (mt.name like '%".addslashes($filter_search)."%' or mt.titletag like '%".addslashes($filter_search)."%' or mt.metakey like '%".addslashes($filter_search)."%' or mt.metadesc like '%".addslashes($filter_search)."%') ";
		}		
		$query->clear();
		$query->select('c.id, c.name, mt.metakey, mt.metadesc, mt.titletag');
		$query->from('#__zoo_category c');
		$query->join('LEFT', "`#__ijseo_metags` AS mt ON mt.id = c.id and mt.mtype='zoo_cats'");
		$query->where($where);
		return $query;
	}	
	
	function sincronizeZooItemsWithSeo(){
		$db =& JFactory::getDBO();
		$sql = "select id, name, params from #__zoo_item";
		$db->setQuery($sql);
		$db->query();
		$all_items_from_zoo = $db->loadAssocList("id"); // all values from mtree links and meta datas
		$all_items_name_from_zoo = $db->loadResultArray(1); // all values from mtree links and meta datas
		$links_from_zoo1 = $db->loadAssocList(); // all values from mtree but not with meta datas	
		$links_from_zoo2 = array(); //$db->loadResultArray(2); // all values from mtree but not with meta datas		
		$links_from_zoo = array();
		$items_name_from_zoo = array(); // all names from mtree but not with meta datas
		
		//separate links keywords by ,
		if(isset($links_from_zoo1) && is_array($links_from_zoo1) && count($links_from_zoo1) > 0){
			foreach($links_from_zoo1 as $key=>$value){
				if(trim($value["params"]) != "" && trim($value["params"]) != "{}"){
					$params = json_decode($value["params"], true);					
					$temp = explode(",", trim($params["metadata.keywords"]));
					if(count($temp) > 1){
						$i = 0;
						foreach($temp as $key2=>$value2){
							if(trim($value2) != ""){
								$links_from_zoo[$value["id"]."-".$i++] = trim($value2);
							}
						}						
					}
					else{
						$links_from_zoo[$value["id"]."-0"] = trim($params["metadata.keywords"]);
					}
					$links_from_zoo2[$value["id"]."-0"] = trim($params["metadata.keywords"]);
				}
			}
		}
				
		//separate links name by separators
		$component_params = $this->getComponentParams();
		if(isset($component_params) && isset($component_params->delimiters) && trim($component_params->delimiters) != ""){
			$delimiters = $component_params->delimiters;
			$delimiters = str_split(trim($delimiters));
			if(isset($all_items_from_zoo) && is_array($all_items_from_zoo) && count($all_items_from_zoo) > 0){
				foreach($all_items_from_zoo as $key=>$value){
					$temp = str_replace($delimiters, "*****", $value["name"]);
					$temp_array = explode("*****", $temp);
					$i = 0;
					if(count($temp_array) > 1){						
						foreach($temp_array as $key_temp=>$value_temp){
							if(trim($value_temp) != ""){
								$items_name_from_zoo[$value["id"]."-".$i++] = $value_temp;
							}
						}
						unset($items_name_from_zoo[$key]);
					}
					else{
						$items_name_from_zoo[$value["id"]."-".$i++] = $value["name"];
					}					
				}
			}
		}		
		//------------------------------------
				
		$sql = "select id, title from #__ijseo_keys";
		$db->setQuery($sql);
		$db->query();
		$keys_from_seo = $db->loadResultArray(1); // all keywords from seo
		
		$sql = "select keyword, type_id from #__ijseo_keys_id where type = 'zoo_items'";
		$db->setQuery($sql);
		$db->query();
		$keys_id_from_seo = $db->loadResultArray(0); // all key_id from seo
		
		$sql = "select id, metakey from #__ijseo_metags where mtype = 'zoo_items'";
		$db->setQuery($sql);
		$db->query();
		$metags_from_seo = $db->loadResultArray(1); // all metags from seo
		$metags_from_seo1 = $db->loadResultArray(0); // all id metags from seo
		
		$sql = "select name from #__ijseo_metags where mtype = 'zoo_items'";		
		$db->setQuery($sql);
		$db->query();
		$mtags_name_from_seo = $db->loadResultArray(); // all titlekeys from seo
		
		$sql = "select title, joomla_id from #__ijseo_titlekeys where type = 'zoo_items'";
		$db->setQuery($sql);
		$db->query();
		$titlekeys_from_seo = $db->loadResultArray(0); // all titlekeys from seo								
		
		//differences for insert new rows
		$links_keys = array_diff($links_from_zoo, $keys_from_seo); // difference bettwin links and keywords
		$links_key_id = array_diff($links_from_zoo, $keys_id_from_seo); // difference bettwin links and key_id
		$links_metags = array_diff($links_from_zoo2, $metags_from_seo); // difference bettwin links and key_id
		$links_name_titlekeys = array_diff($items_name_from_zoo, $titlekeys_from_seo); // difference bettwin links_name and titlekeys
			
		$jnow =& JFactory::getDate();
		$date_time = $jnow->toMySQL();
		
		//insert all new keywords in seo tables
		if(isset($links_keys) && is_array($links_keys) && count($links_keys) > 0){
			foreach($links_keys as $key=>$value){
				if(trim($value) != ""){
					$sql = "insert into #__ijseo_keys(`title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`) values ('".addslashes(trim($value))."', 0, 0, -1, '".$date_time."', 0)";
					$db->setQuery($sql);
					$db->query();
				}
			}
		}
		
		//insert all new keys_id in seo tables
		if(isset($links_key_id) && is_array($links_key_id) && count($links_key_id) > 0 && isset($all_items_from_zoo) && count($all_items_from_zoo) > 0){
			foreach($links_key_id as $key=>$value){
				if(trim($value) != ""){
					$temp = explode("-", $key);
					$id = $temp["0"];
					$sql = "insert into #__ijseo_keys_id (`keyword`, `type`, `type_id`) values ('".addslashes(trim($value))."', 'zoo_items', ".intval($id).")";					
					$db->setQuery($sql);
					$db->query();
				}
			}
		}
		
		//insert all new metags in seo tables
		if(isset($links_metags) && is_array($links_metags) && count($links_metags) > 0 && isset($all_items_from_zoo) && count($all_items_from_zoo) > 0){
			foreach($links_metags as $key=>$value){
				$temp = explode("-", $key);
				$id = intval($temp["0"]);
				$metadesc = "";
				$metakey = "";
				
				$params = $all_items_from_zoo[$id]["params"];
				$params = json_decode($params, true);
				$metadesc = $params["metadata.description"];
				$metakey = $params["metadata.keywords"];
				
				if(in_array($id, $metags_from_seo1)){
					$sql = "update #__ijseo_metags set metakey='".addslashes(trim($metakey))."', metadesc='".addslashes(trim($metadesc))."' where mtype='zoo_items' and id=".$id;
				}
				else{					 
					$sql = "insert into #__ijseo_metags (`mtype`, `id`, `name`, `titletag`, `metakey`, `metadesc`) values ('zoo_items', ".$all_items_from_zoo[$id]["id"].", '".addslashes(trim($all_items_from_zoo[$id]["name"]))."', '".addslashes(trim($all_items_from_zoo[$id]["name"]))."', '".addslashes(trim($metakey))."', '".addslashes(trim($metadesc))."')";
				}
				$db->setQuery($sql);
				$db->query();
			}
		}
		
		//insert all new titlekeys in seo tables
		if(isset($links_name_titlekeys) && is_array($links_name_titlekeys) && count($links_name_titlekeys) > 0 && isset($all_items_from_zoo) && count($all_items_from_zoo) > 0){
			foreach($links_name_titlekeys as $key=>$value){
				if(trim($value != "")){
					$temp = explode("-", $key);
					$id = $temp["0"];
					$sql = "insert into #__ijseo_titlekeys (`title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`, `type`, `joomla_id`) values ('".addslashes(trim($value))."', 0, 0, -1, '".$date_time."', 0, 'zoo_items', ".$id.")";
					$db->setQuery($sql);
					$db->query();
				}
			}
		}

		//differences for delete old rows
		$keys_for_delete = array_diff($keys_id_from_seo, $links_from_zoo); // difference bettwin links and key_id
		if(isset($keys_for_delete) && is_array($keys_for_delete) && count($keys_for_delete) > 0){
			foreach($keys_for_delete as $key=>$value){
				$sql = "delete from #__ijseo_keys where title = '".addslashes(trim($value))."'";				
				$db->setQuery($sql);
				$db->query();
				$sql = "delete from #__ijseo_keys_id where type='zoo_items' and keyword='".addslashes(trim($value))."'";
				$db->setQuery($sql);
				$db->query();
			}
		}
		
		$metags_for_delete = array_diff($mtags_name_from_seo, $all_items_name_from_zoo); // difference bettwin links and key_id
		if(isset($metags_for_delete) && is_array($metags_for_delete) && count($metags_for_delete) > 0){
			foreach($metags_for_delete as $key=>$value){
				$sql = "delete from #__ijseo_metags where name = '".addslashes(trim($value))."' and mtype='zoo_items'";				
				$db->setQuery($sql);
				$db->query();				
			}
		}
	}
	
	function sincronizeZooCategoriesWithSeo(){
		$db =& JFactory::getDBO();
		$sql = "select id, name from #__zoo_category";
		$db->setQuery($sql);
		$db->query();
		$all_zoo_cats = $db->loadResultArray(1);
		$all_cats = $db->loadAssocList("name");
		
		$sql = "select id, name from #__ijseo_metags where mtype='zoo_cats'";
		$db->setQuery($sql);
		$db->query();
		$all_metags_cats = $db->loadResultArray(1);
		
		$sql = "select id, title from #__ijseo_titlekeys where type='zoo_cats'";
		$db->setQuery($sql);
		$db->query();
		$all_titlekeys_cats = $db->loadResultArray(1);
		
		$diff_zoo_metags = array_diff($all_zoo_cats, $all_metags_cats);
		$diff_zoo_titlekeys = array_diff($all_zoo_cats, $all_titlekeys_cats);
		
		if(isset($diff_zoo_metags) && is_array($diff_zoo_metags) && count($diff_zoo_metags) > 0){
			foreach($diff_zoo_metags as $key=>$value){
				$sql = "insert into #__ijseo_metags(`mtype`, `id`, `name`, `titletag`, `metakey`, `metadesc`) values ('zoo_cats', ".$all_cats[trim($value)]["id"].", '".addslashes(trim($value))."', '".addslashes(trim($value))."', '', '')";
				$db->setQuery($sql);
				$db->query();
			}
		}
		$jnow = & JFactory::getDate();
		$date =  $jnow->toMySQL();
		if(isset($diff_zoo_titlekeys) && is_array($diff_zoo_titlekeys) && count($diff_zoo_titlekeys) > 0){
			foreach($diff_zoo_titlekeys as $key=>$value){
				$sql = "insert into #__ijseo_titlekeys(`title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`, `type`, `joomla_id`) values ('".addslashes(trim($value))."', 0, 0, -1, '".$date."', 0, 'zoo_cats', ".$all_cats[trim($value)]["id"].")";				
				$db->setQuery($sql);
				$db->query();
			}
		}
	}
		
	function save(){
		$zoo = JRequest::getVar("zoo", "");
		if($zoo == "1"){
			if($this->saveZooItems()){
				return true;
			}
			return false;
		}
		elseif($zoo == "2"){
			if($this->saveZooCategories()){
				return true;
			}
			return false;
		}
	}
	
	function saveZooItems(){		
		$component_params = $this->getComponentParams();		
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		
		$ids = JRequest::getVar("cid", "", "post", "array");
		$page_title = JRequest::getVar("page_title", "", "post", "array");
		$metakey = JRequest::getVar("metakey", "", "post", "array");
		$metadesc = JRequest::getVar("metadesc", "", "post", "array");
		
		foreach($ids as $key=>$id){			
			if($page_title[$id] != "" || $metakey[$id] != "" || $metadesc[$id] != ""){																			
				$all_ptitlekeys = $this->getAllTitleKeysItems();
				$all_seo_metags = $this->getAllMetagsItems();
				$all_items_name = $this->getAllItemsName();
				$all_items_key_desc = $this->getAllItemsKeysDesc();
				$jnow = & JFactory::getDate();
				$date =  $jnow->toMySQL();
				
				//save title keywords if option is set to keywords from titlemetatag
				if(trim($component_params->delimiters) != ""){
					$delimiters = str_split(trim($component_params->delimiters));
					$page_title_temp = $page_title[$id];				
					$page_title_temp = str_replace($delimiters, "******", $page_title_temp);//replace |,:; with ****** and then for each element we have a new row in  _ijseo_titlekeys
					$page_title_array = explode("******", $page_title_temp);
										
					if(is_array($page_title_array) && count($page_title_array) > 0){
						foreach($page_title_array as $ptkey=>$ptvalue){
							if(isset($all_ptitlekeys) && is_array($all_ptitlekeys) && !in_array($ptvalue, $all_ptitlekeys) && trim($ptvalue) != ""){
								$sql = "insert into #__ijseo_titlekeys values('', '".addslashes(trim($ptvalue))."', 0, 0, -1,  '".$date."', 0, 'zoo_items', ".intval($id).")";								
								$db->setQuery($sql);
								$db->query();
							}
						}								
					}
				}
				else{
					if(trim($page_title[$id]) != ""){
						$sql = "insert into #__ijseo_titlekeys values('', '".addslashes(trim($page_title[$id]))."', 0, 0, -1,  '".$date."', 0, 'zoo_items', ".intval($id).")";
						$db->setQuery($sql);
						$db->query();
					}
				}
				
				// save values in metags tables of seo
				if(isset($all_seo_metags[$id])){
					$sql = "update #__ijseo_metags set titletag='".addslashes(trim($page_title[$id]))."', metakey='".addslashes(trim($metakey[$id]))."', metadesc='".addslashes($metadesc[$id])."' where mtype = 'zoo_items' and id=".intval($id);
				}
				elseif(!isset($all_seo_metags[$id])){
					$sql = "insert into #__ijseo_metags (`mtype`, `id`, `name`, `titletag`, `metakey`, `metadesc`) values ('zoo_items', ".intval($id).", '".addslashes($all_items_name[$id]["name"])."', '".addslashes(trim($page_title[$id]))."', '".addslashes(trim($metakey[$id]))."', '".addslashes($metadesc[$id])."')";
				}						
				$db->setQuery($sql);
				$db->query();
				
				$metakey_string = trim($metakey[$id]);
				$metadesc_string = trim($metadesc[$id]);				
				$params = json_decode($all_items_key_desc[$id]["params"], true);
				$params["metadata.keywords"] = trim($metakey_string);
				$params["metadata.description"] = trim($metadesc_string);
				$params = json_encode($params);
				
				//save in zoo tables then on edit all values will be save in another seo tables
				$sql = "update #__zoo_item set params='".addslashes(trim($params))."' where id=".intval($id);				
				$db->setQuery($sql);
				$db->query();				
			}			
		}
		return true;	
	}
	
	function saveZooCategories(){		
		$component_params = $this->getComponentParams();		
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		
		$sql = "select title from #__ijseo_keys";
		$db->setQuery($sql);
		$db->query();
		$all_keys = $db->loadResultArray();
		
		$ids = JRequest::getVar("cid", "", "post", "array");
		$page_title = JRequest::getVar("page_title", "", "post", "array");
		$metakey = JRequest::getVar("metakey", "", "post", "array");
		$metadesc = JRequest::getVar("metadesc", "", "post", "array");
		
		$for_delete_old_keys = array();
		
		foreach($ids as $key=>$id){			
			if($page_title[$id] != "" || $metakey[$id] != "" || $metadesc[$id] != ""){																			
				$old_keys_for_this_cat = $this->getKeysForCurrentCat($id);
				$all_ptitlekeys = $this->getAllTitleKeysCategories();				
				$all_seo_metags = $this->getAllMetagsCategories();
				$all_listings_name = $this->getAllCategoriesName();
				$jnow = & JFactory::getDate();
				$date =  $jnow->toMySQL();
				
				//save title keywords if option is set to keywords from titlemetatag
				if(trim($component_params->delimiters) != ""){
					$delimiters = str_split(trim($component_params->delimiters));
					$page_title_temp = $page_title[$id];				
					$page_title_temp = str_replace($delimiters, "******", $page_title_temp);//replace |,:; with ****** and then for each element we have a new row in  _ijseo_titlekeys
					$page_title_array = explode("******", $page_title_temp);
										
					if(is_array($page_title_array) && count($page_title_array) > 0){
						foreach($page_title_array as $ptkey=>$ptvalue){
							if(isset($all_ptitlekeys) && is_array($all_ptitlekeys) && !in_array($ptvalue, $all_ptitlekeys) && trim($ptvalue) != ""){
								$sql = "insert into #__ijseo_titlekeys values('', '".addslashes(trim($ptvalue))."', 0, 0, -1,  '".$date."', 0, 'zoo_cats', ".intval($id).")";								
								$db->setQuery($sql);
								$db->query();
							}
						}								
					}
				}
				else{
					if(trim($page_title[$id]) != ""){
						$sql = "insert into #__ijseo_titlekeys values('', '".addslashes(trim($page_title[$id]))."', 0, 0, -1,  '".$date."', 0, 'zoo_cats', ".intval($id).")";
						$db->setQuery($sql);
						$db->query();
					}
				}
				
				// save values in metags tables of seo
				if(isset($all_seo_metags[$id])){
					$sql = "update #__ijseo_metags set titletag='".addslashes(trim($page_title[$id]))."', metakey='".addslashes(trim($metakey[$id]))."', metadesc='".addslashes($metadesc[$id])."' where mtype = 'zoo_cats' and id=".intval($id);
				}
				elseif(!isset($all_seo_metags[$id])){
					$sql = "insert into #__ijseo_metags (`mtype`, `id`, `name`, `titletag`, `metakey`, `metadesc`) values ('zoo_cats', ".intval($id).", '".addslashes($all_listings_name[$id]["name"])."', '".addslashes(trim($metakey[$id]))."', '".addslashes($metadesc[$id])."')";
				}						
				$db->setQuery($sql);
				$db->query();
				
				if(trim($metakey[$id]) != ""){
					$temp_array = explode(",", trim($metakey[$id]));
					if(isset($temp_array) && is_array($temp_array) && count($temp_array) > 0){
						//here I made difference beet-win old keys and new keys to delete old keys
						if(isset($old_keys_for_this_cat) && is_array($old_keys_for_this_cat) && count($old_keys_for_this_cat) > 0){
							$temp_diff = array_diff($old_keys_for_this_cat, $temp_array);
							//delete old keys
							if(isset($temp_diff) && is_array($temp_diff) && count($temp_diff) > 0){
								foreach($temp_diff as $old_key=>$old_value){
									$sql = "delete from #__ijseo_keys where title='".addslashes(trim($old_value))."'";
									$db->setQuery($sql);
									$db->query();
									
									$sql = "delete from #__ijseo_keys_id where keyword='".addslashes(trim($old_value))."' and type='zoo_cats'";
									$db->setQuery($sql);
									$db->query();
								}
							}
						}
						foreach($temp_array as $temp_key=>$temp_value){
							if(!in_array(trim($temp_value), $all_keys)){
								$sql = "insert into #__ijseo_keys(`title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`) values ('".addslashes(trim($temp_value))."', 0, 0, -1, '".$date."', 0)";
								$db->setQuery($sql);
								$db->query();
								
								$sql = "insert into #__ijseo_keys_id(`keyword`, `type`, `type_id`) values ('".addslashes(trim($temp_value))."', 'zoo_cats', ".$id.")";								
								$db->setQuery($sql);
								$db->query();
							}
						}
					}
					else{
						if(!in_array(trim($temp_value), $all_keys)){
							$sql = "insert into #__ijseo_keys(`title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`) values ('".addslashes(trim($metakey[$id]))."', 0, 0, -1, '".$date."', 0)";
							$db->setQuery($sql);
							$db->query();
							
							$sql = "insert into #__ijseo_keys_id(`keyword`, `type`, `type_id`) values ('".addslashes(trim($metakey[$id]))."', 'zoo_cats', ".$id.")";							
							$db->setQuery($sql);
							$db->query();
						}
					}
				}
			}
		}
		return true;
	}
	
	function getKeysForCurrentCat($id){
		$db =& JFactory::getDBO();
		$sql = "select keyword from #__ijseo_keys_id where type='zoo_cats' and type_id=".intval($id);
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResultArray();
		return $result;
	}
	
	function getAllItemsKeysDesc(){
		$db =& JFactory::getDBO();
		$sql = "select id, params from #__zoo_item";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadAssocList("id");
		return $result;
	}
	
	function getAllTitleKeysItems(){
		$db =& JFactory::getDBO();
		$sql = "select title from #__ijseo_titlekeys where type='zoo_items'";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResultArray();
		return $result;
	}
	
	function getAllTitleKeysCategories(){
		$db =& JFactory::getDBO();
		$sql = "select title from #__ijseo_titlekeys where type='zoo_cats'";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResultArray();
		return $result;
	}
	
	function getAllMetagsItems(){
		$db =& JFactory::getDBO();
		$sql = "select id, titletag from #__ijseo_metags where mtype = 'zoo_items'";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadAssocList("id");
		return $result;
	}
	
	function getAllMetagsCategories(){
		$db =& JFactory::getDBO();
		$sql = "select id, titletag from #__ijseo_metags where mtype = 'zoo_cats'";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadAssocList("id");
		return $result;
	}
	
	function getAllItemsName(){
		$db =& JFactory::getDBO();
		$sql = "select id, name from #__zoo_item";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadAssocList("id");
		return $result;
	}
	
	function getAllCategoriesName(){
		$db =& JFactory::getDBO();
		$sql = "select id, name from #__zoo_category";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadAssocList("id");
		return $result;
	}
	
	function getAllDesc(){
		$db =& JFactory::getDBO();
		$zoo = JRequest::getVar("zoo", "0");
		$sql = "";
		$result = "";
		if($zoo == "1"){
			$sql = "select id, params from #__zoo_item";
			$db->setQuery($sql);
			$db->query();
			$result = $db->loadAssocList("id");
			if(isset($result) && count($result) > 0){
				foreach($result as $key=>$value){
					$params = json_decode($value["params"], true);
					$desc = $params["metadata.description"];
					$result[$key]["description"] = $desc;
				}
			}
		}
		elseif($zoo == "2"){
			$sql = "select id, description from #__zoo_category";
			$db->setQuery($sql);
			$db->query();
			$result = $db->loadAssocList("id");
		}
		return $result;
	}
	
	function getAllTitles(){
		$db =& JFactory::getDBO();
		$zoo = JRequest::getVar("zoo", "0");
		$sql = "";
		$result = "";
		if($zoo == "1"){
			$sql = "select id, name as title from #__zoo_item";
			$db->setQuery($sql);
			$db->query();
			$result = $db->loadAssocList("id");
		}
		elseif($zoo == "2"){
			$sql = "select id, name as title from #__zoo_category";
			$db->setQuery($sql);
			$db->query();
			$result = $db->loadAssocList("id");
		}
		return $result;
	}
	
	function getAllTitlesTag(){
		$db =& JFactory::getDBO();
		$zoo = JRequest::getVar("zoo", "0");
		$sql = "";
		$result = "";
		if($zoo == "1"){
			$sql = "select id, titletag as title from #__ijseo_metags where mtype='zoo_items'";
			$db->setQuery($sql);
			$db->query();
			$result = $db->loadAssocList("id");
		}
		elseif($zoo == "2"){
			$sql = "select id, titletag as title from #__ijseo_metags where mtype='zoo_cats'";
			$db->setQuery($sql);
			$db->query();
			$result = $db->loadAssocList("id");
		}
		return $result;
	}
	
	function getAllKeywords(){
		$db =& JFactory::getDBO();
		$zoo = JRequest::getVar("zoo", "0");
		$sql = "";
		$result = "";
		if($zoo == "1"){
			$sql = "select id, metakey from #__ijseo_metags where mtype='zoo_items'";
			$db->setQuery($sql);
			$db->query();
			$result = $db->loadAssocList("id");
		}
		elseif($zoo == "2"){
			$sql = "select id, metakey from #__ijseo_metags where mtype='zoo_cats'";
			$db->setQuery($sql);
			$db->query();
			$result = $db->loadAssocList("id");
		}
		return $result;
	}
	
	function genMetadesc(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$all_desc = $this->getAllDesc();		
		$zoo= JRequest::getVar("zoo", "0");
		foreach($ids as $key=>$id){			
			$desc = "";
			if(isset($all_desc) && count($all_desc) > 0){
				$params = $this->getComponentParams();
				if($params->ijseo_type_desc == "Words"){
					$exclude_key = $params->exclude_key;
					$temp1 = "";
					foreach($exclude_key as $e_key=>$e_value){					
						$temp1 = str_replace($e_value, " ", strip_tags($all_desc[$id]["description"]));
					}
					$temp2 = explode(" ", $temp1);
					$delete = array_splice($temp2, 0, $params->ijseo_allow_no_desc);					
					$desc = implode(" ", $delete);					
				}
				else{
					if(isset($all_desc[$id]["description"])){
						$exclude_key = $params->exclude_key;
						$temp1 = "";
						foreach($exclude_key as $e_key=>$e_value){					
							$temp1 = str_replace($e_value, " ", strip_tags($all_desc[$id]["description"]));
						}					
						$temp1 = str_replace($exclude_key, " ", strip_tags($all_desc[$id]["description"]));
						$desc = mb_substr($temp1, 0, $params->ijseo_allow_no_desc);
					}
				}
			}			
			if($zoo == "1"){
				$sql = "select params from #__zoo_item where id=".$id;
				$db->setQuery($sql);
				$db->query();
				$params = $db->loadResult();
				if(isset($params) && trim($params) != ""){
					$params = json_decode($params, true);
					$params["metadata.description"] = trim($desc);
					$params = json_encode($params);
					$sql = "update #__zoo_item set params = '".$params."' where id=".$id;
					$db->setQuery($sql);
					$db->query();
					
					$sql = "update #__ijseo_metags set metadesc = '".trim($desc)."' where mtype='zoo_items' and id=".$id;
					$db->setQuery($sql);
					if(!$db->query()){
						return false;
					}
				}
			}
			elseif($zoo == "2"){
				$sql = "update #__ijseo_metags set metadesc = '".trim($desc)."' where mtype='zoo_cats' and id=".$id;
				$db->setQuery($sql);
				if(!$db->query()){
					return false;
				}
			}	
		}
		return true;
	}
	
	function copyArticleToTitle(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$all_titles = $this->getAllTitles();
		$zoo = JRequest::getVar("zoo", "0");
		$type = "";
		if($zoo == "1"){
			$type = "zoo_items";
		}
		elseif($zoo == "2"){
			$type = "zoo_cats";
		}
		foreach($ids as $key=>$id){
			$title = $all_titles[$id]["title"];
			$query->clear();
			$query->update('#__ijseo_metags');
			$query->set("`titletag`='".addslashes(trim($title))."'");
			$query->where('id='.$id." and mtype='".$type."'");
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
		}
		return true;
	}
	
	function copyArticleToKey(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$all_titles = $this->getAllTitles();
		$zoo = JRequest::getVar("zoo", "0");
		$type = "";
		$table = "";
		$col_id = "";
		if($zoo == "1"){
			foreach($ids as $key=>$id){
				$title = $all_titles[$id]["title"];
				$query->clear();
				$query->update('#__ijseo_metags');
				$query->set("`metakey`='".addslashes(trim($title))."'");
				$query->where('id='.$id." and mtype='zoo_items'");
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
				
				$sql = "select params from #__zoo_item where id=".$id;
				$db->setQuery($sql);
				$db->query();
				$params = $db->loadResult();
				$params = json_decode($params, true);
				$params["metadata.keywords"] = trim($title);
				
				$params_encode = json_encode($params);
					
				$query->clear();
				$query->update('#__zoo_item');
				$query->set("`params`='".$params_encode."'");
				$query->where('id='.$id);
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
			}
		}
		elseif($zoo == "2"){
			foreach($ids as $key=>$id){
				$sql = "select keyword from #__ijseo_keys_id where type='zoo_cats' and type_id=".$id;
				$db->setQuery($sql);
				$db->query();
				$old_keys = $db->loadResultArray();				
				
				$title = $all_titles[$id]["title"];
				$query->clear();
				$query->update('#__ijseo_metags');
				$query->set("`metakey`='".addslashes(trim($title))."'");
				$query->where('id='.$id." and mtype='zoo_cats'");
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
				
				$jnow =& JFactory::getDate();
				$date_time = $jnow->toMySQL();
				
				$new_keys = explode(",", $title);
				if(isset($new_keys) && is_array($new_keys) && count($new_keys) > 0){
					foreach($new_keys as $k_key=>$k_value){
						$sql = "insert into #__ijseo_keys_id(`keyword`, `type`, `type_id`) values ('".addslashes(trim($k_value))."', 'zoo_cats', ".$id.")";
						$db->setQuery($sql);
						$db->query();
						
						$sql = "insert into #__ijseo_keys(`title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`) values ('".addslashes(trim($k_value))."', 0, 0, -1, '".$date_time."', 0)";						
						$db->setQuery($sql);
						$db->query();
					}
				}
				
				if(isset($old_keys) && is_array($old_keys) && count($old_keys) > 0){
					foreach($old_keys as $o_key=>$o_value){
						$sql = "delete from #__ijseo_keys where title = '".addslashes(trim($o_value))."'";
						$db->setQuery($sql);
						$db->query();
						
						$sql = "delete from #__ijseo_keys_id where keyword = '".addslashes(trim($o_value))."' and type='zoo_cats'";
						$db->setQuery($sql);
						$db->query();
					}
				}
			}
		}
		
		return true;
	}
	
	function copyTitleToKey(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$all_titles = $this->getAllTitlesTag();
		$zoo = JRequest::getVar("zoo", "0");
		$type = "";
		$table = "";
		$col_id = "";
		if($zoo == "1"){
			foreach($ids as $key=>$id){
				$title = $all_titles[$id]["title"];
				$query->clear();
				$query->update('#__ijseo_metags');
				$query->set("`metakey`='".addslashes(trim($title))."'");
				$query->where('id='.$id." and mtype='zoo_items'");
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
				
				$sql = "select params from #__zoo_item where id=".$id;
				$db->setQuery($sql);
				$db->query();
				$params = $db->loadResult();
				$params = json_decode($params, true);
				$params["metadata.keywords"] = trim($title);
				
				$params_encode = json_encode($params);
					
				$query->clear();
				$query->update('#__zoo_item');
				$query->set("`params`='".$params_encode."'");
				$query->where('id='.$id);
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
			}
		}
		elseif($zoo == "2"){
			foreach($ids as $key=>$id){
				$sql = "select keyword from #__ijseo_keys_id where type='zoo_cats' and type_id=".$id;
				$db->setQuery($sql);
				$db->query();
				$old_keys = $db->loadResultArray();				
				
				$title = $all_titles[$id]["title"];
				$query->clear();
				$query->update('#__ijseo_metags');
				$query->set("`metakey`='".addslashes(trim($title))."'");
				$query->where('id='.$id." and mtype='zoo_cats'");
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
				
				$jnow =& JFactory::getDate();
				$date_time = $jnow->toMySQL();
				
				$new_keys = explode(",", $title);
				if(isset($new_keys) && is_array($new_keys) && count($new_keys) > 0){
					foreach($new_keys as $k_key=>$k_value){
						$sql = "insert into #__ijseo_keys_id(`keyword`, `type`, `type_id`) values ('".addslashes(trim($k_value))."', 'zoo_cats', ".$id.")";
						$db->setQuery($sql);
						$db->query();
						
						$sql = "insert into #__ijseo_keys(`title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`) values ('".addslashes(trim($k_value))."', 0, 0, -1, '".$date_time."', 0)";						
						$db->setQuery($sql);
						$db->query();
					}
				}
				
				if(isset($old_keys) && is_array($old_keys) && count($old_keys) > 0){
					foreach($old_keys as $o_key=>$o_value){
						$sql = "delete from #__ijseo_keys where title = '".addslashes(trim($o_value))."'";
						$db->setQuery($sql);
						$db->query();
						
						$sql = "delete from #__ijseo_keys_id where keyword = '".addslashes(trim($o_value))."' and type='zoo_cats'";
						$db->setQuery($sql);
						$db->query();
					}
				}
			}
		}
		
		return true;
	}
	
	function copyKeyToTitle(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$zoo = JRequest::getVar("zoo", "0");
		$type = "";				
		if($zoo == "1"){
			$type = "zoo_items";
		}
		elseif($zoo == "2"){
			$type = "zoo_cats";
		}
		$all_keywords = $this->getAllKeywords();
		
		foreach($ids as $key=>$id){
			$key = $all_keywords[$id]["metakey"];
			$query->clear();
			$query->update('#__ijseo_metags');
			$query->set("`titletag`='".addslashes(trim($key))."'");
			$query->where('id='.$id." and mtype='".$type."'");		
			$db->setQuery($query);
			if(!$db->query()){
				die($db->getQuery());
				return false;
			}
		}
		return true;
	}
		
	function getComponentParams(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('params');
		$query->from('#__ijseo_config');
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string);
		return $result;
	}
}

?>