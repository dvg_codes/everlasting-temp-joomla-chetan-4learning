<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');
jimport( 'joomla.utilities.date' );

class iJoomla_SeoModelArticles extends JModelList{

	protected $_context = 'com_ijoomla_seo.articles';
	private $total=0;

	function populateState(){
		// Initialize variables.
		$app = JFactory::getApplication('administrator');
		// Load the list state.
		$this->setState('list.start', $app->getUserStateFromRequest($this->_context . '.list.start', 'limitstart', 0, 'int'));
		$this->setState('list.limit', $app->getUserStateFromRequest($this->_context . '.list.limit', 'limit', $app->getCfg('list_limit', 25) , 'int'));
		$this->setState('selected', JRequest::getVar('cid', array()));
		$catid = $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		$this->setState('filter.catid', $catid);
	}
	
	function getPagination(){
		$pagination=parent::getPagination();
		$pagination->total=$this->total;
		if($pagination->total%$pagination->limit>0)
			$nr_pages=intval($pagination->total/$pagination->limit)+1;
		else $nr_pages=intval($pagination->total/$pagination->limit);
		$pagination->set('pages.total',$nr_pages);
		$pagination->set('pages.stop',$nr_pages);		
		return $pagination;
	}
	
	function getAuthors(){
		$db =& JFactory::getDBO();		
		$query = " SELECT c.created_by,  u.name ".
				" FROM (#__content AS c) ".
				" LEFT JOIN #__users AS u ON (u.id = c.created_by) ".
				" WHERE c.state <> -1 ".
				" AND c.state <> -2 ".
				" GROUP BY u.name ".
				" ORDER BY u.name ";
		$db->setQuery($query);		
		$db->query();
		$result = $db->loadObjectList();
		return $result;	
	}	
	
	function getItems(){
		$config = new JConfig();	
		$app = JFactory::getApplication('administrator');
		$limistart = $app->getUserStateFromRequest($this->context.'.list.start', 'limitstart');
		$limit = $app->getUserStateFromRequest($this->context.'.list.limit', 'limit', $config->list_limit);

		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$query = $this->getListQuery();
		
		$db->setQuery($query);
		$db->query();
		$result	= $db->loadObjectList();
		$this->total=count($result);
		
		$db->setQuery($query,$limistart,$limit);
		$db->query();
		$result	= $db->loadObjectList();	
		return $result;
	}
	
	function getListQuery(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		$catid 		= $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		
		$filter_author = $app->getUserStateFromRequest($this->context.'.filter.author', 'filter_authorid','','string');
		$this->setState('filter.author', $filter_author, 'string');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','','string');
		$this->setState('filter.missing', $filter_missing, 'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state, 'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
		
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_author = "";
			$filter_state = "";
			$filter_search = "";
			$catid = "";
			$filter_missing = JRequest::getVar("value", "", "get");			
			$this->setState('filter.author', "" ,'string');
			$this->setState('filter.missing', $filter_missing, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
		}
		
		$where=" 1=1 ";
		
		if(intval($catid)>0){
			$where.= " and c.catid=".intval($catid);
		}
		
		if($filter_author != "0" && $filter_author != ""){
			$where.= " and c.created_by=".intval($filter_author);
		}
		$search_missing_title = false;
		switch ($filter_missing){
			case "1":
				$search_missing_title = true;
				//$where .= "AND (c.attribs LIKE '%\"page_title\":\"\"%' OR c.attribs NOT LIKE '%page_title%') ";
				//$where.= " and c.attribs like '%\"page_title\":\"\"%' or c.attribs not like '%page_title%'";
				break;
			case "2":
				$where.= " and c.metakey='' ";
				break;
			case "3":
				$where.= " and c.metadesc='' ";
				break;
			case "4":
				//$where .= "AND (c.attribs LIKE '%\"page_title\":\"\"%' OR c.attribs NOT LIKE '%page_title%') ";
				$where .= " and (c.attribs like '%\"page_title\":\"\"%' or  c.metakey='' or c.metadesc='')";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.=" and c.state=1 ";
				break;
			case "2":
				$where.=" and c.state=0 ";
				break;
			case "3":
				$where.=" and c.state=2 ";
				break;
			case "4":
				$where.=" and c.state=-2 ";
				break;	
			default:
				$where.=" and c.state in (0, 1) ";
				break;
		}
		
		if($filter_search != ""){ 
			$where.=" and (c.title like '%".addslashes($filter_search)."%' or c.metakey like '%".addslashes($filter_search)."%' or c.metadesc like '%".addslashes($filter_search)."%') ";
		}		
		
		$query->select('c.id, c.title, c.metakey, c.metadesc, c.attribs, mt.titletag');
		$query->from('#__content c');
		$query->join('LEFT', '`#__ijseo_metags` AS mt ON c.id = mt.id and mt.mtype=\'article\'');
		$query->where($where);
		
		if ($search_missing_title) {
			$query = "SELECT * FROM (" . $query . ") AS w WHERE w.titletag = '' ";
		}
		//echo (string)$query;die();
		
		return $query;		
	}
	
	function getAttribs($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('attribs');
		$query->from('#__content');
		$query->where("id=".$id);
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string);
		return $result;
	}
	
	function getIntroFullText($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('`introtext`, `fulltext`');
		$query->from('#__content');
		$query->where("`id`=".$id);
		$db->setQuery($query);		
		$db->query();	
		$result = $db->loadAssocList();
		return $result;
	}	
	
	function getMetakey($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('metakey');
		$query->from('#__content');
		$query->where("id=".$id);
		$db->setQuery($query);		
		$db->query();
		$result = $db->loadResult();
		return $result;
	}
	
	function getArticleTitle($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('title');
		$query->from('#__content');
		$query->where("id=".$id);
		$db->setQuery($query);		
		$db->query();
		$result = $db->loadResult();
		return $result;
	}
	
	function saveMetaTitle($page_title, $id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("count(*)");
		$query->from("#__ijseo_title");
		$query->where("article_id=".$id);
		$db->setQuery($query);
		$db->query();
		$result = $db->loadResult();
		if($result != "0" && $result != NULL && $result != ""){
			$query->update('#__ijseo_title');
			$query->set("`title`='".addslashes($page_title)."'");
			$query->where('article_id='.$id);
			$db->setQuery($query);
			$db->query();
		}
		else{
			$date = new JDate();
			$query->clear();
			$query->insert("#__ijseo_title");
			$query->set("`article_id`=".$id);
			$query->set("`title`='".addslashes(trim($page_title))."'");
			$query->set('rank='.$this->getRank($page_title));
			$query->set('rchange=0');
			$query->set('mode=-1');
			$query->set("checkdate='".$date."'");
			$query->set("sticky=0");								
			$db->setQuery((string)$query);
			$db->query();
		}
	}
	
	function getAllMTitleKeys(){
		$db =& JFactory::getDBO();
		$sql = "select concat(title, '[', type, ']') from #__ijseo_titlekeys";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResultArray();
		return $result;
	} 
	
	function getTitleTipe(){
		$types = JRequest::getVar("types", "articles");
		$return = "";
		switch($types){
			case "articles" : {
				$return = "article";
				break;
			}
			case "menus" : {
				$return = JRequest::getVar("menu_types", "");
				break;
			}
		}
		return $return;
	}
	
	function getAllContentTitle($ids){
		$db =& JFactory::getDBO();
		if(isset($ids) && is_array($ids) && count($ids) > 0){
			$sql = "select id, title from #__content where id in (".implode(",", $ids).")";
			$db->setQuery($sql);
			$db->query();
			$result = $db->loadAssocList("id");
			return $result;
		}
		else{
			return array();
		}
	}
	
	function save(){				
		$component_params = $this->getComponentParams();
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->clear();
		
		$ids = JRequest::getVar("cid", "", "post", "array");
		$page_title = JRequest::getVar("page_title", "", "post", "array");
		$metakey = JRequest::getVar("metakey", "", "post", "array");
		$metadesc = JRequest::getVar("metadesc", "", "post", "array");
		$all_content_title = $this->getAllContentTitle($ids);
		
		$matakeys = array();
				
		foreach($ids as $key=>$id){			
			if($page_title[$id] != "" || $metakey[$id] != "" || $metadesc[$id] != ""){
				//save title keywords if option is set to keywords from titlemetatag
				if($component_params->ijseo_keysource == "1"){
					$all_ptitlekeys = $this->getAllMTitleKeys();
					$jnow = & JFactory::getDate();
					$date =  $jnow->toMySQL();
					$title_type = $this->getTitleTipe();
					
					if(trim($component_params->delimiters) != ""){
						$delimiters = str_split(trim($component_params->delimiters));
						$page_title_temp = $page_title[$id];				
						$page_title_temp = str_replace($delimiters, "******", $page_title_temp);//replace |,:; with ****** and then for each element we have a new row in  _ijseo_titlekeys
						$page_title_array = explode("******", $page_title_temp);
						if(is_array($page_title_array) && count($page_title_array) > 0){
							foreach($page_title_array as $ptkey=>$ptvalue){							
								if(isset($all_ptitlekeys) && is_array($all_ptitlekeys) && !in_array($ptvalue."[".$title_type."]", $all_ptitlekeys) && trim($ptvalue) != ""){
									$sql = "insert into #__ijseo_titlekeys values('', '".addslashes(trim($ptvalue))."', 0, 0, -1,  '".$date."', 0, '".$title_type."', ".intval($id).")";
									$db->setQuery($sql);
									$db->query();
								}
								elseif(trim($ptvalue) == ""){
									$sql = "update #__ijseo_titlekeys set title='' where type='article' and joomla_id=".intval($id);
									$db->setQuery($sql);
									$db->query();
									
								}
							}								
						}
					}
					else{
						if(trim($page_title[$id]) != ""){
							$sql = "insert into #__ijseo_titlekeys values('', '".addslashes(trim($page_title[$id]))."', 0, 0, -1,  '".$date."', 0, '".$title_type."', ".intval($id).")";
							$db->setQuery($sql);
							$db->query();
						}
					}
				}
				
				$params = $this->getAttribs($id);										
				$params->page_title = trim($page_title[$id]);								
				$attribs = json_encode($params);					
				$attribs = str_replace("'", "''", $attribs);
				$attribs = str_replace("\\", "\\\\", $attribs);																	
				$query->clear();		
				$query->update('#__content');
				$query->set("`attribs`='".$attribs."'". 
							", `metakey`='".addslashes(trim($metakey[$id]))."'".
							", `metadesc`='".addslashes(trim($metadesc[$id]))."'");
				$query->where('id='.$id);
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
				if(!$this->existMetatile($id)){
					$sql = "insert into #__ijseo_metags (`mtype`, `id`, `name`, `titletag`, `metakey`, `metadesc`) values ('article', ".$id.", '".addslashes(trim($all_content_title[$id]["title"]))."', '".addslashes(trim($page_title[$id]))."', '".addslashes(trim($metakey[$id]))."', '".addslashes(trim($metadesc[$id]))."')";
					$db->setQuery($sql);
					if(!$db->query()){
						return false;
					}
				}
				else{
					$sql = "update #__ijseo_metags set titletag='".addslashes(trim($page_title[$id]))."', metakey='".addslashes(trim($metakey[$id]))."', `metadesc`='".addslashes(trim($metadesc[$id]))."' where mtype='article' and id=".intval($id);
					$db->setQuery($sql);
					if(!$db->query()){
						return false;
					}
				}				
				$matakeys[] = $metakey[$id];
				if(trim($page_title[$id]) != ""){
					$this->saveMetaTitle($page_title[$id], $id);
				}
				//save meta in seo component tables
				$return = $this->sincronizeKeys($matakeys, $id);
				if($return === FALSE){
					return false;
				}
				$matakeys = array();
			}
			elseif($page_title[$id] == "" || $metakey[$id] == "" || $metadesc[$id] == ""){
				$params = $this->getAttribs($id);										
				$params->page_title = trim($page_title[$id]);								
				$attribs = json_encode($params);
				$attribs = str_replace("'", "''", $attribs);
				$attribs = str_replace("\\", "\\\\", $attribs);
				$query->clear();
				$query->update('#__content');
				$query->set("`attribs`='".$attribs."'".
							", `metakey`='".addslashes(trim($metakey[$id]))."'".
							", `metadesc`='".addslashes(trim($metadesc[$id]))."'");
				$query->where('id='.$id);
				
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
				$sql = "update #__ijseo_titlekeys set title='".trim($page_title[$id])."' where type='article' and joomla_id=".intval($id);
				$db->setQuery($sql);
				$db->query();
				
				$sql = "update #__ijseo_metags set titletag='".trim($page_title[$id])."', metakey='".trim($metakey[$id])."', metadesc='".trim($metadesc[$id])."' where mtype='article' and id=".intval($id);
				$db->setQuery($sql);
				$db->query();
				
			}
		}
		
		//delete all old meta from component tables	
		if(!$this->emptyOldKeys()){
			return false;
		}
		return true;	
	}
	
	function existMetatile($id){
		$db =& JFactory::getDBO();
		$sql = "select count(*) from #__ijseo_metags where mtype='article' and id=".intval($id);
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResult();
		if($result == "0"){
			return false;
		}
		return true;
	}
	
	//delete all old meta that doesn't exist in _content table
	function emptyOldKeys(){
		$all_keys = array();
		$keyword_id = array();
		$keyword = array();
		$artilce_key_id = array();
		$menus = array();
		$menus_id = array();
		
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("title");
		$query->from("#__ijseo_keys");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadAssocList();
		foreach($result as $key=>$value){
			$all_keys[] = trim($value["title"]);
		}
		
		//$db =& JFactory::getDBO();		
		//$query = $db->getQuery(true);
		$query->clear();
		$query->select("metakey, id");
		$query->from("#__content");
		$query->where("metakey <> ''");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadAssocList();		
		foreach($result as $key=>$value){
			$value["metakey"] = str_replace(", ", ",", $value["metakey"]);
			$temp = explode(",", trim($value["metakey"]));
			$keyword = array_merge($keyword, $temp);
			$value["metakey"] = str_replace(",", "|".$value["id"].",", trim($value["metakey"]));
			$value["metakey"] .= "|".$value["id"];	
			$temp = explode(",", trim($value["metakey"]));									
			$keyword_id = array_merge($keyword_id, $temp);
		}
		
		//$db =& JFactory::getDBO();		
		//$query = $db->getQuery(true);
		$query->clear();
		$query->select("params, id");
		$query->from("#__menu");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadAssocList();		
		foreach($result as $key=>$value){
			$temp1 = json_decode($value["params"], true);
			if(isset($temp1["menu-meta_keywords"]) && trim($temp1["menu-meta_keywords"]) != ""){
				$temp2 = explode(",", $temp1["menu-meta_keywords"]);
				$menus = array_merge($menus, $temp2);
				
				$temp1["menu-meta_keywords"] = str_replace(",", "|".$value["id"].",", trim($temp1["menu-meta_keywords"]));
				$temp1["menu-meta_keywords"] .= "|".$value["id"];
				$temp2 = explode(",", $temp1["menu-meta_keywords"]);
				$menus_id = array_merge($menus_id, $temp2);
			}
		}
		
		//make difference beetwin all keys from component tables and keys from _content, so delete only useless keys 					
		$difference1 = array_diff($all_keys, $keyword);
		//make difference beetwin result first diference and keys from menus, so this not delete keys from component if that key is in menus and not in _content
		$difference2 = array_diff($difference1, $menus);		
		
		//delete from  _ijseo_keys (real useless keys)		
		foreach($difference2 as $key=>$value){			
			$query->clear();
			$query->delete('#__ijseo_keys');
			$query->where("title='".addslashes(trim($value))."'");
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
			else{
				$query->clear();
				$query->delete('#__ijseo_keys_id');
				$query->where("keyword='".addslashes(trim($value))."' and type='article'");		
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
			}
		}
		//delete from  _ijseo_keys_id (real useless keys)
		$query->clear();
		$query->select("keyword, type_id");
		$query->from("#__ijseo_keys_id");
		$query->where("type='article'");		
		$db->setQuery($query);
		$db->query();
		$result = $db->loadAssocList();
		foreach($result as $key=>$value){
			$artilce_key_id[] = $value["keyword"]."|".$value["type_id"];
		}
				
		$difference = array_diff($artilce_key_id, $keyword_id);		
						
		foreach($difference as $key=>$value){
			$temp = explode("|",$value);
			$query->clear();
			$query->delete('#__ijseo_keys_id');
			$query->where("`keyword`='".addslashes(trim($temp["0"]))."' and `type_id` = ".$temp["1"]." and `type`='article'");			
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
		}		
		return true;
	}
	
	function sincronizeKeys($matakeys, $id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$date = new JDate();
		$string = implode(",", $matakeys);
		$allvalues = explode(",", $string);		
				
		foreach($allvalues as $key=>$value){
			if(!$this->exist(trim($value)) && trim($value) != ""){
				$query->clear();
				$query->insert("#__ijseo_keys");
				$query->set("`title`='".addslashes(trim($value))."'");
				$query->set('rank='.$this->getRank(trim($value)));
				$query->set('rchange=0');
				$query->set('mode=-1');
				$query->set("checkdate='".$date."'");
				$query->set("sticky=0");								
				$db->setQuery((string)$query);
				if(!$db->query()){
					return false;
				}
				else{
					$query->clear();
					$query->insert("#__ijseo_keys_id");
					$query->set("`keyword`='".addslashes(trim($value))."'");
					$query->set("`type`='article'");
					$query->set('type_id='.$id);							
					$db->setQuery((string)$query);
					if(!$db->query()){
						return false;
					}
				}
			}
			if(!$this->existInKeyId(trim($value), $id) && trim($value) != ""){
				$query->clear();
				$query->insert("#__ijseo_keys_id");
				$query->set("`keyword`='".addslashes(trim($value))."'");
				$query->set("`type`='article'");
				$query->set('type_id='.$id);							
				$db->setQuery((string)$query);
				if(!$db->query()){
					return false;
				}
			}
		}
		return true;
	}
	
	function exist($value){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("id");
		$query->from("#__ijseo_keys");
		$query->where("title = '".addslashes($value)."'");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadResult();
		if($result != NULL || $result != ""){
			return true;
		}
		return false;
	}
	
	function existInKeyId($value, $id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("type_id");
		$query->from("#__ijseo_keys_id");
		$query->where("keyword = '".addslashes($value)."' and `type_id`=".$id." and type='article'");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadResult();
		if($result != NULL || $result != ""){
			return true;
		}
		return false;
	}
	
	function getRank($value){
		return 0;
	}
	
	function copyKeyToTitle(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_titletag = array();
		foreach($ids as $key=>$id){
			$query->clear();
			$attribs = $this->getAttribs($id);			
			$metakey = $this->getMetakey($id);			
			$session_titletag[$id] = $metakey;
			/*$attribs->page_title = addslashes($metakey);
			$params = json_encode($attribs);
			$params = str_replace("\\\\", "\\", $params);						
			$query->clear();
			$query->update('#__content');
			$query->set("`attribs`='".$params."'");
			$query->where('id='.$id);			
			$db->setQuery($query);
			if($db->query()){
				$sql = "update #__ijseo_metags set titletag='".addslashes(trim($metakey))."' where mtype='article' and id=".$id;
				$db->setQuery($sql);
				if(!$db->query()){
					return false;
				}
			}
			else{
				return false;
			}*/
		}
		$_SESSION["session_titletag"] = $session_titletag;
		return true;
	}
	
	function copyTitleToKey(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_metakey = array();
		foreach($ids as $key=>$id){
			$attribs = $this->getAttribs($id);
			$metakey = $attribs->page_title;
			$session_metakey[$id] = $metakey;
			/*$query->clear();
			$query->update('#__content');
			$query->set("`metakey`='".addslashes($metakey)."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}*/
		}
		$_SESSION["session_metakey"] = $session_metakey;
		return true;
	}
	
	function copyArticleToKey(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_metakey = array();
		foreach($ids as $key=>$id){
			$article = $this->getArticleTitle($id);
			$metakey = $article;
			$session_metakey[$id] = $metakey;
			/*$attribs->page_title = $metakey;
			$query->clear();
			$query->update('#__content');
			$query->set("`metakey`='".addslashes($metakey)."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}*/
		}
		$_SESSION["session_metakey"] = $session_metakey;
		return true;
	}
	
	function copyArticleToTitle(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_titletag = array();
		foreach($ids as $key=>$id){
			$attribs = $this->getAttribs($id);
			$article = $this->getArticleTitle($id);
			$session_titletag[$id] = $article;
			/*$params->metakey = addslashes($params->metakey);
			$params->metadesc = addslashes($params->metadesc);
			$attribs->page_title = addslashes($article);
			$params_database = json_encode($attribs);			
			$params_database = str_replace("\\\\", "\\", $params_database);
			$query->clear();
			$query->update('#__content');
			$query->set("`attribs`='".$params_database."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			
			if($db->query()){
				$sql = "update #__ijseo_metags set titletag='".addslashes(trim($article))."' where mtype='article' and id=".$id;
				$db->setQuery($sql);
				if(!$db->query()){
					return false;
				}
			}
			else{
				return false;
			}*/
		}
		$_SESSION["session_titletag"] = $session_titletag;
		return true;
	}
	
	function genMetadesc(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_description = array();
		foreach($ids as $key=>$id){
			$intr_full = $this->getIntroFullText($id);			
			$desc = "";
			if(count($intr_full) > 0){
				$params = $this->getComponentParams();
				
				if($params->ijseo_type_desc == "Words"){
					$exclude_key = $params->exclude_key;
					$temp1 = "";
					foreach($exclude_key as $e_key=>$e_value){					
						$temp1 = str_replace($e_value, " ", strip_tags($intr_full["0"]["introtext"]));
					}
					$temp2 = explode(" ", $temp1);
					$delete = array_splice($temp2, 0, $params->ijseo_allow_no_desc);					
					$desc = implode(" ", $delete);					
				}
				else{
					if(isset($intr_full["0"]["introtext"])){
						$exclude_key = $params->exclude_key;
						$temp1 = "";
						foreach($exclude_key as $e_key=>$e_value){
							$temp1 = str_replace(" ".$e_value." ", " ", strip_tags($intr_full["0"]["introtext"]));
							$intr_full["0"]["introtext"] = $temp1;
						}					
						$desc = mb_substr($temp1, 0, $params->ijseo_allow_no_desc);
					}
				}
			}
			$session_description[$id] = $desc;
			/*$query->clear();
			$query->update('#__content');
			$query->set("`metadesc`='".addslashes($desc)."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}*/
		}
		$_SESSION["session_description"] = $session_description;
		return true;
	}
	
	function getComponentParams(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('params');
		$query->from('#__ijseo_config');
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string);
		return $result;
	}
}

?>