<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.utilities.date' );

jimport('joomla.application.component.modellist');

class iJoomla_SeoModelMenus extends JModelList{

	protected $_context = 'com_ijoomla_seo.menus';
	private $total=0;

	function populateState(){
		// Initialize variables.
		$app = JFactory::getApplication('administrator');
		// Load the list state.
		$this->setState('list.start', $app->getUserStateFromRequest($this->_context . '.list.start', 'limitstart', 0, 'int'));
		$this->setState('list.limit', $app->getUserStateFromRequest($this->_context . '.list.limit', 'limit', $app->getCfg('list_limit', 25) , 'int'));
		$this->setState('selected', JRequest::getVar('cid', array()));
		$catid = $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		$this->setState('filter.catid', $catid);
	}
	
	function getPagination(){
		$pagination=parent::getPagination();
		$pagination->total=$this->total;
		if($pagination->total%$pagination->limit>0)
			$nr_pages=intval($pagination->total/$pagination->limit)+1;
		else $nr_pages=intval($pagination->total/$pagination->limit);
		$pagination->set('pages.total',$nr_pages);
		$pagination->set('pages.stop',$nr_pages);
		return $pagination;
	}			
	
	function getItems(){
		$config = new JConfig();
		$app		= JFactory::getApplication('administrator');
		$limistart	= $app->getUserStateFromRequest($this->context.'.list.start', 'limitstart');
		$limit		= $app->getUserStateFromRequest($this->context.'.list.limit', 'limit', $config->list_limit);
				
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$query = $this->getListQuery();
				
		$db->setQuery($query);
		$db->query();
		$result	= $db->loadObjectList();
		$this->total=count($result);
		
		$db->setQuery($query,$limistart,$limit);
		$db->query();
		$result	= $db->loadObjectList();	
		return $result;
	}
	
	function getListQuery(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','any','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
		
		$menu_types = $app->getUserStateFromRequest($this->context.'.filter.menu_types', 'menu_types','','string');
		$this->setState('filter.menu_types', $menu_types, 'string');				
			
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_author = "";
			$filter_state = "";
			$filter_search = "";
			$catid = "";
			$filter_missing = JRequest::getVar("value", "", "get");			
			$this->setState('filter.author', "" ,'string');
			$this->setState('filter.missing', $filter_missing, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
		}	
							
		$where=" 1=1 ";
					
		if($menu_types != "" && $menu_types != "0"){
			$where.= " and menutype='".$menu_types."'";
		}
					
		switch ($filter_missing){
			case "1":
				$where.= " and (params like '%\"page_title\":\"\"%' or params not like '%page_title%')";
				break;
			case "2":
				$where.= " and (params like '%\"menu-meta_keywords\":\"\"%' or params not like '%menu-meta_keywords%')";
				break;
			case "3":
				$where.= " and (params like '%\"menu-meta_description\":\"\"%' or params not like '%menu-meta_description%')";
				break;
			case "4":
				$where.= " and (params like '%\"page_title\":\"\"%' or  params like '%\"menu-meta_keywords\":\"\"%' or params like '%\"menu-meta_description\":\"\"%' or params not like '%page_title%' or params not like '%menu-meta_keywords%' or params not like '%menu-meta_description%')";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.=" and published=1 ";
				break;
			case "2":
				$where.=" and published=0 ";
				break;
			case "3":
				$where.=" and published=-2 ";
				break;				
			default:
				$where.=" and published in (0, 1) ";
				break;
		}
		
		if($filter_search!=""){ 
			$where.=" and (title like '%".addslashes($filter_search)."%' or params like '%".addslashes($filter_search)."%') ";
		}		
		$query->clear();
		$query->select('id, title, params, link');
		$query->from('#__menu');
		$query->where($where);
		$query->order('lft asc');		
		return $query;		
	}		
	
	function getParams($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('params');
		$query->from('#__menu');
		$query->where("id=".$id);
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string, true);
		return $result;
	}
	
	function getAllMTitleKeys(){
		$db =& JFactory::getDBO();
		$sql = "select concat(title, '[', type, ']') from #__ijseo_titlekeys";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResultArray();
		return $result;
	} 
	
	function getTitleTipe(){
		$types = JRequest::getVar("types", "articles");
		$return = "";
		switch($types){
			case "articles" : {
				$return = "article";
				break;
			}
			case "menus" : {
				$return = JRequest::getVar("menu_types", "");
				break;
			}
		}
		return $return;
	}
	
	function deleteOldKeys($menutype, $ids) {
		$db = &JFactory::getDBO();
		$sql = "DELETE FROM `#__ijseo_keys_id` WHERE `type`='usermenu' AND `type_id` IN (" . implode(',', $ids) . ")";
		$db->setQuery($sql);
		$db->query($sql);
	}
	
	function save(){		
		$component_params = $this->getComponentParams();		
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		
		$ids = JRequest::getVar("cid", "", "post", "array");
		$page_title = JRequest::getVar("page_title", "", "post", "array");
		$metakey = JRequest::getVar("metakey", "", "post", "array");
		$metadesc = JRequest::getVar("metadesc", "", "post", "array");
		
		$menutype = JRequest::getVar("menu_types", "");
		$all_item_menus = $this->getItemsMenu($menutype);
		
		$matakeys = array();
		
		$this->deleteOldKeys($menutype, $ids);
	
		foreach($ids as $key=>$id){			
			if($page_title[$id] != "" || $metakey[$id] != "" || $metadesc[$id] != ""){							
				//save title keywords if option is set to keywords from titlemetatag
				if($component_params->ijseo_keysource == "1"){
					$all_ptitlekeys = $this->getAllMTitleKeys();
					$jnow = & JFactory::getDate();
					$date =  $jnow->toMySQL();
					$title_type = $this->getTitleTipe();
					
					if(trim($component_params->delimiters) != ""){
						$delimiters = str_split(trim($component_params->delimiters));
						$page_title_temp = $page_title[$id];				
						$page_title_temp = str_replace($delimiters, "******", $page_title_temp);//replace |,:; with ****** and then for each element we have a new row in  _ijseo_titlekeys
						$page_title_array = explode("******", $page_title_temp);
						if(is_array($page_title_array) && count($page_title_array) > 0){
							foreach($page_title_array as $ptkey=>$ptvalue) {
								if(isset($all_ptitlekeys) && is_array($all_ptitlekeys) && !in_array($ptvalue."[".$title_type."]", $all_ptitlekeys) && trim($ptvalue) != ""){
									$sql = "insert into #__ijseo_titlekeys values('', '".addslashes(trim($ptvalue))."', 0, 0, -1,  '".$date."', 0, '".$title_type."', ".intval($id).")";
									$db->setQuery($sql);
									$db->query();
								}
								elseif(trim($ptvalue) == ""){
									$sql = "update #__ijseo_titlekeys set title='' where type='".$menutype."' and joomla_id=".intval($id);
									$db->setQuery($sql);
									$db->query();
								}
							}
						}
					}
					else{
						if(trim($page_title[$id]) != ""){
							$sql = "insert into #__ijseo_titlekeys values('', '".addslashes(trim($page_title[$id]))."', 0, 0, -1,  '".$date."', 0, '".$title_type."', ".intval($id).")";
							$db->setQuery($sql);
							$db->query();
						}
					}
				}
				
				$params = $this->getParams($id);				
				$params["page_title"] = $page_title[$id];
				$params["menu-meta_keywords"] = $metakey[$id];
				$params["menu-meta_description"] = $metadesc[$id];
				$param = json_encode($params);
				$param = str_replace("'", "''", $param);
				$param = str_replace("\\", "\\\\", $param);
				$query->clear();
				$query->update('#__menu');
				$query->set("`params`='".$param."'");
				$query->where('id='.$id);				
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
				$matakeys[] = $metakey[$id];
				//save keys in component tables
				$this->sincronizeMTAGS($id, $page_title[$id], $metakey[$id], $metadesc[$id], $all_item_menus);
				$return = $this->sincronizeKeys($matakeys, $id);
				if($return === FALSE){
					return false;
				}
				$matakeys = array();
			}
			elseif($page_title[$id] == "" || $metakey[$id] == "" || $metadesc[$id] == ""){
				$params = $this->getParams($id);				
				$params["page_title"] = $page_title[$id];
				$params["menu-meta_keywords"] = $metakey[$id];
				$params["menu-meta_description"] = $metadesc[$id];
				$param = json_encode($params);
				$param = str_replace("'", "''", $param);
				$param = str_replace("\\", "\\\\", $param);
				$query->clear();
				$query->update('#__menu');
				$query->set("`params`='".$param."'");
				$query->where('id='.$id);				
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
				$sql = "update #__ijseo_titlekeys set title='".trim($page_title[$id])."' where type='".$menutype."' and joomla_id=".intval($id);
				$db->setQuery($sql);
				$db->query();
				
				$sql = "update #__ijseo_metags set titletag='".trim($page_title[$id])."', metakey='".trim($metakey[$id])."', metadesc='".trim($metadesc[$id])."' where mtype='".$menutype."' and id=".intval($id);
				$db->setQuery($sql);
				$db->query();
			}
		}
		
		//delete old keys, useless keys
		/*if(!$this->emptyOldKeys()){
			return false;
		}*/
		
		//echo "<pre>";var_dump($_POST);die();
		
		return true;	
	}
	
	function getItemsMenu($menutype){
		$db =& JFactory::getDBO();
		$sql = "select id, title from #__menu where menutype='".$menutype."'";	
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadAssocList("id");
		return $result;
	}
	
	function sincronizeMTAGS($id, $metatitle, $metakey, $metadesc, $all_item_menus){
		$menutype = JRequest::getVar("menu_types", "");
		$db =& JFactory::getDBO();
		if($menutype != "" && $menutype != "0"){			
			if($this->existMETAGS($id, $all_item_menus[$id]["title"], $menutype)){
				if(trim($metatitle) == "" && trim($metakey) == "" && trim($metadesc) == ""){
					$sql = "delete from #__ijseo_metags where id=".intval($id);
				}
				else{
					$sql = "update #__ijseo_metags set titletag='".addslashes(trim($metatitle))."', metakey='".addslashes(trim($metakey))."', metadesc='".addslashes(trim($metadesc))."' where id=".intval($id)." and mtype='".trim($menutype)."'";
				}	
			}
			else{
				$sql = "insert into #__ijseo_metags (`mtype`, `id`, `name`, `titletag`, `metakey`, `metadesc`) values ('".trim($menutype)."', ".$id.", '".addslashes(trim($all_item_menus[$id]["title"]))."', '".addslashes(trim($metatitle))."', '".addslashes(trim($metakey))."', '".addslashes(trim($metadesc))."')";
			}
			$db->setQuery($sql);
			$db->query();
		}
	}
	
	function existMETAGS($id, $title, $menutype){
		$db =& JFactory::getDBO();
		$sql = "select count(*) from #__ijseo_metags where id=".intval($id)." and name='".addslashes(trim($title))."' and mtype='".trim($menutype)."'";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResult();
		if($result > 0){
			return true;
		}
		return false;
	}
	
	function emptyOldKeys(){		
		$all_keys = array();
		$keyword_id = array();
		$keyword = array();
		$menus = array();
		$menus_id = array();
		$menus_key_id = array();
		$temp1 = "";
		
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("title");
		$query->from("#__ijseo_keys");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadAssocList();
		foreach($result as $key=>$value){
			$all_keys[] = $value["title"];
		}
				
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("metakey, id");
		$query->from("#__content");
		$query->where("metakey <> ''");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadAssocList();		
		foreach($result as $key=>$value){
			$temp = explode(",", $value["metakey"]);
			$keyword = array_merge($keyword, $temp);
			$value["metakey"] = str_replace(",", "|".$value["id"].",", trim($value["metakey"]));
			$value["metakey"] .= "|".$value["id"];	
			$temp = explode(",", $value["metakey"]);									
			$keyword_id = array_merge($keyword_id, $temp);
		}
						
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("params, id");
		$query->from("#__menu");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadAssocList();		
		foreach($result as $key=>$value){
			$temp1 = json_decode($value["params"], true);
			if(isset($temp1["menu-meta_keywords"]) && trim($temp1["menu-meta_keywords"]) != ""){
				$temp2 = explode(",", $temp1["menu-meta_keywords"]);
				$menus = array_merge($menus, $temp2);
				
				$temp1["menu-meta_keywords"] = str_replace(",", "|".$value["id"].",", trim($temp1["menu-meta_keywords"]));
				$temp1["menu-meta_keywords"] .= "|".$value["id"];
				$temp2 = explode(",", $temp1["menu-meta_keywords"]);
				$menus_id = array_merge($menus_id, $temp2);
			}
		}
		//make difference beetwin all keys from component tables and keys from _content, so delete only useless keys
		$difference1 = array_diff($all_keys, $keyword);			
		//make difference beetwin result first diference and keys from menus, so this not delete keys from component if that key is in menus and not in _content
		$difference2 = array_diff($difference1, $menus);		
		//delete from  _ijseo_keys (real useless keys)
		foreach($difference2 as $key=>$value){			
			$query->clear();
			$query->delete('#__ijseo_keys');
			$query->where("title='".addslashes($value)."'");		
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
			else{
				$query->clear();
				$query->delete('#__ijseo_keys_id');
				$query->where("keyword='".addslashes($value)."' and type='".JRequest::getVar("menu_types")."'");						
				$db->setQuery($query);
				if(!$db->query()){
					return false;
				}
			}
		}
		//delete from  _ijseo_keys_id (real useless keys)
		$query->clear();
		$query->select("keyword, type_id");
		$query->from("#__ijseo_keys_id");
		$query->where("type='".JRequest::getVar("menu_types")."'");
		
		$db->setQuery($query);
		$db->query();
		$result = $db->loadAssocList();
		foreach($result as $key=>$value){
			$temp1["menu-meta_keywords"] = str_replace(",", "|".$value["type_id"].",", $value["keyword"]);
			$temp1["menu-meta_keywords"] .= "|".$value["type_id"];			
			$temp = explode(",", $temp1["menu-meta_keywords"]);
			$menus_key_id = array_merge($menus_key_id, $temp);
		}
			
		$difference = array_diff($menus_key_id, $menus_id);				
		
		foreach($difference as $key=>$value){
			$temp = explode("|",$value);
			$query->clear();
			$query->delete('#__ijseo_keys_id');
			$query->where("`keyword`='".addslashes($temp["0"])."' and `type_id` = ".$temp["1"]." and type='".JRequest::getVar("menu_types")."'");		
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
		}
		return true;
	}
	
	function sincronizeKeys($matakeys, $id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$date = new JDate();
		$string = implode(",", $matakeys);
		$allvalues = explode(",", $string);
		foreach($allvalues as $key=>$value){			
			if(!$this->exist($value) && $value != ""){				
				$query->clear();
				$query->insert("#__ijseo_keys");
				$query->set("`title`='".trim(addslashes($value))."'");
				$query->set('rank='.$this->getRank($value));
				$query->set('rchange=0');
				$query->set('mode=-1');
				$query->set("checkdate='".$date."'");
				$query->set("sticky=0");								
				$db->setQuery($query);				
				if(!$db->query()){
					return false;
				}
				else{
					$sql = "SELECT `keyword` FROM #__ijseo_keys_id 
								WHERE `keyword` = '" . trim(addslashes($value)) . "' 
								AND `type`='".JRequest::getVar("menu_types")."' AND `type_id` = '" . $id . "' LIMIT 1 ";
					$db->setQuery($sql);
					$exists_k = $db->loadResult();
					if (!$exists_k) {
						/*
						$query->clear();
						$query->insert("#__ijseo_keys_id");
						$query->set("`keyword`='".trim(addslashes($value))."'");
						$query->set("`type`='".JRequest::getVar("menu_types")."'");
						$query->set('type_id='.$id);							
						$db->setQuery((string)$query);			
						*/
						$sql = "INSERT INTO `#__ijseo_keys_id` (`keyword`, `type`, `type_id`) 
							VALUES ('" . trim(addslashes($value)) . "', '" . JRequest::getVar("menu_types") . "', '" . $id . "');";
						$db->setQuery($sql);
						$sqlz[] = $sql;
						if(!$db->query()){
							return false;
						}
					}
				}
			}
			$type = JRequest::getVar("menu_types");
			if(!$this->existInKeyId($value, $type, $id) && $value != "") {
				$sql = "SELECT `keyword` FROM #__ijseo_keys_id 
							WHERE `keyword` = '" . trim(addslashes($value)) . "' 
							AND `type`='".JRequest::getVar("menu_types")."' AND `type_id` = '" . $id . "' LIMIT 1 ";
				$db->setQuery($sql);
				$exists_k = $db->loadResult();
				if (!$exists_k) {
					/*
					$query->clear();
					$query->insert("#__ijseo_keys_id");
					$query->set("`keyword`='".trim(addslashes($value))."'");
					$query->set("`type`='".JRequest::getVar("menu_types")."'");
					$query->set('type_id='.$id);
					$db->setQuery((string)$query);
					*/					
					$sql = "INSERT INTO `#__ijseo_keys_id` (`keyword`, `type`, `type_id`) 
						VALUES ('" . trim(addslashes($value)) . "', '" . JRequest::getVar("menu_types") . "', '" . $id . "');";
					$db->setQuery($sql);
					$sqlz[] = $sql;
					if(!$db->query()){
						return false;
					}
				}
			}
		}
		
		//echo "<pre>";var_dump($sqlz);echo "<hr />";
		
		return true;
	}
	
	function exist($value){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("id");
		$query->from("#__ijseo_keys");
		$query->where("title = '".addslashes($value)."'");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadResult();
		if($result != NULL || $result != ""){
			return true;
		}
		return false;
	}
	
	function existInKeyId($value, $type, $id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("type_id");
		$query->from("#__ijseo_keys_id");
		$query->where("keyword = '".addslashes($value)."' and `type_id`=".$id." and type='".$type."'");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadResult();
		if($result != NULL || $result != ""){
			return true;
		}
		return false;
	}
	
	function getRank($value){
		return 0;
	}
	
	function getArticleTitle($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('title');
		$query->from('#__menu');
		$query->where("id=".$id);
		$db->setQuery($query);		
		$db->query();
		$result = $db->loadResult();
		return $result;	
	}
	
	function copyKeyToTitle(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_titletag = array();
		foreach($ids as $key=>$id){
			$query->clear();
			$params = $this->getParams($id);
			$session_titletag[$id] = $params["menu-meta_keywords"];
			/*$params["menu-meta_keywords"] = addslashes($params["menu-meta_keywords"]);
			$params["menu-meta_description"] = addslashes($params["menu-meta_description"]); 
			$params["page_title"] = $params["menu-meta_keywords"];			
			$params_database = json_encode($params);			
			$params_database = str_replace("\\\\", "\\", $params_database);
			$query->clear();
			$query->update('#__menu');
			$query->set("`params`='".$params_database."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				die($db->getQuery());
				return false;
			}*/
		}
		$_SESSION["session_titletag"] = $session_titletag;
		return true;
	}
	
	function copyTitleToKey(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_metakey = array();
		foreach($ids as $key=>$id){
			$params = $this->getParams($id);
			$session_metakey[$id] = $params["page_title"];
			/*$params["page_title"] = addslashes($params["page_title"]);
			$params["menu-meta_description"] = addslashes($params["menu-meta_description"]);			
			$params["menu-meta_keywords"] = $params["page_title"];
			$params_database = json_encode($params);			
			$params_database = str_replace("\\\\", "\\", $params_database);
			$query->clear();
			$query->update('#__menu');
			$query->set("`params`='".$params_database."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}*/
		}
		$_SESSION["session_metakey"] = $session_metakey;
		return true;
	}
	
	function copyArticleToKey(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_metakey = array();
		foreach($ids as $key=>$id){
			$params = $this->getParams($id);
			$title = $this->getArticleTitle($id);			
			$session_metakey[$id] = $title;
			/*$params["page_title"] = addslashes($params["page_title"]);
			$params["menu-meta_description"] = addslashes($params["menu-meta_description"]);			
			$params["menu-meta_keywords"] = addslashes($title);
			$params_database = json_encode($params);			
			$params_database = str_replace("\\\\", "\\", $params_database);
			$query->clear();
			$query->update('#__menu');
			$query->set("`params`='".$params_database."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}*/
		}
		$_SESSION["session_metakey"] = $session_metakey;
		return true;
	}
	
	function copyArticleToTitle(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_titletag = array();
		foreach($ids as $key=>$id){
			$params = $this->getParams($id);
			$title = $this->getArticleTitle($id);
			$session_titletag[$id] = $title;
			/*$params["menu-meta_keywords"] = addslashes($params["menu-meta_keywords"]);
			$params["menu-meta_description"] = addslashes($params["menu-meta_description"]);			
			$params["page_title"] = $title;
			$params_database = json_encode($params);			
			$params_database = str_replace("\\\\", "\\", $params_database);
			$query->clear();
			$query->update('#__menu');
			$query->set("`params`='".$params_database."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}*/
		}
		$_SESSION["session_titletag"] = $session_titletag;
		return true;
	}
	
	function getComponentParams(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('params');
		$query->from('#__ijseo_config');
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string);
		return $result;
	}
}

?>