<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');
jimport( 'joomla.utilities.date' );

class iJoomla_SeoModelKeyseasyblog extends JModelList{

	protected $_context = 'com_ijoomla_seo.keyseasyblog';
	private $total=0;

	function populateState(){
		// Initialize variables.
		$app = JFactory::getApplication('administrator');
		// Load the list state.
		$this->setState('list.start', $app->getUserStateFromRequest($this->_context . '.list.start', 'limitstart', 0, 'int'));
		$this->setState('list.limit', $app->getUserStateFromRequest($this->_context . '.list.limit', 'limit', $app->getCfg('list_limit', 25) , 'int'));
		$this->setState('selected', JRequest::getVar('cid', array()));
		$catid = $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		$this->setState('filter.catid', $catid);
	}
	
	function getPagination() {
		if ($this->total == 0) { return false; }	
		$pagination = parent::getPagination();
		$pagination->total=$this->total;
		if($pagination->total%$pagination->limit>0)
			$nr_pages=intval($pagination->total/$pagination->limit)+1;
		else $nr_pages=intval($pagination->total/$pagination->limit);
		$pagination->set('pages.total',$nr_pages);
		$pagination->set('pages.stop',$nr_pages);
		return $pagination;
	}
	
	function getItems(){			
		$config = new JConfig();
		$app		= JFactory::getApplication('administrator');
		$limistart	= $app->getUserStateFromRequest($this->context.'.list.start', 'limitstart');
		$limit		= $app->getUserStateFromRequest($this->context.'.list.limit', 'limit', $config->list_limit);
		$params 	= $this->getComponentParams();
		 		
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$easyblog = JRequest::getVar("easyblog", "0");
		
		// Keywords Meta Tags
		if ($params->ijseo_keysource == "0") {
			// Items
			if ($easyblog == "1") {
				$query = $this->getListQuery1();
			// Categories
			} elseif ($easyblog == "2") {
				$query = $this->getListQuery11();
			}
		// Title Meta Tags
		} else {
			// Items
			if ($easyblog == "1") {
				$query = $this->getListQuery2();
			}
			// Categories
			elseif ($easyblog == "2") {
				$query = $this->getListQuery22();
			}			
		}
		
		// echo ((string)$query) . "<br />";
		
		$db->setQuery($query);		
		$db->query();
		$result	= $db->loadObjectList();
		$this->total=count($result);
		
		$db->setQuery($query,$limistart,$limit);
		$db->query();
		$result	= $db->loadAssocList();
								
		$col = JRequest::getVar("col", "");

		if($col != ""){
			$sort_1 = JRequest::getVar("sort1", "");
			$sort_2 = JRequest::getVar("sort2", "");
			$sort_3 = JRequest::getVar("sort3", "");
			$sort_4 = JRequest::getVar("sort4", "");
			$sort_5 = JRequest::getVar("sort5", "");
			$sort_6 = JRequest::getVar("sort6", "");
			$sort_type = "asc";
			
			switch($col){
				case "title":
					$sort_type = $sort_1;
					break;
				case "rank":
					$sort_type = $sort_2;
					break;
				case "rchange":
					$sort_type = $sort_3;
					break;
				case "checkdate":
					$sort_type = $sort_4;
					break;
				case "sticky":
					$sort_type = $sort_5;
					break;	
			}			
			$sortArray = array();
			
			foreach($result as $res){
				foreach($res as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
			$orderby = $col;
			if($sort_type == "asc"){
				array_multisort($sortArray[$orderby], SORT_ASC, $result);	
			}
			else{
				array_multisort($sortArray[$orderby], SORT_DESC, $result);
			}			
			return $result;	
		}
		else{
			return $result;
		}
	}
	
	// Keywords - Items
	function getListQuery1(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		$catid 		= $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
		
		$key_search = $app->getUserStateFromRequest($this->context.'.filter.key_search', 'key_search','','string');
		$this->setState('filter.key_search', $key_search, 'string');
		
		$filter_sticky = $app->getUserStateFromRequest($this->context.'.filter.sticky', 'sticky','','string');
		$this->setState('filter.sticky', $filter_sticky, 'string');
		
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_author = "";
			$filter_state = "";
			$filter_search = "";
			$catid = "";
			$filter_missing = "";
			$key_search = "";
			$filter_sticky = JRequest::getVar("value", "", "get");			
			$this->setState('filter.author', "" ,'string');
			$this->setState('filter.sticky', $filter_sticky, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
			$this->setState('filter.missing', "", 'string');
			$this->setState('filter.key_search', "", 'string');
		}
			
		$where = " ki.keyword=k.title and ki.type='easyblog-item'";
		
		if($filter_sticky != "" && $filter_sticky != "0"){
			$where.= " and k.sticky=1 ";
		}
		
		switch ($filter_missing){
			case "1":
				$where.= " and ki.type_id in (select id from #__ijseo_metags where titletag='' and mtype='easyblog-item') ";
				break;
			case "2":
				$where.= " and ki.type_id in (select id from #__ijseo_metags where metakey='' and mtype='easyblog-item') ";
				break;
			case "3":
				$where.= " and ki.type_id in (select id from #__ijseo_metags where metadesc='' and mtype='easyblog-item') ";
				break;
			case "4":
				$where.= " and ki.type_id in (select id from #__ijseo_metags where (metadesc='' or titletag='' or metakey='') and mtype='easyblog-item') ";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.=" and ki.type_id in (select id from #__easyblog_post where published=1) ";
				break;
			case "2":
				$where.=" and ki.type_id in (select id from #__easyblog_post where published=0) ";
				break;			
			default:
				break;
		}
		
		if($filter_search != ""){ 
			$where.= " and ki.type_id in (select id from #__ijseo_metags where (metadesc like '%".addslashes($filter_search)."%' or titletag like '%".addslashes($filter_search)."%' or metakey like '%".addslashes($filter_search)."%')) ";
		}
		
		if($key_search != ""){ 
			$where.=" and ki.keyword like '%".addslashes($key_search)."%'";
		}
		
		$keyup_doun = JRequest::getVar("keyup_doun", "");
		//for values from default page calculated with ajax
		$jnow =& JFactory::getDate();
		$date = $jnow->toMySQL();
		if($keyup_doun != ""){
			$query->select('distinct *');
			$query->from('#__ijseo_keys k');
			$query->join('LEFT', '#__ijseo_keys_id ki on ki.keyword = k.title');
			$query->where("ki.type = 'easyblog-item' and k.mode = 1 and k.rchange != 0 ");
			$query->group('k.title');			
		}
		else{			
			$query->select('distinct *');
			$query->from('#__ijseo_keys k, #__ijseo_keys_id ki');
			$query->where($where);
			$query->group('k.title');
		}
        
        $query->order("CASE rank WHEN 0 THEN 9999 ELSE rank END");
        
		return $query;
	}
	
	// Keywords - Categories
	function getListQuery11(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		$catid 		= $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
		
		$key_search = $app->getUserStateFromRequest($this->context.'.filter.key_search', 'key_search','','string');
		$this->setState('filter.key_search', $key_search, 'string');
		
		$filter_sticky = $app->getUserStateFromRequest($this->context.'.filter.sticky', 'sticky','','string');
		$this->setState('filter.sticky', $filter_sticky, 'string');
		
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_author = "";
			$filter_state = "";
			$filter_search = "";
			$catid = "";
			$filter_missing = "";
			$key_search = "";
			$filter_sticky = JRequest::getVar("value", "", "get");			
			$this->setState('filter.author', "" ,'string');
			$this->setState('filter.sticky', $filter_sticky, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
			$this->setState('filter.missing', "", 'string');
			$this->setState('filter.key_search', "", 'string');
		}
			
		$where = " ki.keyword=k.title and ki.type='easyblog-cat'";
		
		if($filter_sticky != "" && $filter_sticky != "0"){
			$where.= " and k.sticky=1 ";
		}
		
		switch ($filter_missing){
			case "1":
				$where.= " and ki.type_id in (select id from #__ijseo_metags where titletag='' and mtype='easyblog-cat') ";
				break;
			case "2":
				$where.= " and ki.type_id in (select id from #__ijseo_metags where metakey='' and mtype='easyblog-cat') ";
				break;
			case "3":
				$where.= " and ki.type_id in (select id from #__ijseo_metags where metadesc='' and mtype='easyblog-cat') ";
				break;
			case "4":
				$where.= " and ki.type_id in (select id from #__ijseo_metags where (metadesc='' or titletag='' or metakey='') and mtype='easyblog-cat') ";
				break;
			default:
				break;
		}
		
		switch ($filter_state) {
			case "1":
				$where.=" and ki.type_id in (select id from #__easyblog_categories where published=1) ";
				break;
			case "2":
				$where.=" and ki.type_id in (select id from #__easyblog_categories where published=0) ";
				break;			
			default:
				break;
		}
		
		if($filter_search != ""){ 
			$where.= " and ki.type_id in (select id from #__ijseo_metags where (metadesc like '%".addslashes($filter_search)."%' or titletag like '%".addslashes($filter_search)."%' or metakey like '%".addslashes($filter_search)."%')) ";
		}
		
		if($key_search != ""){ 
			$where.=" and ki.keyword like '%".addslashes($key_search)."%'";
		}					
		
		$keyup_doun = JRequest::getVar("keyup_doun", "");
		//for values from default page calculated with ajax
		$jnow =& JFactory::getDate();
		$date = $jnow->toMySQL();
		if($keyup_doun != ""){
			$query->select('distinct *');
			$query->from('#__ijseo_keys k');
			$query->join('LEFT', '#__ijseo_keys_id ki on ki.keyword = k.title');
			$query->where("ki.type = 'easyblog-cat' and k.mode = 1 and k.rchange != 0 ");
			$query->group('k.title');			
		}
		else{			
			$query->select('distinct *');
			$query->from('#__ijseo_keys k, #__ijseo_keys_id ki');
			$query->where($where);
			$query->group('k.title');
		}
        
        $query->order("CASE rank WHEN 0 THEN 9999 ELSE rank END");
        
		return $query;
	}
	
	// Title - Items
	function getListQuery2(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		$catid 		= $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
		
		$key_search = $app->getUserStateFromRequest($this->context.'.filter.key_search', 'key_search','','string');
		$this->setState('filter.key_search', $key_search, 'string');
		
		$filter_sticky = $app->getUserStateFromRequest($this->context.'.filter.sticky', 'sticky','','string');
		$this->setState('filter.sticky', $filter_sticky, 'string');
		
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_author = "";
			$filter_state = "";
			$filter_search = "";
			$filter_missing = "";
			$key_search = "";
			$filter_sticky = JRequest::getVar("value", "", "get");			
			$this->setState('filter.mtreecat', "-1" ,'string');
			$this->setState('filter.sticky', $filter_sticky, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
			$this->setState('filter.missing', "", 'string');
			$this->setState('filter.key_search', "", 'string');
		}
			
		$where = " type='easyblog-item' ";
		
		if($filter_sticky != "" && $filter_sticky != "0"){
			$where.= " and sticky=1 ";
		}
		
		switch ($filter_missing){
			case "1":
				//$where.= " and joomla_id in (select link_id from #__mt_links where attribs like '%\"page_title\":\"\"%')";
				break;
			case "2":
				$where.= " and joomla_id in (select id from #__easyblog_post where metakey='' )";
				break;
			case "3":
				$where.= " and joomla_id in (select id from #__easyblog_post where metadesc='' )";
				break;
			case "4":
				$where.= " and joomla_id in (select id from #__easyblog_post where metakey='' OR metadesc='' )";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.= " and joomla_id in (select id from #__easyblog_post where published=1) ";
				break;
			case "2":
				$where.= " and joomla_id in (select id from #__easyblog_post where published=0) ";
				break;
			default:
				break;
		}
		
		if($filter_search != ""){ 
			$where.= " and joomla_id in (select id from #__easyblog_post where name like '%".addslashes($filter_search)."%' )";
		}
		
		if($key_search != ""){ 
			$where.=" and title like '%".addslashes($key_search)."%'";
		}
							
		$jnow =& JFactory::getDate();
		$date = $jnow->toMySQL();
		
		$query->select('*');
		$query->from('#__ijseo_titlekeys');
		$query->where($where);
        
        $query->order("CASE rank WHEN 0 THEN 9999 ELSE rank END");
		
        return $query;
	}
	
	// Title - Categories
	function getListQuery22(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		$catid 		= $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
		
		$key_search = $app->getUserStateFromRequest($this->context.'.filter.key_search', 'key_search','','string');
		$this->setState('filter.key_search', $key_search, 'string');
		
		$filter_sticky = $app->getUserStateFromRequest($this->context.'.filter.sticky', 'sticky','','string');
		$this->setState('filter.sticky', $filter_sticky, 'string');
		
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_author = "";
			$filter_state = "";
			$filter_search = "";
			$filter_missing = "";
			$key_search = "";
			$filter_sticky = JRequest::getVar("value", "", "get");			
			$this->setState('filter.mtreecat', "-1" ,'string');
			$this->setState('filter.sticky', $filter_sticky, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
			$this->setState('filter.missing', "", 'string');
			$this->setState('filter.key_search', "", 'string');
		}
			
		$where = " type='zoo_cats' ";		
		
		if($filter_sticky != "" && $filter_sticky != "0"){
			$where.= " and sticky=1 ";
		}
		
		switch ($filter_missing){
			case "1":
				//$where.= " and joomla_id in (select link_id from #__mt_links where attribs like '%\"page_title\":\"\"%')";
				break;
			case "2":
				//$where.= " and joomla_id in (select cat_id from #__mt_cats where metakey='')";
				break;
			case "3":
				//$where.= " and joomla_id in (select cat_id from #__mt_cats where metadesc='') ";
				break;
			case "4":
				//$where.= " and joomla_id in (select cat_id from #__mt_cats where metakey='' or metadesc='')";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.= " and joomla_id in (select id from #__easyblog_category where published=1) ";
				break;
			case "2":
				$where.= " and joomla_id in (select id from #__easyblog_category where published=0) ";
				break;
			default:
				break;
		}
		if($filter_search != ""){ 
			$where.=" and joomla_id in (select id from #__easyblog_category where name like '%".addslashes($filter_search)."%') ";
		}
		
		if($key_search != ""){ 
			$where.=" and title like '%".addslashes($key_search)."%'";
		}
							
		$jnow =& JFactory::getDate();
		$date = $jnow->toMySQL();
		
		$query->select('*');
		$query->from('#__ijseo_titlekeys');
		$query->where($where);
        
        $query->order("CASE rank WHEN 0 THEN 9999 ELSE rank END");
		
		return $query;
	}
	
	function getComponentParams(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('params');
		$query->from('#__ijseo_config');
		$db->setQuery($query);
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string);
		return $result;
	}
	
	function getStickyUnsticky(){
		$params = $this->getComponentParams();
		$ids = JRequest::getVar("cid", array());		
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$value = "";
		$task = JRequest::getVar("task", "");
		if($task == "sticky"){
			$value = "1";
		}
		else{
			$value = "0";
		}
		foreach($ids as $key=>$id){
			$query->clear();
			if($params->ijseo_keysource == "1"){
				$query->update('#__ijseo_titlekeys');
			}
			else{
				$query->update('#__ijseo_keys');
			}
			$query->set("sticky=".$value);
			$query->where('id='.$id);			
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
		}
		return true;
	}
	
	function getMtreeCategories(){
		$db =& JFactory::getDBO();
		$sql = "select cat_id, cat_name from #__mt_cats where cat_published=1 and cat_approved=1";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadAssocList();
		return $result;
	}
}

?>