<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class iJoomla_SeoModelPages extends JModelList{

	protected $_context = 'com_ijoomla_seo.articles';
	private $total=0;

	function populateState(){
		// Initialize variables.
		$app = JFactory::getApplication('administrator');
		// Load the list state.
		$this->setState('list.start', $app->getUserStateFromRequest($this->_context . '.list.start', 'limitstart', 0, 'int'));
		$this->setState('list.limit', $app->getUserStateFromRequest($this->_context . '.list.limit', 'limit', $app->getCfg('list_limit', 25) , 'int'));
		$this->setState('selected', JRequest::getVar('cid', array()));
		$catid = $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		$this->setState('filter.catid', $catid);
	}
	
	function getPagination(){
		$pagination=parent::getPagination();
		$pagination->total=$this->total;
		if($pagination->total%$pagination->limit>0)
			$nr_pages=intval($pagination->total/$pagination->limit)+1;
		else $nr_pages=intval($pagination->total/$pagination->limit);
		$pagination->set('pages.total',$nr_pages);
		$pagination->set('pages.stop',$nr_pages);
		return $pagination;
	}	
	
	function getItems(){
		$config = new JConfig();
		$app		= JFactory::getApplication('administrator');
		$limistart	= $app->getUserStateFromRequest($this->context.'.list.start', 'limitstart');
		$limit		= $app->getUserStateFromRequest($this->context.'.list.limit', 'limit', $config->list_limit);
				
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$query = $this->getListQuery();
		
		$db->setQuery($query);
		$db->query();
		$result	= $db->loadObjectList();
		$this->total=count($result);
		
		$db->setQuery($query,$limistart,$limit);
		$db->query();
		$result	= $db->loadObjectList();	
		return $result;
	}
	
	function getListQuery(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		$catid 		= $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		
		$filter_author = $app->getUserStateFromRequest($this->context.'.filter.author', 'filter_authorid','','string');
		$this->setState('filter.author', $filter_author,'string');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
				
		$where=" 1=1 ";
		
		if(intval($catid)>0){
			$where.= " and catid=".intval($catid);
		}
		
		if($filter_author != "0" && $filter_author != ""){
			$where.= " and created_by=".intval($filter_author);
		}
		
		switch ($filter_missing){
			case "1":
				$where.= " and attribs like '%\"page_title\":\"\"%'";
				break;
			case "2":
				$where.= " and metakey='' ";
				break;
			case "3":
				$where.= " and metadesc='' ";
				break;
			case "4":
				$where.= " and (attribs like '%\"page_title\":\"\"%' or  metakey='' or metadesc='')";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.=" and state=1 ";
				break;
			case "2":
				$where.=" and state=0 ";
				break;
			case "3":
				$where.=" and state=2 ";
				break;
			case "4":
				$where.=" and state=-2 ";
				break;	
			default:
				$where.=" and state <> -2 ";
				break;
		}
		
		if($filter_search!=""){ 
			$where.=" and (title like '%".addslashes($filter_search)."%' or metakey like '%".addslashes($filter_search)."%' or metadesc like '%".addslashes($filter_search)."%') ";
		}		
		
		$query->select('`id`, `title`, `metakey`, `metadesc`, `attribs`, `introtext`, `fulltext`');
		$query->from('#__content');
		$query->where($where);		
				
		return $query;		
	}
	
	function getAuthors(){
		$db =& JFactory::getDBO();		
		$query = " SELECT c.created_by,  u.name ".
				" FROM (#__content AS c) ".
				" LEFT JOIN #__users AS u ON (u.id = c.created_by) ".
				" WHERE c.state <> -1 ".
				" AND c.state <> -2 ".
				" GROUP BY u.name ".
				" ORDER BY u.name ";
		$db->setQuery($query);		
		$db->query();
		$result = $db->loadObjectList();
		return $result;	
	}
	
	function getAttribs($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('attribs');
		$query->from('#__content');
		$query->where("id=".$id);
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string);
		return $result;
	}
	
	function savepage(){
		$id = JRequest::getVar("id");
		$mtitle = JRequest::getVar("mtitle", "");
		$metakey = JRequest::getVar("metakey", "");
		$metadesc = JRequest::getVar("metadesc", "");
		$attribs = $this->getAttribs($id);
		$attribs->page_title = $mtitle;
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$attrib = json_encode($attribs);
		$attrib = str_replace("'", "\'", $attrib);
		$query->clear();
		$query->update('#__content');		
		$query->set("`metakey`='".addslashes($metakey)."', `metadesc`='".addslashes($metadesc)."', `attribs`='".$attrib."'");
		$query->where('id='.$id);		
		$db->setQuery($query);	
		if(!$db->query()){
			return false;
		}
		return true;
	}
}

?>