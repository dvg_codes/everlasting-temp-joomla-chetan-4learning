<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/
// ini_set('display_errors', 1);
// error_reporting(E_ALL);

defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.utilities.date' );

jimport('joomla.application.component.modellist');

class iJoomla_SeoModelKunena extends JModelList {

	protected $_context = 'com_ijoomla_seo.kunena';
	private $total = 0;

	function populateState() {
		// Initialize variables.
		$app = JFactory::getApplication('administrator');
		// Load the list state.
		$this->setState('list.start', $app->getUserStateFromRequest($this->_context . '.list.start', 'limitstart', 0, 'int'));
		$this->setState('list.limit', $app->getUserStateFromRequest($this->_context . '.list.limit', 'limit', $app->getCfg('list_limit', 25) , 'int'));
		$this->setState('selected', JRequest::getVar('cid', array()));
		$catid = $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		$this->setState('filter.catid', $catid);
	}
	
	function getPagination() {
		$pagination = parent::getPagination();
		$pagination->total=$this->total;
		if($pagination->total%$pagination->limit>0)
			$nr_pages = intval($pagination->total/$pagination->limit)+1;
		else $nr_pages = intval($pagination->total/$pagination->limit);
		$pagination->set('pages.total',$nr_pages);
		$pagination->set('pages.stop',$nr_pages);
		return $pagination;
	}
	
	function getItems() {
		$config = new JConfig();
		$app	= JFactory::getApplication('administrator');
		$limistart = $app->getUserStateFromRequest($this->context.'.list.start', 'limitstart');
		$limit = $app->getUserStateFromRequest($this->context.'.list.limit', 'limit', $config->list_limit);
				
		$db =& JFactory::getDBO();
		//$query = $db->getQuery(true);
		$query = $this->getListQuery();
		
		if ($query != NULL) {
			$db->setQuery($query);
			$db->query();
			$result	= $db->loadObjectList();
			$this->total=count($result);
			
			$db->setQuery($query,$limistart,$limit);
			$db->query();
			$result	= $db->loadObjectList();				
		} else {
			$this->total = 0;
			$result = NULL;
		}
		
		return $result;
	}
	
	function existsKunena() {
		$db = &JFactory::getDBO();
		$sql = "SHOW TABLES";
		$db->setQuery($sql);
		$tables = $db->loadResultArray();
		$config =& JFactory::getConfig();
		if (!in_array($config->getValue( 'config.dbprefix' ) . "kunena_categories", $tables)) { return false; } else { return true; }
	}
	
	function getListQuery() {		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','any','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
		
		$menu_types = $app->getUserStateFromRequest($this->context.'.filter.menu_types', 'menu_types','','string');
		$this->setState('filter.menu_types', $menu_types, 'string');

		$filter = JRequest::getVar("filter", "", "get");
		if ($filter != "") {
			$filter_author = "";
			$filter_state = "";
			$filter_search = "";
			$catid = "";
			$filter_missing = JRequest::getVar("value", "", "get");			
			$this->setState('filter.author', "" ,'string');
			$this->setState('filter.missing', $filter_missing, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
		}	

		$where="";

		switch ($filter_missing){
			case "1":
				$where.= " AND m.titletag = '' ";
				break;
			case "2":
				$where.= " AND m.metakey ='' ";
				break;
			case "3":
				$where.= " AND m.metadesc ='' ";
				break;
			case "4":
				$where.= " AND ( m.metadesc = '' OR m.titletag = '' AND m.metakey = '') ";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.= " AND published=1 ";
				break;
			case "2":
				$where.= " AND published=0 ";
				break;
			case "3":
				$where.= " AND trash=1 ";
				break;				
			default:
				$where.= "";
				break;
		}
		
		if($filter_search!=""){ 
			$where.=" AND (title LIKE '%".addslashes($filter_search)."%') ";
		}
		
		$kunena_type = 2;
		
		$query = NULL;
		
		if ($kunena_type) {
			// check the items "branch"
			if ($kunena_type == 1) {
				/*
				$query = "
					SELECT k.id, k.title, m.titletag, m.metakey, m.metadesc 
					FROM #__kunena_items AS k 
					JOIN #__ijseo_metags AS m 
					ON k.id = m.id 
					WHERE m.mtype = 'kunena-item'
					{$where} ORDER BY k.id DESC";
					*/
			} elseif ($kunena_type == 2) {
			// check the categories "branch"
				$query = "
					SELECT k.id, k.name AS title, m.titletag, m.metakey, m.metadesc 
					FROM #__kunena_categories AS k 
					JOIN #__ijseo_metags AS m 
					ON k.id = m.id 
					WHERE m.mtype = 'kunena-cat'
					{$where} ORDER BY k.id DESC";
			}
		}
		//echo $query . "<br />";//die();
		
		return $query;		
	}
	
	function sync() {
		$db = &JFactory::getDBO();
		$component_params = $this->getComponentParams();
		//  ************ Sync kunena categories
		$sql = "
			SELECT *
			FROM `#__kunena_categories`
			WHERE `id` NOT
			IN (
				SELECT `id`
				FROM `#__ijseo_metags`
				WHERE mtype = 'kunena-cat'
			)
			";
		$sqlz[] = $sql;
		$db->setQuery($sql);
		$categs = $db->loadObjectList();
		if (is_array($categs))
		foreach($categs as $cat) {
			$sql = "
				INSERT INTO `#__ijseo_metags` (
				`mtype` ,`id` ,`name` ,`titletag` ,`metakey` ,`metadesc`
				)
				VALUES (
				\"kunena-cat\", \"{$cat->id}\", \"{$cat->name}\", \"{$cat->name}\", '', \"{$cat->description}\"
				);
			";
			$sqlz[] = $sql;
			$db->setQuery($sql);
			$db->query();
		}
		//die();
		//echo "<pre>";var_dump($sqlz);die();
	}
	
	function getParams($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('params');
		$query->from('#__menu');
		$query->where("id=".$id);
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string, true);
		return $result;
	}
	
	function getAllMTitleKeys(){
		$db =& JFactory::getDBO();
		$sql = "SELECT concat(title, '[', type, ']') FROM #__ijseo_titlekeys";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResultArray();
		return $result;
	} 

	function check_exists_key_title($type, $key) {
		$db = &JFactory::getDBO();
		$sql = "SELECT `id` FROM #__ijseo_{$type} WHERE `title` = {$key}";
		$db->setQuery($sql);
		$exists = $db->loadResult();
		if (!$exists) {
			if ($type == 'title') {
				$sql = "INSERT INTO `#__ijseo_title` (`id`, `article_id`, `title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`) 
							VALUES (NULL, '0', '" . $key . "', '0', '0', '-1', NOW(), '0');";
			} else {
				$sql = "INSERT INTO `#__ijseo_keys` (`id`, `title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`) 
							VALUES (NULL, '{$key}', '0', '0', '-1', NOW(), '0');";
			}
			$db->setQuery();
			$db->query();
		}
	}
	
	function save() {
		$component_params = $this->getComponentParams();
		$db =& JFactory::getDBO();
		
		$ids = JRequest::getVar("cid", "", "post", "array");
		$page_title = JRequest::getVar("page_title", "", "post", "array");
		$metakey = JRequest::getVar("metakey", "", "post", "array");
		$metadesc = JRequest::getVar("metadesc", "", "post", "array");
		$kunena_type = 'kunena-cat';
		$title_type = 'kunena-cat';
		$the_type = 'kunena-cat'; 
		foreach($ids as $id) {
			$sql = "UPDATE `#__ijseo_metags` SET `titletag` = \"" . $page_title[$id] . "\",
						`metakey` = \"" . $metakey[$id] . "\",
						`metadesc` = \"" . $metadesc[$id] . "\" 
						WHERE `mtype` = '{$the_type}' AND `id` = '" . $id . "'  
						LIMIT 1 ;";	
			$sqlz[] = $sql;
			$db->setQuery($sql);
			$db->query();
			// if keywords are read from the title meta
			if ($component_params->ijseo_keysource == "1") {
				$all_ptitlekeys = $this->getAllMTitleKeys();
				//echo "<pre>";var_dump($all_ptitlekeys);echo "<br />";
				$jnow = & JFactory::getDate();
				$date =  $jnow->toMySQL();
				if (trim($component_params->delimiters) != "") {
					$delimiters = str_split(trim($component_params->delimiters));
					$page_title_temp = $page_title[$id];
					//replace |,:; with ****** and then for each element we have a new row in  _ijseo_titlekeys
					$page_title_temp = str_replace($delimiters, "******", $page_title_temp);
					$page_title_array = explode("******", $page_title_temp);						
					if (is_array($page_title_array) && count($page_title_array) > 0) {
						foreach($page_title_array as $ptkey => $ptvalue) {
							$ptvalue = trim($ptvalue);
							//echo $ptvalue."[".$title_type."]" . "<hr />";
							if (isset($all_ptitlekeys) && is_array($all_ptitlekeys) && !in_array($ptvalue."[".$title_type."]", $all_ptitlekeys) && trim($ptvalue) != ""){
								$sql = "INSERT INTO #__ijseo_titlekeys VALUES ('', '".addslashes(trim($ptvalue))."', 0, 0, -1,  '".$date."', 0, '".$title_type."', ".intval($id).")";
								$sqlz[] = $sql;
								$db->setQuery($sql);
								$db->query();
							} elseif (trim($ptvalue) == "") {
								$sql = "UPDATE #__ijseo_titlekeys SET title='' WHERE type='" . $title_type . "' AND joomla_id=" . intval($id);
								$sqlz[] = $sql;
								$db->setQuery($sql);
								$db->query();
							}
						}
					}
				}
			} else {
				$sql = "DELETE FROM `#__ijseo_keys_id` 
							WHERE `type` = '{$the_type}' AND `type_id` = {$id}";
				$sqlz[] = $sql;
				$db->setQuery($sql);
				$db->query();
				
				// echo $sql . "<br />";die();
			
				// if keywords are read from the keywords field ( and split by comma , )
				$keyws_array = explode(',', $metakey[$id]);
				if (is_array($keyws_array) && count($keyws_array) > 0) {
					foreach($keyws_array as $ptkey => $ptvalue) {
						$ptvalue = trim($ptvalue);
						if ($ptvalue == '') continue;
						$sql = "SELECT `title` FROM `#__ijseo_keys` WHERE `title` = '{$ptvalue}' LIMIT 1";
						$sqlz[] = $sql;
						$db->setQuery($sql);
						$existsKey = $db->loadResult();
						//echo $ptvalue . " - "; var_dump($existsKey); echo "<hr />";
						if (!$existsKey) {
							$sql = "
								INSERT INTO `#__ijseo_keys` (`title`, `rank`, `rchange`, `mode`, `checkdate`, `sticky`) 
								VALUES ('{$ptvalue}', '0', '0', '-1', NOW(), '0');";
							$sqlz[] = $sql;
							$db->setQuery($sql);
							$db->query();
						}

						$sql = "SELECT `keyword` FROM `#__ijseo_keys_id` 
									WHERE `keyword` = {$ptvalue} AND `type` = {$the_type} AND `type_id` = {$id} LIMIT 1";
						$sqlz[] = $sql;
						$db->setQuery($sql);
						$exists_keys_id = $db->loadResult();
						if (!$exists_keys_id) {
							$sql = "INSERT INTO `#__ijseo_keys_id` (`keyword`, `type`, `type_id`) 
										VALUES ('{$ptvalue}', '{$the_type}', '{$id}');";
							$sqlz[] = $sql;
							$db->setQuery($sql);
							$db->query();				
						}
					}
				}
			}
		}

		// echo "<pre>";var_dump($sqlz);die();
		return true;	
	}
	
	function sincronizeMTAGS($id, $metatitle, $metakey, $metadesc, $all_item_menus){
		$menutype = JRequest::getVar("menu_types", "");
		$db =& JFactory::getDBO();
		if($menutype != "" && $menutype != "0"){			
			if($this->existMETAGS($id, $all_item_menus[$id]["title"], $menutype)){
				if(trim($metatitle) == "" && trim($metakey) == "" && trim($metadesc) == ""){
					$sql = "delete from #__ijseo_metags where id=".intval($id);
				}
				else{
					$sql = "update #__ijseo_metags set titletag=\"".addslashes(trim($metatitle))."\", 
							   metakey=\"".addslashes(trim($metakey))."\", 
							   metadesc=\"".addslashes(trim($metadesc))."\" 
							   where id=".intval($id)." and mtype='".trim($menutype)."'";
				}	
			}
			else{
				$sql = "insert into #__ijseo_metags (`mtype`, `id`, `name`, `titletag`, `metakey`, `metadesc`) 
							values ('".trim($menutype)."', ".$id.", '".addslashes(trim($all_item_menus[$id]["title"]))."',
							\"".addslashes(trim($metatitle))."\", \"".addslashes(trim($metakey))."\", \"".addslashes(trim($metadesc))."\")";
			}
			$db->setQuery($sql);
			$db->query();
		}
	}
	
	function existMETAGS($id, $title, $menutype){
		$db =& JFactory::getDBO();
		$sql = "select count(*) from #__ijseo_metags where id=".intval($id)." and name='".addslashes(trim($title))."' and mtype='".trim($menutype)."'";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResult();
		if($result > 0){
			return true;
		}
		return false;
	}
	
	function emptyOldKeys(){
	}
	
	function sincronizeKeys($matakeys, $id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$date = new JDate();
		$string = implode(",", $matakeys);
		$allvalues = explode(",", $string);
		foreach($allvalues as $key=>$value){			
			if(!$this->exist($value) && $value != ""){				
				$query->clear();
				$query->insert("#__ijseo_keys");
				$query->set("`title`='".trim(addslashes($value))."'");
				$query->set('rank='.$this->getRank($value));
				$query->set('rchange=0');
				$query->set('mode=-1');
				$query->set("checkdate='".$date."'");
				$query->set("sticky=0");								
				$db->setQuery($query);				
				if(!$db->query()){
					return false;
				}
				else{
					$query->clear();
					$query->insert("#__ijseo_keys_id");
					$query->set("`keyword`='".trim(addslashes($value))."'");
					$query->set("`type`='".JRequest::getVar("menu_types")."'");
					$query->set('type_id='.$id);							
					$db->setQuery((string)$query);					
					if(!$db->query()){
						return false;
					}					
				}
			}
			$type = JRequest::getVar("menu_types");
			if(!$this->existInKeyId($value, $type, $id) && $value != ""){				
				$query->clear();
				$query->insert("#__ijseo_keys_id");
				$query->set("`keyword`='".trim(addslashes($value))."'");
				$query->set("`type`='".JRequest::getVar("menu_types")."'");
				$query->set('type_id='.$id);							
				$db->setQuery((string)$query);				
				if(!$db->query()){
					return false;
				}
			}
		}
		return true;
	}
	
	function exist($value){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("id");
		$query->from("#__ijseo_keys");
		$query->where("title = '".addslashes($value)."'");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadResult();
		if($result != NULL || $result != ""){
			return true;
		}
		return false;
	}
	
	function existInKeyId($value, $type, $id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("type_id");
		$query->from("#__ijseo_keys_id");
		$query->where("keyword = '".addslashes($value)."' and `type_id`=".$id." and type='".$type."'");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadResult();
		if($result != NULL || $result != ""){
			return true;
		}
		return false;
	}
	
	function getRank($value){
		return 0;
	}
	
	function getArticleTitle($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('title');
		$query->from('#__menu');
		$query->where("id=".$id);
		$db->setQuery($query);		
		$db->query();
		$result = $db->loadResult();
		return $result;	
	}
	
	function copyKeyToTitle(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_titletag = array();
		foreach($ids as $key=>$id){
			$query->clear();
			$params = $this->getParams($id);
			$session_titletag[$id] = $params["menu-meta_keywords"];
			/*$params["menu-meta_keywords"] = addslashes($params["menu-meta_keywords"]);
			$params["menu-meta_description"] = addslashes($params["menu-meta_description"]); 
			$params["page_title"] = $params["menu-meta_keywords"];			
			$params_database = json_encode($params);			
			$params_database = str_replace("\\\\", "\\", $params_database);
			$query->clear();
			$query->update('#__menu');
			$query->set("`params`='".$params_database."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				die($db->getQuery());
				return false;
			}*/
		}
		$_SESSION["session_titletag"] = $session_titletag;
		return true;
	}
	
	function copyTitleToKey(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_metakey = array();
		foreach($ids as $key=>$id){
			$params = $this->getParams($id);
			$session_metakey[$id] = $params["page_title"];
			/*$params["page_title"] = addslashes($params["page_title"]);
			$params["menu-meta_description"] = addslashes($params["menu-meta_description"]);			
			$params["menu-meta_keywords"] = $params["page_title"];
			$params_database = json_encode($params);			
			$params_database = str_replace("\\\\", "\\", $params_database);
			$query->clear();
			$query->update('#__menu');
			$query->set("`params`='".$params_database."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}*/
		}
		$_SESSION["session_metakey"] = $session_metakey;
		return true;
	}
	
	function copyArticleToKey(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_metakey = array();
		foreach($ids as $key=>$id){
			$params = $this->getParams($id);
			$title = $this->getArticleTitle($id);			
			$session_metakey[$id] = $title;
			/*$params["page_title"] = addslashes($params["page_title"]);
			$params["menu-meta_description"] = addslashes($params["menu-meta_description"]);			
			$params["menu-meta_keywords"] = addslashes($title);
			$params_database = json_encode($params);			
			$params_database = str_replace("\\\\", "\\", $params_database);
			$query->clear();
			$query->update('#__menu');
			$query->set("`params`='".$params_database."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}*/
		}
		$_SESSION["session_metakey"] = $session_metakey;
		return true;
	}
	
	function copyArticleToTitle(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$ids = JRequest::getVar("cid", "", "post", "array");
		$session_titletag = array();
		foreach($ids as $key=>$id){
			$params = $this->getParams($id);
			$title = $this->getArticleTitle($id);
			$session_titletag[$id] = $title;
			/*$params["menu-meta_keywords"] = addslashes($params["menu-meta_keywords"]);
			$params["menu-meta_description"] = addslashes($params["menu-meta_description"]);			
			$params["page_title"] = $title;
			$params_database = json_encode($params);			
			$params_database = str_replace("\\\\", "\\", $params_database);
			$query->clear();
			$query->update('#__menu');
			$query->set("`params`='".$params_database."'");
			$query->where('id='.$id);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}*/
		}
		$_SESSION["session_titletag"] = $session_titletag;
		return true;
	}
	
	function getComponentParams(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('params');
		$query->from('#__ijseo_config');
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string);
		return $result;
	}
}

?>