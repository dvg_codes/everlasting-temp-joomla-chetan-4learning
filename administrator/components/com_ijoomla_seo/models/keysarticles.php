<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');
jimport( 'joomla.utilities.date' );

class iJoomla_SeoModelKeysarticles extends JModelList{

	protected $_context = 'com_ijoomla_seo.keysarticles';
	private $total=0;

	function populateState(){
		// Initialize variables.
		$app = JFactory::getApplication('administrator');
		// Load the list state.
		$this->setState('list.start', $app->getUserStateFromRequest($this->_context . '.list.start', 'limitstart', 0, 'int'));
		$this->setState('list.limit', $app->getUserStateFromRequest($this->_context . '.list.limit', 'limit', $app->getCfg('list_limit', 25) , 'int'));
		$this->setState('selected', JRequest::getVar('cid', array()));
		$catid = $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		$this->setState('filter.catid', $catid);
	}
	
	function getPagination(){
		$pagination=parent::getPagination();
		$pagination->total=$this->total;
		if($pagination->total%$pagination->limit>0)
			$nr_pages=intval($pagination->total/$pagination->limit)+1;
		else $nr_pages=intval($pagination->total/$pagination->limit);
		$pagination->set('pages.total',$nr_pages);
		$pagination->set('pages.stop',$nr_pages);
		return $pagination;
	}
	
	function getAuthors(){
		$db =& JFactory::getDBO();		
		$query = " SELECT c.created_by,  u.name ".
				" FROM (#__content AS c) ".
				" LEFT JOIN #__users AS u ON (u.id = c.created_by) ".
				" WHERE c.state <> -1 ".
				" AND c.state <> -2 ".
				" GROUP BY u.name ".
				" ORDER BY u.name ";
		$db->setQuery($query);		
		$db->query();
		$result = $db->loadObjectList();
		return $result;	
	}	
	
	function getItems(){			
		$config = new JConfig();
		$app		= JFactory::getApplication('administrator');
		$limistart	= $app->getUserStateFromRequest($this->context.'.list.start', 'limitstart');
		$limit		= $app->getUserStateFromRequest($this->context.'.list.limit', 'limit', $config->list_limit);
		$params 	= $this->getComponentParams();
		 		
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		
		if($params->ijseo_keysource == "0"){
			$query = $this->getListQuery();			
		}
		else{
			$query = $this->getListQuery2();
			$this->sincronizeMetaTitles();
		}
		
		$db->setQuery($query);		
		$db->query();
		$result	= $db->loadObjectList();
		$this->total=count($result);
		
		$db->setQuery($query,$limistart,$limit);
		$db->query();
		$result	= $db->loadAssocList();
								
		$col = JRequest::getVar("col", "");

		if($col != ""){
			$sort_1 = JRequest::getVar("sort1", "");
			$sort_2 = JRequest::getVar("sort2", "");
			$sort_3 = JRequest::getVar("sort3", "");
			$sort_4 = JRequest::getVar("sort4", "");
			$sort_5 = JRequest::getVar("sort5", "");
			$sort_6 = JRequest::getVar("sort6", "");
			$sort_type = "asc";
			
			switch($col){
				case "title":
					$sort_type = $sort_1;
					break;
				case "rank":
					$sort_type = $sort_2;
					break;
				case "rchange":
					$sort_type = $sort_3;
					break;
				case "checkdate":
					$sort_type = $sort_4;
					break;
				case "sticky":
					$sort_type = $sort_5;
					break;	
			}			
			$sortArray = array();
			
			foreach($result as $res){
				foreach($res as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
			$orderby = $col;
			if($sort_type == "asc"){
				array_multisort($sortArray[$orderby], SORT_ASC, $result);	
			}
			else{
				array_multisort($sortArray[$orderby], SORT_DESC, $result);
			}
			
			return $result;	
		}
		else{				
			return $result;
		}
	}
	
	function getListQuery(){		
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		$catid 		= $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		
		$filter_author = $app->getUserStateFromRequest($this->context.'.filter.author', 'filter_authorid','','string');
		$this->setState('filter.author', $filter_author,'string');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
		
		$key_search = $app->getUserStateFromRequest($this->context.'.filter.key_search', 'key_search','','string');
		$this->setState('filter.key_search', $key_search, 'string');
		
		$filter_sticky = $app->getUserStateFromRequest($this->context.'.filter.sticky', 'sticky','','string');
		$this->setState('filter.sticky', $filter_sticky, 'string');
		
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_author = "";
			$filter_state = "";
			$filter_search = "";
			$catid = "";
			$filter_missing = "";
			$key_search = "";
			$filter_sticky = JRequest::getVar("value", "", "get");			
			$this->setState('filter.author', "" ,'string');
			$this->setState('filter.sticky', $filter_sticky, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
			$this->setState('filter.missing', "", 'string');
			$this->setState('filter.key_search', "", 'string');
		}
			
		$where = " ki.keyword=k.title and ki.type='article'";
		
		if(intval($catid)>0){
			$where.= " and ki.type_id in (select id from #__content where catid=".intval($catid).") and ki.type='article'";
		}
		
		if($filter_author != "0" && $filter_author != ""){
			$where.= " and ki.type_id in (select id from #__content where created_by=".intval($filter_author).")";
		}
		
		if($filter_sticky != "" && $filter_sticky != "0"){
			$where.= " and k.sticky=1 ";
		}
		
		switch ($filter_missing){
			case "1":
				$where.= " and ki.type_id in (select id from #__content where attribs like '%\"page_title\":\"\"%')";
				break;
			case "2":
				$where.= " and ki.type_id in (select id from #__content where metakey='')";
				break;
			case "3":
				$where.= " and ki.type_id in (select id from #__content where metadesc='') ";
				break;
			case "4":
				$where.= " and ki.type_id in (select id from #__content where attribs like '%\"page_title\":\"\"%' or  metakey='' or metadesc='')";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.=" and ki.type_id in (select id from #__content where state=1) ";
				break;
			case "2":
				$where.=" and ki.type_id in (select id from #__content where state=0) ";
				break;
			case "3":
				$where.=" and ki.type_id in (select id from #__content where state=2) ";
				break;
			case "4":
				$where.=" and ki.type_id in (select id from #__content where state=-2) ";
				break;	
			default:
				break;
		}
		
		if($filter_search != ""){ 
			$where.=" AND ki.type_id in (select id from #__content where title like '%".addslashes($filter_search)."%' or metakey like '%".addslashes($filter_search)."%' or metadesc like '%".addslashes($filter_search)."%') ";
		}
		
		if($key_search != ""){ 
			$where.=" AND ki.keyword like '%".addslashes($key_search)."%'";
		}					
		
		$keyup_doun = JRequest::getVar("keyup_doun", "");
		//for values from default page calculated with ajax
		$jnow =& JFactory::getDate();
		$date = $jnow->toMySQL();
		if($keyup_doun != "") {
			$query->select('distinct *');
			$query->from('#__ijseo_keys k');
			$query->join('LEFT', '#__ijseo_keys_id ki on ki.keyword = k.title');
            if ($keyup_doun == 'down') {
                $up_down = 0;
                $kchange = "k.rchange !=  0";
            }  elseif ($keyup_doun == 'true') {
                $up_down = 1;
                $kchange = "k.rchange !=  0";                
            } else {
                $up_down = -1;
                $kchange = "k.rchange = 0 AND k.rank <> 0";
            }
			$query->where("
                ki.type = 'article' AND k.mode = '" . $up_down. "' AND " . $kchange . "
                AND ki.type_id IN 
                (
                    SELECT id FROM #__content 
                    WHERE state=1 AND (publish_down > '".$date."' OR publish_down='0000-00-00 00:00:00') 
                )");
			$query->group('k.title');			
		}
		else{			
			$query->select('distinct *');
			$query->from('#__ijseo_keys k, #__ijseo_keys_id ki');
			$query->where($where." 
                AND ki.type_id IN 
                (
                    SELECT id FROM #__content 
                    WHERE state=1 AND (publish_down > '".$date."' OR publish_down='0000-00-00 00:00:00') 
                )");
			$query->group('k.title');
		}
        
        $query->order("CASE k.rank WHEN 0 THEN 9999 ELSE k.rank END");
        
        //echo "<pre>";var_dump($query->__toString());die();
		
		return $query;		
	}
	
	function getListQuery2(){
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');
		$catid 		= $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		
		$filter_author = $app->getUserStateFromRequest($this->context.'.filter.author', 'filter_authorid','','string');
		$this->setState('filter.author', $filter_author,'string');
		
		$filter_missing = $app->getUserStateFromRequest($this->context.'.filter.missing', 'atype','','string');
		$this->setState('filter.missing', $filter_missing,'string');
		
		$filter_state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state','','string');
		$this->setState('filter.published', $filter_state,'string');
		
		$filter_search = $app->getUserStateFromRequest($this->context.'.filter.search', 'search','','string');
		$this->setState('filter.search', $filter_search, 'string');
		
		$key_search = $app->getUserStateFromRequest($this->context.'.filter.key_search', 'key_search','','string');
		$this->setState('filter.key_search', $key_search, 'string');
		
		$filter_sticky = $app->getUserStateFromRequest($this->context.'.filter.sticky', 'sticky','','string');
		$this->setState('filter.sticky', $filter_sticky, 'string');
		
		$filter = JRequest::getVar("filter", "", "get");
		if($filter != ""){
			$filter_author = "";
			$filter_state = "";
			$filter_search = "";
			$catid = "";
			$filter_missing = "";
			$key_search = "";
			$filter_sticky = JRequest::getVar("value", "", "get");			
			$this->setState('filter.author', "" ,'string');
			$this->setState('filter.sticky', $filter_sticky, 'string');
			$this->setState('filter.published', "" ,'string');
			$this->setState('filter.search', "", 'string');
			$this->setState('filter.missing', "", 'string');
			$this->setState('filter.key_search', "", 'string');
		}
			
		$where = " type='article' ";
		
		if(intval($catid)>0){
			$where.= " and joomla_id in (select id from #__content where catid=".intval($catid).") ";
		}
		
		if($filter_author != "0" && $filter_author != ""){
			$where.= " and joomla_id in (select id from #__content where created_by=".intval($filter_author).")";
		}
		
		if($filter_sticky != "" && $filter_sticky != "0"){
			$where.= " and sticky=1 ";
		}
		
		switch ($filter_missing){
			case "1":
				$where.= " and joomla_id in (select id from #__content where attribs like '%\"page_title\":\"\"%')";
				break;
			case "2":
				$where.= " and joomla_id in (select id from #__content where metakey='')";
				break;
			case "3":
				$where.= " and joomla_id in (select id from #__content where metadesc='') ";
				break;
			case "4":
				$where.= " and joomla_id in (select id from #__content where attribs like '%\"page_title\":\"\"%' or  metakey='' or metadesc='')";
				break;
			default:
				break;
		}
		
		switch ($filter_state){
			case "1":
				$where.=" and joomla_id in (select id from #__content where state=1) ";
				break;
			case "2":
				$where.=" and joomla_id in (select id from #__content where state=0) ";
				break;
			case "3":
				$where.=" and joomla_id in (select id from #__content where state=2) ";
				break;
			case "4":
				$where.=" and joomla_id in (select id from #__content where state=-2) ";
				break;	
			default:
				break;
		}
		
		if($filter_search != ""){ 
			$where.=" and joomla_id in (select id from #__content where title like '%".addslashes($filter_search)."%' or metakey like '%".addslashes($filter_search)."%' or metadesc like '%".addslashes($filter_search)."%') ";
		}
		
		if($key_search != ""){ 
			$where.=" and title like '%".addslashes($key_search)."%'";
		}					
		$jnow =& JFactory::getDate();
		$date = $jnow->toMySQL();
		
		$query->select('*');
		$query->from('#__ijseo_titlekeys');
		$query->where($where." and joomla_id in (select id from #__content where state=1 and (publish_down > '".$date."' OR publish_down='0000-00-00 00:00:00') )");
					
		return $query;		
	}
	
	function getAttribs($id){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('attribs');
		$query->from('#__content');
		$query->where("id=".$id);
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string);
		return $result;
	}
	
	function savepage(){
		$id = JRequest::getVar("id");
		$mtitle = JRequest::getVar("mtitle", "");
		$metakey = JRequest::getVar("metakey", "");
		$metadesc = JRequest::getVar("metadesc", "");
		$attribs = $this->getAttribs($id);
		$attribs->page_title = $mtitle;
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$attrib = json_encode($attribs);
		$attrib = str_replace("'", "\'", $attrib);
		$query->clear();
		$query->update('#__content');		
		$query->set("`metakey`='".addslashes($metakey)."', `metadesc`='".addslashes($metadesc)."', `attribs`='".$attrib."'");
		$query->where('id='.$id);
		
		$db->setQuery($query);
		if(!$db->query()){
			return false;
		}
		return true;
	}
	
	function getComponentParams(){
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();		
		$query->select('params');
		$query->from('#__ijseo_config');
		$db->setQuery($query);		
		$db->query();
		$result_string = $db->loadResult();
		$result = json_decode($result_string);
		return $result;
	}
	
	function sincronizeMetaTitles(){
		$keyword = array();	
		$titles = array();
		
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("attribs");
		$query->from("#__content");
		$db->setQuery($query);
		$db->query();
		$result = $db->loadAssocList();	
		foreach($result as $key=>$value){
			$temp = json_decode($value["attribs"]);
			if(trim($temp->page_title) != ""){		
				$keyword = array_merge($keyword, (array)$temp->page_title);
			}
		}
		
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$query->clear();
		$query->select("title");
		$query->from("#__ijseo_title");
		$db->setQuery($query);
		$db->query();		
		$result = $db->loadAssocList();			
		foreach($result as $key=>$value){			
			$titles = array_merge($titles, (array)$value["title"]);
		}
		
		$difference = array_diff($titles, $keyword);
		$sql = "delete from #__ijseo_title where title in ('".implode("','", $difference)."')";		
		$db->setQuery($sql);
		$db->query();
	}
	
	function getStickyUnsticky(){
		$params = $this->getComponentParams();
		$ids = JRequest::getVar("cid", array());		
		$db =& JFactory::getDBO();		
		$query = $db->getQuery(true);
		$value = "";
		$task = JRequest::getVar("task", "");
		if($task == "sticky"){
			$value = "1";
		}
		else{
			$value = "0";
		}
		foreach($ids as $key=>$id){
			$query->clear();
			if($params->ijseo_keysource == "1"){
				$query->update('#__ijseo_titlekeys');
			}
			else{
				$query->update('#__ijseo_keys');
			}
			$query->set("sticky=".$value);
			$query->where('id='.$id);			
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
		}
		return true;
	}
}

?>