<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class iJoomla_SeoModelRedirect extends JModelList{

	protected $_context = 'com_ijoomla_seo.redirect';
	private $total=0;

	function populateState(){
		// Initialize variables.
		$app = JFactory::getApplication('administrator');
		// Load the list state.
		$this->setState('list.start', $app->getUserStateFromRequest($this->_context . '.list.start', 'limitstart', 0, 'int'));
		$this->setState('list.limit', $app->getUserStateFromRequest($this->_context . '.list.limit', 'limit', $app->getCfg('list_limit', 25) , 'int'));
		$this->setState('selected', JRequest::getVar('cid', array()));
		$catid = $app->getUserStateFromRequest($this->context.'.filter.catid', 'filter_catid');
		$this->setState('filter.catid', $catid);
	}
	
	function getPagination(){
		$pagination=parent::getPagination();
		$pagination->total=$this->total;
		if($pagination->total%$pagination->limit>0)
			$nr_pages=intval($pagination->total/$pagination->limit)+1;
		else $nr_pages=intval($pagination->total/$pagination->limit);
		$pagination->set('pages.total',$nr_pages);
		$pagination->set('pages.stop',$nr_pages);
		return $pagination;
	}	
	
	function getItems(){
		$config = new JConfig();
		$app = JFactory::getApplication('administrator');
		$limistart = $app->getUserStateFromRequest($this->context.'.list.start', 'limitstart');
		$limit = $app->getUserStateFromRequest($this->context.'.list.limit', 'limit', $config->list_limit);
				
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->clear();
		$query = $this->getListQuery();		
		$db->setQuery($query);
		$db->query();
		$result	= $db->loadObjectList();		
		$this->total=count($result);
		
		$db->setQuery($query,$limistart,$limit);
		$db->query();
		$result	= $db->loadObjectList();
		return $result;
	}
	
	function getListQuery(){		
		$where = " 1=1 ";
		
		$cat_filter = JRequest::getVar("cat_filter", "0");
		if($cat_filter != "0"){
			$where .= " and ij.catid=".$cat_filter;
		}
		
		$search = JRequest::getVar("search", "");
		if($search != ""){
			$where .= " and ij.name like '%".addslashes($search)."%'";
		}
			
		$database	= JFactory::getDBO();
		$query		= $database->getQuery(true);
		$app 		= JFactory::getApplication('administrator');				
		$query->select('ij.*, ijc.name as cat_name');
		$query->from('#__ijseo as ij');
		$query->leftJoin('#__ijseo_redirect_category ijc on ij.catid=ijc.id');
		$query->where($where);	
		return $query;		
	}
	
	function resetHit(){
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$ids = JRequest::getVar("cid");
		foreach($ids as $key=>$value){
			$query->clear();
			$query->update('#__ijseo');
			$query->set("`hits`=0, last_hit_reset='".date("Y-m-d h:m:s")."'");
			$query->where('id='.$value);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
		}
		return true;
	}
	
	function remove(){
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$ids = JRequest::getVar("cid");
		foreach($ids as $key=>$value){
			$query->clear();
			$query->delete('#__ijseo');
			$query->where('id='.$value);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
		}
		return true;
	}
	
	function getAllCategories(){
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$app = JFactory::getApplication('administrator');				
		$query->select('id, name');
		$query->from('#__ijseo_redirect_category');
		$db->setQuery($query);
		$db->query();
		return $db->loadObjectList();
	}
}

?>