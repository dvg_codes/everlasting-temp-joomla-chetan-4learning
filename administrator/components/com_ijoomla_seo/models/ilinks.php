<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class iJoomla_SeoModelIlinks extends JModelList{

	protected $_context = 'com_ijoomla_seo.ilinks';
	private $total=0;

	function populateState(){
		$app = JFactory::getApplication('administrator');
		$this->setState('list.start', $app->getUserStateFromRequest($this->_context . '.list.start', 'limitstart', 0, 'int'));
		$this->setState('list.limit', $app->getUserStateFromRequest($this->_context . '.list.limit', 'limit', $app->getCfg('list_limit', 25) , 'int'));
		$this->setState('selected', JRequest::getVar('cid', array()));
	}
	
	function getPagination(){
		$pagination=parent::getPagination();
		$pagination->total=$this->total;
		if($pagination->total%$pagination->limit>0)
			$nr_pages=intval($pagination->total/$pagination->limit)+1;
		else $nr_pages=intval($pagination->total/$pagination->limit);
		$pagination->set('pages.total',$nr_pages);
		$pagination->set('pages.stop',$nr_pages);
		return $pagination;
	}	
	
	function getItems(){
		$config = new JConfig();
		$app = JFactory::getApplication('administrator');
		$limistart = $app->getUserStateFromRequest($this->context.'.list.start', 'limitstart');
		$limit = $app->getUserStateFromRequest($this->context.'.list.limit', 'limit', $config->list_limit);
				
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->clear();
		$query = $this->getListQuery();
		
		$db->setQuery($query);
		$db->query();
		$result	= $db->loadObjectList();		
		$this->total=count($result);
		
		$db->setQuery($query,$limistart,$limit);
		$db->query();
		$result	= $db->loadObjectList();
		return $result;
	}
	
	function getListQuery(){
		$where = " 1=1 ";
		
		$cat_filter = JRequest::getVar("cat_filter", "0");
		if($cat_filter != "0"){
			$where .= " and ij.catid=".$cat_filter;
		}
		
		$search = JRequest::getVar("search", "");
		if($search != ""){
			$where .= " and ij.name like '%".addslashes($search)."%'";
		}
			
		$database = JFactory::getDBO();
		$query = $database->getQuery(true);
		$app = JFactory::getApplication('administrator');				
		$query->select('ij.*, ijc.name as cat_name');
		$query->from('#__ijseo_ilinks as ij');
		$query->leftJoin('#__ijseo_ilinks_category ijc on ij.catid=ijc.id');
		$query->where($where);		
		return $query;		
	}
	
	function remove(){
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$ids = JRequest::getVar("cid");
		$query->clear();
		$query->delete('#__ijseo_ilinks');
		$query->where('id in('.implode(",",$ids).')');
		$db->setQuery($query);
		if(!$db->query()){
			return false;
		}		
		return true;
	}
	
	function getAllCategories(){
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$app = JFactory::getApplication('administrator');				
		$query->select('id, name');
		$query->from('#__ijseo_ilinks_category');
		$db->setQuery($query);
		$db->query();
		return $db->loadObjectList();
	}
	
	function publish(){
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$ids = JRequest::getVar("cid");
		foreach($ids as $key=>$value){
			$query->clear();
			$query->update('#__ijseo_ilinks');
			$query->set("`published`=1");
			$query->where('id='.$value);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
		}
		return true;
	}
	
	function unpublish(){		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$ids = JRequest::getVar("cid");		
		foreach($ids as $key=>$value){
			$query->clear();
			$query->update('#__ijseo_ilinks');
			$query->set("`published`=0");
			$query->where('id='.$value);
			$db->setQuery($query);
			if(!$db->query()){
				return false;
			}
		}
		return true;
	}
}

?>