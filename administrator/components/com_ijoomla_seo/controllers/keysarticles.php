<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');

class iJoomla_SeoControllerKeysarticles extends iJoomla_SeoController{
	
	function __construct() {	  
		parent::__construct();		
		// Register Extra tasks		
		$this->registerTask('', 'keysarticles');
		$this->registerTask('keysarticles', 'keysarticles');
		$this->registerTask('view_articles', 'viewArticles');
		$this->registerTask('edit_article', 'editArticle');
		$this->registerTask('savepage', 'savepage');
		$this->registerTask('sticky', 'sticky_unsticky');
		$this->registerTask('unsticky', 'sticky_unsticky');		
	}			
	
	function keysarticles(){
		JRequest::setVar( 'view', 'Keysarticles' );	
		parent::display();
	}		
	
	function viewArticles(){
		JHTML::_('behavior.modal');
		include_once(JPATH_ROOT.DS."administrator".DS."components".DS."com_ijoomla_seo".DS."helpers".DS."pages.php");
		$page = new Page();
		$page->createviewArticle();
	}
	
	function editArticle(){
		include_once(JPATH_ROOT.DS."administrator".DS."components".DS."com_ijoomla_seo".DS."helpers".DS."pages.php");
		$page = new Page();
		$page->createEditArticle();
	}
	
	function savepage(){
		$model = $this->getModel('pages');
		$res = $model->savepage();
		$link = JURI::base()."index.php?option=com_ijoomla_seo&controller=keysarticles&view_articles&dataOBJ=".addslashes(JRequest::getVar("metakey", ""));		
		if ($res === TRUE) {	
			$msg = JText::_("COM_IJOOMLA_SEO_EDT_PAGE_SAVED_SUCCESSFULLY");
			$this->setRedirect($link, $msg);
		} 
		elseif($res === FALSE) {
		 	$msg = JText::_("COM_IJOOMLA_SEO_EDT_PAGE_SAVED_UNSUCCESSFULLY");		
			$this->setRedirect($link, $msg, 'notice');
		}
	}
	
	function sticky_unsticky(){
		$model = $this->getModel('keysarticles');
		$result = $model->getStickyUnsticky();
		$link = "index.php?option=com_ijoomla_seo&controller=keysarticles";
		if($result === TRUE){
			$msg = JText::_("COM_IJOOMLA_SEO_STICKY_SUCCESSFULLY");
			$this->setRedirect($link, $msg);
		}
		else{
			$msg = JText::_("COM_IJOOMLA_SEO_STICKY_UNSUCCESSFULLY");
			$this->setRedirect($link, $msg, 'notice');
		}
	}
		
	function cancel(){
		$msg = JText::_('COM_IJOOMLA_SEO_OPERATION_CANCELED');
		$this->setRedirect('index.php?option=com_ijoomla_seo', $msg);
	}
}

?>