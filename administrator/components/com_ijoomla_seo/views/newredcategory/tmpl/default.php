<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');
JHtml::_('behavior.formvalidation');

include(JPATH_ROOT.DS."administrator".DS."components".DS."com_ijoomla_seo".DS."left.php");

$document =& JFactory::getDocument();
$document->addStyleSheet("components/com_ijoomla_seo/css/seostyle.css");
$document->addScript("components/com_ijoomla_seo/javascript/scripts.js");

$id = JRequest::getVar("id", "0", "get");
$name = "";
$published = "1";

if($id != "0"){
	$values = $this->getValues();
	$name = $values["0"]->name;
	$name = str_replace('"', "&quot;", $name);
	$published = $values["0"]->published;
}

?>

<script type="text/javascript">
	Joomla.submitbutton = function(pressbutton){
		var form = document.adminForm;		
		if (pressbutton == 'cancel') {
			submitform(pressbutton);
		}		
		else if(pressbutton == 'save' || pressbutton == 'apply') {
			if (form.name.value == ""){
				alert("<?php echo JText::_("COM_IJOOMLA_SEO_CAT_NAME"); ?>: <?php echo " ".JText::_("COM_IJOOMLA_SEO_IS_REQUIRED")."."; ?>");
			}			
			else{
				submitform( pressbutton );		
			}	
		}
		else{
			submitform( pressbutton );
		}
		
	}
</script>

<form action="index.php" method="post" name="adminForm">
	
	<table border="0" width="100%" class="adminlist">
		<tbody>
			<tr>
				<th align="center" colspan="2">
					<?php
						if($id != ""){
							echo JText::_("COM_IJOOMLA_SEO_REDIRECT_MANAGER")."&nbsp;:Edit&nbsp;[&nbsp;<small>".$name."</small>&nbsp;]";
						}
						else{
							echo JText::_("COM_IJOOMLA_SEO_REDIRECT_MANAGER")."&nbsp;:Edit&nbsp;[&nbsp;New&nbsp;]";
						}
					?>			
				</th>
			</tr>
	 		<tr>
				<td width="100px"><?php echo JText::_("COM_IJOOMLA_SEO_CAT_NAME"); ?>:<span style="color:#FF0000">*</span></td>
				<td><input type="text" name="name" value="<?php echo $name; ?>"></td>
			</tr>
			<tr>
				<td><?php echo JText::_("COM_IJOOMLA_SEO_CAT_PUBLISHED"); ?></td>
				<td>
					<input type="radio" class="inputbox" value="0" id="published0" name="published" <?php if($published=="0"){echo 'checked="checked"';} ?> >
					<label for="published0"><?php echo JText::_("JNO"); ?></label>
					<input type="radio" class="inputbox" value="1" id="published1" name="published" <?php if($published=="1"){echo 'checked="checked"';} ?> >
					<label for="published1"><?php echo JText::_("JYES"); ?></label>
				</td>
			</tr>
			<tr>
				</tr>
		</tbody>
		<tfoot>
				<td colspan="2">&nbsp;</td>
		</tfoot>			
	 </table>
	
	<input type="hidden" name="option" value="com_ijoomla_seo" />
	<input type="hidden" name="controller" value="newredcategory" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>

</td></tr></table>
<p><center><?php echo JText::_("COM_IJOOMLA_SEO_POWERED_BY") ; ?>&nbsp;<a href='http://www.ijoomla.com' target='_blank'><?php echo JText::_("COM_IJOOMLA_SEO_COMPONENT_TITLE"); ?></a></center></p>	