<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');

include(JPATH_ROOT.DS."administrator".DS."components".DS."com_ijoomla_seo".DS."left.php");

$document =& JFactory::getDocument();
$document->addStyleSheet("components/com_ijoomla_seo/css/seostyle.css");
$document->addScript("components/com_ijoomla_seo/javascript/scripts.js");

?>
<form action="index.php" method="post" name="adminForm">
	<?php
		$file = JPATH_SITE.DS."administrator".DS."language".DS."en-GB".DS."en-GB.com_ijoomla_seo.ini";
		if(is_writable($file)){
			echo "en-GB.com_ijoomla_seo.ini is <span style=\"color:green\">".JText::_("COM_IJOOMLA_SEO_FILE_WRITABLE")."</span>";
		}
		else{
			echo "en-GB.com_ijoomla_seo.ini is <span style=\"color:red\">".JText::_("COM_IJOOMLA_SEO_FILE_UNWRITABLE")."</span>";
		}		
	?>
	
	<table border="1" width="100%" class="adminform">
		<tbody>
			<tr>
				<th><?php echo $file; ?></th>
			</tr>
			<tr>
				<td>
					<textarea class="inputbox" name="filecontent" rows="25" cols="110" style="width: 100%;"><?php echo $this->language; ?></textarea>
				</td>
			</tr>
		</tbody>
	</table>
	
	<input type="hidden" name="option" value="com_ijoomla_seo" />
	<input type="hidden" name="controller" value="language" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHtml::_('form.token'); ?>
</form>

</td></tr></table>
<p><center><?php echo JText::_("COM_IJOOMLA_SEO_POWERED_BY") ; ?>&nbsp;<a href='http://www.ijoomla.com' target='_blank'><?php echo JText::_("COM_IJOOMLA_SEO_COMPONENT_TITLE"); ?></a></center></p>	