<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');

include(JPATH_ROOT.DS."administrator".DS."components".DS."com_ijoomla_seo".DS."left.php");

$document =& JFactory::getDocument();
$document->addStyleSheet("components/com_ijoomla_seo/css/seostyle.css");
$document->addScript("components/com_ijoomla_seo/javascript/scripts.js");

function removetrim(&$val){		
	$val = trim($val);	
	return ($val != "");	
}	

?>

<form action="index.php" method="post" name="adminForm">
	<table width="100%">
		<tr>
			<td>
				<span class="title_page"><?php echo JText::_("COM_IJOOMLA_SEO_SEO_PAGES"); ?></span>
			</td>
            <td align="right">
				<a class="modal seo_video" rel="{handler: 'iframe', size: {x: 740, y: 425}}" href="index.php?option=com_ijoomla_seo&controller=about&task=vimeo&id=13155651">                
                    <img src="<?php echo JURI::base(); ?>components/com_ijoomla_seo/images/icon_video.gif" class="video_img" />
                    <?php echo JText::_("COM_IJOOMLA_SEO_PAGES_VID"); ?>
                </a>
            </td>
		</tr>
		<tr>
			<td width="40%" class="description_zone">
				<?php echo JText::_("COM_IJOOMLA_SEO_PAGES_DESCRIPTION"); ?>
			</td>
			<td width="60%" class="filter_zone">
				<table width="100%">					
					<tr>
						<td align="right">							
							<select name="filter_catid" class="inputbox" onchange="this.form.submit()">
								<option value="">-- <?php echo JText::_('COM_IJOOMLA_SEO_SELECT_CATEGORY');?> --</option>
								<?php echo JHtml::_('select.options', JHtml::_('category.options', 'com_content'), 'value', 'text', $this->state->get('filter.catid'));?>
							</select>
							<?php
							echo $this->createCriterias();
							?>
						</td>
					</tr>
					<tr>
						<td align="right" colspan="2">
							<?php
                                 $search = JRequest::getVar("search", "");
                            ?>
                            <?php echo JText::_("COM_IJOOMLA_SEO_FILTER"); ?>: <input type="text" name="search" value="<?php echo $search;?>" class="text_area" onChange="document.adminForm.submit();" />&nbsp;<input type="button" onclick="this.form.submit();" value="<?php echo JText::_("COM_IJOOMLA_SEO_GO"); ?>" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br/>
	
	      
    <table class="adminlist" cellpadding="0" cellspacing="0" border="0" width="100%">            
        <tr class="row1">
            <th width="15">#</th>
            <th><?php echo JText::_("COM_IJOOMLA_SEO_ARTICLES"); ?></th>
            <th width="30px"><?php echo JText::_("COM_IJOOMLA_SEO_EDITMTAGS"); ?></th>			
            <th width="120px"><?php echo JText::_("COM_IJOOMLA_SEO_OUTGOING_LINKS"); ?></th>
            <th><?php echo JText::_("COM_IJOOMLA_SEO_KEYWORDS_PHRASES"); ?></th>
            <th width="140px">#<?php echo JText::_("COM_IJOOMLA_SEO_TIMES_ON_CONTENT"); ?></th>
        </tr>
        <?php
		$app		= JFactory::getApplication('administrator');
		$limistart	= $app->getUserStateFromRequest('com_ijoomla_seo.pages'.'.list.start', 'limitstart');
		$limit		= $app->getUserStateFromRequest('com_ijoomla_seo.pages'.'.list.limit', 'limit');
		$k = $limistart+1;
		$z = 0;
		for($i=0; $i<count($this->items); $i++){
			$item=$this->items[$i];
			$attribs = json_decode($item->attribs);							
		?>
        <tr class="<?php echo "row".$z; ?>">
        	<td>
            	<?php echo $k;?>
            </td>
            <td>
            	<a href="index.php?option=com_content&task=article.edit&id=<?php echo $item->id ?>" target="_blank">
                <?php echo $item->title ?></a>
            </td>
            <td  align="center">
			<?php					
				$obj = new stdClass();
				$obj->id = $item->id;
				$attribs = json_decode($item->attribs);
								
				$obj->metakey = trim($item->metakey);
				$obj->metadesc = trim($item->metadesc);
				if(isset($attribs->page_title)){
					$obj->titletag = trim($attribs->page_title);
				}
				$content = $item->introtext.$item->fulltext;
            ?>
            <a rel="{handler: 'iframe', size: {x: 600, y: 500}}" href="index.php?option=com_ijoomla_seo&amp;task=edit_page&amp;tmpl=component&amp;controller=pages&amp;id=<?php echo $item->id; ?>&amp;dataOBJ=<?php echo htmlspecialchars(json_encode($obj)); ?>" class="modal" ><?php echo JText::_("COM_IJOMLA_SEO_EDIT"); ?></a>
            </td>
            <td align="center">
			<?php                
                $sitehost = preg_quote($_SERVER['HTTP_HOST']);
                $regx = "/<a\s[^>]*href=\"(http:\/\/|www)(?!(http:\/\/)?$sitehost)(?!\.\.)(?!index.php)([^\"]+)\">([^<]+)<\/a>/i";
                preg_match_all($regx, $content, $links, PREG_PATTERN_ORDER);
                $oLinks = count($links[0]);															
                
                if($oLinks){
                    echo '<a  rel="{handler: \'iframe\', size: {x: 500, y: 250}}" href="index.php?option=com_ijoomla_seo&controller=pages&amp;tmpl=component&amp;task=outLinks&amp;data='.$item->id.'" class="modal">'.$oLinks.'</a>';
                }
                else{
                    echo $oLinks;
                }	
            ?>
            </td>
            <?php
				$keywords = array();
				$attribs = json_decode($item->attribs);				
				$kws1 = explode(",", $attribs->page_title);				
				$kws2 = explode(",", $item->metakey);					 	
				$kws = array_merge($kws1, $kws2);					
				$keywords = array_keys(array_count_values(array_filter($kws, "removetrim")));								
				$num = count($keywords);
				if($num){
					if($keywords[$num-1] == ''){
						unset($keywords[$num-1]);
						$num--;
					}	
				}
				$out = array( );					 
				if(!empty($keywords)){
					@preg_match_all("/\b$keywords[0]\b/iU", $content, $out, PREG_PATTERN_ORDER);
				}	
			?>
            <td>
				<?php if(!empty($keywords)) echo $keywords[0] ?>
            </td>
            <td align="center">
            	<?php if(!empty($out)) echo count($out[0]) ?>
            </td>
		</tr>            
		<?php
         // show the keyword on new line
         $nums = count($keywords);
         if($nums > 1){
		 	for ($j=1; $j < $nums; $j++) {
         ?>
                <tr class="<?php echo "row".$z; ?>">
                     <td colspan="4">&nbsp;</td>
                     <td><?php echo $keywords[$j]?></td>
                     <td align="center">
                        <?php 
							preg_match_all("/\b$keywords[$j]\b/iU", $content, $out, PREG_PATTERN_ORDER);
                           	echo count($out[0]);
                        ?>
                     </td>
                </tr>
            <?php
            }//for
         }//if
		$k++;
		$z = 1 - $z;
        }
		?>      
		<tfoot>
		<tr>
			<td colspan="6">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>
		</tfoot>
	</table>
	<input type="hidden" name="option" value="com_ijoomla_seo" />
	<input type="hidden" name="controller" value="pages" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHtml::_('form.token'); ?>
</form>

</td></tr></table>
<p><center><?php echo JText::_("COM_IJOOMLA_SEO_POWERED_BY") ; ?>&nbsp;<a href='http://www.ijoomla.com' target='_blank'><?php echo JText::_("COM_IJOOMLA_SEO_COMPONENT_TITLE"); ?></a></center></p>	