<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');
JHtml::_('behavior.formvalidation');

include(JPATH_ROOT.DS."administrator".DS."components".DS."com_ijoomla_seo".DS."left.php");

$document =& JFactory::getDocument();
$document->addStyleSheet("components/com_ijoomla_seo/css/seostyle.css");
$document->addScript("components/com_ijoomla_seo/javascript/scripts.js");

$id = JRequest::getVar("id", "0", "get");
$name = "";
$catid = "";
$links_to = "";
$rel_nofollow = "1";
$target = "";
$link_text = "";
$image = "";

if($id != "0"){
	$values = $this->getValues();	
	$name = $values["0"]->name;
	$name = str_replace('"', "&quot;", $name);
	$catid = $values["0"]->catid;
	$links_to = $values["0"]->links_to;
	$rel_nofollow = $values["0"]->rel_nofollow;
	$target = $values["0"]->target;
	$link_text = $values["0"]->link_text;
	$image = $values["0"]->image; 	
}

?>

<script type="text/javascript">
	function changeDisplayImage() {            
		if(document.adminForm.image.value != ''){
			document.adminForm.imagelib.src='../images/ijseo_redirects/' + document.adminForm.image.value;
		} 
		else{
			document.adminForm.imagelib.src='../images/blank.png';
		}
	}
	
	Joomla.submitbutton = function(pressbutton){
		var form = document.adminForm;		
		if (pressbutton == 'cancel') {
			submitform(pressbutton);
		}		
		else if(pressbutton == 'save' || pressbutton == 'apply') {
			if (form.name.value == ""){
				alert("<?php  echo JText::_("COM_IJOOMLA_SEO_NAME"); ?>: <?php echo " ".JText::_("COM_IJOOMLA_SEO_IS_REQUIRED")."."; ?>");
			} 
			else if (form.links_to.value == ""){
				alert("<?php echo JText::_("COM_IJOOMLA_SEO_LINKS_TO"); ?>: <?php echo " ".JText::_("COM_IJOOMLA_SEO_IS_REQUIRED")."."; ?>");
			}
			else{
				submitform( pressbutton );		
			}	
		}
		else{
			submitform( pressbutton );
		}
		
	}
</script>

<form action="index.php" method="post" name="adminForm">
	<?php
		if($id == "0"){
			echo "<h2>".JText::_("COM_IJOOMLA_SEO_REDIRCET_NEW")."</h2>";
		}
		else{
			echo "<h2>".JText::_("COM_IJOOMLA_SEO_REDIRCET_EDIT")."</h2>";
		}
		echo "<br/>";
	?>
	<table border="0" width="100%" class="adminlist">
		<tbody>
			<tr>
				<td width="120"><?php  echo JText::_("COM_IJOOMLA_SEO_NAME"); ?>:<span style="color:#FF0000;">*</span></td>
				<td>
					<input name="name" type="text" class="inputbox" id="name" value="<?php echo $name; ?>" size="50" maxlength="50" />
					<?php 
                        echo JHTML::tooltip(
                            JText::_("COM_IJOOMLA_SEO_TOOLTIP_EDIT_NAME"), 
                            JText::_("COM_IJOOMLA_SEO_SEO_REDIRECT"), 
                            JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                        ); 
                    ?>
				</td>
			</tr>
			<tr>
				<td><?php echo JText::_("COM_IJOOMLA_SEO_CATEGORY"); ?>:</td>
				<td><?php echo $this->getAllCategories($catid); ?></td>
			</tr>
			<tr>
				<td><?php echo JText::_("COM_IJOOMLA_SEO_LINKS_TO"); ?>:<span style="color:#FF0000;">*</span></td>
				<td>
					<input name="links_to" type="text" class="inputbox" id="links_to" value="<?php echo $links_to; ?>" size="50" />
					<?php 
                        echo JHTML::tooltip(
                            JText::_("COM_IJOOMLA_SEO_SEO_TOOLTIP_EDIT_LINKS_TO"), 
                            JText::_("COM_IJOOMLA_SEO_SEO_REDIRECT"),
                            JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                        ); 
                    ?>
				</td>
			</tr>
			<tr>
				<td><?php echo JText::_("COM_IJOOMLA_SEO_LINK_REL_NO_FOLLOW"); ?>?<span style="color:#FF0000;">*</span></td>
				<td>
					<?php
						$categories = array();
						$categories[] = JHTML::_('select.option', "1", JText::_("JYES"), 'id', 'name');
						$categories[] = JHTML::_('select.option', "0", JText::_("JNO"), 'id', 'name');
						echo JHTML::_('select.genericlist', $categories, "rel_nofollow", "", 'id', 'name', $rel_nofollow);
						echo JHTML::tooltip(
                            JText::_("COM_IJOOMLA_SEO_TOOLTIP_LINK_REL_NO_FOLLOW"), 
                            JText::_("COM_IJOOMLA_SEO_SEO_REDIRECT"),
                            JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                        );
					?>
				</td>
			</tr>
			<tr>
				<td><?php echo JText::_("COM_IJOOMLA_SEO_TARGET"); ?>:<span style="color:#FF0000;">*</span></td>
				<td>
					<?php
						$arr_targets = array();
						$arr_targets[] = JHTML::_('select.option', '_blank', JText::_("COM_IJOOMLA_SEO_TARGET_BLANK"), 'id', 'name');
						$arr_targets[] = JHTML::_('select.option', '_self', JText::_("COM_IJOOMLA_SEO_TARGET_SELF"), 'id', 'name');
						$arr_targets[] = JHTML::_('select.option', '_parent', JText::_("COM_IJOOMLA_SEO_TARGET_PARENT"), 'id', 'name');
						$arr_targets[] = JHTML::_('select.option', '_top', JText::_("COM_IJOOMLA_SEO_TARGET_TOP"), 'id', 'name');
						echo JHTML::_('select.genericlist', $arr_targets, 'target', '', 'id', 'name', $target);
						echo JHTML::tooltip(
                            JText::_("COM_IJOOMLA_SEO_TOOLTIP_TARGET"), 
                            JText::_("COM_IJOOMLA_SEO_SEO_REDIRECT"),
                            JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                        );
					?>
				</td>
			</tr>
			<tr>
				<td><?php echo JText::_("COM_IJOOMLA_SEO_LINK_TEXT"); ?>:</td>
				<td>
					<input name="link_text" type="text" class="inputbox" id="link_text" value="<?php echo $link_text; ?>" size="50" />
					<?php 
                        echo JHTML::tooltip(
                            str_replace("''", '"', JText::_("COM_IJOOMLA_SEO_TOOLTIP_LINK_TEXT")), 
                            JText::_("COM_IJOOMLA_SEO_SEO_REDIRECT"),
                            JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                        ); ?>
				</td>
			</tr>
			<tr>
				<td><?php echo JText::_("COM_IJOOMLA_SEO_IMAGE"); ?>:</td>
				<td>
					<?php
						$javascript = 'onchange="changeDisplayImage();"';
						$directory = '/images/ijseo_redirects';
						echo JHTML::_('list.images',  'image', $image, $javascript, $directory, "bmp|gif|jpg|png|swf");
						echo JHTML::tooltip(
                            str_replace("''", '"', JText::_("COM_IJOOMLA_SEO_TOOLTIP_IMAGE")), 
                            JText::_("COM_IJOOMLA_SEO_SEO_REDIRECT"),
                            JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                        );
					?>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<?php
				
					if ($image != ""){						
						if (eregi("swf", $redirect_row->image)) {
							?>
							<img src="../images/blank.png" name="imagelib">
							<?php
						} elseif (eregi("gif|jpg|png", $image)) {
							?>
							<img src="../images/ijseo_redirects/<?php echo $image; ?>" name="imagelib" id="imagelib"/>
							<?php
						} else {
							?>
							<img src="images/blank.png" name="imagelib" id="imagelib"/>
							<?php
						}
					}
					else{
						echo '<img src="'.JUri::base()."/components/com_ijoomla_seo/images/blank.png".'" name="imagelib" id="imagelib"/>';
					}	
					?>
				</td>
			</tr>	 		
		</tbody>					
	 </table>
	
	<input type="hidden" name="option" value="com_ijoomla_seo" />
	<input type="hidden" name="controller" value="newredirect" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>

</td></tr></table>
<p><center><?php echo JText::_("COM_IJOOMLA_SEO_POWERED_BY") ; ?>&nbsp;<a href='http://www.ijoomla.com' target='_blank'><?php echo JText::_("COM_IJOOMLA_SEO_COMPONENT_TITLE"); ?></a></center></p>	