<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');
JHtml::_('behavior.formvalidation');

include(JPATH_ROOT.DS."administrator".DS."components".DS."com_ijoomla_seo".DS."left.php");

$document =& JFactory::getDocument();
$document->addStyleSheet("components/com_ijoomla_seo/css/seostyle.css");
$document->addScript("components/com_ijoomla_seo/javascript/scripts.js");
$document->addScript("components/com_ijoomla_seo/javascript/newilinks.js");

$id = JRequest::getVar("id", "0", "get");
$ilinkType = "";

$name = "";
$t_article = "block";
$t_menu = "none";
$t_menu_2 = "none";
$t_url = "none";
$menu_type = "";
$location1 = "";
$location2 = "http://";
$location = "";
$published = "1";
$type = "";
$target = "";
$articleId = "";
$loc_id = "";
$catid = "";
$other_phrases = "0";
$title = "";
$include_in = 1;
$activate_for_some = 0;

if($id != "0"){
	$values = $this->getValues();
	$name = $values["0"]->name;
    $title = $values["0"]->title;
	$name = str_replace('"', "&quot;", $name);
	$published = $values["0"]->published;
	$type = $values["0"]->type;
	$location = $values["0"]->location;	
	$target = $values["0"]->target;	
	$articleId = $values["0"]->articleId;	
	$location2 = $values["0"]->location2;	
	$menu_type = $values["0"]->menu_type;
	$loc_id = $values["0"]->loc_id;	
	$location1 = $values["0"]->location1;
	$catid = $values["0"]->catid;
	$other_phrases = $values["0"]->other_phrases;
    $include_in = $values["0"]->include_in;
    $activate_for_some = $values["0"]->activate_for_some;
	
	if($type == "1"){
		$t_article = "block";
		$t_menu = "none";
		$t_menu_2 = "none";
		$t_url = "none";
	}
	elseif($type == "2"){
		$t_article = "none";
		$t_menu = "block";
		$t_menu_2 = "block";
		$t_url = "none";
	}
	elseif($type == "3"){
		$t_article = "none";
		$t_menu = "none";
		$t_menu_2 = "none";
		$t_url = "block";
	}
}

if (is_array($values) && is_array($values["0"]->articles) && (count($values["0"]->articles)) ) {
    $has_assigned_articles = true;
} else {
    $has_assigned_articles = false;
}

//echo "<pre>"; var_dump($include_in); die();

?>

<script type="text/javascript">
	Joomla.submitbutton = function(pressbutton){
		var form = document.adminForm;		
		if (pressbutton == 'cancel') {
			submitform(pressbutton);
		}		
		else if(pressbutton == 'save' || pressbutton == 'apply') {
			if (form.name.value == "") {
				alert("<?php echo JText::_("COM_IJOOMLA_SEO_WORD_PHRASE"); ?> <?php echo " ".JText::_("COM_IJOOMLA_SEO_IS_REQUIRED")."."; ?>");
			}
			else if (form.catid.value == "0") {
				alert("<?php echo JText::_("COM_IJOOMLA_SEO_CATEGORY"); ?> <?php echo " ".JText::_("COM_IJOOMLA_SEO_IS_REQUIRED")."."; ?>");
			}			
			else if (!form.title.value) {
				alert("<?php echo JText::_("COM_IJOOMLA_TITLE_TOOLTIP"); ?> <?php echo " ".JText::_("COM_IJOOMLA_SEO_IS_REQUIRED")."."; ?>");
			}			
			else{
				submitform( pressbutton );		
			}	
		}
		else{
			submitform( pressbutton );
		}
		
	}
    setTimeout(function() {
        changeMenu();
    }, 1);
</script>

<form action="index.php" method="post" name="adminForm">

	<table width="100%" class="adminlist" border="0" cellpadding="2" cellspacing="2">
		<tr><th colspan="2"><?php  echo JText::_("COM_IJOOMLA_SEO_AUTOMATIC_KEYLINK"); ?> </th></tr>
		<tr>
			<td width="22%"><?php echo JText::_("COM_IJOOMLA_SEO_WORD_PHRASE"); ?>:<span style="color:#FF0000;">*</span></td>
			<td>
                <input name="name" type="text" class="inputbox" id="name" value="<?php echo $name; ?>" size="50" />
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_KEYPHRASE_TIP"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    );
                ?>    
            </td>
		  </tr>
		  <tr>
			<td><?php echo JText::_("COM_IJOOMLA_SEO_CATEGORY"); ?>:<span style="color:#FF0000;">*</span></td>
			<td>
                <?php echo $this->selectAllCategories($catid); ?>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_CATEGORY_TIP"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    );
                ?>
            </td>
		  </tr>
          <tr>
            <td><?php echo JText::_('COM_IJOOMLA_TITLE_TOOLTIP'); ?>:<span style="color:#FF0000;">*</span></td>
            <td>
                <input name="title" type="text" class="inputbox" id="title" value="<?php echo $title; ?>" size="50" />
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_TITLE_TOOL_TIP"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    );
                ?>
            </td>
          </tr>
		  <tr>
			<td width="22%"><?php echo JText::_("COM_IJOOMLA_SEO_WORD_PHRASE_LINK_TO"); ?></td>
			<td>				
				<table cellpadding="0" cellspacing="0" border="0" >
					<tr>
						<td valign="top">
							<div style="float:left;">
							<?php
								echo $this->getType($type); 								
							?>
							</div>
						</td>
						<td align="left">
							<div id="t_article" style="display:<?php echo $t_article; ?>;float:left;">
								<?php
									echo $this->displayArticle($articleId, $type, trim($location));
								?>
							</div>
							<div id="t_menu" style="display:<?php echo $t_menu; ?>;float:left;">
								<?php
									echo $this->getAllMenu($menu_type);
								?>
							</div>
							<div id="t_menu_2" style="display:<?php echo $t_menu_2; ?>;float:left;">
								<?php
									if(isset($menu_type) && trim($menu_type) != ""){
										echo $this->getAllMenuItems($menu_type, $loc_id);
									}
								?>
							</div>
							<div id="t_url" style="display:<?php echo $t_url; ?>;float:left;">
								<?php
									echo $this->displayUrl($location2);
								?>
							</div>&nbsp;
                            <?php 
                                echo JHTML::tooltip(
                                    "",
                                    JText::_("COM_IJOOMLA_SEO_LINKTO_TIP"), 
                                    JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                                );
                            ?>
						</td>											
					</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td width="22%"><?php echo JText::_("COM_IJOOMLA_SEO_OPEN_LINK"); ?></td>
			<td>
				 <?php
					  echo $this->openLink($target);
				 ?>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_TARGET_TIP"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    );
                ?>                 
			</td>
		</tr>
		<tr>
			<td width="22%"><?php echo JText::_("COM_IJOOMLA_SEO_PUBLISHED"); ?></td>
			<td>
				 <?php
					  echo $this->publishedOptions($published);
				 ?>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_PUBLISH_TIP"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    );
                ?>
                 
			</td>
		 </tr>
		 <tr>
			<td width="22%"><?php echo JText::_("COM_IJOOMLA_SEO_OTHER_PHRASES"); ?></td>
			<td>
				 <?php
					  echo $this->otherPhrases($other_phrases);
				 ?>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_OTHER_PHRASES_TIP"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    );
                ?>
                 
			</td>
		 </tr>
         <tr>
            <td valign="top"><?php echo JText::_('COM_IJOOMLA_SEO_INCLUDE_IN_ARTICLES'); ?></td>
            <td>
                <input type="radio" name="include_in" value="0" <?php if ($include_in == '0') { echo 'checked="checked"';  } ?> />
                <label>
                    <?php echo JText::_('COM_IJOOMLA_SEO_ALL_ARTICLES'); ?>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_INCLUDE_IN_ARTICLES_TIP"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    );
                ?>                
                </label>
                <br />
                <input type="radio" name="include_in" value="1" <?php if ($include_in == '1') { echo 'checked="checked"';  } ?> />
                <label><?php echo JText::_('COM_IJOOMLA_SEO_SELECTED_ARTICLES'); ?></label>
                <br /><br />
                
                <div id="contains_selects">
                <?php 
                    if (!$has_assigned_articles) {
                        $document->addScriptDeclaration('window.addEvent("domready", Newilinks.clickAddMore);');
                    } else {
                        $js_output = '';
                        foreach ($values["0"]->articles as $element) {
                            $js_output .= "Newilinks.clickAddMore({
                                'id': '" . $element->id . "',
                                'title': '" . $element->title . "'
                            });";
                        }
                        $document->addScriptDeclaration('window.addEvent("domready", function() {
                            ' . $js_output . '
                        });');
                    }
                ?>
                </div>
                <a href="#" id="addmore_seo"><?php echo JText::_('COM_IJOOMLA_SEO_ADD_MORE'); ?></a>
                <input type="hidden" id="last_clicked" value="" />                
            </td>
         </tr>
         <!--
         <tr>
            <td valign="top">
                <?php echo JText::_('COM_IJOOMLA_SEO_ACTIVATE_LINK'); ?>
            </td>
            <td>
                <input type="radio" name="activate_for_some" value="0" 
                    <?php if ($activate_for_some == 0) { echo 'checked="checked"'; } ?>
                />
                <label><?php echo JText::_('COM_IJOOMLA_SEO_ON_EVERY_ACTIVATE'); ?></label>
                <br />
                <input type="radio" name="activate_for_some" value="1" 
                    <?php if ($activate_for_some == 1) { echo 'checked="checked"'; } ?>                
                />
                <label><?php echo JText::_('COM_IJOOMLA_SEO_ON_FIRST_MIDDLE_LAST'); ?></label>
            </td>
         </tr>
         -->
	</table>
	
	<input type="hidden" name="option" value="com_ijoomla_seo" />
	<input type="hidden" name="controller" value="newilinks" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>

</td></tr></table>
<p><center><?php echo JText::_("COM_IJOOMLA_SEO_POWERED_BY") ; ?>&nbsp;<a href='http://www.ijoomla.com' target='_blank'><?php echo JText::_("COM_IJOOMLA_SEO_COMPONENT_TITLE"); ?></a></center></p>	