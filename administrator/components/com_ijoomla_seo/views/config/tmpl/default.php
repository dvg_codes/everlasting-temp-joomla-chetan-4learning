<?php
/**
* @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
* @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html) 
* @author  iJoomla.com webmaster@ijoomla.com
* @url   http://www.ijoomla.com/licensing/
* the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  
* are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
* More info at http://www.ijoomla.com/licensing/
*/

defined('_JEXEC') or die('Restricted Access');
jimport('joomla.html.pane');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
require_once(JPATH_ROOT.DS."administrator".DS."components".DS."com_ijoomla_seo".DS."helpers".DS."reader.php");
include(JPATH_ROOT.DS."administrator".DS."components".DS."com_ijoomla_seo".DS."left.php");

$document =& JFactory::getDocument();
$document->addStyleSheet("components/com_ijoomla_seo/css/seostyle.css");
$document->addScript("components/com_ijoomla_seo/javascript/stats.js");

$params = $this->params;

$pane =& JPane::getInstance('tabs', array('startOffset'=>$startOffset));

?>
<span class="title_page"><?php echo JText::_("COM_IJOOMLA_SEO_SETTINGS"); ?></span>
<div class="description_zone"><?php echo JText::_("COM_IJOOMLA_SEO_SETTINGS_DESCRIPTION"); ?></div>
<form name = 'adminForm' method = "post" action = ''>
<?php 
    echo $pane->startPane("SEO-pane");
    echo $pane->startPanel( JText::_('COM_IJOOMLA_SEO_GENERAL'), "general-settings" );
?>

<table align="center" width="100%" cellspacing="5" cellpadding="5">
    <tr>
        <th width="15%"></th><th></th><th width="45%"></th>
    </tr>
    <tr>
        <td colspan="3" class="stitle">
            <table width="100%">
                <tr>
                    <td>								
                        <?php echo JText::_("COM_IJOOMLA_SEO_ADD_ALT_AUTO"); ?>
                    </td>
                    <td align="right">                    
                        <a class="modal seo_video" rel="{handler: 'iframe', size: {x: 740, y: 425}}" href="index.php?option=com_ijoomla_seo&controller=about&task=vimeo&id=13155423">
                            <img src="<?php echo JURI::base(); ?>components/com_ijoomla_seo/images/icon_video.gif" class="video_img" />
                            <?php echo JText::_("COM_IJOOMLA_SEO_ADD_ALT_AUTO_VIDEO"); ?>                  
                        </a>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2"><?php echo JText::_("COM_IJOOMLA_SEO_USE"); ?>&nbsp;
            <select name="Image_what">
                <option value="up to" <?php if(isset($params->ijseo_Image_what) && $params->ijseo_Image_what=="up to") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_UPTO"); ?></option>
                <option value="only" <?php if(isset($params->ijseo_Image_what)&& $params->ijseo_Image_what=="only") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_ONLY"); ?></option>
            </select>
            <select name="Image_number">
                <option value="1" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="1") echo "selected"; ?>>1'st</option>
                <option value="2" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="2") echo "selected"; ?>>2'nd</option>
                <option value="3" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="3") echo "selected"; ?>>3'rd</option>
                <option value="4" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="4") echo "selected"; ?>>4'th</option>
                <option value="5" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="5") echo "selected"; ?>>5'th</option>
                <option value="6" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="6") echo "selected"; ?>>6'th</option>
                <option value="7" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="7") echo "selected"; ?>>7'th</option>
                <option value="8" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="8") echo "selected"; ?>>8'th</option>
                <option value="9" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="9") echo "selected"; ?>>9'th</option>
                <option value="10" <?php if(isset($params->ijseo_Image_number) && $params->ijseo_Image_number=="10") echo "selected"; ?>>10'th</option>
            </select>
            <select name="Image_where">
                <option value="keyword" <?php if(isset($params->ijseo_Image_where) && $params->ijseo_Image_where=="keyword") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_KEYWORD"); ?></option>
                <option value="phrase" <?php if(isset($params->ijseo_Image_where) && $params->ijseo_Image_where=="phrase") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_PHRASE"); ?></option>
            </select>
            &nbsp; <?php echo JText::_("COM_IJOOMLA_SEO_AS"); ?> &#39;alt&#39; <?php echo JText::_("COM_IJOOMLA_SEO_TAG"); ?> &nbsp;
            <select name="Image_when">
                <option value="Always" <?php 
                    if (isset($params->ijseo_Image_when) && $params->ijseo_Image_when=="Always") { 
                        echo 'selected="selected"';
                    } 
                ?>><?php echo JText::_("COM_IJOOMLA_SEO_ALWAYS"); ?></option>
                <option value="NotSpecified" <?php 
                    if (isset($params->ijseo_Image_when) && $params->ijseo_Image_when=="NotSpecified") {
                        echo 'selected="selected"'; 
                    }
                ?>><?php echo JText::_("COM_IJOOMLA_SEO_ONLYWHEN"); ?></option>
                <option value="Never" <?php 
                    if (isset($params->ijseo_Image_when) && $params->ijseo_Image_when=="Never") {
                        echo 'selected="selected"';
                    }
                ?>><?php echo JText::_("COM_IJOOMLA_SEO_NEVER"); ?></option>
            </select>
            <?php 
                echo JHTML::tooltip(
                    '', 
                    JText::_("COM_IJOOMLA_SEO_THISWILLUSEAKWONIMAGE"), 
                    JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                ); 
            ?>            
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
            <td class="stitle" colspan="3">
            <table width="100%"  cellpadding="2" cellspacing="2">
                <tr>
                    <td align="left">								
                        <?php 
                            echo JText::_('COM_IJOOMLA_SEO_STYLIZE') . ' ' . JText::_("COM_IJOOMLA_SEO_KEYWORDS"); 
                        ?>
                    </td>
                    <td align="right">
                        <a class="modal seo_video" rel="{handler: 'iframe', size: {x: 740, y: 425}}" href="index.php?option=com_ijoomla_seo&controller=about&task=vimeo&id=13155772">                            
                            <img src="<?php echo JURI::base(); ?>components/com_ijoomla_seo/images/icon_video.gif" class="video_img" />
                            <?php echo JText::_("COM_IJOOMLA_SEO_STYLIZE_KEYWORDS_VIDEO"); ?>
                        </a>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </tr>
        <tr>
            <td width="20%"><?php echo JText::_("COM_IJOOMLA_SEO_WRAP_KEYWORDS_PHRASES_WITH"); ?>:</td>
            <td>
                 <select name="wrap_key" id="wrap_key">
                    <option value="nowrap" <?php if($params->ijseo_wrap_key=="nowrap") echo "selected"; ?>>
                        <?php echo JText::_('COM_IJOOMLA_SEO_DO_NOT_WRAP'); ?>
                    </option>
                    <option value="strong" <?php if($params->ijseo_wrap_key=="strong") echo "selected"; ?>>&lt;strong&gt; keyword / phrase &lt;/strong&gt;</option>
                    <option value="b" <?php if($params->ijseo_wrap_key=="b") echo "selected"; ?>>&lt;b&gt; keyword / phrase &lt;/b&gt;</option>
                    <option value="u" <?php if($params->ijseo_wrap_key=="u") echo "selected"; ?>>&lt;u&gt; keyword / phrase &lt;/u&gt;</option>
                 </select>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_WRAP_KEY"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    )
                ?>
            </td>
            <td>&nbsp;</td>
        </tr>

        <!-- Wrap partial words -->
        <tr>
            <td><?php echo JText::_("COM_IJOOMLA_SEO_WRAP_PARTIAL_WORDS"); ?>:</td>
            <td>
                 <select name="wrap_partial" id="wrap_partial">
                    <option value="1" <?php if(isset($params->ijseo_wrap_partial) && $params->ijseo_wrap_partial=="1"){ echo "selected";} ?>><?php echo JText::_("JYES"); ?></option>
                    <option value="0" <?php if(isset($params->ijseo_wrap_partial) && $params->ijseo_wrap_partial=="0"){ echo "selected";} ?>><?php echo JText::_("JNO"); ?></option>
                 </select>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_WRAP_INFO"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    )
                ?>
            </td>
            <td>&nbsp;</td>
        </tr>
    <tr>
        <td class="stitle" colspan="3">
            <table width="100%">
                <tr>
                    <td>								
                        <?php echo JText::_("COM_IJOOMLA_SEO_REPLACE_JOMCLASSES"); ?>
                    </td>
                    <td align="right">
                        <a class="modal seo_video" rel="{handler: 'iframe', size: {x: 740, y: 425}}" href="index.php?option=com_ijoomla_seo&controller=about&task=vimeo&id=13155680">
                            <img src="<?php echo JURI::base(); ?>components/com_ijoomla_seo/images/icon_video.gif" class="video_img" />                            
                            <?php echo JText::_("COM_IJOOMLA_SEO_REPLACE_JOMCLASSES_VIDEO"); ?>
                        </a>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="5"><?php echo JText::_("COM_IJOOMLA_SEO_REPLACE"); ?></td>
        <td align="left">
            <input type="text" name="Replace1" id="Replace1" value="<?php echo $params->ijseo_Replace1; ?>" />&nbsp;<?php echo JText::_("COM_IJOOMLA_SEO_WITH"); ?>&nbsp;
            <select name="Replace1_with">
                <option value="H1" <?php if($params->ijseo_Replace1_with == "H1") echo "selected"; ?>>H1</option>
                <option value="H2" <?php if($params->ijseo_Replace1_with == "H2") echo "selected"; ?>>H2</option>
                <option value="H3" <?php if($params->ijseo_Replace1_with == "H3") echo "selected"; ?>>H3</option>
                <option value="H4" <?php if($params->ijseo_Replace1_with == "H4") echo "selected"; ?>>H4</option>
                <option value="H5" <?php if($params->ijseo_Replace1_with == "H5") echo "selected"; ?>>H5</option>
                <option value="H6" <?php if($params->ijseo_Replace1_with == "H6") echo "selected"; ?>>H6</option>
                <option value="noreplace" <?php if($params->ijseo_Replace1_with=="noreplace") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_DONT_REPLACE"); ?></option>
            </select>
            <?php 
                echo JHTML::tooltip(
                    "",
                    JText::_("COM_IJOOMLA_SEO_THISWILLREPLACESPECCLASS"), 
                    JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                ); 
            ?>        
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="5"><?php echo JText::_("COM_IJOOMLA_SEO_REPLACE"); ?></td>
            <td>
                <input type="text" name="Replace2" id="Replace2" value="<?php echo $params->ijseo_Replace2; ?>" />&nbsp;with&nbsp;
                <select name="Replace2_with">
                    <option value="H1" <?php if($params->ijseo_Replace2_with == "H1") echo "selected"; ?>>H1</option>
                    <option value="H2" <?php if($params->ijseo_Replace2_with == "H2") echo "selected"; ?>>H2</option>
                    <option value="H3" <?php if($params->ijseo_Replace2_with == "H3") echo "selected"; ?>>H3</option>
                    <option value="H4" <?php if($params->ijseo_Replace2_with == "H4") echo "selected"; ?>>H4</option>
                    <option value="H5" <?php if($params->ijseo_Replace2_with == "H5") echo "selected"; ?>>H5</option>
                    <option value="H6" <?php if($params->ijseo_Replace2_with == "H6") echo "selected"; ?>>H6</option>
                    <option value="noreplace" <?php if($params->ijseo_Replace2_with == "noreplace") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_DONT_REPLACE"); ?></option>
                </select>
            <?php 
                echo JHTML::tooltip(
                    "",
                    JText::_("COM_IJOOMLA_SEO_THISWILLREPLACESPECCLASS"), 
                    JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                ); 
            ?>        
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="5"><?php echo JText::_("COM_IJOOMLA_SEO_REPLACE"); ?></td>
            <td>
                 <input type="text" name="Replace3" id="Replace3" value="<?php echo $params->ijseo_Replace3; ?>" />&nbsp;with&nbsp;
                 <select name="Replace3_with">
                    <option value="H1" <?php if($params->ijseo_Replace3_with == "H1") echo "selected"; ?>>H1</option>
                    <option value="H2" <?php if($params->ijseo_Replace3_with == "H2") echo "selected"; ?>>H2</option>
                    <option value="H3" <?php if($params->ijseo_Replace3_with == "H3") echo "selected"; ?>>H3</option>
                    <option value="H4" <?php if($params->ijseo_Replace3_with == "H4") echo "selected"; ?>>H4</option>
                    <option value="H5" <?php if($params->ijseo_Replace3_with == "H5") echo "selected"; ?>>H5</option>
                    <option value="H6" <?php if($params->ijseo_Replace3_with == "H6") echo "selected"; ?>>H6</option>
                    <option value="noreplace" <?php if($params->ijseo_Replace3_with == "noreplace") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_DONT_REPLACE"); ?></option>
                </select>
            <?php 
                echo JHTML::tooltip(
                    "",
                    JText::_("COM_IJOOMLA_SEO_THISWILLREPLACESPECCLASS"), 
                    JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                ); 
            ?>        
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="5"><?php echo JText::_("COM_IJOOMLA_SEO_REPLACE"); ?></td>
            <td>
                 <input type="text" name="Replace4" id="Replace4" value="<?php echo $params->ijseo_Replace4; ?>" />&nbsp;with&nbsp;
                 <select name="Replace4_with">
                    <option value="H1" <?php if($params->ijseo_Replace4_with == "H1") echo "selected"; ?>>H1</option>
                    <option value="H2" <?php if($params->ijseo_Replace4_with == "H2") echo "selected"; ?>>H2</option>
                    <option value="H3" <?php if($params->ijseo_Replace4_with == "H3") echo "selected"; ?>>H3</option>
                    <option value="H4" <?php if($params->ijseo_Replace4_with == "H4") echo "selected"; ?>>H4</option>
                    <option value="H5" <?php if($params->ijseo_Replace4_with == "H5") echo "selected"; ?>>H5</option>
                    <option value="H6" <?php if($params->ijseo_Replace4_with == "H6") echo "selected"; ?>>H6</option>
                    <option value="noreplace" <?php if($params->ijseo_Replace4_with == "noreplace") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_DONT_REPLACE"); ?></option>
                </select>
            <?php 
                echo JHTML::tooltip(
                    "",
                    JText::_("COM_IJOOMLA_SEO_THISWILLREPLACESPECCLASS"), 
                    JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                ); 
            ?>        
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td  width="5"><?php echo JText::_("COM_IJOOMLA_SEO_REPLACE"); ?></td>
            <td>
                 <input type="text" name="Replace5" id="Replace5" value="<?php echo $params->ijseo_Replace5; ?>" />&nbsp;with&nbsp;
                 <select name="Replace5_with">
                        <option value="H1" <?php if($params->ijseo_Replace5_with == "H1") echo "selected"; ?>>H1</option>
                        <option value="H2" <?php if($params->ijseo_Replace5_with == "H2") echo "selected"; ?>>H2</option>
                        <option value="H3" <?php if($params->ijseo_Replace5_with == "H3") echo "selected"; ?>>H3</option>
                        <option value="H4" <?php if($params->ijseo_Replace5_with == "H4") echo "selected"; ?>>H4</option>
                        <option value="H5" <?php if($params->ijseo_Replace5_with == "H5") echo "selected"; ?>>H5</option>
                        <option value="H6" <?php if($params->ijseo_Replace5_with == "H6") echo "selected"; ?>>H6</option>
                        <option value="noreplace" <?php if($params->ijseo_Replace5_with == "noreplace") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_DONT_REPLACE"); ?></option>
                </select>
            <?php 
                echo JHTML::tooltip(
                    "",
                    JText::_("COM_IJOOMLA_SEO_THISWILLREPLACESPECCLASS"), 
                    JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                ); 
            ?>        
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <?php 
        echo $pane->endPanel();
        echo $pane->startPanel(JText::_('COM_IJOOMLA_SEO_KEYWORDS'), "keywords-settings" );
    ?>
    <table width="100%"  cellpadding="2" cellspacing="2">
        <tr>
            <td colspan="3" class="stitle">
                <table width="100%">
                    <tr>
                        <td>								
                            <?php echo JText::_("COM_IJOOMLA_SEO_KEYWORDS_MANAGER"); ?>
                        </td>
                        <td align="right">
                            <a class="modal seo_video" rel="{handler: 'iframe', size: {x: 740, y: 425}}" href="index.php?option=com_ijoomla_seo&controller=about&task=vimeo&id=13155493">
                                <img src="<?php echo JURI::base(); ?>components/com_ijoomla_seo/images/icon_video.gif" class="video_img" />
                                <?php echo JText::_("COM_IJOOMLA_SEO_KEYWORDS_MANAGER_VIDEO"); ?>
                            </a>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>								
            </td>
        </tr>
        <!-- Keyword/phrases source -->          
        <tr>
            <td valign="top" width="17%">
                <?php echo JText::_("COM_IJOOMLA_SEO_KEYWORD_PHRASES_SOURCE"); ?>
             </td>
             <td valign="top" align="left">
             <?php
                $opts = array();
                $opts[] = JHTML::_('select.option', '1', JText::_("COM_IJOOMLA_SEO_TITLE_MTAGS"));
                $opts[] = JHTML::_('select.option', '0', JText::_("COM_IJOOMLA_SEO_KEYWORDS_MTAGS"));
                echo $lists['keysource'] = JHTML::_('select.genericlist', $opts, 'keysource', ' size="1"', 'value', 'text', $params->ijseo_keysource);
             ?>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_KEYWORDS_TITLE_METATAG"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>
             </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <?php echo JText::_("COM_IJOOMLA_SEO_DELIMITERS"); ?>
            </td>
            <td align="left">
                <input type="text" name="delimiters" value="<?php echo $params->delimiters; ?>" />
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_TOOL_TIP"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>
            </td>
            <td>&nbsp;</td>
        </tr>                        
</table>
<?php 
    echo $pane->endPanel();
    echo $pane->startPanel( JText::_('COM_IJOOMLA_SEO_METATAGS'), "metatag-settings" );
?>
<table  cellpadding="2" cellspacing="2" width="100%">
        <tr>
            <td class="stitle" colspan="3">
                <table width="100%">
                <tr>
                    <td>								
                        <?php echo JText::_("COM_IJOOMLA_SEO_SEO_METATAGS"); ?>
                    </td>
                    <td align="right">
                        <a class="modal seo_video" rel="{handler: 'iframe', size: {x: 740, y: 425}}" href="index.php?option=com_ijoomla_seo&controller=about&task=vimeo&id=13155382">
                            <img src="<?php echo JURI::base(); ?>components/com_ijoomla_seo/images/icon_video.gif" class="video_img" />
                            <?php echo JText::_("COM_IJOOMLA_SEO_METATAGS_VIDEO"); ?>
                        </a>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
         </tr>
        <tr>
            <td width="17%"><?php echo JText::_("COM_IJOOMLA_SEO_TITLE_COUNTER"); ?></td>
            <td>
                 <input type="text" name = "allow_no2" value ="<?php if ($params->ijseo_allow_no2) {
                    echo $params->ijseo_allow_no2; 
                 } else {
                    echo "76";
                 }
                 ?>">
                 <select name="type_title" id="type_title">
                    <option value="Characters" <?php if($params->ijseo_type_key=="Characters") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_CHARACTERS"); ?></option>
                    <option value="Words" <?php if($params->ijseo_type_key=="Words") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_WORDS"); ?></option>
                 </select>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_COUNTERDESCRIPTION"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td width="17%"><?php echo JText::_("COM_IJOOMLA_SEO_KEYWORDS_COUNTER"); ?></td>
            <td>
                 <input type="text" name = "allow_no" value ="<?php echo $params->ijseo_allow_no; ?>">
                 <select name="type_key" id="type_key">
                    <option value="Characters" <?php if($params->ijseo_type_key=="Characters") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_CHARACTERS"); ?></option>
                    <option value="Words" <?php if($params->ijseo_type_key=="Words") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_WORDS"); ?></option>
                 </select>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_COUNTERDESCRIPTION"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><?php echo JText::_("COM_IJOMLA_SEO_DESCRIPTIONLENGHT"); ?></td>
            <td>
                <!-- Length -->
                <input type="text" name = "allow_no_desc" value ="<?php echo $params->ijseo_allow_no_desc; ?>">
                <select name="type_desc" id="type_desc">
                    <option value="Characters" <?php if($params->ijseo_type_desc=="Characters") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_CHARACTERS"); ?></option>
                    <option value="Words" <?php if($params->ijseo_type_desc=="Words") echo "selected"; ?>><?php echo JText::_("COM_IJOOMLA_SEO_WORDS"); ?></option>
                </select>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_DESCRIPTIONLENGHTTOUSE"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>
            </td>
            <td>&nbsp;</td>
         </tr>                         
        <tr>
            <td><?php echo JText::_("COM_IJOOMLA_SEO_GETDESCRIPTIONFROM"); ?></td>
            <td>
                <select name = 'gdesc'>
                    <option value = 'intro' <?php  if ($params->ijseo_gdesc == 'intro') echo "selected";?> ><?php echo JText::_("COM_IJOOMLA_SEO_INTROTEXT"); ?></option>
                    <option value = 'full' <?php  if ($params->ijseo_gdesc == 'full') echo "selected";?> ><?php echo JText::_("COM_IJOOMLA_SEO_MAINTEXT"); ?></option>
                </select>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_INTROTEXTDESCRIPTION"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>            
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
             <td><?php echo JText::_("COM_IJOOMLA_SEO_OMIT_KEYWORDS_TAGS"); ?>:</td>
             <td><textarea cols="40" rows="5" name="exclude_key"><?php
                    $text = '';										
                    if(!empty($params->exclude_key)){
                        foreach($params->exclude_key as $value)
                            $text.= trim($value).",";
                            $text = substr($text, 0, -1);
                        }
                        echo $text;
                    ?></textarea>
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_OMITKT"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>
            </td>
        </tr>                        
    </table>
<?php 
    echo $pane->endPanel();
    echo $pane->startPanel( JText::_('COM_IJOOMLA_SEO_GOOGLE_PING'), "google-settings" );
?>
<table width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td colspan="3" class="stitle">
                <table width="100%">
                    <tr>
                        <td width="11%">								
                            <?php echo JText::_("COM_IJOOMLA_SEO_GOOGLE_PING"); ?>
                        </td>
                        <td align="right">
                            <a class="modal seo_video" rel="{handler: 'iframe', size: {x: 740, y: 425}}" href="index.php?option=com_ijoomla_seo&controller=about&task=vimeo&id=13155395">
                                <img src="<?php echo JURI::base(); ?>components/com_ijoomla_seo/images/icon_video.gif" class="video_img" />
                                <?php echo JText::_("COM_IJOOMLA_SEO_GOOGLE_PING_VIDEO"); ?>
                            </a>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>								
            </td>
        </tr>	
        <tr>
            <td valign="top" width="22%">
                <?php echo JText::_("COM_IJOOMLA_SEO_POSITION_CHECK_METHOD"); ?>
             </td>
             <td valign="top">
                <?php
                    echo $lists['gposition'] = JHTML::_('select.booleanlist', 'gposition','', $params->ijseo_gposition, JText::_("COM_IJOOMLA_SEO_AUTOMATICALLY"), JText::_("COM_IJOOMLA_SEO_MANUALLY"));
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_POSITION_RANK_HELP"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>
             </td>
             <td>&nbsp;</td>
        </tr>						
        <tr>
            <td valign="top">
                <?php echo JText::_("COM_IJOOMLA_SEO_CHECK_GOOGLE_RANK"); ?>
            </td>
            <td valign="top">
                <?php
                    $cgr = array();
                    $cgr[] = JHTML::_('select.option', '1', JText::_("COM_IJOOMLA_SEO_ONCE_A_DAY"));
                    $cgr[] = JHTML::_('select.option', '2', JText::_("COM_IJOOMLA_SEO_ONCE_EVERY_2_DAYS"));
                    $cgr[] = JHTML::_('select.option', '6', JText::_("COM_IJOOMLA_SEO_ONCE_A_WEEK"));
                    echo $lists['check_gr'] = JHTML::_('select.genericlist', $cgr, 'check_gr', ' size="1"', 'value', 'text', $params->ijseo_check_grank);
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_CHANGE_GR"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>
             </td>
             <td>&nbsp;</td>
        </tr>                      
        <tr>
            <td><?php echo JText::_("COM_IJOOMLA_SEO_CHECK_SITES"); ?></td>
            <td>
                google.<input type="text" name="ijseo_check_ext" value="<?php echo $params->ijseo_check_ext; ?>" />
                <?php 
                    echo JHTML::tooltip(
                        "",
                        JText::_("COM_IJOOMLA_SEO_EXTENSION"), 
                        JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                    ); 
                ?>
            </td>
                    
            <td></td>
        </tr>
        <tr>
            <td>
                <?php echo JText::_("COM_IJOOMLA_SEO_CHEKFOR_FIRST"); ?>
            </td>
            <td>
                <select size="1" id="check_nr" name="check_nr">
                <?php 
                    for($i=1; $i<11; $i++){
                        $selected = "";
                        if($params->check_nr == $i*5){
                            $selected = "selected";												
                        }
                        echo "<option value=\"".($i*5)."\" ".$selected." >".($i*5)."</option>";
                    }																														
                 ?>
                 </select >
                 &nbsp;<?php echo JText::_('COM_IJOOMLA_SEO_SEARCH_RESULTS'); ?>
					<?php 
                        echo JHTML::tooltip(
                            "",
                            JText::_("COM_IJOOMLA_SEO_CHECK_FIRST_RESULTS"), 
                            JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                        ); 
                    ?>                 
            </td>
            <td></td>
        </tr>
</table>
<?php 
    echo $pane->endPanel();
    echo $pane->startPanel( JText::_('COM_IJOOMLA_SEO_KEYWORD_LINKING'), "keylinks-settings" );
?>
<table width="100%" cellpadding="2" cellspacing="2">
    <tr>
        <td colspan="3" class="stitle">
            <table width="100%">
                <tr>
                    <td>								
                        <?php echo JText::_("COM_IJOOMLA_SEO_KEYWORD_LINKING"); ?>
                    </td>
                    <td align="right">
                        <a class="modal seo_video" rel="{handler: 'iframe', size: {x: 740, y: 425}}" href="index.php?option=com_ijoomla_seo&controller=about&task=vimeo&id=28827842">
                            <img src="<?php echo JURI::base(); ?>components/com_ijoomla_seo/images/icon_video.gif" class="video_img" />
                            <?php echo JText::_("COM_IJOOMLA_SEO_KEYWORD_LINKING_VIDEO"); ?>
                        </a>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>								
        </td>
    </tr>	
    <tr>
         <td valign="top" width="40%">
            <?php echo JText::_("COM_IJOOMLA_SEO_CASE_SENSITIVE"); ?>:
         </td>
         <td valign="top" align="left">
            <?php echo JText::_("COM_IJOOMLA_SEO_NO"); ?>
			<input type="radio" value="0" name="case_sensitive" <?php 
				if (!isset($params->case_sensitive) || ($params->case_sensitive == 0)) { echo 'checked="checked"'; } ?> />
            &nbsp;
            <?php echo JText::_("COM_IJOOMLA_SEO_YES"); ?>
			<input type="radio" value="1" name="case_sensitive" <?php 
				if (isset($params->case_sensitive) && ($params->case_sensitive == 1)) { echo 'checked="checked"'; } ?> />
            <?php 
                echo JHTML::tooltip(
                    "",
                    JText::_("COM_IJOOMLA_SEO_CASE_SENSITIVE_TIP"), 
                    JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                ); 
            ?>
         </td>
         <td>&nbsp;</td>
	 </tr>
	 <tr>
		<td valign="top" width="40%">
            <?php echo JText::_('COM_IJOOMLA_SEO_SEARCH_BETWEEN'); ?>:
        </td>
		<td colspan="2" align="left">
			(<?php echo JText::_('COM_IJOOMLA_SEO_START'); ?>) <input type="text" name="sb_start" value="<?php 
				if (isset($params->sb_start)) { echo $params->sb_start; }
			?>" /> <?php echo JText::_('COM_IJOOMLA_SEO_AND'); ?> 
			(<?php echo JText::_('COM_IJOOMLA_SEO_END'); ?>) <input type="text" name="sb_end" value="<?php 
				if (isset($params->sb_end)) { echo $params->sb_end; }			
			?>" />
            <?php 
                echo JHTML::tooltip(
                    "",
                    JText::_("COM_IJOOMLA_SEO_SEARCH_BETWEEN_TIP"), 
                    JURI::root() . "administrator/components/com_ijoomla_seo/images/tooltip.png"
                ); 
            ?>			
		</td>
	 </tr>
    <tr>
        <td colspan="3" align="left">
        <?php 
            $link = "http://www.ijoomla.com/seo_find_div.txt";
            $content = read_from_ijoomla($link);
            echo "<br /><div id='ijoomla_message'>" . $content . "</div>";
        ?>        
        </td>
    </tr>
</table>        
<input type = "hidden" name = "action" value = 'save_config'>
<input type = "hidden" name = "task" value = "config">
<input type = "hidden" name = "option" value = "com_ijoomla_seo">
<?php 
    echo $pane->endPanel();
    echo $pane->endPane();
?>
</form>


</td></tr></table>
<p><center><?php echo JText::_("COM_IJOOMLA_SEO_POWERED_BY") ; ?>&nbsp;<a href='http://www.ijoomla.com' target='_blank'><?php echo JText::_("COM_IJOOMLA_SEO_COMPONENT_TITLE"); ?></a></center></p>	