<?php

function com_uninstall(){
	// create lang and config backups
    $path_backup = JPATH_SITE."/components/ijoomla_seo_backup";     
    $lang_source = JPATH_SITE."/administrator/language/en-GB";
    $lang = "/en-GB.com_ijoomla_seo.ini";
    if(!is_dir($path_backup)){ 
		mkdir($path_backup,0777);
	}	
    @copy($lang_source.$lang, $path_backup.$lang);
    
    remove_plugin();
    echo "Component successfully uninstalled.";
}

function remove_plugin () {
	$database = JFactory::getDBO();

	$mosConfig_absolute_path = JPATH_ROOT;
	$query = "delete from #__extensions where element='ijseo_plugin'";
	$database->setQuery($query);
	$database->query();
	$query = "delete from #__extensions where element='ijseo'";
	$database->setQuery($query);
	$database->query();
		
	unlink (@$mosConfig_absolute_path."/plugins/content/ijseo_plugin/ijseo_plugin.php");
	unlink (@$mosConfig_absolute_path."/plugins/content/ijseo_plugin/ijseo_plugin.xml");
	unlink (@$mosConfig_absolute_path."/plugins/system/ijseo/ijseo.php");
	unlink (@$mosConfig_absolute_path."/plugins/system/ijseo/ijseo.xml");	
}


?>
