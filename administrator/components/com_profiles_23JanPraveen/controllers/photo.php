<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Photo controller class.
 */
class ProfilesControllerPhoto extends JControllerForm
{

    function __construct()
    {
        $this->view_list = 'photos';
        parent::__construct();
        
        if(!empty($_FILES['jform']))
        {
        	foreach($_FILES['jform']['name'] as $field => $filename)
        	{
        		if(!empty($filename))
        		{
    				jimport('joomla.filesystem.file');
        			$_POST['jform'][$field] = JFile::makeSafe(strtolower($filename));
        		}
        	}
        }
        
    }
    
    // for the ajax delete function in the gallery manager
	public function delete()
	{
		$app	= JFactory::getApplication();
		$pk		= JRequest::getInt('id');
		$model	= $this->getModel();
		$db		= $model->getDbo();
		$query	= $db->getQuery(true);
		
		$query->select('*')
			->from('#__profiles_photos AS a')
			->where('a.id = '.$pk);
			
		$db->setQuery((string)$query);
		$obj	= $db->loadObject();
		
		if($model->delete($pk))
		{
    		$dir = '/uploads/profiles/'.$obj->id.'/';
    		$full_dir = JPATH_SITE.$dir;
    		
    		$to_delete = array(
    			$full_dir.$obj->path,
    			$full_dir.'307_254_'.$obj->path,
    			$full_dir.'165_120_'.$obj->path,
    			$full_dir.'150_150_'.$obj->path,
    			$full_dir.'thumbnail_'.$obj->path,
    		);
    		
			JFile::delete($to_delete);
			$result = 'success';
		}
		else
		{
			$result = 'fail';
		}
		
		echo json_encode(array('result' => $result));
		$app->close();
    }
	
	// Handles the ajax reordering of the photos
	public function reorderphotos()
	{
		$app	= JFactory::getApplication();
		$model = $this->getModel('photo');
		
		$order = explode(',', JRequest::getVar('new_order'));
		$pks = array_values($order);
		$new_order = array_keys($order);
		$status = $model->saveOrder($pks, $new_order);
			
		if($status->getError())
		{
			$result = 'fail';
		}
		else
		{
			$result = 'success';
		}
		 
		echo json_encode(array('result' => $result));
		$app->close();
	}

	/**
	 * Gets the URL arguments to append to an item redirect.
	 *
	 * @param   integer  $recordId  The primary key id for the item.
	 * @param   string   $urlVar    The name of the URL variable for the id.
	 *
	 * @return  string  The arguments to append to the redirect URL.
	 *
	 * @since   11.1
	 */
	protected function getRedirectToItemAppend($recordId = null, $urlVar = 'id')
	{
		$tmpl	= JRequest::getCmd('tmpl');
		$layout	= JRequest::getCmd('layout', 'edit');
		$uid	= JRequest::getInt('uid');
		$append = '';

		// Setup redirect info.
		if ($tmpl)
		{
			$append .= '&tmpl=' . $tmpl;
		}

		if ($layout)
		{
			$append .= '&layout=' . $layout;
		}

		if ($recordId)
		{
			$append .= '&' . $urlVar . '=' . $recordId;
		}

		if ($uid)
		{
			$append .= '&uid=' . $uid;
		}

		return $append;
	}
    
    public function postSaveHook($model, $validData)
    {
// /* 		print_r($model);
		// print_r($validData);
		// print_r($_FILES['jform']);
		// exit(); */
    	ini_set('memory_limit', '256M');
    	if(!empty($_FILES['jform']))
    	{
    		jimport('joomla.filesystem.file');
			jimport('joomla.filesystem.folder');
			
			$model = $this->getModel();
			$db		= $model->getDbo();
			$query	= $db->getQuery(true);
			$query->select('a.id');
			$query->from('#__profiles_families as a');
			$query->where('a.user_id = '.$validData['family_id']);

			$db->setQuery((string)$query);
			if (!$db->query()) {
				JError::raiseError(500, $db->getErrorMsg());
			}
			$result = $db->loadObject();
    		
    		$dir = '/uploads/profiles/'.$result->id.'/';

    		$full_dir = JPATH_SITE.$dir;
    		
    		$field = 'path';
			$file = new stdClass;
			foreach($_FILES['jform'] as $key => $values)
			{
    			$file->$key		= $values[$field];
			}
			if($file->error)
			{
				return;
			}
			$parts = explode('.', $file->name);
			$file->ext = strtolower(array_pop($parts));
			$allowed_ext = explode(',', 'jpg,jpeg,png,gif');
			if(in_array($file->ext, $allowed_ext))
			{
				$file->ok = true;
			}
			$file->tmp_info = getimagesize($file->tmp_name);
			if(!is_int($file->tmp_info[0]) || !is_int($file->tmp_info[1]) || !preg_match("/image/i", $file->tmp_info['mime']))
			{
				break;
			}
			$file->name = JFile::makeSafe(strtolower($file->name));
			if(!is_dir($full_dir))
			{
				JFolder::create($full_dir, 0755);
			}
			if(is_file($full_dir.$file->name))
			{
				JFile::delete($full_dir.$file->name);
			}
			if(JFile::upload($file->tmp_name, $full_dir.$file->name))
			{
				$image = Image::load($full_dir.$file->name);
				// Check if it's wider than 600 pixels
				if($file->tmp_info[0] > 600)
				{
					$image->resize(600)->save_pa('tmp_');
					JFile::delete($full_dir.$file->name);
					JFile::move('tmp_'.$file->name, $file->name, $full_dir);
					$image = Image::load($full_dir.$file->name);
				}
				$image->crop_resize(307, 254)->save_pa('307_254_');
				$image->crop_resize(165, 120)->save_pa('165_120_');
				$image->crop_resize(150, 150)->save_pa('150_150_');
				$image->crop_resize(60, 60)->save_pa('thumbnail_');
				$_POST['jform'][$field] = $file->name;
			}
    	}
    }
}