<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Profiles helper.
 */
class ProfilesHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{

		JSubMenuHelper::addEntry(
			JText::_('COM_PROFILES_TITLE_FAMILIES'),
			'index.php?option=com_profiles&view=families',
			$vName == 'families'
		);
		/*
		JSubMenuHelper::addEntry(
			JText::_('COM_PROFILES_TITLE_PHOTOS'),
			'index.php?option=com_profiles&view=photos',
			$vName == 'photos'
		);
		*/
		JSubMenuHelper::addEntry(
			JText::_('COM_PROFILES_TITLE_RECENTS'),
			'index.php?option=com_profiles&view=recents',
			$vName == 'recents'
		);
		JSubMenuHelper::addEntry(
			JText::_('COM_PROFILES_TITLE_SUCCESSES'),
			'index.php?option=com_profiles&view=successes',
			$vName == 'successes'
		);

	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_profiles';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;
	}
}
