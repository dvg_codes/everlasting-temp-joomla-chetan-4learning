// @version 1.0.0
// @package RSFinder! 1.0.0
// @copyright (C) 2009-2010 www.rsjoomla.com
// @license GPL, http://www.gnu.org/licenses/gpl-2.0.html

function checkKeycode(e) {
	var keycode;
	if (window.event) {
		keycode = window.event.keyCode;
		ctrlKey = window.event.ctrlKey;
	}
	else if (e) {
		keycode = e.which;
		ctrlKey = e.ctrlKey;
	}
	
	if(ctrlKey && keycode == 38) {
		document.getElementById('rsquick').focus();
		document.getElementById('rsquick').value = '';
	}
	if(keycode == 27){
		//document.getElementById('rsquick').value = '';
		document.getElementById('rsResults').style.display = 'none';
	}
	if(keycode==40){
		nextItem('down' , rs_results);
	}
	if(keycode==38){
		nextItem('up' , rs_results);
	}
	if(keycode==13)
	{
		gotoItem(rs_results);
	}
}

function gotoItem(items){
	for(i=0;i<items;i++)
		if(document.getElementById('result_' + i).className == 'rsActive')
			document.location = document.getElementById('result_' + i).href;
}

function nextItem(direction, items)
{	
	if(items>0)
	{
		current_active = -1;
		//get active item
		for(i=0;i<items;i++)
		{
			if(document.getElementById('result_' + i))
			{
				if(document.getElementById('result_' + i).className == 'rsActive') 
				{
					current_active = i;
				}
			}
		}
		
		if(direction == 'up') current_active -= 1;
		else current_active += 1;
		if(current_active == -1) current_active = items-1;
		if(current_active == items) current_active = 0;	
		
		for(i=0;i<items;i++)
		{
			if(document.getElementById('result_' + i))
			{
				document.getElementById('result_' + i).className = 'rsInactive';
			}
			if(i==current_active)
			{
				if(document.getElementById('result_' + i))
				{
					document.getElementById('result_' + i).className = 'rsActive';
				}
				//document.getElementById('result_' + i).focus();
			}
			
		}
	}
}

function resolveMouseOver(items)
{
	if(items>0)
	{
		for(i=0;i<items;i++)
		{
			if(document.getElementById('result_' + i))
			{
				document.getElementById('result_' + i).onmouseover = function(){
					for(i=0;i<items;i++)
					{
						document.getElementById('result_' + i).className = 'rsInactive';
					}
					this.className = 'rsActive';
				}
				document.getElementById('result_' + i).onmouseout = function(){
					this.className = 'rsInactive';
				}
			}
		}
	}
}

var generateResultsTimer = 0;
function timerGenerateResults(evt)
{
	if (generateResultsTimer > 0)
		clearTimeout(generateResultsTimer);
	
	generateResultsTimer = setTimeout(function(){ generateResults(evt); }, 1000);
}

function generateResults(e)
{
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	
	var link = document.getElementById('rsf_link');
	
	if (link.hasClass('mod_rsfinder'))
	{
		link.removeClass('mod_rsfinder');
		if (!link.hasClass('mod_rsfinder_loader'))
			link.addClass('mod_rsfinder_loader');
	}
	
	if(document.getElementById('rsquick').value.length > 1 && keycode != 40 && keycode != 38 && keycode != 27 )
	{
		query = document.getElementById('rsquick').value;
		xmlHttp=CreateXMLHttpRequest();
		xmlHttp.open("GET","index.php?rsquick=" + query,true);
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4) 
			{
				document.getElementById('rsResultsUl').innerHTML=xmlHttp.responseText;
				document.getElementById('rsResults').style.display='block';
				rs_results = xmlHttp.responseText.split("\n").length - 1;
				nextItem('down',1);
				resolveMouseOver(rs_results);
				
				if (link.hasClass('mod_rsfinder_loader'))
				{
					link.removeClass('mod_rsfinder_loader');
					if (!link.hasClass('mod_rsfinder'))
						link.addClass('mod_rsfinder');
				}
				
				//document.getElementById('content').innerHTML = xmlHttp.responseText;
			}
		}
		xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlHttp.setRequestHeader("Content-length", 1000);
		xmlHttp.setRequestHeader("Connection", "close");
		xmlHttp.send(null);
	} else
	{	
		document.getElementById('rsResultsUl').innerHTML = '';
		if (link.hasClass('mod_rsfinder_loader'))
		{
			link.removeClass('mod_rsfinder_loader');
			if (!link.hasClass('mod_rsfinder'))
				link.addClass('mod_rsfinder');
		}		
	}
}


function CreateXMLHttpRequest()
{
	//Ajax script to create the browser based ActiveXObject object
	var xmlHttpRequest = false;
	try
	{
		if(window.ActiveXObject)
		{
			for(var i = 5; i; i--)
			{
				try
				{
					if(i == 2)
					{
						xmlHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
					}
					 else
					{
						xmlHttpRequest = new ActiveXObject("Msxml2.XMLHTTP." + i + ".0");
					}
					break;
				}
				catch(excNotLoadable)
				{
					xmlHttpRequest = false;
				}
			}
		}
		else if(window.XMLHttpRequest)
		{
			xmlHttpRequest = new XMLHttpRequest();
		}
	}
	catch(excNotLoadable)
	{
		xmlHttpRequest = false;
	}
	return xmlHttpRequest;
}