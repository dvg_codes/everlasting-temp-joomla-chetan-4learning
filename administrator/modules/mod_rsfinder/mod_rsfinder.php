<?php
/**
* @version 1.0.0
* @package RSFinder! 1.0.0
* @copyright (C) 2009-2010 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
$document = &JFactory::getDocument();
$document->addScript( JURI::root() . 'administrator/modules/mod_rsfinder/mod_rsfinder.js' );
$document->addStyleSheet( JURI::root() . 'administrator/modules/mod_rsfinder/mod_rsfinder.css' );	
?>
<span>
<a href="http://www.rsjoomla.com" id="rsf_link" class="editlinktip hasTip mod_rsfinder" target="_blank" title="RSfinder! from RSjoomla.com::Visit rsjoomla.com for updates and other interesting Joomla! add-ons">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
<input type="text" class="editlinktip hasTip inputbox" style="100px;" id="rsquick" onkeydown="rs_setPosition(this)" autocomplete="off" <?php echo defined('RSFINDER') ? 'title="RSfinder! from RSjoomla.com::Use the Ctrl + Down arrow to trigger the search"' : 'title="Please enable the System - rsfinder plugin first in Extensions / Plugin manager"';?>/><br/>
<div id="rsResults">
	<ul id="rsResultsUl"></ul>
</div>
</span>

<script type="text/javascript">
<?php if(defined('RSFINDER')){ ?>
	var browser=navigator.appName;
	if(browser == 'Opera')document.onkeyup = checkKeycode;
	else document.onkeydown = checkKeycode;
	document.getElementById('rsquick').onkeyup = timerGenerateResults;
	var xmlHttp = null;
	var rs_results = null;
<?php } ?>

function rs_setPosition(what)
{
	var popup = document.getElementById('rsResults');
	var curleft = curtop = 0;
	if (what.offsetParent) {
		do {
			curleft += what.offsetLeft;
			curtop += what.offsetTop;
		} while (what = what.offsetParent);
	}
	
	
	popup.style.left = parseInt(curleft - 122) + 'px';
	popup.style.top = parseInt(curtop + 19) + 'px';
	popup.style.display = 'block';
}
</script>