<?php
jimport('joomla.application.component.model');
jimport('joomla.application.component.modellist');
jimport('joomla.application.component.helper');
require_once(JPATH_SITE.'/components/com_profiles/models/recent.php');

$items = ProfilesModelRecent::getInstance()->getItems(15);
?>
		<div class="gallery-box">
			<div class="holder">
				<div class="container">
					<h2>Recent Adoptions | <a href="/waiting-families">view all</a></h2>
					<div class="carousel">
						<a href="#" class="link-prev">previous</a>
						<div class="hold">
							<ul>
							<?php foreach($items as $item) : ?>
								<li>
									<a href="/recent-adoptions"><img width="160" height="116" src="/uploads/recents/<?php echo $item->id . '/165_120_' . $item->recent_story_image; ?>" alt="<?php echo $item->last_name; ?> Family" /></a>
									<p>We adopted in<br /><span><?php echo preg_replace('/[^0-9]/', '', $item->recent_story); ?> months</span></p>
								</li>
							<?php endforeach; ?>
							</ul>
						</div>
						<a href="#" class="link-next">next</a>
					</div>
				</div>
			</div>
		</div>