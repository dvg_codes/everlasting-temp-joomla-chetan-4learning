<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_articles_news
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="newsflash<?php echo $moduleclass_sfx; ?>">
<ul>
<?php

foreach ($list as $item)
{
	
	echo '<li><a href="'.$item->link.'"><span class="h5">'.$item->title.'</span>'.date('F jS, Y', strtotime($item->created)).'</a></li>';

}

?>
</ul>
</div>