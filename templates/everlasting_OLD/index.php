<?php
/**
 * @package		Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

JHtml::_('behavior.framework', true);

// get params
$app                = JFactory::getApplication();
$doc				= JFactory::getDocument();
$templateparams     = $app->getTemplate(true)->params;
	
define('TEMPLATE_PATH', '/templates/everlasting/');
define('CSS_PATH', TEMPLATE_PATH.'css/');
define('IMG_PATH', TEMPLATE_PATH.'images/');
define('JS_PATH', TEMPLATE_PATH.'js/');

$doc->addScript(JS_PATH.'jquery-1.6.2.min.js');
$doc->addScript(JS_PATH.'jquery.cycle.all.latest.min.js');
$doc->addScript(JS_PATH.'jquery.main.js');
$doc->addScript('/components/com_profiles/assets/js/jquery.infinitescroll.min.js');

$is_home = false;
$menu = & JSite::getMenu();
$active = $menu->getActive();
$menuname = $active->title;
$parentId = $active->tree[0];

if ($menu->getActive() == $menu->getDefault()) {
   $body_class = 'home';
   $is_home = true;
}
if($menu->getActive()->alias === 'free-application')
{
	$body_class = 'application-page';
}
if ($active->id == '506') {
	$body_class = 'waiting-families';
}


/*
$head = $this->getHeadData();
unset($head['scripts']['/media/system/js/mootools-core.js']);
unset($head['scripts']['/media/system/js/mootools-more.js']);
unset($head['scripts']['/media/system/js/core.js']);
$this->setHeadData($head);
*/

?><!DOCTYPE html>
<html lang="<?=$this->language?>">
<head>
	<jdoc:include type="head" />
    <link rel="stylesheet" href="/templates/system/css/system.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo CSS_PATH; ?>general.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo CSS_PATH; ?>site.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo CSS_PATH; ?>print.css" type="text/css" media="print" />
    <link rel="shortcut icon" href="/favicon.png" type="image/png" />
    <link href="https://plus.google.com/b/104392048981977095935/104392048981977095935" rel="publisher" />
    <meta name="robots" content="index,follow" />
	<meta name="google-site-verification" content="uuH25c_-RSlMcTvGzqX1N0j4EIKe-uPZ2n5wZmhTLyQ" />
	<meta name="p:domain_verify" content="b0aed9dbf343f4df3dcbbbfa3045b127"/>
	<script type="text/javascript">
    	var _gaq = _gaq || [];
    	_gaq.push(['_setAccount', 'UA-7033481-1']);
    	_gaq.push(['_trackPageview']);

	    (function() {
    	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	 })();
    	 if (typeof window.console == 'undefined') window.console = {log: function () {}};
	</script>
	<!--[if IE 7]>
		<link href="<?php echo CSS_PATH; ?>ie7only.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '962828510418977');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=962828510418977&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<?php




$phone = ($parentId == "471" || $active == "506") ? "888-398-6660<span class=\"textus\">or text us:</span><span class=\"textnumber\">630-364-8361</span>" : "866-406-2702";
$free_app = ($parentId == "471" || $active == "506") ? "" : "<a id=\"free_application_btn\" href=\"/free-application\"></a>";

//print_r ($active->id);
//print_r ($parentId)
?>

<body class="<?php echo $body_class; ?>">
	<div class="dropshadow">
		<div class="main-info">
		    <div class="wrapper">
				<a id="logo" href="/">Everlasting Adoptions</a>
				<span id="header_phone">Call Us Today!<br/><span><?php echo $phone;?></span></span>
				<!-- <a id="click_to_chat_btn" href="javascript:void(0);" onclick="launchWSA();"></a> -->
				<!--<a id="free_application_btn" href="/free-application"></a>-->
				<? echo $free_app ?>
                <a target="_blank" id="bbblink" class="ruvtbum" href="http://www.bbb.org/losangelessiliconvalley/business-reviews/adoption-services/everlasting-adoptions-in-santa-monica-ca-100051741#bbbseal" title="Everlasting Adoptions, Inc., Adoption Services, Santa Monica, CA" style="display: block;position: relative;overflow: hidden; width: 60px; height: 108px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="http://seal-sanjose.bbb.org/logo/ruvtbum/everlasting-adoptions-100051741.png" width="120" height="108" alt="Everlasting Adoptions, Inc., Adoption Services, Santa Monica, CA" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-sanjose.bbb.org' + unescape('%2Flogo%2Feverlasting-adoptions-100051741.js') + "' type='text/javascript'%3E%3C/script%3E"));</script>
				<br class="clear"/>
		    </div>
		</div>
		<div id="nav">
			<div class="wrapper">
				<jdoc:include type="modules" name="nav" />
			</div>
		</div>
		<?php if($is_home):?>
				<jdoc:include type="modules" name="slider" />
		<?php endif; ?>
		<div id="main" class="main">
			<div id="top" class="header_graphic">
				<jdoc:include type="modules" name="header_graphic" />
			</div>
			<div class="wrapper">
				<div id="content">
					<jdoc:include type="message" />
					<jdoc:include type="component" />
				</div>
				<div id="sidebar">
					<jdoc:include type="modules" name="left" style="blank" />
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
	    <div class="wrapper">
			<div class="holder">
			    <div class="column first">
			    	<jdoc:include type="modules" name="footer-1" style="blank" />
			    </div>
			    <div class="column second">
			    	<jdoc:include type="modules" name="footer-2" style="blank" />
			    </div>
			    <div class="column third">
			    	<jdoc:include type="modules" name="footer-3" style="blank" />
			    </div>
			</div>
	    </div>
	</div>
	<div class="copyright-block">
	    <div class="wrapper">
			<p class="ee"><a href="http://www.jm-experts.com/" target="_blank" title="Joomla Experts">Joomla Experts</a></p>
			<p>&copy; Copyright <?php echo date('Y'); ?> Everlasting Adoptions. All Rights Reserved - <a href="/privacy">Privacy Policy</a> |</p>
	    </div>
	</div>

<script type="text/javascript">
// <![CDATA[
	function stopRKey(evt) { 
	    var evt = (evt) ? evt : ((event) ? event : null); 
	    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
	    if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
	}
	document.onkeypress = stopRKey;
// ]]>
</script>
<?php if ($parentId == "471" || $active == "506") {
	//echo $pregnantchat;
	}
	?>
<jdoc:include type="modules" name="debug" />
<script type="text/javascript">
	var cloak = jQuery('.cloak');
	cloak.each(function(){
		var email = jQuery(this).attr('href').replace("_AT_", "@");
		jQuery(this).attr('href',email);
		var content = jQuery(this).html().replace("_AT_", "@");
		jQuery(this).html(content); 
	});
</script>
<!-- start number replacer -->
<script type="text/javascript"><!--
vs_account_id      = "CtjSgFZ4NPpeXwCw";
//--></script>
<script type="text/javascript" src="//adtrack.voicestar.com/euinc/number-changer.js">
</script>
<!-- end ad widget -->
<?php if($_SERVER['REMOTE_ADDR'] == '111.125.141.94' || $_SERVER['REMOTE_ADDR'] == '199.127.251.158') {?>
<!-- No live chat for dev team. -->
<?php } else { ?>


<jdoc:include type="modules" name="livechatbot"/>
<?php } ?>

</body>
</html>