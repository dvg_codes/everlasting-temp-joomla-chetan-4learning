<?php
/**
 * @package		Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

JHtml::_('behavior.framework', true);

// get params
$app                = JFactory::getApplication();
$doc				= JFactory::getDocument();
$templateparams     = $app->getTemplate(true)->params;
	
define('TEMPLATE_PATH', '/templates/beez_20/');
define('CSS_PATH', TEMPLATE_PATH.'css/');
define('IMG_PATH', TEMPLATE_PATH.'images/');
define('JS_PATH', TEMPLATE_PATH.'js/');

$doc->addScript(JS_PATH.'jquery-1.6.2.min.js');
$doc->addScript(JS_PATH.'jquery.cycle.all.latest.min.js');
$doc->addScript(JS_PATH.'jquery.main.js');

$menu = & JSite::getMenu();
if ($menu->getActive() == $menu->getDefault()) {
   $body_class = 'home';
}
if($menu->getActive()->alias === 'free-application')
{
	$body_class = 'application-page';
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Super User" />
	<title>404 - Everlasting Adoptions</title>
	<link rel="stylesheet" href="/components/com_rsform/assets/calendar/calendar.css" type="text/css" />
	
	<link rel="stylesheet" href="/components/com_rsform/assets/css/front.css" type="text/css" />
	<script src="/media/system/js/caption.js" type="text/javascript"></script>
	<script src="/components/com_rsform/assets/js/script.js" type="text/javascript"></script>
	<script src="/templates/beez_20/js/jquery-1.6.2.min.js" type="text/javascript"></script>
	<script src="/templates/beez_20/js/jquery.cycle.all.latest.min.js" type="text/javascript"></script>
	<script src="/templates/beez_20/js/jquery.main.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	window.addEvent('load', function() {
		new JCaption('img.caption');
	});
	</script>
    <link rel="stylesheet" href="/templates/system/css/system.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo CSS_PATH; ?>general.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo CSS_PATH; ?>site.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo CSS_PATH; ?>print.css" type="text/css" media="print" />
    <link rel="shortcut icon" href="/favicon.png" type="image/png" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta name="google-site-verification" content="uuH25c_-RSlMcTvGzqX1N0j4EIKe-uPZ2n5wZmhTLyQ" />
	<?php /*
	<script type="text/javascript">
    	var _gaq = _gaq || [];
    	_gaq.push(['_setAccount', 'UA-7033481-1']);
    	_gaq.push(['_trackPageview']);

	    (function() {
    	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	 })();
	</script>
	*/ ?>
	<!--[if IE 7]>
		<link href="<?php echo CSS_PATH; ?>ie7only.css" rel="stylesheet" type="text/css" />
	<![endif]-->
</head>
<body class="<?php echo $body_class; ?>">
	<div class="dropshadow">
		<div class="main-info">
		    <div class="wrapper">
				<a id="logo" href="/">Everlasting Adoptions</a>
				<span id="header_phone">Call Us Today! <span>866-406-2702</span></span>
				<a id="click_to_chat_btn" href="javascript:void(0);" onclick="launchWSA();"></a>
				<a id="free_application_btn" href="/free-application"></a>
		    </div>
		</div>
		<div id="nav">
			<div class="wrapper">
				<ul class="menu"><li class="item-435 current active"><a href="/">Home</a></li><li class="item-471 parent"><a href="/pregnant">Pregnant?</a></li><li class="item-506"><a href="/waiting-families">Waiting Families</a></li><li class="item-473 parent"><a href="/hoping-to-adopt">Hoping to Adopt?</a></li><li class="item-474 parent"><a href="/free-application">Free Application</a></li><li class="item-475"><a href="/recent-adoptions">Recent Adoptions</a></li><li class="item-476"><a href="/blog">Blog</a></li><li class="item-477"><a href="/contact-us">Contact Us</a></li></ul>
			</div>
		</div>
		<div id="main" class="main">
			<div id="top" class="header_graphic">
				
			</div>
			<div class="wrapper">
				
	<div class="error404">
		<div id="outline">
		<div id="errorboxoutline">
			<div id="errorboxheader"> <?php echo $this->title; ?></div>
			<div id="errorboxbody">
			<p><strong><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></strong></p>
				<ol>
					<li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
					<li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
					<li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
					<li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
					<li><?php echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND'); ?></li>
					<li><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></li>
				</ol>
			<p><strong><?php echo JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES'); ?></strong></p>

				<ul>
					<li><a href="<?php echo $this->baseurl; ?>/index.php" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a></li>
					<li><a href="<?php echo $this->baseurl; ?>/index.php?option=com_search" title="<?php echo JText::_('JERROR_LAYOUT_SEARCH_PAGE'); ?>"><?php echo JText::_('JERROR_LAYOUT_SEARCH_PAGE'); ?></a></li>

				</ul>

			<p><?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?>.</p>
			<div id="techinfo">
			<p><?php echo $this->error->getMessage(); ?></p>
			<p>
				<?php if ($this->debug) :
					echo $this->renderBacktrace();
				endif; ?>
			</p>
			</div>
			</div>
		</div>
		</div>
	</div>
	
			</div>
		</div>
	</div>
	<div id="footer">
	    <div class="wrapper">
			<div class="holder">
			</div>
	    </div>
	</div>
	<div class="copyright-block">
	    <div class="wrapper">
			<p class="ee"><a href="http://www.electriceasel.com/" title="Chicago Web Design and Development">electric easel website design</a></p>
			<p>&copy; Copyright <?php echo date('Y'); ?> Everlasting Adoptions. All Rights Reserved - <a href="/privacy">Privacy Policy</a></p>
	    </div>
	</div>
<!-- Start AliveChat Tracking Code -->
	<script type="text/javascript">
	// <![CDATA[
	    function wsa_include_js(){
			var wsa_host = (("https:" == document.location.protocol) ? "https://" : "http://");
			var js = document.createElement('script');
			js.setAttribute('language', 'javascript');
			js.setAttribute('type', 'text/javascript');
			js.setAttribute('src',wsa_host + 'www.websitealive7.com/4702/Visitor/vTracker_v2.asp?websiteid=241&groupid=4702');
			document.getElementsByTagName('head').item(0).appendChild(js);
	    }
		if (window.attachEvent) {window.attachEvent('onload', wsa_include_js);}
		else if (window.addEventListener) {window.addEventListener('load', wsa_include_js, false);}
		else {document.addEventListener('load', wsa_include_js, false);}
		
		function launchWSA()
		{
			window.open('http://www.websitealive7.com/4702/rRouter.asp?groupid=4702&websiteid=241&departmentid=0&dl='+escape(document.location.href),'','width=400,height=400');
		}
	// ]]>
	</script>
<!-- End AliveChat Tracking Code -->
<script type="text/javascript">
// <![CDATA[
	function stopRKey(evt) { 
	    var evt = (evt) ? evt : ((event) ? event : null); 
	    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
	    if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
	}
	document.onkeypress = stopRKey;
// ]]>
</script>
</body>
</html>