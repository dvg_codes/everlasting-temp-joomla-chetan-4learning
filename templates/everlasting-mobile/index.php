<?php
/**
 * @package		Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

define('TEMPLATE_PATH', "/templates/{$this->template}/");
define('CSS_PATH', TEMPLATE_PATH.'css/');
define('IMG_PATH', TEMPLATE_PATH.'images/');
define('JS_PATH', TEMPLATE_PATH.'js/');

//JHtml::_('behavior.framework', true);
unset($this->_scripts['/media/system/js/caption.js']);
unset($this->_scripts['/media/system/js/mootools-core.js']);
unset($this->_scripts['/media/system/js/core.js']);
if (isset($this->_script['text/javascript']))
{
	$this->_script['text/javascript'] = preg_replace('%window\.addEvent\(\'load\',\s*function\(\)\s*{\s*new\s*JCaption\(\'img.caption\'\);\s*}\);\s*%', '', $this->_script['text/javascript']);
	if (empty($this->_script['text/javascript'])) {
		unset($this->_script['text/javascript']);
	}
}

JFactory::getDocument()
	// add stylesheets
	->addStyleSheet('/components/com_profiles/assets/css/sumoselect.min.css')
	->addStyleSheet(CSS_PATH.'mobile.css')
	// add scripts
	->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js')
	->addScript(JS_PATH.'jquery.touchwipe.min.js')
	->addScript('/components/com_profiles/assets/js/jquery.sumoselect.min.js')
	->addScript(JS_PATH.'jquery.main.js')
    ->addScript('/components/com_profiles/assets/js/jquery.infinitescroll.min.js')
	->addScriptDeclaration('
	// <![CDATA[
    (function(document,navigator,standalone) {
	    // prevents links from apps from oppening in mobile safari
	    // this javascript must be the first script in your <head>
	    if ((standalone in navigator) && navigator[standalone]) {
	        var curnode, location=document.location, stop=/^(a|html)$/i;
	        document.addEventListener("click", function(e) {
	            curnode=e.target;
	            while (!(stop).test(curnode.nodeName)) {
	                curnode=curnode.parentNode;
	            }
	            // Condidions to do this only on links to your own app
	            // if you want all links, use if("href" in curnode) instead.
	            if("href" in curnode && ( curnode.href.indexOf("http") || ~curnode.href.indexOf(location.host) ) ) {
	                e.preventDefault();
	                location.href = curnode.href;
	            }
	        },false);
	    }
	})(document,window.navigator,"standalone");
	window.addEventListener("load", function(){
		setTimeout(function(){
			window.scrollTo(0,1);
		}, 0);
	});
	// ]]>
	')
	->setTab("\t");

$is_home = false;
$menu = JSite::getMenu();
if ($menu->getActive() == $menu->getDefault()) {
   $bodyClasses[] = 'home';
   $is_home = true;
}

// set bodyClasses
$bodyClasses = array();
$uri = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : $_SERVER['REDIRECT_URL'];
$parts = explode('/', $uri);
foreach ($parts as $part) {
	if (empty($part)) continue;
	$bodyClasses[] = strtolower(str_replace('.html', '', $part));
}

$active = $menu->getActive();
$menuname = $active->title;
$parentId = $active->tree[0];

$phone = '866-406-2702';
if (in_array('pregnant', $bodyClasses)) {
	$pregnantSection = true;
	$phone = '888-398-6660';
}

?><!DOCTYPE html>
<html lang="<?=$this->language?>">
<head>
	<jdoc:include type="head" />
    <link rel="shortcut icon" href="/favicon.png" type="image/png" />
    <meta name="robots" content="index,follow" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
    <meta name="p:domain_verify" content="b0aed9dbf343f4df3dcbbbfa3045b127"/>
    <link href="https://plus.google.com/b/104392048981977095935/104392048981977095935" rel="publisher" />
	<script type="text/javascript">
	// <![CDATA[
    	var _gaq = _gaq || [];
    	_gaq.push(['_setAccount', 'UA-7033481-1']);
    	_gaq.push(['_trackPageview']);

	    (function() {
    	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	 })();
    // ]]>
	</script>
	<!--[if gte IE 9]>
	  <style type="text/css">
	    .gradient {
	       filter: none;
	    }
	  </style>
	<![endif]-->
</head>
<body class="<?php echo implode(' ', $bodyClasses); ?>">
	<div id="page-container">
		<div id="menu">
			<jdoc:include type="modules" name="mobile-nav" />
		</div>
		<div id="container">
			<div id="header">
				<a id="showMenu">Menu</a>
				<ul class="social-icon-list">
					<li><a class="social-icon facebook" target="_blank" href="https://www.facebook.com/EverlastingAdoptions?ref=hl"></a></li>
					<li><a class="social-icon twitter" target="_blank" href="https://twitter.com/everlastadopt"></a></li>
					<li><a class="social-icon pinterest" target="_blank" href="http://www.pinterest.com/babyadoption/"></a></li>
					<li><a class="social-icon google_plus" target="_blank" href="https://plus.google.com/104392048981977095935/about"></a></li>
				</ul>
				<a id="logo" href="/"></a>
			</div>
			<?php if (!$is_home) : ?>
			<div id="phone-top">
				<a id="phone-top" href="tel:1-<?php echo $phone; ?>">Call Anytime 1-<?php echo $phone; ?></a>
			</div>
			<div id="content-header">
				<jdoc:include type="modules" name="content-header" />
			</div>
			<?php endif; ?>
			<div id="main">
				<?php if ($is_home) : ?>
				<div class="homenav">
					<a class="nav-pregnant" href="/pregnant">Pregnant?<span>Considering Adoption</span></a>
					<a class="nav-hoping" href="/hoping-to-adopt">Hoping<span>to Adopt?</span></a>
					<a class="nav-waiting" href="/waiting-families">Families<span>Waiting to Adopt</span></a>
					<a class="nav-recent" href="/recent-adoptions">Recent<span>Adoptions</span></a>
					<div class="clear"></div>
				</div>
				<?php else: ?>
				<jdoc:include type="component" />
				<?php endif; ?>
			</div>
			<div id="above_footer">
				<a href="/pregnant">Pregnant? Start Here<span></span></a>
                <?php if (!$pregnantSection) : ?>
                <a href="/success-stories.html">Success Stories<span></span></a>
                <?php else : ?>
                    <a href="/waiting-families">Waiting Families<span></span></a>
                <?php endif; ?>
				<a href="tel:1-<?php echo $phone; ?>">Call Anytime 1-<?php echo $phone; ?><span></span></a>
				<?php if (!$pregnantSection) : ?>
				<a href="/free-application">Free Application<span></span></a>
				<?php endif; ?>
			</div>
			<div id="footer">
				<h4>Everlasting Adoptions</h4>
				<a href="tel:1-<?php echo $phone; ?>">1-<?php echo $phone; ?></a>
				<p>401 Wilshire Boulevard, 12th Floor<br/>Santa Monica, CA 90401</p>
				<p>Everlasting Adoptions is registered with the state of California under the provisions of the Registry of California Adoption Facilitators (CA Family Code Section 8632.5)</p>
				<a href="http://www.everlastingadoptions.com/?noMobile=true">Visit Full Site</a>
                <a target="_blank" id="bbblink" class="ruvtbum" href="http://www.bbb.org/losangelessiliconvalley/business-reviews/adoption-services/everlasting-adoptions-in-santa-monica-ca-100051741#bbbseal" title="Everlasting Adoptions, Inc., Adoption Services, Santa Monica, CA" style="display: block;position: relative;overflow: hidden; width: 60px; height: 108px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="http://seal-sanjose.bbb.org/logo/ruvtbum/everlasting-adoptions-100051741.png" width="120" height="108" alt="Everlasting Adoptions, Inc., Adoption Services, Santa Monica, CA" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-sanjose.bbb.org' + unescape('%2Flogo%2Feverlasting-adoptions-100051741.js') + "' type='text/javascript'%3E%3C/script%3E"));</script>
				<p class="copyright">&copy; <?php echo date('Y'); ?> Everlasting Adoptions. All Rights Reserved</p>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	var cloak = jQuery('.cloak');
	cloak.each(function(){
		var email = jQuery(this).attr('href').replace("_AT_", "@");
		jQuery(this).attr('href',email);
		var content = jQuery(this).html().replace("_AT_", "@");
		jQuery(this).html(content); 
	});
	</script>
</body>
</html>