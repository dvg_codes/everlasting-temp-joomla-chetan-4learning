jQuery.noConflict();
jQuery(document).ready(function ($){
	$(function () {
	    var menuStatus;
	    var menuWidth = "210px";
	    var contentContainer = $('#container');
	 
	    // Show menu
	    $("#showMenu").click(function (e) {
	    	e.preventDefault();
	        if (menuStatus != true) {
	            contentContainer.animate({
	                marginLeft: menuWidth,
	            }, 300, function () {
	                menuStatus = true
	            });
	            return false;
	        } else {
	            contentContainer.animate({
	                marginLeft: "0px",
	            }, 300, function () {
	                menuStatus = false
	            });
	            return false;
	        }
	    });
	    
	    contentContainer.click(function(e){
	        if (menuStatus) {
	        	e.preventDefault();
	            contentContainer.animate({
	                marginLeft: "0px",
	            }, 300, function () {
	                menuStatus = false;
	            });
	        }
	    });
	 
	   $('#header').touchwipe({
			min_move_x: 30,
			min_move_y: 30,
	    	wipeleft: function () {
		        if (menuStatus) {
		            contentContainer.animate({
		                marginLeft: "0px",
		            }, 300, function () {
		                menuStatus = false;
		            });
		        }
		    },
		    wipeRight: function() {
			    if (!menuStatus) {
				    contentContainer.animate({
					    marginLeft: menuWidth,
					}, 300, function() {
						menuStatus = true;
				    });
			    }
		    },
		    preventDefaultEvents: false
		        
	    });
	 
	    // Menu behaviour
	    $("#menu li a").click(function () {
	        var p = $(this).parent();
	        if ($(p).hasClass('active')) {
	            $("#menu li").removeClass('active');
	        } else {
	            $("#menu li").removeClass('active');
	            $(p).addClass('active');
	        }
	    });
	});
	var leadcf101 = $("#crmWebToEntityForm input[name=LEADCF101]");
	if (leadcf101.length > 0)
	{
		leadcf101.attr("checked", "checked");
	}
	var from_mobile = $("#from_mobile");
	if (from_mobile.length > 0)
	{
		from_mobile.val("true");
	}

    $('.sumoselect').SumoSelect();
});