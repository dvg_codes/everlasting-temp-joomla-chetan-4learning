<?php
//$doc = JFactory::getDocument();
//$doc->addStyleSheet('/components/com_profiles/assets/css/contact.css');
$sanity_check = rand();
if ($this->request_sent) :
?>
<div class="success">
	Your request has been sent.
</div>
<?php else: ?>
	<div class="form_wrapper" id="contact_form">
		<h3>Contact This Family</h3>
	    <form action="/index.php" method="post" name="contact_form">
	    	<fieldset class="formFieldset">
		    	<ol class="formContainer" style="list-style:none">
		            <li class="rsform-block">
		            	<div class="formCaption">
		            		<label>Name:</label>
		            	</div>
		            	<div class="formBody">
			            	<input class="required" type="text" name="jform[name]" value="" />
			            	<span class="formClr"></span>
		            	</div>
		            	<div class="formDescription"></div>
		            </li>
		            <li class="rsform-block">
		            	<div class="formCaption">
		            		<label>Email:</label>
		            	</div>
		            	<div class="formBody">
		            		<input class="required email" type="text" name="jform[email]" value="" />
			            	<span class="formClr"></span>
		            	</div>
		            	<div class="formDescription"></div>
		            </li>
		            <li class="rsform-block">
		            	<div class="formCaption">
		            		<label>Phone:</label>
		            	</div>
		            	<div class="formBody">
		            		<input class="required phone" type="text" name="jform[phone]" value="" />
			            	<span class="formClr"></span>
		            	</div>
		            	<div class="formDescription"></div>
		            </li>
		            <li class="rsform-block">
		            	<div class="formCaption">
		            		<label>Race of your baby:</label>
		            	</div>
		            	<div class="formBody">
		            		<select name="jform[baby_race]">
		            			<option value="Please Select">Race of your baby:</option><option value="African American">African American</option><option value="Asian">Asian</option><option value="Caucasian">Caucasian</option><option value="Caucasian / African American">Caucasian / African American</option><option value="Caucasian / Asaian">Caucasian / Asaian</option><option value="Caucasian / Hispanic">Caucasian / Hispanic</option><option value="Hispanic">Hispanic</option><option value="Hispanic / African American">Hispanic / African American</option><option value="Pacific Islander">Pacific Islander</option><option value="Other">Other</option>
		            		</select>
			            	<span class="formClr"></span>
		            	</div>
		            	<div class="formDescription"></div>
		            </li>
		            <li class="rsform-block">
		            	<div class="formCaption">
		            		<label>Due Date:</label>
		            	</div>
		            	<div class="formBody">
		            		<input class="required due_date" type="text" name="jform[due_date]" value="" />
			            	<span class="formClr"></span>
		            	</div>
		            	<div class="formDescription"></div>
		           	</li>
		            <li class="rsform-block">
		            	<div class="formCaption">
		            		<label>Message:</label>
		            	</div>
		            	<div class="formBody">
		            		<textarea name="jform[message]" cols="30" rows="5"></textarea>
			            	<span class="formClr"></span>
		            	</div>
		            	<div class="formDescription"></div>
		            </li>
		            <li class="rsform-block" style="text-align:center">
		            	<input type="submit" id="submit" value="Submit" />
		            </li>
				</ol>
		        <input id="spmck" style="display:none" type="text" name="jform[sanity_check]" value="<?php echo $sanity_check ?>" />
		        <input type="hidden" name="id" value="<?php echo $this->family->id; ?>" />
		        <input type="hidden" name="jform[check]" value="<?php echo $sanity_check ?>" />
		        <input type="hidden" name="option" value="com_profiles" />
		        <input type="hidden" name="task" value="send_contact" />
		        <input type="hidden" name="mobile" value="true" />
	    	</fieldset>
	    </form>
	</div>
<?php endif; ?>