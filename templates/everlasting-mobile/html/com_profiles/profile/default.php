<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// no direct access
defined('_JEXEC') or die;

$family = $this->family;

if ($family->state !== '1') {
	$app = JFactory::getApplication();
	$app->redirect(JRoute::_('index.php?option=com_profiles'), 'Not a valid profile.', 'error');
}

$img_path = '/uploads/profiles/'.$family->id.'/';
$main_image = $this->gallery[0]->path;

if (!$main_image) {
	$main_image = $family->about_us_image;
}

$fullname = $family->first_name;
if ($family->spouse_name) {
	ProfilesHelper::$single = false;
	$fullname .= ' & '.$family->spouse_name;
}
$title = $fullname . ' - ' . $doc->title;
if ($family->seo_title) {
	$title = $family->seo_title;
}
$fullname = htmlspecialchars($fullname);

$doc = JFactory::getDocument();
$doc->setTitle($title);
$doc->addScript();

if ($this->gallery) {
	$count = count($this->gallery);
	$i = 1;
	$photos = '';
	foreach($this->gallery as $photo)
	{
		$showing = "Image {$i} of {$count}";
		$photos .= PHP_EOL . "\t\t\t\t" . "{";
		$photos .= PHP_EOL . "\t\t\t\t\t" . "'href' : '/uploads/profiles/".$family->id."/".$photo->path."',";
		$photos .= PHP_EOL . "\t\t\t\t\t" . "'title': '<span class=\"name\">".$fullname."</span><span class=\"showing\">".$showing."</span>'";
		$photos .= PHP_EOL . "\t\t\t\t" . "},";
		$i++;
	}
	unset($i);
	$photos = rtrim($photos, ',');
	$doc->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js');
	$doc->addScript('/templates/everlasting-mobile/js/jquery.cycle.all.latest.min.js');
	$doc->addScript('/templates/everlasting-mobile/js/jquery.touchwipe.min.js');
	$doc->addScriptDeclaration("
	jQuery(document).ready(function($){
		var gallery = $('.touchwipe');
		
		gallery.cycle({
			fx: 'scrollHorz',
			timeout: 0,
			next: '',
			prev: '',
			speed: 300,
			nowrap: 0
		});
		
	    gallery.touchwipe({
	    	wipeLeft: function() {
	    		gallery.cycle('next');
		    },
		    wipeRight: function() {
		    	gallery.cycle('prev');
		    },
		    preventDefaultEvents: false
		        
	    });
	});
	");
}
?>
<div id="user_profile">
	<?php if (JRequest::getVar('request_sent', null) !== null) : ?>
	<div class="success">Thank you for contacting us.</div>
	<?php endif; ?>
	<?php if(JRequest::getVar('tmpl') !== 'component'): ?>
	<a class="contact" style="margin:auto 38px;" onclick="event.preventDefault();history.back();" href="<?php echo JRoute::_('index.php?option=com_profiles'); ?>">&laquo; back to waiting families</a>
	<?php endif; ?>
	<div class="links">
		<?php
		$videorel = '';
		if (strpos($family->video, 'yout')){
			$videorel = 'youtube';
		} elseif (strpos($family->video, 'vimeo')) {
			$videorel = 'vimeo';
		}
		
		echo $family->video ? '<a rel="'.$videorel.'" class="video" href="'.$family->video.'">our video</a>' : '';
		?>
	</div>
	<br />
	<h1>Adoption Profile</h1>
	<h2><?php echo $fullname; ?></h2>
	<span class="swipe-help">&lt; swipe &gt;</span>
	<div id="profile_top">
    	<div id="profile-photos">
    		<div class="touchwipe">
	    		<img class="main-photo" alt="<?php echo $fullname; ?>" src="<?php echo $img_path.'307_254_'.$main_image; ?>" />
	        	<?php if($this->gallery) {
		        	$i=1;
					foreach ($this->gallery as $photo) {
						if ($photo->path === $main_image) continue;
						echo ProfilesHelper::familyImage($photo->path, $family->id, '307_254_', false);
						$i++;
					}
				} ?>
    		</div>
        </div>
        <div class="buttons">
        	<a href="#contact_form" class="contact">contact</a>
        </div>
        <div id="quick_info">
        	<?php
        	if (isset($family->adopt_race)) {
        		$races = implode(', ', json_decode($family->adopt_race, true));
        		echo '<p><span>Race of child interested in adopting: </span>'.$races.'</p>';
        	}
        	if (isset($family->adopt_gender)) {
        		echo '<p><span>Gender of child interested in adopting: </span>'.$family->adopt_gender.'</p>';
        	}
			?>

        </div>
    </div>
    <div class="clear"></div>
	<div id="profile_tabs">
    	<?php if($family->dear_birthmother): ?>
    	<div class="section" id="dear_birthmother">
        	<h4>Dear Birthmother,</h4>
        	<?php
			echo '<p>'.ProfilesHelper::formatProfileText($family->dear_birthmother).'</p>';
			?>
		</div>
		<? endif; ?>
        
    	<?php if($family->about_us): ?>
		<hr />
    	<div class="section" id="about_us">
        	<h3>About <?php echo ProfilesHelper::meOrUs(); ?></h3>
        	<?php 
			echo ProfilesHelper::familyImage($family->about_us_image, $family->id, '307_254_', false); ?>
			        <div class="buttons">
        	<a href="#contact_form" class="contact">contact</a>
        </div>
			<?php echo '<p>'.ProfilesHelper::formatProfileText($family->about_us).'</p>';
			?>
		</div>
		<? endif; ?>
        
    	<?php if($family->our_home): ?>
		<hr />
		<div class="section" id="our_home">
        	<h3><?php echo ProfilesHelper::myOrOur(); ?> Home</h3>
    		<?php
			echo ProfilesHelper::familyImage($family->our_home_image, $family->id, '307_254_', false); ?>
			        <div class="buttons">
        	<a href="#contact_form" class="contact">contact</a>
        </div> <?php
			echo '<p>'.ProfilesHelper::formatProfileText($family->our_home).'</p>';
			?>
		</div>
		<? endif; ?>
        
    	<?php if($family->ext_family): ?>
		<hr />
		<div class="section" id="ext_family">
        	<h3><?php echo ProfilesHelper::myOrOur(); ?> Extended Family</h3>
        	<span class="swipe-help">&lt; swipe &gt;</span>
        	<div class="touchwipe">
        	<?php
			echo ProfilesHelper::familyImage($family->ext_family_image, $family->id, '307_254_', false);
			echo ProfilesHelper::familyImage($family->ext_family_image_spouse, $family->id, '307_254_', false); ?>
        	</div>
			        <div class="buttons">
        	<a href="#contact_form" class="contact">contact</a>
        </div> <?php
        	echo '<p>'.ProfilesHelper::formatProfileText($family->ext_family).'</p>';
        	?>
        </div>
        <? endif; ?>
        
    	<?php if($family->family_traditions): ?>
		<hr />
		<div class="section" id="family_traditions">
        	<h3><?php echo ProfilesHelper::myOrOur(); ?> Family Traditions</h3>
        	<?php
			echo ProfilesHelper::familyImage($family->family_traditions_image, $family->id, '307_254_', false); ?>
			        <div class="buttons">
        	<a href="#contact_form" class="contact">contact</a>
        </div> <?php
			echo '<p>'.ProfilesHelper::formatProfileText($family->family_traditions).'</p>';
			?>
		</div>
		<? endif; ?>
        
    	<?php if($family->adoption_story): ?>
		<hr />
		<div class="section" id="adoption_story">
        	<h3>What Led <?php echo ProfilesHelper::meOrUs(); ?> To Adoption</h3>
        	<?php
			echo ProfilesHelper::familyImage($family->adoption_story_image, $family->id, '307_254_', false); ?>
			       <?php
			echo '<p>'.ProfilesHelper::formatProfileText($family->adoption_story).'</p>';
			?>
		</div>
		<? endif; ?>
		
		<hr />
        <div class="section" id="favorites">
        	<div class="person">
            	<h3>Facts About <?php echo $family->first_name?></h3>
            	<ul id="my-facts">
            		<li><span>Occupation:</span> <span><?php echo htmlspecialchars($family->my_occupation);?></li>
            		<?php if($family->my_religion != '' || $family->spouse_religion != '') : ?>
            			<li><span>Religion:</span> <span><?php echo htmlspecialchars($family->my_religion);?></li>
            		<?php endif; ?>
            		<li><span>Education:</span> <span><?php echo htmlspecialchars($family->my_education);?></li>
            		<li><span>Favorite Food:</span> <span><?php echo htmlspecialchars($family->my_food);?></li>
            		<li><span>Favorite Hobby:</span> <span><?php echo htmlspecialchars($family->my_hobby);?></li>
            		<li><span>Favorite Movie:</span> <span><?php echo htmlspecialchars($family->my_movie);?></li>
            		<li><span>Favorite Sport:</span> <span><?php echo htmlspecialchars($family->my_sport);?></li>
            		<li><span>Favorite Holiday:</span> <span><?php echo htmlspecialchars($family->my_holiday);?></li>
            		<li><span>Favorite Music Group:</span> <span><?php echo htmlspecialchars($family->my_music_group);?></li>
            		<li><span>Favorite TV Show:</span> <span><?php echo htmlspecialchars($family->my_tv_show);?></li>
            		<li><span>Favorite Book:</span> <span><?php echo htmlspecialchars($family->my_book);?></li>
            		<li><span>Favorite Subject in School:</span> <span><?php echo htmlspecialchars($family->my_subject_in_school);?></li>
            		<li><span>Favorite Vacation Spot:</span> <span><?php echo htmlspecialchars($family->my_vacation_spot);?></li>
            	</ul>
            	<br class="clear"/>
            </div>
            <?php if(!$is_single) : ?>
        	<br/><div class="spouse">
            	<h3>Facts About <?php echo $family->spouse_name?></h3>
            	<ul id="spouse-facts">
            		<li><span>Occupation:</span> <span><?php echo htmlspecialchars($family->spouse_occupation);?></span></li>
            		<?php if($family->my_religion != '' || $family->spouse_religion != '') : ?>
	            		<li><span>Religion:</span> <span><?php echo htmlspecialchars($family->spouse_religion);?></span></li>
	            	<?php endif; ?>
            		<li><span>Education:</span> <span><?php echo htmlspecialchars($family->spouse_education);?></span></li>
            		<li><span>Favorite Food:</span> <span><?php echo htmlspecialchars($family->spouse_food);?></span></li>
            		<li><span>Favorite Hobby:</span> <span><?php echo htmlspecialchars($family->spouse_hobby);?></span></li>
            		<li><span>Favorite Movie:</span> <span><?php echo htmlspecialchars($family->spouse_movie);?></span></li>
            		<li><span>Favorite Sport:</span> <span><?php echo htmlspecialchars($family->spouse_sport);?></span></li>
            		<li><span>Favorite Holiday:</span> <span><?php echo htmlspecialchars($family->spouse_holiday);?></span></li>
            		<li><span>Favorite Music Group:</span> <span><?php echo htmlspecialchars($family->spouse_music_group);?></span></li>
            		<li><span>Favorite TV Show:</span> <span><?php echo htmlspecialchars($family->spouse_tv_show);?></span></li>
            		<li><span>Favorite Book:</span> <span><?php echo htmlspecialchars($family->spouse_book);?></span></li>
            		<li><span>Favorite Subject in School:</span> <span><?php echo htmlspecialchars($family->spouse_subject_in_school);?></span></li>
            		<li><span>Favorite Vacation Spot:</span> <span><?php echo htmlspecialchars($family->spouse_vacation_spot);?></span></li>
            	</ul>
            	<br class="clear"/>
        	</div>
            <?php endif;?>
            <div class="clear"></div>
        </div>
    </div>
	<hr />
	<div id="contact_form">
		<?php echo $this->loadTemplate('form'); ?>
	</div>
</div>