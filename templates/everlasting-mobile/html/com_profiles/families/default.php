<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// no direct access
defined('_JEXEC') or die;

$max = $this->pagination->total;

$infiniteJS = <<<IJS
    // <![CDATA[
    (function($)
    {
        $(document).ready(function()
        {
            var maxPage = ${max} / 5;
            $('#main').infinitescroll({
                navSelector  : ".pagination",
                nextSelector : ".link-next",
                itemSelector : ".waiting-families",
                path : function (pagenum) {
                    if(pagenum < maxPage + 1) {
                    pagenum--;
                    return '/waiting-families?start=' + pagenum * 5;
                    }
                    else {
                        return false;
                    }
                }
            });

        });
    })(jQuery);
    // ]]>
IJS;

JFactory::getDocument()->addScriptDeclaration($infiniteJS);

?>
<form class="family-search" action="<?php echo JRoute::_('index.php?option=com_profiles&view=families'); ?>" method="get">
	<input type="text" name="filter_search" placeholder="Search Families by Name" style="display:inline-block;" value="<?php echo $this->state->get('filter.search'); ?>" />
	<input type="submit" value="Search" style="margin-top:0;" />
	<input type="submit" name="reset" value="Clear Search" style="margin-top:0;" />
</form>
<?php if ($this->error) echo '<p class="error">' . $this->error . '</p>'; ?>
<h2 style="color:#2977A8">Families Waiting to Adopt</h2>
<ul class="waiting-families">
<?php foreach($this->families as $family) :
	
	switch ($family->profile_status) {
		case 'adopted':
			$family->link = 'javascript::void(0);';
			$family->link_text = "Adopted";
			$family->banner = '<span class="banner-adopted"></span>';
			break;
		case 'connected':
			$family->link = 'javascript::void(0);';
			$family->link_text = "Connected";
			$family->banner = '<span class="banner-connected"></span>';
			break;
		default:
			$family->link = JRoute::_('index.php?option=com_profiles&view=profile&id='.$family->id);
			$family->link_text = "Family Profile";
			$family->banner = null;
			break;
	}

	$main_image = $family->about_us_image;
	if (!$main_image) {
		$gallery = $this->getGalleryByFamilyID($family->id);
		$main_image = $gallery->path;
	}
	
	$intro = $family->about_us;
	if ($family->dear_birthmother) {
		$intro = $family->dear_birthmother;
	}
	$intro	= explode(' ', htmlspecialchars(substr(strip_tags($intro), 0, 190)));
	$last	= array_pop($intro);
	$intro	= implode(' ', $intro).'...';
	$fullname = $family->first_name;
	if ($family->spouse_name) {
		$fullname .= ' &amp; '. $family->spouse_name;
	}
	?>
	<li>
		<?php echo $family->banner; ?>
		<h3><a href="<?php echo $family->link; ?>"><?php echo $fullname; ?></a></h3>
		<a href="<?php echo $family->link; ?>">
		<?php
		if (is_file(JPATH_SITE.'/uploads/profiles/'.$family->id.'/307_254_'.$main_image)) {
			echo '<img width="272" style="width:272px" class="feature" src="/uploads/profiles/'.$family->id.'/307_254_'.$main_image.'" alt="'.$family->last_name.' Family" />';
		} else {
			echo '<img width="272" style="width:272px" class="feature" src="/images/comingsoon.jpg" alt="Adoption Profile Photo Coming Soon" />';
		}
		echo '</a>';
		?>
		<p><?php echo $intro; ?>&nbsp;</p>
		<a class="view_profile" href="<?php echo $family->link; ?>"><?php echo $family->link_text; ?></a>
	</li>	
<?php endforeach; ?>
</ul>
<div class="pagination">
	<?php echo $this->pagination->getPrevLink(); ?>
	<?php echo $this->pagination->getNextLink(); ?>
	<div class="clear"></div>
</div>