<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// no direct access
defined('_JEXEC') or die;
?>
<h2 style="color:#2977A8">Success Stories</h2>
<div class="pagination">
	<?php echo $this->pagination->getPrevLink(); ?>
	<span class="counter">
		<?php echo $this->pagination->getResultsCounter(); ?>
	</span>
	<?php echo $this->pagination->getNextLink(); ?>
	<div class="clear"></div>
</div>
<ul class="waiting-families">
<?php foreach($this->families as $family) : ?>
	<li class="fam-<?php echo $family->id; ?>">
		<h3><?php
	
			echo $family->first_name;
			if ($family->spouse_name) {
				echo ' &amp; '.$family->spouse_name;
			}
		
		?></h3>
		<?php
		
		if( is_file(JPATH_SITE.'/uploads/successes/'.$family->id.'/307_254_'.$family->success_story_image)) {
			echo '<img class="feature" src="/uploads/successes/'.$family->id.'/307_254_'.$family->success_story_image.'" alt="'.$family->last_name.' Family" />';
		} elseif( is_file(JPATH_SITE.'/uploads/successes/'.$family->id.'/'.$family->success_story_image)) {
            echo '<img class="feature" src="/uploads/successes/'.$family->id.'/'.$family->success_story_image.'" alt="'.$family->last_name.' Family" style="display:block;margin-left:auto;margin-right:auto;" />';
        } else {
			echo '<img class="feature" style="width:272px" src="/images/comingsoon.jpg" alt="Adoption Profile Photo Coming Soon" />';
		}
		
		if ($family->id == '132') {
			echo '<img class="feature2" src="/images/traci-tom-baby2.jpg" alt="" />';
		}
		?>
		<div class="success-story"><p><?php echo nl2br($family->success_story) ?></p></div>
        <a class="free-application-button" href="/free-application">Start Your Journey Now<span></span></a>
		<div class="clear"></div>
	</li>	
<?php endforeach; ?>
</ul>
<div class="pagination">
	<?php echo $this->pagination->getPrevLink(); ?>
	<span class="counter">
		<?php echo $this->pagination->getResultsCounter(); ?>
	</span>
	<?php echo $this->pagination->getNextLink(); ?>
	<div class="clear"></div>
</div>