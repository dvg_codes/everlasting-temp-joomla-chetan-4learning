<?php
// no direct access
defined('_JEXEC') or die;

JLoader::register('ProfilesControllerProfiles', JPATH_SITE . '/components/com_profiles/controllers/profiles.php');

class ModFavoritesHelper
{
	protected $data = array();

	public function __construct($params = null, $module = null)
	{
		$this->params = $params;
		$this->module = $module;
		$this->data['module_class_sfx'] = htmlspecialchars($this->params->get('moduleclass_sfx'));
		$this->data['items'] = $this->getItems();
	}

	public function render()
	{
		extract($this->data);

		require JModuleHelper::getLayoutPath('mod_favorites', $this->params->get('layout', 'default'));

	}

	protected function getItems()
	{
		$items = JFactory::getSession()->get('favorites', array(), 'profiles');

		foreach ($items as $key => $item)
		{
			$item->html = ProfilesControllerProfiles::buildHtml($item->id);
		}

		return $items;
	}
}
