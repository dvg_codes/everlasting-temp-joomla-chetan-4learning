jQuery(document).ready(function ($) {
	$('a.add_fav').live('click', function () {
		var base   = $(this),
			baseId = base.attr('id'),
			addId  = baseId.split('-');

		$.post('/index.php',
			{
				option: 'com_profiles',
				tmpl:   'component',
				task:   'profiles.addFavorite',
				id:     addId[1]
			},
			function (resp) {
				if (resp.type == 'success') {
					$('.favorites .contents .intro').hide();
					$(resp.html).hide().appendTo('.favorites .contents').fadeIn(500);
				}
			},
			'json'
		);
	});

	$('.favorites a.rm_fav').live('click', function () {
		var base   = $(this),
			baseId = base.attr('id'),
			rmId   = baseId.split('-');

		$.post('/index.php',
			{
				option: 'com_profiles',
				tmpl:   'component',
				task:   'profiles.delFavorite',
				id:     rmId[1]
			},
			function (resp) {
				if (resp.type == 'success') {
					$('#remove-' + resp.id).parent('.favorite').remove();
				}
			},
			'json'
		);
	});
});
