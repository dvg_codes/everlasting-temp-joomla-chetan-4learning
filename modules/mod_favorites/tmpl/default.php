<?php defined('_JEXEC') or die; ?>
<div class="favorites <?php echo $module_class_sfx ?>">
	<h3>favorites</h3>
	<div class="intro">
		<p style="font-size:11px;padding-bottom:0;text-align:center;">Click "Add to Favorites" on a profile to save a quick link for later.</p>
		<p style="text-align:center;"><a href="/waiting-families">View Waiting Families</a></p>
	</div>
	<div class="contents">
		<?php foreach ($items as $item) echo $item->html; ?>
	</div>
</div>
