<?php
// no direct access
defined('_JEXEC') or die;

JFactory::getDocument()
	//->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js')
	->addScript('/components/com_profiles/assets/js/jquery.js')
	->addScript('/modules/mod_favorites/assets/js/favorites.js')
	->addStyleSheet('/modules/mod_favorites/assets/css/favorites.css');

JLoader::register('ModFavoritesHelper', dirname(__FILE__) . '/helper.php');

$mod = new ModFavoritesHelper($params, $module);

$mod->render();
