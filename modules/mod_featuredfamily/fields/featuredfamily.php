<?php
class JFormFieldFeaturedFamily extends JFormField
{
   protected $type = 'featuredfamily';
   protected function getInput()
   {
      $db = JFactory::getDBO();
      $query = "SELECT first_name,last_name,spouse_name,id FROM #__profiles_families where state =1";
      $db->setQuery($query);
      $categories = $db->loadObjectList();
      $var_list ='';
      foreach($categories as $category){
        // $var_list.= '<li><input name="'.$this->name.'[]" type="checkbox" value="'.$category->id.'"><label for="jform_category_id'.$category->id.'">'.$category->title.'</label></li>';
         $var_list.= '<option value="'.$category->id.'">"'.$category->first_name.' '.$category->last_name.' "</option>';
      }
      return '<select>'.$var_list.'</select>';
   }
}
?>