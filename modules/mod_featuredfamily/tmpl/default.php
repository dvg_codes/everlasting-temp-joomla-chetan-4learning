<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_footer
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<ul class="waiting-families" style="">
<?php
 	$db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$query->select('*');
	$query->from($db->quoteName('#__profiles_families'));
	$query->where($db->quoteName('id')." = ".$db->quote($params->get('family')));
	$db->setQuery($query);
	$family = $db->loadObject(); 
  	switch ($family->profile_status) {
		case 'adopted':
			$family->link = 'javascript::void(0);';
			$family->link_text = "Successfully Adopted";
			$family->banner = '<span class="banner-adopted"></span>';

			if ($family->spouse_name) {
				$family->banner = '<span class="banner-adopted"></span>';
			}
			else {
				$family->banner = '<span class="banner-i-adopted"></span>';
			}
			break;
		case 'connected':
			$family->link = 'javascript::void(0);';
			$family->link_text = "Currently Connected";
			$family->banner = '<span class="banner-connected"></span>';
			break;
		default:
			$family->link = JRoute::_('index.php?option=com_profiles&view=profile&id='.$family->id);
			$family->link_text = "View Profile";
			$family->banner = null;
			break;
	}  

	$main_image = $family->about_us_image;
	if (!$main_image) {
		//$gallery = $this->getGalleryByFamilyID($family->id);
		$main_image = $gallery->path;
	}

	$intro = $family->about_us;
	if ($family->dear_birthmother) {
		$intro = $family->dear_birthmother;
	}
	?>
	<li class="featured-family">
		<?php echo $family->banner; ?>
		<a name="profile-<?php echo $family->id; ?>"></a>
		<?php
		echo '<a href="'.$family->link.'">';
		if (is_file(JPATH_SITE.'/uploads/profiles/'.$family->id.'/165_120_'.$main_image)) {
			echo '<img class="feature" src="/uploads/profiles/'.$family->id.'/165_120_'.$main_image.'" alt="'.$family->last_name.' Family" />';
		} else {
			echo '<img class="feature" src="/images/comingsoon.jpg" alt="Adoption Profile Photo Coming Soon" />';
		}
		echo '</a>';
		?>
		<h3><?php

			echo '<a href="'.$family->link.'"><span class="featuredtext">Featured Family:</span> '.$family->first_name;
			if ($family->spouse_name) {
				echo ' &amp; '.$family->spouse_name;
			}
			echo '</a>';

		?></h3>
		<p><?php echo htmlspecialchars(substr($intro, 0, 400)); ?>…&nbsp;<a href="<?php echo $family->link; ?>">&raquo; View Profile</a></p>
		<div class="clear"></div>
	</li>
</ul>
</ul>

<style>
.featured-family > p {
    margin-bottom: 0 !important;
}
.featured-family {
    background: #e7f8ff none repeat scroll 0 0;
    border: 2px solid #93badc !important;
    border-radius: 8px;
    padding: 5px !important;
}
</style>
