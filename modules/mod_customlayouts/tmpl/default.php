<div id="crmWebToEntityForm" align="center"><form accept-charset="UTF-8" action="https://crm.zoho.com/crm/WebToLeadForm" method="post" name="WebToLeads514698000000052001" onsubmit="javascript:document.charset=&quot;UTF-8&quot;; return checkMandatery()"><input name="LEADCF101" type="checkbox" style="display: none;" /> <input name="xnQsjsdp" type="hidden" value="kAkKqOdasFs$" /> <input name="xmIwtLD" type="hidden" value="Xl7szadpMU2fdquktJW8*G-*B6-MHnNR" /> <input name="actionType" type="hidden" value="TGVhZHM=" /> <input name="returnURL" type="hidden" value="http://www.everlastingadoptions.com/free-application/thank-you" />
<div class="section">
<h6>General Information</h6>
<div class="field"><label>First Name:</label> <input maxlength="40" name="First Name" type="text" /></div>
<div class="field"><label>Last Name:</label> <input maxlength="80" name="Last Name" type="text" /></div>
<div class="field"><label>Spouse's First Name:</label> <input maxlength="50" name="LEADCF26" type="text" /></div>
<div class="field"><label>Spouse's Last Name:</label> <input maxlength="50" name="LEADCF28" type="text" /></div>
<div class="field"><label>Street Address:</label> <input maxlength="50" name="LEADCF33" type="text" /></div>
<div class="field"><label>City:</label> <input maxlength="30" name="City" type="text" /></div>
<div class="field"><label>State:</label><select name="LEADCF32"><option value="-None-">-None-</option><option value="AL">AL</option><option value="AK">AK</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="FL">FL</option><option value="GA">GA</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PA">PA</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select></div>
<div class="field"><label>Zip:</label> <input maxlength="5" name="LEADCF42" type="text" /></div>
<div class="field"><label>Email:</label> <input maxlength="100" name="Email" type="text" /></div>
<div class="field"><label>Home Phone:</label> <input maxlength="15" name="LEADCF43" type="text" /></div>
<div class="field"><label>Your Cell Phone:</label> <input maxlength="15" name="LEADCF35" type="text" /></div>
<div class="field"><label>Spouse's Cell Phone:</label> <input maxlength="15" name="LEADCF22" type="text" /></div>
<div class="field hide-mobile"><label>Best time to reach you:</label><select name="LEADCF1"><option value="-None-">-None-</option><option value="Morning">Morning</option><option value="Afternoon">Afternoon</option><option value="Evening">Evening</option></select></div>
<div class="field"><label>How did you hear about Everlasting Adoptions?</label>
<select name="LEADCF20">
	<option value="-None-">-None-</option>
	<option value="Google Search">Google Search</option>
	<option value="Facebook">Facebook</option>
	<option value="Other Internet Search">Other Internet Search</option>
	<option value="Newspaper Ad">Newspaper Ad</option>
	<option value="Family/Friends">Family/Friends</option>
	<option value="Church Bulletin">Church Bulletin</option>
	<option value="Other">Other</option>
	






</select>
</div>
<div class="field"><label>When would you be financially ready to start the adoption process?</label><select name="LEADCF47"><option value="-None-">-None-</option><option value="Today">Today</option><option value="3-6 Months">3-6 Months</option><option value="6 Months - 1 Year">6 Months - 1 Year</option><option value="1 Year+">1 Year+</option></select></div>
</div>
<div class="section">
<h6>Type of Child You Wish To Adopt</h6>
<div class="field hide-mobile"><label>Preferred Gender:</label><select name="LEADCF45"><option value="-None-">-None-</option><option value="Girl">Girl</option><option value="Boy">Boy</option><option value="No Preference">No Preference</option></select></div>
<div class="field"><label>Preferred Race of Baby:</label><select multiple="multiple" name="LEADCF46" class="sumoselect"><option value="Caucasian">Caucasian</option><option value="African American">African American</option><option value="Hispanic">Hispanic</option><option value="Asian">Asian</option><option value="Native American">Native American</option><option value="African American / Caucasian">African American / Caucasian</option><option value="Hispanic / Caucasian">Hispanic / Caucasian</option><option value="Hispanic / Other">Hispanic / Other</option><option value="Asian / Caucasian">Asian / Caucasian</option><option value="No Preference">No Preference</option><option value="Other">Other</option></select></div>
<div class="clear hide-mobile" style="font-style: italic;">(Ctrl + click to select multiple races)</div>
</div>
<div class="section">
<h6>Employment History</h6>
<div class="field"><label>Your Occupation:</label> <input maxlength="100" name="LEADCF40" type="text" /></div>
<div class="field"><label>Spouse's Occupation:</label> <input maxlength="100" name="LEADCF29" type="text" /></div>
</div>
<div class="section">
<h6>General Descriptions</h6>
<div class="field"><label>Your Date of Birth:</label> <input maxlength="20" name="LEADCF37" type="text" /></div>
<div class="field"><label>Your Race:</label><select name="LEADCF122"><option value="-None-">-None-</option><option value="African American">African American</option><option value="Asian">Asian</option><option value="Caucasian">Caucasian</option><option value="Hispanic">Hispanic</option><option value="Latino">Latino</option><option value="Native American">Native American</option></select></div>
<!--<div class="field hide-mobile">
				<label>Your Hobbies, Talents and Interests:</label>
				<textarea name='LEADCF39' rows="5" cols="85"></textarea>
			</div>
			<div class="field hide-mobile">
				<label>Your Community Service:</label>
				<textarea name='LEADCF36' rows="5" cols="85"></textarea>
			</div>-->
<div class="field hide-mobile"><label>Your Religion:</label> <input maxlength="50" name="LEADCF49" type="text" /></div>
<div class="field"><label>Spouse's Date of Birth:</label> <input maxlength="20" name="LEADCF24" type="text" /></div>
<div class="field"><label>Spouse's Race:</label><select name="LEADCF30"><option value="-None-">-None-</option><option value="African American">African American</option><option value="Asian">Asian</option><option value="Caucasian">Caucasian</option><option value="Hispanic">Hispanic</option><option value="Latino">Latino</option><option value="Native American">Native American</option></select></div>
<!--<div class="field hide-mobile">
				<label>Spouse's Hobbies, Talents and Interests:</label>
				<textarea name='LEADCF27' rows="5" cols="85"></textarea>
			</div>
			<div class="field hide-mobile">
				<label>Spouse's Community Service / Activities:</label>
				<textarea name='LEADCF23' rows="5" cols="85"></textarea>
			</div>--></div>
<div class="section hide-mobile">
<h6>Marital History</h6>
<div class="field"><label>Date of Marriage:</label> <input maxlength="20" name="LEADCF15" type="text" /></div>
</div>
<div class="section hide-mobile">
<h6>Education</h6>
<div class="field"><label>Your Education Level:</label><select name="LEADCF38"><option value="-None-">-None-</option><option value="High School Not Graduated">High School Not Graduated</option><option value="High School Graduated / GED">High School Graduated / GED</option><option value="Some College Credit Hours">Some College Credit Hours</option><option value="Associates Degree">Associates Degree</option><option value="Professional / Vocational">Professional / Vocational</option><option value="Bachelors Degree">Bachelors Degree</option><option value="Masters Degree">Masters Degree</option><option value="Doctoral Degree">Doctoral Degree</option><option value="Postdoctoral">Postdoctoral</option><option value="None">None</option></select></div>
<div class="field"><label>Your Year Graduated:</label> <input maxlength="20" name="LEADCF41" type="text" /></div>
<div class="field"><label>Spouse's Education Level:</label><select name="LEADCF25"><option value="-None-">-None-</option><option value="High School Not Graduated">High School Not Graduated</option><option value="High School Graduated / GED">High School Graduated / GED</option><option value="Some College Credit Hours">Some College Credit Hours</option><option value="Associates Degree">Associates Degree</option><option value="Professional / Vocational">Professional / Vocational</option><option value="Bachelors Degree">Bachelors Degree</option><option value="Masters Degree">Masters Degree</option><option value="Doctoral Degree">Doctoral Degree</option><option value="Postdoctoral">Postdoctoral</option><option value="None">None</option></select></div>
<div class="field"><label>Spouse's Year Graduated:</label> <input maxlength="20" name="LEADCF31" type="text" /></div>
</div>
<div class="section">
<h6>Financial History</h6>
<div class="field"><label>Combined, Total, Annual Household Income:</label><select name="LEADCF19" required=""><option value="">-None-</option><option value="Less than $50,000">Less than $50,000</option><option value="$50,000 - $75,000">$50,000 - $75,000</option><option value="$75,000 - $100,000">$75,000 - $100,000</option><option value="$100,000 - $150,000">$100,000 - $150,000</option><option value="$150,000+">$150,000+</option></select></div>
<div class="field hide-mobile"><label>Own or Rent?</label><select name="LEADCF121"><option value="-None-">-None-</option><option value="Own">Own</option><option value="Rent">Rent</option></select></div>
</div>
<div class="section hide-mobile">
<h6>Children <br /><span>Please list the children in your family and indicate if the child is adopted or if the child is biological.</span></h6>
<div class="field"><label>Child 1 Adopted or Biological:</label><select name="LEADCF2"><option value="-None-">-None-</option><option value="Biological">Biological</option><option value="Adopted">Adopted</option></select></div>
<div class="field"><label>Child 1 Age:</label> <input maxlength="2" name="LEADCF3" type="text" /></div>
<div class="field"><label>Child 1 Gender:</label><select name="LEADCF4"><option value="-None-">-None-</option><option value="Boy">Boy</option><option value="Girl">Girl</option></select></div>
<div class="field"><label>Child 2 Adopted or Biological:</label><select name="LEADCF5"><option value="-None-">-None-</option><option value="Biological">Biological</option><option value="Adopted">Adopted</option></select></div>
<div class="field"><label>Child 2 Age:</label> <input maxlength="2" name="LEADCF6" type="text" /></div>
<div class="field"><label>Child 2 Gender:</label><select name="LEADCF7"><option value="-None-">-None-</option><option value="Boy">Boy</option><option value="Girl">Girl</option></select></div>
<div class="field"><label>Child 3 Adopted or Biological:</label><select name="LEADCF8"><option value="-None-">-None-</option><option value="Biological">Biological</option><option value="Adopted">Adopted</option></select></div>
<div class="field"><label>Child 3 Age:</label> <input maxlength="2" name="LEADCF9" type="text" /></div>
<div class="field"><label>Child 3 Gender:</label><select name="LEADCF10"><option value="-None-">-None-</option><option value="Boy">Boy</option><option value="Girl">Girl</option></select></div>
<div class="field"><label>Child 4 Adopted or Biological:</label><select name="LEADCF11"><option value="-None-">-None-</option><option value="Biological">Biological</option><option value="Adopted">Adopted</option></select></div>
<div class="field"><label>Child 4 Age:</label> <input maxlength="2" name="LEADCF12" type="text" /></div>
<div class="field"><label>Child 4 Gender:</label><select name="LEADCF13"><option value="-None-">-None-</option><option value="Boy">Boy</option><option value="Girl">Girl</option></select></div>
</div>
<div class="section">
<h6>Personal History</h6>
<div class="field"><label>Declared Bankruptcy?</label> <input name="LEADCF106" type="checkbox" /></div>
<div class="field hide-mobile"><label>Been under psychiatric care?</label> <input name="LEADCF102" type="checkbox" /></div>
<div class="field hide-mobile"><label>Dishonorable discharge from the military?</label> <input name="LEADCF104" type="checkbox" /></div>
<div class="field"><label>Been arrested?</label> <input name="LEADCF109" type="checkbox" /></div>
<div class="field hide-mobile"><label>Placed a child for adoption?</label> <input name="LEADCF110" type="checkbox" /></div>
<div class="field hide-mobile"><label>Past due on court ordered child support?</label> <input name="LEADCF108" type="checkbox" /></div>
<!--<div class="field">
				<label>If you check any of the above, please explain:</label>
				<textarea name='LEADCF17' rows="5" cols="85"></textarea>
			</div>-->
<div class="field hide-mobile"><label>Reasons for wanting to adopt:</label> <textarea cols="85" name="LEADCF48" rows="5"></textarea></div>
</div>
<!--<div class="section hide-mobile">
			<h6>General Lifestyle Information</h6>
			<div class="field">
				<label>Describe your neighborhood and home:</label>
				<textarea name='LEADCF16' rows="5" cols="85"></textarea>
			</div>
			<div class="field">
				<label>Describe your pastimes and hobbies:</label>
				<textarea name='LEADCF34' rows="5" cols="85"></textarea>
			</div>
			<div class="field">
				<label>How will your life change after adoption?</label>
				<textarea name='LEADCF21' rows="5" cols="85"></textarea>
			</div>
		</div>-->
<div class="section hide-mobile">
<h6>Home Study<span class="hide-mobile"> &amp; Attorneys</span><br /><span class="hide-mobile">If you do not have a home study we can provide a contact for you.</span></h6>
<div class="field"><label>Have you completed a home study?</label><select name="LEADCF18"><option value="-None-">-None-</option><option value="Yes">Yes</option><option value="No">No</option></select></div>
<div class="field"><label>If yes, completed by?</label> <input maxlength="50" name="LEADCF14" type="text" /></div>
<div class="field"><label>Are you signed up with other adoption agencies or adoption consultants?</label> <input name="LEADCF103" type="checkbox" /></div>
<div class="field"><label>If yes, please provide the names and numbers of the other agencies:</label> <textarea cols="85" name="LEADCF50" rows="5"></textarea></div>
</div>
<div id="ajaxUploadContainer" class="section hide-mobile">
<h6>Photo</h6>
<div class="field"><label>Please upload a photo (jpg, gif, png):</label>
<div id="ajaxUpload"><span>Choose File</span></div>
<input name="LEADCF44" type="text" /></div>
</div>
<div class="section">
<h6>Read &amp; Submit</h6>

<p>Please note: We will contact you within one to three days of receipt of your complete application. Contact will be made by email or phone. If you have trouble uploading a photo, please consider emailing it to: <a href="mailto:sheila@everlastingadoptions.com">sheila@everlastingadoptions.com</a> with the Subject Line:  Photo for  Application.  If you do not hear from us, please email us at <a href="mailto:info@everlastingadoptions.com">info@everlastingadoptions.com</a>.  </p>

<div class="hide-mobile"><strong>Everlasting Adoptions</strong><br /> 401 Wilshire Blvd<br /> 12th Floor<br /> Santa Monica, CA 90401<br /><br />
<p>If you have any questions, please email us at: <a href="mailto:info@everlastingadoptions.com">info@everlastingadoptions.com</a></p>
<p>Thank you for your interest in Everlasting Adoptions.</p>
</div>
<p style="text-align: center; display: inline-block;"><input type="submit" value="Submit Application" onclick="_gaq.push(['_trackPageview, '/app_sub']);" /></p>
<p class="disclaimer">This is an application, not a contract. There is no obligation by either party. Vital statistics are confidential and are not shared with birthparents. By submitting this application you admit that your answers are true and correct. For more information, please read our privacy statement.</p>
</div>
</form></div>
<script type="text/javascript" src="/components/com_profiles/assets/js/ajaxupload.js"></script>
<script type="text/javascript">
// <![CDATA[
var mndFields=new Array('First Name','Last Name');
var fldLangVal=new Array('First Name','Last Name');
function checkMandatery() {
    for(i=0;i<mndFields.length;i++){
        var fieldObj=document.forms['WebToLeads514698000000052001'][mndFields[i]];
        if((fieldObj) && ((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0){
            alert(fldLangVal[i] +' cannot be empty');
            fieldObj.focus();
            return false;
        } else if(fieldObj && (fieldObj.nodeName=='SELECT') && (fieldObj.options[fieldObj.selectedIndex].value=='-None-')){
            alert(fldLangVal[i] +' cannot be none');
            fieldObj.focus();
            return false;
        }
    }
}
jQuery(document).ready(function($){
    $('input[name="LEADCF44"]').hide();
    var button = $('#ajaxUpload'), interval;
    new AjaxUpload(button, {
        action: '/index.php?option=com_profiles&tmpl=component&task=upload', 
        name: 'ajaxUpload',
        onSubmit: function(file, ext){
        // change button text, when user selects file           
            button.text('Uploading');
             
            // If you want to allow uploading only 1 file at time,
            // you can disable upload button
            this.disable();
     
            // Uploding -> Uploading. -> Uploading...
            interval = window.setInterval(function(){
                var text = button.text();
                if (text.length < 13){
                    button.text(text + '.');                    
                } else {
                    button.text('Uploading');               
                }
            }, 200);
        },
        onComplete: function(file, response){
            var resp = $.parseJSON(response);
            console.log(file);
            console.log(resp);
     
            button.text('Uploaded');
     
            window.clearInterval(interval);
     
            // enable upload button
            this.enable();
 
            // add thumbnail for preview
            var img = document.createElement('img');
            img.src = resp.filepath;
            jQuery('#ajaxUploadContainer').append(img);
     
            // add full path to form input
            var file_path = 'http://www.everlastingadoptions.com' + resp.filepath;
            var file_path = file_path.replace('thumb_', 'full_');
            $('input[name="LEADCF44"]').val(file_path);
        }
    });
});
// ]]>
</script>