<!-- Note :
   - You can modify the font style and form style to suit your website. 
   - Code lines with comments “Do not remove this code”  are required for the form to work properly, make sure that you do not remove these lines of code. 
   - The Mandatory check script can modified as to suit your business needs. 
   - It is important that you test the modified form before going live.-->
<div id='crmWebToEntityForm' style='width:600px;margin:auto;'>
   <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
   <form action='https://crm.zoho.com/crm/WebForm'  name=WebForm514698000010745185 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory()' accept-charset='UTF-8'>

	 <!-- Do not remove this code. -->
	<input type='text' style='display:none;' name='xnQsjsdp' value='e96106276d95d8c6f9a9232756173c2716e08961adbc2d271505b03fa7a17503'/>
	<input type='hidden' name='zc_gad' id='zc_gad' value=''/>
	<input type='text' style='display:none;' name='xmIwtLD' value='1e33d7a34f6ac0310e895bb72e6ac0595c4a52fcc2c4fc7882dd2fe2a087978e'/>
	<input type='text' style='display:none;'  name='actionType' value='Q3VzdG9tTW9kdWxlNQ=='/>

	<input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;www.everlastingadoptions.com&#x2f;waiting-families.html' /> 
	 <!-- Do not remove this code. -->
	<style>
		tr , td { 
			padding:6px;
			border-spacing:0px;
			border-width:0px;
			}
	</style>
	<table style='width:600px;background-color:white;color:black'>

	<tr><td colspan='2' style='text-align:left;color:black;font-family:Arial;font-size:14px;'><strong>Birth Mother Information Form</strong></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Please Choose All that Apply</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF189' multiple>
			<option value='I&#x20;am&#x20;just&#x20;starting&#x20;my&#x20;adoption&#x20;search&#x20;and&#x20;am&#x20;looking&#x20;for&#x20;information.'>I am just starting my adoption search and am looking for information.</option>
			<option value='I&#x20;would&#x20;like&#x20;to&#x20;speak&#x20;with&#x20;an&#x20;adoption&#x20;consultant.'>I would like to speak with an adoption consultant.</option>
			<option value='I&#x20;am&#x20;sure&#x20;about&#x20;my&#x20;adoption&#x20;decision.'>I am sure about my adoption decision.</option>
			<option value='I&#x20;would&#x20;like&#x20;a&#x20;full&#x20;packet&#x20;of&#x20;information.'>I would like a full packet of information.</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>First Name<span style='color:red;'>*</span></td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='120' name='NAME' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Last Name</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='255' name='COBJ5CF7' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>What is your preferred method of communication&#x3f;<span style='color:red;'>*</span></td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF2'>
			<option value='-None-'>-None-</option>
			<option value='Email'>Email</option>
			<option value='Text'>Text</option>
			<option value='Phone'>Phone</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Would you like us to call from a blocked number&#x3f;</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF180'>
			<option value='-None-'>-None-</option>
			<option value='Yes'>Yes</option>
			<option value='No'>No</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>What is the best time to contact you&#x3f;</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF185'>
			<option value='-None-'>-None-</option>
			<option value='Morning'>Morning</option>
			<option value='Afternoon'>Afternoon</option>
			<option value='Evening'>Evening</option>
			<option value='Anytime'>Anytime</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Email</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='100' name='Email' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Phone</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='30' name='COBJ5CF8' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Would you like a confidential information packet&#x3f;</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF188'>
			<option value='-None-'>-None-</option>
			<option value='Yes'>Yes</option>
			<option value='No'>No</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Address 1</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='255' name='COBJ5CF9' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Address 2</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='255' name='COBJ5CF10' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>City</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='255' name='COBJ5CF3' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>State<span style='color:red;'>*</span></td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='255' name='COBJ5CF4' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Zip Code</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='9' name='COBJ5CF54' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Interested in traditional couple&#x3f;</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF162'>
			<option value='-None-'>-None-</option>
			<option value='Yes'>Yes</option>
			<option value='No'>No</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Interested in financially stable single woman&#x3f;</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF164'>
			<option value='-None-'>-None-</option>
			<option value='Yes'>Yes</option>
			<option value='No'>No</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Interested in same sex couple&#x3f;</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF166'>
			<option value='-None-'>-None-</option>
			<option value='Yes'>Yes</option>
			<option value='No'>No</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>How old are you&#x3f;</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='9' name='COBJ5CF51' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Baby Due Date</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='20' name='COBJ5CF81' placeholder='MM/dd/yyyy' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Baby Sex</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF28'>
			<option value='-None-'>-None-</option>
			<option value='Boy'>Boy</option>
			<option value='Girl'>Girl</option>
			<option value='Unkniown'>Unkniown</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>What is your race&#x3f;</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF186'>
			<option value='-None-'>-None-</option>
			<option value='Caucasian'>Caucasian</option>
			<option value='African&#x20;American'>African American</option>
			<option value='Hispanic'>Hispanic</option>
			<option value='Asian'>Asian</option>
			<option value='Caucasian&#x2f;African&#x20;American&#x20;Mix'>Caucasian&#x2f;African American Mix</option>
			<option value='Caucasian&#x2f;Hispanic&#x20;Mix'>Caucasian&#x2f;Hispanic Mix</option>
			<option value='Hispanic&#x2f;African&#x20;American&#x20;Mix'>Hispanic&#x2f;African American Mix</option>
			<option value='Caucasian&#x2f;Asian&#x20;Mix'>Caucasian&#x2f;Asian Mix</option>
			<option value='Other'>Other</option>
			<option value='Unknown&#x20;Mix'>Unknown Mix</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>What is the birth father&#x27;s race&#x3f;</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF187'>
			<option value='-None-'>-None-</option>
			<option value='Caucasian'>Caucasian</option>
			<option value='African&#x20;American'>African American</option>
			<option value='Hispanic'>Hispanic</option>
			<option value='Asian'>Asian</option>
			<option value='Caucasian&#x2f;African&#x20;American&#x20;Mix'>Caucasian&#x2f;African American Mix</option>
			<option value='Caucasian&#x2f;Hispanic&#x20;Mix'>Caucasian&#x2f;Hispanic Mix</option>
			<option value='Hispanic&#x2f;African&#x20;American&#x20;Mix'>Hispanic&#x2f;African American Mix</option>
			<option value='Caucasian&#x2f;Asian&#x20;Mix'>Caucasian&#x2f;Asian Mix</option>
			<option value='Other'>Other</option>
			<option value='Unknown&#x20;Mix'>Unknown Mix</option>
			<option value='Race&#x20;Unknown'>Race Unknown</option>
		</select></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>How did you find us&#x3f;<span style='color:red;'>*</span></td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ5CF154'>
			<option value='-None-'>-None-</option>
			<option value='Google&#x20;Search'>Google Search</option>
			<option value='Other&#x20;Internet&#x20;Search&#x20;Engine'>Other Internet Search Engine</option>
			<option value='Facebook'>Facebook</option>
			<option value='Newspaper&#x20;Ad'>Newspaper Ad</option>
			<option value='Church&#x20;Bulletin'>Church Bulletin</option>
			<option value='Family&#x2f;Friend'>Family&#x2f;Friend</option>
			<option value='Doctor&#x2f;Crisis&#x20;Center&#x2f;School'>Doctor&#x2f;Crisis Center&#x2f;School</option>
			<option value='Previously&#x20;Placed&#x20;With&#x20;Us'>Previously Placed With Us</option>
			<option value='Other'>Other</option>
		</select></td></tr>

	<tr><td colspan='2' style='text-align:center; padding-top:15px;'>
		<input style='font-size:12px;color:#131307' type='submit' value='Submit' />
		<input type='reset' style='font-size:12px;color:#131307' value='Reset' />
	    </td>
	</tr>
   </table>
	<script>
 	  var mndFileds=new Array('NAME','COBJ5CF2','COBJ5CF154','COBJ5CF4');
 	  var fldLangVal=new Array('First Name','What is your preferred method of communication?','How did you find us?','State');
		var name='';
		var email='';

 	  function checkMandatory() {
		for(i=0;i<mndFileds.length;i++) {
		  var fieldObj=document.forms['WebForm514698000010745185'][mndFileds[i]];
		  if(fieldObj) {
			if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
			 if(fieldObj.type =='file')
				{ 
				 alert('Please select a file to upload.'); 
				 fieldObj.focus(); 
				 return false;
				} 
			alert(fldLangVal[i] +' cannot be empty.'); 
   	   	  	  fieldObj.focus();
   	   	  	  return false;
			}  else if(fieldObj.nodeName=='SELECT') {
  	   	   	 if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
				alert(fldLangVal[i] +' cannot be none.'); 
				fieldObj.focus();
				return false;
			   }
			} else if(fieldObj.type =='checkbox'){
 	 	 	 if(fieldObj.checked == false){
				alert('Please accept  '+fldLangVal[i]);
				fieldObj.focus();
				return false;
			   } 
			 } 
			 try {
			     if(fieldObj.name == 'Last Name') {
				name = fieldObj.value;
 	 	 	    }
			} catch (e) {}
		    }
		}
	     }
	   
</script>
	</form>
</div>