<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
//$doc->addScript('http://code.jquery.com/jquery-1.8.3.js');
$doc->addScript(JURI::root().'modules/mod_customsidebar/tmpl/asset/script.js');
$doc->addStyleSheet(JURI::root().'modules/mod_customsidebar/tmpl/asset/style.css');
$doc->addStyleSheet(JURI::root().'modules/mod_customsidebar/tmpl/asset/animate.css');
$doc->addStyleSheet(JURI::root().'modules/mod_customsidebar/tmpl/asset/fontawesome/css/font-awesome.min.css');
// Note. It is important to remove spaces between elements.
?>
<?php // The menu class is deprecated. Use nav instead. ?>
<div class="mobilemenu_contianer">
<nav id="aside-menu" class="">
<div>	
<div class="gkAsideMenu">
<ul class="gkmenu" <?php
	$tag = '';
	if ($params->get('tag_id') != null)
	{
		$tag = $params->get('tag_id') . '';
		echo ' id="' . $tag . '"';
	}
?>>
<?php
foreach ($list as $i => &$item)
{
	$class = 'item-' . $item->id;
	
	if ($item->parent)
	{
		$class .= ' haschild';
	}
	if (!empty($class))
	{
		$class = ' class="' . trim($class) . '"';
	}
	echo '<li' . $class . '>';
	// Render the menu item.
	switch ($item->type) :
		case 'separator':
		case 'url':
		case 'component':
		case 'heading':
			require JModuleHelper::getLayoutPath('mod_customsidebar', 'default_' . $item->type);
			break;
		default:
			require JModuleHelper::getLayoutPath('mod_customsidebar', 'default_url');
			break;
	endswitch;
	// The next item is deeper.
	if ($item->deeper)
	{
		echo '<ul class=" gkmenu small">';
	}
	elseif ($item->shallower)
	{
		// The next item is shallower.
		echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	else
	{
		// The next item is on the same level.
		echo '</li>';
	}
}
?></ul>
</div>
</div>
</nav><i id="close-menu" class="fa fa-bars menu-open"></i>
</div>
<?php 
$menushow = $params->get('menushow');
$style = '<style>
	@media screen and (min-width: '.$menushow.'px) {
    i#close-menu {
		display:none;
	}
	.mobilemenu_contianer{
		display:none;
	}
}
</style>';
$doc->addCustomTag($style);
