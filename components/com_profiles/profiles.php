<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

require_once('helpers/helpers.php');

JLoader::import('pagination', JPATH_COMPONENT);
JLoader::import('helpers.profiles', JPATH_COMPONENT);

// Execute the task.
$controller	= JController::getInstance('Profiles');
$controller->execute(JRequest::getVar('task',''));
$controller->redirect();
