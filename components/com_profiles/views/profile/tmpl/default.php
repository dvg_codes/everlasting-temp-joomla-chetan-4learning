<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// no direct access
defined('_JEXEC') or die;
$family = $this->family;

if($family->state !== '1')
{
	$app = JFactory::getApplication();
	$app->redirect(JRoute::_('index.php?option=com_profiles'), 'Not a valid profile.', 'error');
}

//var_dump($this->family);

$img_path = '/uploads/profiles/'.$family->id.'/';

$main_image = $this->gallery[0]->path;
if(!$main_image)
{
	$main_image = $family->about_us_image;
}

$doc = JFactory::getDocument();

$fullname = $family->first_name;

global $is_single;
$is_single = true;
if(trim($family->spouse_name))
{
	$is_single = false;
	$fullname .= ' & '.$family->spouse_name;
}

$title = $fullname . ' - ' . $doc->title;
if($family->seo_title)
{
	$title = $family->seo_title;
}
$doc->setTitle($title);

$fullname = htmlspecialchars($fullname);
$doc->addStyleSheet('/components/com_profiles/assets/css/profile.css');
$doc->addStyleSheet('/components/com_profiles/assets/css/fancybox.css');

defined('TEMPLATE_PATH') or define('TEMPLATE_PATH', '/templates/everlasting/');
defined('CSS_PATH') or define('CSS_PATH', TEMPLATE_PATH.'css/');
defined('IMG_PATH') or define('IMG_PATH', TEMPLATE_PATH.'images/');
defined('JS_PATH') or define('JS_PATH', TEMPLATE_PATH.'js/');
$doc->addScript(JS_PATH.'jquery-1.6.2.min.js');
$doc->addScript(JS_PATH.'jquery.cycle.all.latest.min.js');
$doc->addScript(JS_PATH.'jquery.main.js');

$doc->addScriptDeclaration('function ljs(){var d=document;var e=d.createElement("script");e.src="/components/com_profiles/assets/js/jquery.fancybox.js";e.type="text/javascript";d.body.appendChild(e);var f=d.createElement("script");f.src="/components/com_profiles/assets/js/profiles.js";f.type="text/javascript";d.body.appendChild(f)}if(window.addEventListener)window.addEventListener("load",ljs,false);else if(window.attachEvent)window.attachEvent("onload",ljs);else window.onload=ljs;');
if($this->gallery)
{
	$count = count($this->gallery);
	$i = 1;
	$photos = '';
	foreach($this->gallery as $photo)
	{
		$showing = "Image {$i} of {$count}";
		$photos .= PHP_EOL . "\t\t\t\t" . "{";
		$photos .= PHP_EOL . "\t\t\t\t\t" . "'href' : '/uploads/profiles/".$family->id."/".$photo->path."',";
		$photos .= PHP_EOL . "\t\t\t\t\t" . "'title': '<span class=\"name\">".$fullname."</span><span class=\"showing\">".$showing."</span>'";
		$photos .= PHP_EOL . "\t\t\t\t" . "},";
		$i++;
	}
	unset($i);
	$photos = rtrim($photos, ',');
	$doc->addScriptDeclaration("
	var $= jQuery.noConflict();
	jQuery(document).ready(function($){
	    $('#ourgallery').click(function() {
			$.fancybox([".$photos."
				], {
				'titlePosition'	: 'inside',
				'type'			: 'image',
				'changeFade'	: 0
			});
		});
	});
	");
}
?>
				<div id="user_profile">
					<?php if(JRequest::getVar('tmpl') !== 'component'): ?>
	            	<a href="<?php echo JRoute::_('index.php?option=com_profiles'); ?>">&laquo; back to waiting families</a>
	            	<?php endif; ?>
	            	<div class="links">
	            		<?php
	            		
	            		$videorel = '';
	            		if(strpos($family->video, 'yout'))
	            		{
	            			$videorel = 'youtube';
	            		}
	            		elseif(strpos($family->video, 'vimeo'))
	            		{
	            			$videorel = 'vimeo';
	            		}
	            		
	            		echo '<a href="javascript:void(0);" id="add-' . $family->id . '" class="add_fav">Add to Favorites</a>';
	            		echo $family->video ? '<a rel="'.$videorel.'" class="video" href="'.$family->video.'">our video</a>' : '';
	            		echo $family->pdf ? '<a class="pdf" rel="external" href="/uploads/profiles/'.$family->id.'/'.$family->pdf.'">our book</a>' : '';
	            		echo $this->gallery ? '<a class="gallery" id="ourgallery" href="javascript:void(0);">our gallery</a>' : '';
	            		
	            		?>
	            	</div>
	            	<br /><h2><?php echo $fullname; ?></h2>
	            	<div id="profile_top">
	            		<div class="left">
	                	<div id="profile-photos">
	                		<img class="main-photo" width="307px" height="254px" alt="<?php echo $fullname; ?>" src="<?php echo $img_path.'307_254_'.$main_image; ?>" />
                        	<?php if($this->gallery) : ?>	                   
	                        <div class="clear"></div>
	                        <?php endif; ?>
	                    </div>
	            		</div>
	            		<div class="right">
		                    <div id="quick_info">
		                    	<?php
		                    	if(isset($family->adopt_race))
		                    	{
		                    		$races = implode(', ', json_decode($family->adopt_race, true));
		                    		echo '<p><span>Race of child interested in adopting:</span>'.$races.'</p>';
		                    	}
		                    	if(isset($family->adopt_gender))
		                    	{
		                    		echo '<p><span>Gender of child interested in adopting:</span>'.$family->adopt_gender.'</p>';
		                    	}
                                        
                                        // Following details are added in DB and front end search by Praveen Dewangan on 25 January 2017 
                                        // Contact : dvg.techno@gmail.com
                                        if(isset($family->family_type)){ echo '<p><span>Family Type:</span>'.$family->family_type.'</p>';  }
                                        if(isset($family->regions)){ echo '<p><span>Regions :</span>'.$family->regions.'</p>';  }
                                        if(isset($family->children)){ echo '<p><span>Children :</span>'.$family->children.'</p>'; }
                                        if(isset($family->communication)){ echo '<p><span>Communication :</span>'.$family->communication.'</p>'; }
                                        if(isset($family->religion)){ echo '<p><span>Religion:</span>'.$family->religion.'</p>';  }
					
                                        ?>
		                    	
		                    </div>
		                    <div class="buttons">
		                    	<a href="<?php echo JRoute::_('/index.php?option=com_profiles&amp;tmpl=component&amp;view=contact&amp;id='.$family->id); ?>" class="contact">contact this family</a>
		                    </div>
	                    
	                    	<div class="thumbs">
	                            <div class="thumbs_container" style="width:<?php echo ((int) count($this->gallery)*82)?>px">
	                            	<?php
	                            	$i=1;
									foreach($this->gallery as $photo)
									{
										if($i > 4)
										{
											break;
										}
										echo family_image($photo->path, $family->id, 'thumbnail_', false);
										$i++;
									}
									?>
	                            </div>
	                        </div>
	                    
	            		</div>
	                </div>
	                <div class="clear"></div>
	            	<div id="profile_tabs">
						
	                	<?php if($family->dear_birthmother): ?>
	                	<div class="section" id="dear_birthmother">
						
						<?php
						/* 
						*	CHANGE MADE ON 9-28-2016 on Sheil's request for changing the text for this particular family
						*	http://www.everlastingadoptions.com/waiting-families/profile/elizabethandkeith.html
						*/
						
						if($family->id == '314') { ?>
	                    	<h3>Dear Expectant Mother,</h3>
						<?php } elseif($family->id == '363' || $family->id == '406') {?>
							<h3>Hello,</h3>
						<?php } else  { ?>
	                    	<h3>Dear Birthmother,</h3>
						<?php } ?>
	    	            	<?php
							echo '<p>'.format_profile_text($family->dear_birthmother).'</p>';
							?>
						</div>
						<? endif; ?>
	                    
	                	<?php if($family->about_us): ?>
	        			<hr />
	                	<div class="section" id="about_us">
	                    	<h3>About <?php echo me_or_us(); ?></h3>
	    	            	<?php 
							echo family_image($family->about_us_image, $family->id);
							echo '<p>'.format_profile_text($family->about_us).'</p>';
							?>
						</div>
						<? endif; ?>
	                    
	                	<?php if($family->our_home): ?>
	        			<hr />
	        			<div class="section" id="our_home">
		                	<h3><?php echo my_or_our(); ?> Home</h3>
	                		<?php
							echo family_image($family->our_home_image, $family->id);
							echo '<p>'.format_profile_text($family->our_home).'</p>';
							?>
						</div>
						<? endif; ?>
	                    
	                	<?php if($family->ext_family): ?>
	        			<hr />
	        			<div class="section" id="ext_family">
		                	<h3><?php echo my_or_our(); ?> Extended Family</h3>
	    	            	<?php
							echo family_image($family->ext_family_image, $family->id);
							echo family_image($family->ext_family_image_spouse, $family->id);
	                    	echo '<p>'.format_profile_text($family->ext_family).'</p>';
	                    	?>
	                    </div>
	                    <? endif; ?>
	                    
	                	<?php if($family->family_traditions): ?>
	        			<hr />
	        			<div class="section" id="family_traditions">
		                	<h3><?php echo my_or_our(); ?> Family Traditions</h3>
	    	            	<?php
							echo family_image($family->family_traditions_image, $family->id);
							echo '<p>'.format_profile_text($family->family_traditions).'</p>';
							?>
						</div>
						<? endif; ?>
	                    
	                	<?php if($family->adoption_story): ?>
	        			<hr />
	        			<div class="section" id="adoption_story">
		                	<h3>What Led <?php echo me_or_us(); ?> To Adoption</h3>
	    	            	<?php
							echo family_image($family->adoption_story_image, $family->id);
							echo '<p>'.format_profile_text($family->adoption_story).'</p>';
							?>
						</div>
						<? endif; ?>
						
	            		<hr />
	                    <div class="section" id="favorites">
	                    	<div class="person"<?php if(!$is_single) echo ' style="float:left;width:330px;border-right:1px solid #E6E6E6"'; ?>>
		                    	<h3>Facts About <?php echo $family->first_name?></h3>
		                    	<ul>
		                    		<li><span>Occupation:</span> <?php echo htmlspecialchars($family->my_occupation);?></li>
		                    		<?php if($family->my_religion != '' || $family->spouse_religion != '') : ?>
		                    			<li><span>Religion:</span> <?php echo htmlspecialchars($family->my_religion);?></li>
		                    		<?php endif; ?>
		                    		<li><span>Education:</span> <?php echo htmlspecialchars($family->my_education);?></li>
		                    		<li><span>Favorite Food:</span> <?php echo htmlspecialchars($family->my_food);?></li>
		                    		<li><span>Favorite Hobby:</span> <?php echo htmlspecialchars($family->my_hobby);?></li>
		                    		<li><span>Favorite Movie:</span> <?php echo htmlspecialchars($family->my_movie);?></li>
		                    		<li><span>Favorite Sport:</span> <?php echo htmlspecialchars($family->my_sport);?></li>
		                    		<li><span>Favorite Holiday:</span> <?php echo htmlspecialchars($family->my_holiday);?></li>
		                    		<li><span>Favorite Music Group:</span> <?php echo htmlspecialchars($family->my_music_group);?></li>
		                    		<li><span>Favorite TV Show:</span> <?php echo htmlspecialchars($family->my_tv_show);?></li>
		                    		<li><span>Favorite Book:</span> <?php echo htmlspecialchars($family->my_book);?></li>
		                    		<li><span>Favorite Subject in School:</span> <?php echo htmlspecialchars($family->my_subject_in_school);?></li>
		                    		<li><span>Favorite Vacation Spot:</span> <?php echo htmlspecialchars($family->my_vacation_spot);?></li>
		                    	</ul>
	                    	</div>
	                        <?php if(!$is_single) : ?>
	                    	<div class="spouse" style="float:left;width:330px;padding-left:28px;border-left:1px solid #E6E6E6">
		                    	<h3>Facts About <?php echo $family->spouse_name?></h3>
		                    	<ul>
		                    		<li><span>Occupation:</span> <?php echo htmlspecialchars($family->spouse_occupation);?></li>
		                    		<?php if($family->my_religion != '' || $family->spouse_religion != '') : ?>
		                    			<li><span>Religion:</span> <?php echo htmlspecialchars($family->spouse_religion);?></li>
		                    		<?php endif; ?>
		                    		<li><span>Education:</span> <?php echo htmlspecialchars($family->spouse_education);?></li>
		                    		<li><span>Favorite Food:</span> <?php echo htmlspecialchars($family->spouse_food);?></li>
		                    		<li><span>Favorite Hobby:</span> <?php echo htmlspecialchars($family->spouse_hobby);?></li>
		                    		<li><span>Favorite Movie:</span> <?php echo htmlspecialchars($family->spouse_movie);?></li>
		                    		<li><span>Favorite Sport:</span> <?php echo htmlspecialchars($family->spouse_sport);?></li>
		                    		<li><span>Favorite Holiday:</span> <?php echo htmlspecialchars($family->spouse_holiday);?></li>
		                    		<li><span>Favorite Music Group:</span> <?php echo htmlspecialchars($family->spouse_music_group);?></li>
		                    		<li><span>Favorite TV Show:</span> <?php echo htmlspecialchars($family->spouse_tv_show);?></li>
		                    		<li><span>Favorite Book:</span> <?php echo htmlspecialchars($family->spouse_book);?></li>
		                    		<li><span>Favorite Subject in School:</span> <?php echo htmlspecialchars($family->spouse_subject_in_school);?></li>
		                    		<li><span>Favorite Vacation Spot:</span> <?php echo htmlspecialchars($family->spouse_vacation_spot);?></li>
		                    	</ul>
	                    	</div>
	                        <?php endif;?>
	                        <div class="clear"></div>
	                    </div>
	                </div>
					<a href="<?php echo JRoute::_('/index.php?option=com_profiles&amp;tmpl=component&amp;view=contact&amp;id='.$family->id); ?>" class="bottom_contact">contact this family</a>
				</div>