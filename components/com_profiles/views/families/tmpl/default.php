<style>
    .drop{
        width: 19%;
        border: 1px solid #4A9FCC;
        padding: 3px;
        margin-top: 10px;
        background: #4a9fcc;
        color: #ffffff;
    }
</style>
<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// no direct access
defined('_JEXEC') or die;

$max = $this->pagination->total;

/* $infiniteJS = <<<IJS
    // <![CDATA[
    (function($)
    {
        $(document).ready(function()
        {
            var maxPage = ${max} / 6;
            $('#content').infinitescroll({
                navSelector  : ".pagination",
                nextSelector : ".link-next",
                itemSelector : ".waiting-families",
                animate      : true,
                path : function (pagenum) {
                    if(pagenum < maxPage + 1) {
                    pagenum--;
                    return '/waiting-families?start=' + pagenum * 10;
                    }
                    else {
                        return false;
                    }
                }
            });

        });
    })(jQuery);
    // ]]>
IJS; */


$infiniteJS = <<<IJS
    // <![CDATA[
    (function($)
    {
        $(document).ready(function()
        {
            var maxPage = ${max} / 6;
            $('#content').infinitescroll({
                navSelector  : ".pagination",
                nextSelector : ".link-next",
                itemSelector : ".waiting-families",
                animate      : true,
                path : function (pagenum) {
					var pagenum =pagenum -1;
					if(pagenum < maxPage ) {
						return '/waiting-families?start=' + pagenum * 6;
				    }else{
						return false;    
					}
                    
                }
            });

        });
    })(jQuery);
    // ]]>
IJS;

JFactory::getDocument()->addScriptDeclaration($infiniteJS);


?>
<form action="<?php echo JRoute::_('index.php?option=com_profiles&view=families'); ?>" method="get">
        <input type="text" name="filter_search" placeholder="Search Families by Name" style="display:inline-block;" value="<?php echo $this->state->get('filter.search'); ?>" />
	<input type="submit" value="Search" style="margin-top:0;" />
	<input type="submit" name="reset" value="Clear Search" style="margin-top:0;" />
        <br />
    <!--Family Type : -->
    <select name="filter_family" onchange="this.form.submit()" class="drop">
        <option value="">FAMILY TYPE</option>
        <option value="Traditional Married" <?php if($this->state->get('filter.family') == "Traditional Married") echo "selected=true"; ?> >Traditional Married</option>
        <option value="Single Parent" <?php if($this->state->get('filter.family') == "Single Parent") echo "selected=true"; ?>>Single Parent</option>
        <option value="Same Sex Couple" <?php if($this->state->get('filter.family') == "Same Sex Couple") echo "selected=true"; ?>>Same Sex Couple</option>        
    </select>
    
    <!--Regions : -->
    <select name="filter_regions" onchange="this.form.submit()" class="drop">
        <option value="">LOCATION</option>
        <option value="Northeast States" <?php if($this->state->get('filter.regions') == "Northeast States") echo "selected=true"; ?> >Northeast States</option>
        <option value="Southeast States" <?php if($this->state->get('filter.regions') == "Southeast States") echo "selected=true"; ?> >Southeast States</option>
        <option value="Midwest" <?php if($this->state->get('filter.regions') == "Midwest") echo "selected=true"; ?> >Midwest</option>
        <option value="Northwest States" <?php if($this->state->get('filter.regions') == "Northwest States") echo "selected=true"; ?> >Northwest States</option>
        <option value="Southwest States" <?php if($this->state->get('filter.regions') == "Southwest States") echo "selected=true"; ?> >Southwest States</option>        
    </select>
    
    <!--Children :-->
    <select name="filter_children" onchange="this.form.submit()" class="drop">
        <option value="">CHILDREN</option>
        <option value="Has Children" <?php if($this->state->get('filter.children') == "Has Children") echo "selected=true"; ?> >Has Children</option>
        <option value="Does not have Children" <?php if($this->state->get('filter.children') == "Does not have Children") echo "selected=true"; ?> >Does not have Children</option>
    </select>
    <!--<br>-->
    <!--Communication : -->
    <select name="filter_communication" onchange="this.form.submit()" class="drop">
        <option value="">COMMUNICATION</option>
        <option value="Open" <?php if($this->state->get('filter.communication') == "Open") echo "selected=true"; ?>>Open</option>
        <option value="Semi-Open" <?php if($this->state->get('filter.communication') == "Semi-Open") echo "selected=true"; ?>>Semi-Open</option>
        <option value="Closed" <?php if($this->state->get('filter.communication') == "Closed") echo "selected=true"; ?>>Closed</option>
    </select>
    
    <!--Religion :--> 
    <select name="filter_religion" onchange="this.form.submit()" class="drop">
        <option value="">RELIGION</option>
        <option value="Believer" <?php if($this->state->get('filter.religion') == "Believer") echo "selected=true"; ?>>Believer</option>
        <option value="Non Believer" <?php if($this->state->get('filter.religion') == "Non Believer") echo "selected=true"; ?>>Non Believer</option>
    </select>        
</form>
<?php
$jinput = JFactory::getApplication()->input;
$start = $jinput->get('start', '', 'INT');
// If we have start in the post (Ajax load infitne scroll) we don't need the module below 
if(!$start) {
	$document = JFactory::getDocument();
	$renderer = $document->loadRenderer('module');      
	$module   = JModuleHelper::getModule('featuredfamily');
	$params	 = array('style' => $style);
	echo $renderer->render($module,$params);
}
?>

<?php if ($this->error) echo '<p class="error">' . $this->error . '</p>'; ?>
<ul class="waiting-families">
<?php
//echo "<pre>";var_dump($this->pagination->total);die;

foreach($this->families as $family) :

	switch ($family->profile_status) {
		case 'adopted':
			$family->link = 'javascript::void(0);';
			$family->link_text = "Successfully Adopted";
			$family->banner = '<span class="banner-adopted"></span>';

			if ($family->spouse_name) {
				$family->banner = '<span class="banner-adopted"></span>';
			}
			else {
				$family->banner = '<span class="banner-i-adopted"></span>';
			}
			break;
		case 'connected':
			$family->link = 'javascript::void(0);';
			$family->link_text = "Currently Connected";
			$family->banner = '<span class="banner-connected"></span>';
			break;
		default:
			$family->link = JRoute::_('index.php?option=com_profiles&view=profile&id='.$family->id);
			$family->link_text = "View Profile";
			$family->banner = null;
			break;
	}

	$main_image = $family->about_us_image;
	if (!$main_image) {
		$gallery = $this->getGalleryByFamilyID($family->id);
		$main_image = $gallery->path;
	}

	$intro = $family->about_us;
	if ($family->dear_birthmother) {
		$intro = $family->dear_birthmother;
	}
	?>
	<li style="width:32%;">
		<?php echo $family->banner; ?>
		<a name="profile-<?php echo $family->id; ?>"></a>
		<?php
		echo '<a href="'.$family->link.'">';
		if (is_file(JPATH_SITE.'/uploads/profiles/'.$family->id.'/165_120_'.$main_image)) {
			echo '<img class="feature" src="/uploads/profiles/'.$family->id.'/165_120_'.$main_image.'" alt="'.$family->last_name.' Family" style="margin-left:18px;" />';
		} else {
			echo '<img class="feature" src="/images/comingsoon.jpg" alt="Adoption Profile Photo Coming Soon" style="margin-left:18px;" />';
		}
		echo '</a>';
		?>
		<h3 style="text-align:center;"><?php

			echo '<a href="'.$family->link.'">'.$family->first_name;
			if ($family->spouse_name) {
				echo ' &amp; '.$family->spouse_name;
			}
			echo '</a>';

		?></h3>
		<p style="text-align:justify;"><?php echo htmlspecialchars(substr($intro, 0, 50)); ?>…&nbsp;
			<br /><a href="<?php echo $family->link; ?>" style="padding-left:25%;">&raquo; View Profile</a>
		</p>
		<div class="clear"></div>
	</li>
<?php endforeach; ?>
</ul>
<div class="pagination">
	<?php echo $this->pagination->getPrevLink(); ?>
	&nbsp;&nbsp;
	<?php echo $this->pagination->getNextLink(); ?>
	<div class="clear"></div>
</div>
