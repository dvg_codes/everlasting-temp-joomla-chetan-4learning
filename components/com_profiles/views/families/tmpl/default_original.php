<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// no direct access
defined('_JEXEC') or die;

$max = $this->pagination->total;

$infiniteJS = <<<IJS
    // <![CDATA[
    (function($)
    {
        $(document).ready(function()
        {
            var maxPage = ${max} / 10;
            $('#content').infinitescroll({
                navSelector  : ".pagination",
                nextSelector : ".link-next",
                itemSelector : ".waiting-families",
                animate      : true,
                path : function (pagenum) {
                    if(pagenum < maxPage + 1) {
                    pagenum--;
                    return '/waiting-families?start=' + pagenum * 10;
                    }
                    else {
                        return false;
                    }
                }
            });

        });
    })(jQuery);
    // ]]>
IJS;

JFactory::getDocument()->addScriptDeclaration($infiniteJS);


?>
<form action="<?php echo JRoute::_('index.php?option=com_profiles&view=families'); ?>" method="get">
	<input type="text" name="filter_search" placeholder="Search Families by Name" style="display:inline-block;" value="<?php echo $this->state->get('filter.search'); ?>" />
	<input type="submit" value="Search" style="margin-top:0;" />
	<input type="submit" name="reset" value="Clear Search" style="margin-top:0;" />
</form>
<?php
$document = JFactory::getDocument();
$renderer = $document->loadRenderer('module');      
$module   = JModuleHelper::getModule('featuredfamily');
$params	 = array('style' => $style);
echo $renderer->render($module,$params);
?>

<?php if ($this->error) echo '<p class="error">' . $this->error . '</p>'; ?>
<ul class="waiting-families">
<?php
//echo "<pre>";var_dump($this->pagination->total);die;
foreach($this->families as $family) :

	switch ($family->profile_status) {
		case 'adopted':
			$family->link = 'javascript::void(0);';
			$family->link_text = "Successfully Adopted";
			$family->banner = '<span class="banner-adopted"></span>';

			if ($family->spouse_name) {
				$family->banner = '<span class="banner-adopted"></span>';
			}
			else {
				$family->banner = '<span class="banner-i-adopted"></span>';
			}
			break;
		case 'connected':
			$family->link = 'javascript::void(0);';
			$family->link_text = "Currently Connected";
			$family->banner = '<span class="banner-connected"></span>';
			break;
		default:
			$family->link = JRoute::_('index.php?option=com_profiles&view=profile&id='.$family->id);
			$family->link_text = "View Profile";
			$family->banner = null;
			break;
	}

	$main_image = $family->about_us_image;
	if (!$main_image) {
		$gallery = $this->getGalleryByFamilyID($family->id);
		$main_image = $gallery->path;
	}

	$intro = $family->about_us;
	if ($family->dear_birthmother) {
		$intro = $family->dear_birthmother;
	}
	?>
	<li>
		<?php echo $family->banner; ?>
		<a name="profile-<?php echo $family->id; ?>"></a>
		<?php
		echo '<a href="'.$family->link.'">';
		if (is_file(JPATH_SITE.'/uploads/profiles/'.$family->id.'/165_120_'.$main_image)) {
			echo '<img class="feature" src="/uploads/profiles/'.$family->id.'/165_120_'.$main_image.'" alt="'.$family->last_name.' Family" />';
		} else {
			echo '<img class="feature" src="/images/comingsoon.jpg" alt="Adoption Profile Photo Coming Soon" />';
		}
		echo '</a>';
		?>
		<h3><?php

			echo '<a href="'.$family->link.'">'.$family->first_name;
			if ($family->spouse_name) {
				echo ' &amp; '.$family->spouse_name;
			}
			echo '</a>';

		?></h3>
		<p><?php echo htmlspecialchars(substr($intro, 0, 400)); ?>…&nbsp;<a href="<?php echo $family->link; ?>">&raquo; View Profile</a></p>
		<div class="clear"></div>
	</li>
<?php endforeach; ?>
</ul>
<div class="pagination">
	<?php echo $this->pagination->getPrevLink(); ?>
	&nbsp;&nbsp;
	<?php echo $this->pagination->getNextLink(); ?>
	<div class="clear"></div>
</div>
