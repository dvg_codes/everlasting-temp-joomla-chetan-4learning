<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');
jimport('joomla.application.component.modellist');
jimport('joomla.application.component.helper');
jimport('joomla.environment.browser');

JTable::addIncludePath(JPATH_ROOT . '/administrator/components/com_profiles/tables');

/**
 * Model
 */
class ProfilesModelFamilies extends JModelList
{
	protected $context = 'families';
	protected $_items;
	protected $_title = null;
	protected $_pagination = null;
	
	protected $_limit;
	protected $_start;
	
	public function __construct()
	{
		parent::__construct();
		
		$app = JFactory::getApplication();
		$params = $app->getParams();
		
		// Have to set limit manually below, because fucking Joomla doesn't know how to setState properly.
		//$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', 10, 'int');
		
		$start = JRequest::getVar('start', 0, '', 'int');
		
		$this->_limit = (JBrowser::getInstance()->get('_mobile') === true) ? 5 : $params->get('profiles_to_show', 10);
		$this->_start = ($this->_limit != 0 ? (floor($start / $this->_limit) * $this->_limit) : 0);
		
		$this->setState('list.limit', $this->_limit);
		$this->setState('list.start', $this->_start);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		
		// Initialise variables.
		$app = JFactory::getApplication();

		// Load the filter state.

		if ($app->input->get('reset'))
		{
			$app->setUserState($this->context, null);
			$app->redirect(JRoute::_('index.php?option=com_profiles&view=families&Itemid=506'));
		}

		$search = $app->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
                $this->setState('filter.search', $search);
                
                //print_r($search);   exit();
                
                // Adding custom state
                $family = $app->getUserStateFromRequest($this->context.'filter.family', 'filter_family');
                $this->setState('filter.family',$family);
                
                $regions = $app->getUserStateFromRequest($this->context.'filter.regions', 'filter_regions');
                $this->setState('filter.regions',$regions);
                
                $communication = $this->getUserStateFromRequest($this->context.'filter.communication', 'filter_communication');
                $this->setState('filter.communication',$communication);
                
                
                $children = $app->getUserStateFromRequest($this->context.'filter.children', 'filter_children');
                $this->setState('filter.children',$children);
                
                
                
                $religion = $app->getUserStateFromRequest($this->context.'filter.religion', 'filter_religion');
                $this->setState('filter.religion',$religion);
 
                //echo "<pre>"; print_r($this->state); echo "</pre>";

		// Load the parameters.
		$params = JComponentHelper::getParams('com_profiles');
		$this->setState('params', $params);
		
		// List state information.
		parent::populateState('a.ordering', 'asc');
	}

	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   11.1
	 */
	public function getItems()
	{
		// Get a storage key.
		$store = $this->getStoreId();

		// Try to load the data from internal storage.
		if (isset($this->cache[$store]))
		{
			return $this->cache[$store];
		}

		// Load the list items.
		$query = $this->_getListQuery();
		$items = $this->_getList($query, $this->_start, $this->_limit);

		if (empty($items))
		{
			JFactory::getApplication()->setUserState($this->context . '.filter.search', null);
			$this->setState('filter.search', null);

			$query = $this->_getListQuery();
			$items = $this->_getList($query, $this->_start, $this->_limit);

			$this->setError('Sorry, but we did not find any matches for your search request. Please try again.');
		}

		// Check for a database error.
		if ($this->_db->getErrorNum())
		{
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Add the items to the internal cache.
		$this->cache[$store] = $items;

		return $this->cache[$store];
	}

	/**
	 * Method to get a store id based on the model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  An identifier string to generate the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   11.1
	 */
	protected function getStoreId($id = '')
	{
		$id .= ':' . $this->getState('filter.search');

		return parent::getStoreId($id);
	}

	/**
	 * Method to get a JPagination object for the data set.
	 *
	 * @return  JPagination  A JPagination object for the data set.
	 *
	 * @since   11.1
	 */
	public function getPagination()
	{
		// Get a storage key.
		$store = $this->getStoreId('getPagination');

		// Try to load the data from internal storage.
		if (isset($this->cache[$store]))
		{
			return $this->cache[$store];
		}

		// Create the pagination object.
		$page = new ProfilePagination($this->getTotal(), $this->_start, $this->_limit);

		// Add the object to the internal cache.
		$this->cache[$store] = $page;

		return $this->cache[$store];
	}
	
	protected function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$q = $db->getQuery(true)
			->select('a.*')
			->from('`#__profiles_families` AS a')
			->where('a.state = 1')
			->order('ordering ASC');

		// Filter by search in title
		$search = $this->getState('filter.search');
                
                $family = $this->getState('filter.family');
                $regions = $this->getState('filter.regions');
                $children = $this->getState('filter.children');
                $communication = $this->getState('filter.communication');
                $religion = $this->getState('filter.religion');
                
                
                
		if ((!empty($search)) || (!empty($family)) || (!empty($regions)) || (!empty($children)) || (!empty($communication)) || (!empty($religion))  )
		{
			$terms = explode(' ', $search);

			foreach ($terms as $search)
			{
				if ($search === '&' || $search === 'and')
				{
					continue;
				}

				$search = $db->quote('%' . $db->getEscaped($search, true) . '%');
                                
                                $family = $db->quote($db->getEscaped($family, true));
                                
                                $regions  = $db->quote($db->getEscaped($regions, true));
                                $children = $db->quote($db->getEscaped($children, true));
                                
                                $communication = $db->quote($db->getEscaped($communication, true));
                                $religion = $db->quote($db->getEscaped($religion, true));                                
                                
                              $q->where('(a.first_name LIKE ' . $search . '  OR  a.spouse_name LIKE ' . $search . '  OR  a.last_name LIKE ' . $search . ' ) ');
                              
                                if(!empty($family)){     $q->where(' ( a.family_type = ' . $family . ') ');     }
                                //if($family != "''") $q->where(' ( a.family_type = ' . $family . ') ');

                                // AND ( a.regions = ' . $regions . ' ) AND ( a.children = ' . $children . ' ) AND ( a.communication = ' . $communication . ' )  AND ( a.religion = ' . $religion . ' )
                                
                                if($regions != "''"){    $q->where(' ( a.regions = ' . $regions . ') ' );   }
                                if($children != "''"){  $q->where(' ( a.children = ' . $children . ') ' );  } 
                                
                                if($communication != "''"){ $q->where(' ( a.communication = ' . $communication . ') ' );    }
                                if($religion != "''"){  $q->where(' ( a.religion = ' . $religion . ') ' );  }
                                
			}
		}
                //echo "Query==";   echo $q;
		return $q;
	}
}
