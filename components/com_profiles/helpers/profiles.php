<?php
/**
 * @version     1.0.0
 * @package     com_profiles
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

abstract class ProfilesHelper
{
	public static $single = true;

	public static function familyImage($path = null, $id = null, $size = '150_150_', $lightbox = true)
	{
		$img = '/uploads/profiles/'.$id.'/'.$size.$path;
		if (is_file(JPATH_SITE.$img)) {
			if ($lightbox) {
				echo '<a href="/uploads/profiles/'.$id.'/'.$path.'" class="lightbox"><span></span>';
			}
			$info = getimagesize(JPATH_SITE.$img);
			echo '<img src="'.$img.'" alt="" />';
			if ($lightbox) {
				echo '</a>';
			}
		}
	}

	public static function meOrUs($is_single = null)
	{
		return self::$single ? 'Me' : 'Us';
	}

	public static function myOrOur($is_single = null)
	{
		return self::$single ? 'My' : 'Our';
	}

	public static function detectUrl($text)
	{
		return preg_replace("#((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#ie", "'To learn more about us, <a href=\"$1\" target=\"_blank\">click here</a>.$4'", $text);
	}

	public static function formatProfileText($text)
	{
		return stripslashes(nl2br(self::detectUrl($text)));
	}

}
