<?php
/**
 * @copyright   (C) 2010 iJoomla, Inc. - All rights reserved.
 * @license  GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author  iJoomla.com <webmaster@ijoomla.com>
 * @url   http://www.ijoomla.com/licensing/
 * the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript  *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0 
 * More info at http://www.ijoomla.com/licensing/
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.file');

class  plgSystemiJoomlaNews extends JPlugin{

	public function plgSystemiJoomlaNews(&$subject, $config){
		parent::__construct($subject, $config);
		$this->mainframe = JFactory::getApplication();
		$this->loadPlugin();
	}

	public function loadPlugin(){
		$option = JRequest::getVar("option", "");
		$controller = JRequest::getVar("controller", "");
		
		$list_components = array("com_adagency"=>"0",
								 "com_digistore"=>"0",
								 "com_ijoomla_archive"=>"0",
								 "com_ijoomla_rss"=>"0",
								 "com_ijoomla_seo"=>"0",
								 "com_magazine"=>"0",
								 "com_news_portal"=>"0",
								 "com_sidebars"=>"0",
								 "com_surveys"=>"0");
		if($this->mainframe->isAdmin() && isset($list_components[$option]) && $controller == ""){			
			include_once(JPATH_SITE.DS."plugins".DS."system".DS."ijoomlanews".DS."ijoomlanews".DS."feed.php");
			include_once(JPATH_SITE.DS."plugins".DS."system".DS."ijoomlanews".DS."ijoomlanews".DS."tabs.php");
			return true;
		}
		return false;
	}
	
	function onAfterDispatch(){
		if($this->loadPlugin()){
			$document =& JFactory::getDocument();
			$script = 'window.addEvent(\'domready\', function(){ $$(\'dl.tabs\').each(function(tabs){ new JTabs(tabs, {}); }); })';
   			JHTML::_('behavior.mootools');
			$document->addScript(JURI::root()."media/system/js/tabs.js");
			$document->addScriptDeclaration($script);
		}	
	}
	
	public function onAfterRender(){		
		if($this->loadPlugin()){
			$class = new CreateTabs();
			$this->renderStatus();
		}			
	}
	
	public function renderStatus(){
		$articles_list_components = array("com_adagency"=>"http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/ijoomla-ad-agency/RSS2.0/",
								 "com_digistore"=>"http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/ijoomla-digistore/RSS2.0/",
								 "com_ijoomla_archive"=>"http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/ijoomla-search-%26-archive/RSS2.0/",
								 "com_ijoomla_rss"=>"http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/ijoomla-rss/RSS2.0/",
								 "com_ijoomla_seo"=>"http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/ijoomla-seo/RSS2.0/",
								 "com_magazine"=>"http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/ijoomla-magazine/RSS2.0/",
								 "com_news_portal"=>"http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/ijoomla-news-portal/RSS2.0/",
								 "com_sidebars"=>"http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/ijoomla-sidebars/RSS2.0/",
								 "com_surveys"=>"http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/ijoomla-surveys/RSS2.0/");
								 					 
		$changelog_list_components = array("com_adagency"=>"http://adagency.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-ad-agency/change-log/RSS2.0/",
								 "com_digistore"=>"http://ecommerce.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-digistore/change-log/RSS2.0/",
								 "com_ijoomla_archive"=>"http://archive.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-search-a-archive/change-log/RSS2.0/",
								 "com_ijoomla_rss"=>"http://rss.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-rss-feeder/change-log/RSS2.0/",
								 "com_ijoomla_seo"=>"http://seo.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-seo/change-log/RSS2.0/",
								 "com_magazine"=>"http://magazine.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-magazine/change-log/RSS2.0/",
								 "com_news_portal"=>"http://newsportal.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news-portal/change-log/RSS2.0/",
								 "com_sidebars"=>"http://sidebars.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-sidebars/change-log/RSS2.0/",
								 "com_surveys"=>"http://surveys.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-survey/change-log/RSS2.0/");
								 			
		$video_list_components = array("com_adagency"=>"http://adagency.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-ad-agency/videos/RSS2.0/",
								 "com_digistore"=>"http://ecommerce.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-digistore/videos/RSS2.0/",
								 "com_ijoomla_archive"=>"http://archive.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-search-a-archive/videos/RSS2.0/",
								 "com_ijoomla_rss"=>"http://rss.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-rss-feeder/videos/RSS2.0/",
								 "com_ijoomla_seo"=>"http://seo.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-seo/video-tutorials/RSS2.0/",
								 "com_magazine"=>"http://magazine.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-magazine/videos/RSS2.0/",
								 "com_news_portal"=>"http://newsportal.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news-portal/videos/RSS2.0/",
								 "com_sidebars"=>"http://sidebars.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-sidebars/videos/RSS2.0/",
								 "com_surveys"=>"http://surveys.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-survey/videos/RSS2.0/");			
								 
		$html	= JResponse::getBody();		
						
		$pattern = "/table class=\"adminform\" style=(.*)\<\/table>/msU";
		preg_match_all($pattern, $html, $finds);
		
		if(is_array($finds) && isset($finds["0"]["0"])){
			$article_numbers = $this->getArticleNumbers();
			$option = JRequest::getVar("option", "");
			$url = $articles_list_components[$option];
			$feed = new NewsRss();
			//-----------------------------------
			$feed->feed_url($url);
			$feed->set_timeout(10);
			$feed->replace_headers(true);
			$feed->init();
			$articles = $feed->get_items(0, $article_numbers);						
			if(!isset($articles) || $articles !== FALSE){			
				//-----------------------------------
				$feed->feed_url("http://www.ijoomla.com/ijoomla_rss/rss/xml/ijoomla-news/RSS2.0/");
				$feed->set_timeout(10);
				$feed->replace_headers(true);
				$feed->init();
				$ijoomla_news = $feed->get_items(0, $article_numbers);
				//-----------------------------------
				$feed->feed_url("http://www.ijoomla.com/blog/feed/rss/");
				$feed->set_timeout(10);
				$feed->replace_headers(true);
				$feed->init();
				$ijoomla_blog = $feed->get_items(0, $article_numbers);
				//-----------------------------------
				$url = $changelog_list_components[$option];
				$feed->feed_url($url);
				$feed->set_timeout(10);
				$feed->replace_headers(true);
				$feed->init();
				$change_log = $feed->get_items(0, $article_numbers);
				//-----------------------------------
				$url = $video_list_components[$option];
				$feed->feed_url($url);
				$feed->set_timeout(10);
				$feed->replace_headers(true);
				$feed->init();
				$video = $feed->get_items(0, $article_numbers);
				//-----------------------------------
				
				$tabs_class = new CreateTabs();
				$tabs = $tabs_class->tabs($articles, $ijoomla_news, $ijoomla_blog, $change_log, $video, $option);
				$exist = "<".$finds["0"]["0"];	
				$replace = "<table width=\"100%\"><tr><td valign=\"top\">".$exist."</td><td valign=\"top\" width=\"50%\">".$tabs."</td></tr></table>";			
				$html = str_replace($exist, $replace, $html);
				$html = str_replace("513px", "470px", $html);
			}	
		}
		
		JResponse::setBody($html);
	}
	
	function getArticleNumbers(){
		$db =& JFactory::getDBO();
		$sql = "select params from #__extensions where element = 'ijoomlanews'";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadResult();
		if(trim($result == "") || trim($result == "{}")){
			$params = array("nr_articles"=>"5", "text_length"=>"500", "image_width"=>"50");
			$sql = "update #__extensions set params='".json_encode($params)."' where element = 'ijoomlanews'";
			$db->setQuery($sql);
			$db->query();
			return "5";
		}
		else{
			$result = json_decode($result);
			return $result->nr_articles;
		}
	}
}

?>
