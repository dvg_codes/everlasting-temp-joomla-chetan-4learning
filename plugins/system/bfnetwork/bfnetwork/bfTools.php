<?php
/**
 * @package   Blue Flame Network (bfNetwork)
 * @copyright Copyright (C) 2011, 2012, 2013, 2014 Blue Flame IT Ltd. All rights reserved.
 * @license   GNU General Public License version 3 or later
 * @link      http://myJoomla.com/
 * @author    Phil Taylor / Blue Flame IT Ltd.
 *
 * bfNetwork is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bfNetwork is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this package.  If not, see http://www.gnu.org/licenses/
 */
require 'bfEncrypt.php';

/**
 * If we have got here then we have already passed through decrypting
 * the encrypted header and so we are sure we are now secure and no one
 * else cannot run the code below.
 */
final class bfTools
{

    /**
     * We pass the command to run as a simple integer in our encrypted
     * request this is mainly to speed up the decryption process, plus its a
     * single digit(or 2) rather than a huge string to remember :-)
     */
    private $_methods = array(
        1  => 'getCoreHashFailedFileList',
        2  => 'downloadfile',
        3  => 'restorefile',
        4  => 'getSuspectContentFileList',
        5  => 'deleteFile',
        6  => 'checkFTPLayer',
        7  => 'disableFTPLayer',
        8  => 'checkNewDBCredentials',
        9  => 'testDbCredentials',
        10 => 'getFolderPermissions',
        11 => 'setFolderPermissions',
        12 => 'getHiddenFolders',
        13 => 'deleteFolder',
        14 => 'getInstallationFolders',
        15 => 'getRecentlyModified',
        16 => 'getFilePermissions',
        17 => 'setFilePermissions',
        18 => 'getErrorLogs',
        19 => 'getEncrypted',
        20 => 'getUser',
        21 => 'setUser',
        22 => 'setDbPrefix',
        23 => 'setDbCredentials',
        24 => 'getBakTables',
        25 => 'deleteBakTables',
        26 => 'getHtaccessFiles',
        27 => 'setHtaccess',
        28 => 'getUpdatesCount',
        29 => 'getUpdatesDetail',
        30 => 'getDotfiles',
        31 => 'getArchivefiles',
        32 => 'getLargefiles',
        33 => 'fixDbSchema',
        34 => 'getDbSchemaVersion',
        35 => 'checkGoogleFile',
        36 => 'toggleOnline',
        37 => 'getOfflineStatus',
        38 => 'getRobotsFile',
        39 => 'saveRobotsFile',
        40 => 'getTmpfiles',
        41 => 'clearTmpFiles',
        42 => 'getFlufffiles',
        43 => 'clearFlufffiles'
    );

    /**
     * Pointer to the Joomla Database Object
     * @var JDatabaseMysql
     */
    private $_db;

    /**
     * Incoming decrypted vars from the request
     * @var stdClass
     */
    private $_dataObj;

    /**
     * PHP 5 Constructor,
     * I inject the request to the object
     *
     * @param stdClass $dataObj
     */
    public function __construct($dataObj)
    {
        // init Joomla
        require 'bfInitJoomla.php';

        // Set the request vars
        $this->_dataObj = $dataObj;
    }

    /**
     * I'm the controller - I run methods based on the request integer
     */
    public function run()
    {
        if (property_exists($this->_dataObj, 'c')) {

            $c = ( int )$this->_dataObj->c;
            if (array_key_exists($c, $this->_methods)) {

                // call the right method
                $this->{$this->_methods [$c]} ();
            } else {

                // Die if an unknown function
                bfEncrypt::reply('error', 'No Such method #err1 - ' . $c);
            }
        } else {

            // Die if an unknown function
            bfEncrypt::reply('error', 'No Such method #err2');
        }
    }

    /**
     * Method to delete a named file when we know its id
     */
    private function deleteFile()
    {
        // init bfDatabase
        $this->_initDb();

        // Get the filewithpath based on the id
        $this->_db->setQuery('SELECT filewithpath from bf_files WHERE id = ' . ( int )$this->_dataObj->file_id);
        $filewithpath = $this->_db->loadResult();

        // check that the file we got form the database matches to the path we think it should be
        if ($this->_dataObj->filewithpath != $filewithpath) {
            bfEncrypt::reply('failure', array(
                'msg' => 'File Not matching: ' . $filewithpath
            ));
        }

        // If the file doesnt exist then remove from cache and reply
        if (!file_exists(JPATH_BASE . $filewithpath)) {
            $this->_db->setQuery("DELETE FROM bf_files WHERE id = " . ( int )$this->_dataObj->file_id);
            $this->_db->query();
            bfEncrypt::reply('failure', array(
                'msg' => 'File doesn\'t exist: ' . $filewithpath
            ));
        }

        // Attempt to force deletion
        if (!is_writable(JPATH_BASE . $filewithpath)) {
            @chmod(JPATH_BASE . $filewithpath, 0777);
        }

        // delete the file, making sure we prefix with a path
        if (@unlink(JPATH_BASE . $filewithpath)) {
            $this->_db->setQuery("DELETE FROM bf_files WHERE id = " . ( int )$this->_dataObj->file_id);
            $this->_db->query();

            // File deleted - say yes
            bfEncrypt::reply('success', array(
                'msg' => 'File deleted: ' . $filewithpath
            ));
        } else {

            // File deleted - say no
            bfEncrypt::reply('failure', array(
                'msg' => 'File Not Deleted: ' . $filewithpath
            ));
        }
    }

    /**
     * Initialise Joomla, just load as much as we need
     */
    private function _initDb()
    {

        // Only include it once
        if (!class_exists('BfDb')) {
            require 'bfDb.php';
        }

        // but reconnect the database as often as called
        $this->_db = new BfDb();
    }

    /**
     * I delete a folder
     */
    private function deleteFolder()
    {

        // Require more complex methods for dealing with files
        require 'bfFilesystem.php';

        // init mini-Joomla
        $this->_initDb();

        // init our return msg
        $msg = array();

        // hidden or normal - needed for ALL deletes
        $type = $this->_dataObj->type;

        // switch on type
        if ($type == 'hidden') {

            // get the folders cache id
            $folder_id = $this->_dataObj->fid;

            // init
            $msgToReturn                     = array();
            $msgToReturn ['deleted_files']   = 0;
            $msgToReturn ['deleted_folders'] = 0;
            $msgToReturn ['left']            = 0;

            // Do we want to delete all hidden folders?
            if ($folder_id == 'ALL') { // All meaning all hidden folders, not ALL folders in our db!!

                // get all the hidden folders
                $folders = $this->getHiddenFolders(TRUE);

                // foreach hidden folder, delete that hidden folder recursivly
                foreach ($folders as $folder) {

                    // doh - cannot find the folder
                    if (!$folder->folderwithpath) {
                        bfEncrypt::reply('failure', array(
                            'msg' => 'Folder Not Found: ' . $folder->folderwithpath
                        ));
                    }

                    // delete recursive
                    $msg = Bf_Filesystem::deleteRecursive(JPATH_BASE . $folder->folderwithpath, TRUE, $msg);

                    // oh dear we failed
                    if ($msg ['result'] == 'failure') {
                        $msgToReturn                     = array();
                        $msgToReturn ['deleted_files']   = count(@$msg ['deleted_files']);
                        $msgToReturn ['deleted_folders'] = count(@$msg ['deleted_folders']);
                        $msgToReturn ['left']            = $this->getHiddenFolders(TRUE);

                        // send back the error message
                        bfEncrypt::reply('failure', array(
                            'msg' => 'Problem!: ' . json_encode($msgToReturn)
                        ));
                    }
                }

            } else {

                // select the folder to delete
                $this->_db->setQuery('SELECT folderwithpath FROM bf_folders WHERE id = ' . ( int )$folder_id);
                $folderwithpath = $this->_db->loadResult();

                // if the folder is not there
                if (!$folderwithpath) {
                    bfEncrypt::reply('failure', array(
                        'msg' => 'Folder Not Found #msg2#: ' . $folderwithpath
                    ));
                }

                $msg = Bf_Filesystem::deleteRecursive(JPATH_BASE . $folderwithpath, TRUE, $msg);
            }

            // if we deleted some folders
            if (count($msg ['deleted_folders'])) {
                foreach ($msg ['deleted_folders'] as $folder) {
                    $fwp = str_replace('//', '/', str_replace(JPATH_BASE, '', $folder));

                    $sql = "DELETE FROM bf_folders where folderwithpath = '" . $fwp . "'";

                    $this->_db->setQuery($sql);
                    $this->_db->query();
                }
            }

            // if we deleted some files
            if (count($msg ['deleted_files'])) {
                foreach ($msg ['deleted_files'] as $file) {
                    $fwp = str_replace('//', '/', str_replace(JPATH_BASE, '', $file));

                    $sql = "DELETE FROM bf_files where filewithpath = '" . $fwp . "'";
                    $this->_db->setQuery($sql);
                    $this->_db->query();
                }
            }

            // reply back with our warning or success message
            $msgToReturn                     = array();
            $msgToReturn ['deleted_files']   = count($msg ['deleted_files']);
            $msgToReturn ['deleted_folders'] = count($msg ['deleted_folders']);
            $msgToReturn ['left']            = count($this->getHiddenFolders(TRUE));
            if ($msg ['result'] == 'success') {
                bfEncrypt::reply('success', array(
                    'msg' => json_encode($msgToReturn)
                ));
            } else {
                bfEncrypt::reply('failure', array(
                    'msg' => 'Folder Not Deleted #msg3#: ' . json_encode($msgToReturn)
                ));
            }
        }
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getHiddenFolders($internal = FALSE)
    {
        $this->_initDb();
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        if (!$limitstart)
            $limitstart = 0;
        if (!$limit)
            $limit = '9999999999999999';
        $this->_db->setQuery('SELECT * FROM bf_folders WHERE folderwithpath LIKE "%/.%" LIMIT ' . ( int )$limitstart . ', ' . $limit);
        $folders = $this->_db->loadObjectList();

        if ($internal === TRUE) {
            return $folders;
        }

        $this->_db->setQuery('SELECT count(*) FROM bf_folders WHERE folderwithpath LIKE "%/.%"');
        $count = $this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $folders,
            'total' => $count
        ));
    }

    /**
     * I get the number of core files that failed the hash checking
     */
    private function getCoreHashFailedFileList()
    {
        // init mini-Joomla
        $this->_initDb();

        // set up the limit and limit start for the SQL
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        $this->_db->setQuery('SELECT id, filewithpath, filemtime, fileperms FROM bf_files WHERE hashfailed = 1 LIMIT ' . $limitstart . ', ' . $limit);

        // Get the files from the cache
        $files = $this->_db->loadObjectList();

        // get the count as well, for pagination
        $this->_db->setQuery('SELECT count(*) from bf_files WHERE hashfailed = 1');
        $count = $this->_db->loadResult();

        // send back the totals
        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * I get list of database tables that begin with bak_
     */
    private function deleteBakTables()
    {
        $tables = $this->getBakTables(TRUE);

        // for all the bak tables
        foreach ($tables as $table) {

            // compose the sql query
            $this->_db->setQuery("DROP TABLE " . $table[0]);

            // delete the bak_tables
            $this->_db->query();
        }

        $count = count($tables);

        // send back the totals
        bfEncrypt::reply('success', array(
            'tables' => $tables,
            'total'  => $count
        ));
    }

    /**
     * I get list of database tables that begin with bak_
     */
    private function getBakTables($internal = FALSE)
    {
        // init mini-Joomla
        $this->_initDb();

        // Get the database name
        $config = JFactory::getApplication();
        $dbname = $config->getCfg('db', '');

        // compose the sql query
        $this->_db->setQuery("SHOW TABLES WHERE `Tables_in_{$dbname}` like 'bak_%'");

        // Get the bak_tables
        $tables = $this->_db->loadRowList();

        // return array if we are internally calling this method
        if ($internal === TRUE) {
            return $tables;
        }

        // count them
        $count = count($tables);

        // send back the totals
        bfEncrypt::reply('success', array(
            'tables' => $tables,
            'total'  => $count
        ));
    }

    /**
     * Get a list of folders with 777 permissions
     */
    private function getFolderPermissions()
    {
        // init mini-Joomla
        $this->_initDb();

        // set up the limit and the limitstart SQL
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        $this->_db->setQuery('SELECT `id`, `folderwithpath`, `folderinfo` from bf_folders WHERE folderinfo = "777" LIMIT ' . $limitstart . ', ' . $limit);

        // get the files
        $files = $this->_db->loadObjectList();

        // get the count for pagination
        $this->_db->setQuery('SELECT count(*) from bf_folders WHERE `folderinfo` = "777"');
        $count = $this->_db->loadResult();

        // send back the totals
        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * Get a list of files with 777 permissions
     */
    private function getFilePermissions()
    {
        // init mini-Joomla
        $this->_initDb();

        // set up the limit and the limitstart SQL
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        $this->_db->setQuery('SELECT id, filewithpath, fileperms from bf_files WHERE fileperms = "0777" OR fileperms = "777" LIMIT ' . ( int )$limitstart . ', ' . $limit);

        // get the files
        $files = $this->_db->loadObjectList();

        // get the count for pagination
        $this->_db->setQuery('SELECT count(*) from bf_files WHERE fileperms = "0777" OR fileperms = "777"');
        $count = $this->_db->loadResult();

        // send back the totals
        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * Set the permissions on files that have 777 perms to be 644
     */
    private function setFilePermissions()
    {
        $fixed  = 0;
        $errors = 0;

        $this->_initDb();
        $this->_db->setQuery('SELECT id, filewithpath from bf_files WHERE fileperms = "0777" OR fileperms = "777"');
        $files = $this->_db->loadObjectList();
        foreach ($files as $file) {
            if (@chmod(JPATH_BASE . $file->filewithpath, 0644)) {
                $fixed++;
                $this->_db->setQuery('UPDATE bf_files SET fileperms = "0644" WHERE id = "' . ( int )$file->id . '"');
                $this->_db->query();
            } else {
                $errors++;
            }
        }

        $this->_db->setQuery('SELECT count(*) FROM bf_folders WHERE folderinfo LIKE "%777%"');
        $folders_777 = $this->_db->LoadResult();

        $res           = new stdClass ();
        $res->errors   = $errors;
        $res->fixed    = $fixed;
        $res->leftover = $folders_777;

        bfEncrypt::reply('success', $res);
    }

    /**
     * Return the list of files that have been flagged as containing patterns that match our suspect patterns
     * These maybe false positives for suspect content, but might be examples of bad code standards like using
     * ../../../ or eval() method.
     */
    private function getSuspectContentFileList()
    {
        // Connect to the Joomla db
        $this->_initDb();

        // make sure we only retrieve a small dataset
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;

        // Set the query
        $this->_db->setQuery('SELECT id, iscorefile, filewithpath, filemtime, fileperms from bf_files
                                WHERE suspectcontent = 1
                                ORDER BY filewithpath
                                LIMIT ' . ( int )$limitstart . ', ' . $limit);

        // Get an object list of files
        $files = $this->_db->loadObjectList();

        // see how many files there are in total without a limit
        $this->_db->setQuery('SELECT count(*) from bf_files WHERE suspectcontent = 1');
        $count = $this->_db->loadResult();

        // return an encrypted reply
        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getInstallationFolders($internal = FALSE)
    {
        $this->_initDb();

        $this->_db->setQuery('SELECT * FROM bf_folders WHERE folderwithpath REGEXP "/.*installation.*"');
        $folders = $this->_db->loadObjectList();

        if ($internal === TRUE) {
            return $folders;
        }

        $this->_db->setQuery('SELECT * FROM bf_folders WHERE folderwithpath REGEXP "/.*installation.*"');
        $count = $this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $folders,
            'total' => $count
        ));
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getRecentlyModified($internal = FALSE)
    {
        $this->_initDb();
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        if (!$limitstart)
            $limitstart = 0;
        if (!$limit)
            $limit = '9999999999999999';

        $sql = "SELECT * FROM bf_files WHERE filemtime > '" . strtotime('-3 days', time()) . "' ORDER BY filemtime DESC LIMIT " . ( int )$limitstart . ', ' . $limit;
        $this->_db->setQuery($sql);
        $files = $this->_db->LoadObjectList();

        if ($internal === TRUE) {
            return $files;
        }

        $this->_db->setQuery("SELECT count(*) FROM bf_files WHERE filemtime > '" . strtotime('-3 days', time()) . "'");
        $count = $this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getHtaccessFiles($internal = FALSE)
    {
        $this->_initDb();
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        if (!$limitstart)
            $limitstart = 0;
        if (!$limit)
            $limit = '9999999999999999';

        $sql = "SELECT * FROM bf_files WHERE filewithpath LIKE '%/.htaccess' ORDER BY filewithpath DESC LIMIT " . ( int )$limitstart . ', ' . $limit;
        $this->_db->setQuery($sql);
        $files = $this->_db->LoadObjectList();

        if ($internal === TRUE) {
            return $files;
        }

        $this->_db->setQuery("SELECT count(*) FROM bf_files WHERE filewithpath LIKE '%/.htaccess'");
        $count = $this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getLargefiles($internal = FALSE)
    {
        $this->_initDb();
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        if (!$limitstart)
            $limitstart = 0;
        if (!$limit)
            $limit = '9999999999999999';

        $sql = 'SELECT * FROM bf_files WHERE SIZE > 2097152 ORDER BY filemtime DESC LIMIT ' . ( int )$limitstart . ', ' . $limit;
        $this->_db->setQuery($sql);
        $files = $this->_db->LoadObjectList();

        if ($internal === TRUE) {
            return $files;
        }

        $this->_db->setQuery('SELECT COUNT(*) FROM bf_files WHERE SIZE > 2097152');
        $count = (int)$this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getArchivefiles($internal = FALSE)
    {
        $this->_initDb();
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        if (!$limitstart)
            $limitstart = 0;
        if (!$limit)
            $limit = '9999999999999999';

        $sql = 'SELECT * FROM bf_files WHERE
		filewithpath LIKE "%.zip"
		OR filewithpath LIKE "%.tar"
		OR filewithpath LIKE "%.tar.gz"
		OR filewithpath LIKE "%.bz2"
		OR filewithpath LIKE "%.gzip"
		OR filewithpath LIKE "%.bzip2" ORDER BY filemtime DESC LIMIT ' . ( int )$limitstart . ', ' . $limit;
        $this->_db->setQuery($sql);
        $files = $this->_db->LoadObjectList();

        if ($internal === TRUE) {
            return $files;
        }

        $this->_db->setQuery('SELECT count(*) FROM bf_files WHERE
		filewithpath LIKE "%.zip"
		OR filewithpath LIKE "%.tar"
		OR filewithpath LIKE "%.tar.gz"
		OR filewithpath LIKE "%.bz2"
		OR filewithpath LIKE "%.gzip"
		OR filewithpath LIKE "%.bzip2"');
        $count = (int)$this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getTmpfiles($internal = FALSE)
    {
        $this->_initDb();
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        if (!$limitstart)
            $limitstart = 0;
        if (!$limit)
            $limit = '9999999999999999';

        $sql = 'SELECT * FROM bf_files WHERE
		filewithpath LIKE "/tmp%"
		AND
				filewithpath != "/tmp/index.html"
		ORDER BY filemtime DESC LIMIT ' . ( int )$limitstart . ', ' . $limit;
        $this->_db->setQuery($sql);
        $files = $this->_db->LoadObjectList();

        if ($internal === TRUE) {
            return $files;
        }

        $this->_db->setQuery('SELECT count(*) FROM bf_files WHERE
		filewithpath LIKE "/tmp%"
		AND
				filewithpath != "/tmp/index.html"
		ORDER BY filemtime');
        $count = (int)$this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    private function clearFluffFiles()
    {
        require 'bfFilesystem.php';

        $fluffFiles = array(
            '/robots.txt.dist',
            '/web.config.txt',
            '/joomla.xml',
            '/build.xml',
            '/LICENSE.txt',
            '/README.txt',
            '/htaccess.txt',
            '/LICENSES.php',
            '/configuration.php-dist',
            '/CHANGELOG.php',
            '/COPYRIGHT.php',
            '/CREDITS.php',
            '/INSTALL.php',
            '/LICENSE.php',
            '/CONTRIBUTING.md',
            '/phpunit.xml.dist',
            '/README.md',
            '/.travis.yml',
            '/travisci-phpunit.xml',
            '/.gitignore',
        );

        $files = array();

        foreach ($fluffFiles as $file) {

            // ensure we are based correctly
            $fileWithPath = JPATH_BASE . $file;

            // Remove File.
            unlink($fileWithPath);
        }

        $this->getFlufffiles(TRUE);
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getFlufffiles($internal = FALSE)
    {

        $fluffFiles = array(
            '/robots.txt.dist',
            '/web.config.txt',
            '/joomla.xml',
            '/build.xml',
            '/LICENSE.txt',
            '/README.txt',
            '/htaccess.txt',
            '/LICENSES.php',
            '/configuration.php-dist',
            '/CHANGELOG.php',
            '/COPYRIGHT.php',
            '/CREDITS.php',
            '/INSTALL.php',
            '/LICENSE.php',
            '/CONTRIBUTING.md',
            '/phpunit.xml.dist',
            '/README.md',
            '/.travis.yml',
            '/travisci-phpunit.xml',
            '/.gitignore',
        );

        $files               = array();
        $files['present']    = array();
        $files['notpresent'] = array();

        foreach ($fluffFiles as $file) {

            // ensure we are based correctly
            $fileWithPath = JPATH_BASE . $file;

            // determine if the file is present or not
            if (@file_exists($fileWithPath)) { //@ to avoid any nasty warnings
                $files['present'][] = $file;
            } else {
                $files['notpresent'][] = $file;
            }
        }

        bfEncrypt::reply('success', array(
            'total' => count($files['present']),
            'files' => $files
        ));
    }

    private function clearTmpFiles()
    {
        require 'bfFilesystem.php';

        $filesAndFolders = Bf_Filesystem::readDirectory(JPATH_ROOT . '/tmp');

        foreach ($filesAndFolders as $pointer) {
            $pointer = JPATH_ROOT . '/tmp/' . $pointer;

            if (is_dir($pointer)) {
                Bf_Filesystem::deleteRecursive($pointer, TRUE);
            } else {
                unlink($pointer);
            }
        }

        file_put_contents(JPATH_ROOT . '/tmp/index.html', '<html><body bgcolor="#FFFFFF"></body></html> ');

        $this->_initDb();
        $sql = 'DELETE FROM bf_files WHERE
		          filewithpath LIKE "/tmp%"
		            AND
				  filewithpath != "/tmp/index.html"';
        $this->_db->setQuery($sql);
        $this->_db->query();

        bfEncrypt::reply('success', array(
            'res' => TRUE
        ));
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getDotfiles($internal = FALSE)
    {
        $this->_initDb();
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        if (!$limitstart)
            $limitstart = 0;
        if (!$limit)
            $limit = '9999999999999999';

        $sql = "SELECT * FROM bf_files WHERE filewithpath LIKE \"%/.%\" ORDER BY filemtime DESC LIMIT " . ( int )$limitstart . ', ' . $limit;
        $this->_db->setQuery($sql);
        $files = $this->_db->LoadObjectList();

        if ($internal === TRUE) {
            return $files;
        }

        $this->_db->setQuery('SELECT count(*) FROM bf_files WHERE filewithpath LIKE "%/.%"');
        $count = $this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getEncrypted($internal = FALSE)
    {
        $this->_initDb();
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        if (!$limitstart)
            $limitstart = 0;
        if (!$limit)
            $limit = '9999999999999999';

        $sql = "SELECT * FROM bf_files WHERE encrypted = 1 ORDER BY filemtime DESC LIMIT " . ( int )$limitstart . ', ' . $limit;
        $this->_db->setQuery($sql);
        $files = $this->_db->LoadObjectList();

        if ($internal === TRUE) {
            return $files;
        }

        $this->_db->setQuery("SELECT count(*) FROM bf_files WHERE encrypted = 1");
        $count = $this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    /**
     * @param bool $internal
     *
     * @return JUser|mixed|object
     */
    private function getUser($internal = FALSE)
    {
        $this->_initDb();

        switch ($this->_dataObj->searchfield) {
            case 'username' :
                $sql = "SELECT * FROM #__users WHERE username = '%s'";
                $sql = sprintf($sql, $this->_dataObj->searchvalue);
                $this->_db->setQuery($sql);
                $row = $this->_db->loadObject();
                break;
            case 'id' :
                $row = new JUser ();
                $row->load(( int )$this->_dataObj->searchvalue);
                break;
        }

        if ($row->id) {
            // NEVER let the users password leave the remote site
            $row->password = '**REMOVED**';
        }

        if ($internal === TRUE) {
            return $row;
        }

        bfEncrypt::reply('success', array(
            'user' => $row
        ));
    }

    /**
     * @throws exception Exception
     */
    private function setDbPrefix()
    {

        // Require more complex methods for dealing with files
        require 'bfFilesystem.php';

        $this->_initDb();
        $prefix = $this->_dataObj->prefix;
        try {
            $prefix = $this->_validateDbPrefix($prefix);

            /**
             * Performs the actual schema change
             *
             * @package   AdminTools
             * @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
             * @license   GNU General Public License version 3, or later
             *
             * @param $prefix string
             *                The new prefix
             *
             * @return bool False if the schema could not be changed
             */
            $config = JFactory::getConfig();
            if (version_compare(JVERSION, '3.0', 'ge')) {
                $oldprefix = $config->get('dbprefix', '');
                $dbname    = $config->get('db', '');
            } else {
                $oldprefix = $config->getValue('config.dbprefix', '');
                $dbname    = $config->getValue('config.db', '');
            }

            $db  = $this->_db;
            $sql = "SHOW TABLES WHERE `Tables_in_{$dbname}` like '{$oldprefix}%'";
            $db->setQuery($sql);

            if (version_compare(JVERSION, '3.0', 'ge')) {
                $oldTables = $db->loadColumn();
            } else {
                $oldTables = $db->loadResultArray();
            }

            if (empty ($oldTables))
                throw new Exception ('Could not find any tables with the old prefix to change to the new prefix');

            foreach ($oldTables as $table) {
                $newTable = $prefix . substr($table, strlen($oldprefix));
                $sql      = "RENAME TABLE `$table` TO `$newTable`";
                $db->setQuery($sql);
                if (!$db->query()) {
                    // Something went wrong; I am pulling the plug and hope for
                    // the best
                    throw new Exception ('Something went wrong; I am pulling the plug and hope for the best - Contact our support URGENTLY');
                }
            }

            /**
             * Updates the configuration.php file with the given prefix
             *
             * @package   AdminTools
             * @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
             * @license   GNU General Public License version 3, or later
             *
             * @param $prefix string
             *                The prefix to write to the configuration.php file
             *
             * @return bool False if writing to the file was not possible
             */
            // Load the configuration and replace the db prefix
            $config = JFactory::getConfig();
            if (version_compare(JVERSION, '3.0', 'ge')) {
                $oldprefix = $config->get('dbprefix', $prefix);
            } else {
                $oldprefix = $config->getValue('config.dbprefix', $prefix);
            }
            if (version_compare(JVERSION, '3.0', 'ge')) {
                $config->set('dbprefix', $prefix);
            } else {
                $config->setValue('config.dbprefix', $prefix);
            }

            $newConfig = $config->toString('PHP', 'config', array(
                'class' => 'JConfig'
            ));
            // On some occasions, Joomla! 1.6 ignores the configuration and
            // produces "class c". Let's fix this!
            $newConfig = str_replace('class c {', 'class JConfig {', $newConfig);

            if (version_compare(JVERSION, '3.0', 'ge')) {
                $config->set('dbprefix', $oldprefix);
            } else {
                $config->setValue('config.dbprefix', $oldprefix);
            }

            // Try to write out the configuration.php
            $filename = JPATH_ROOT . DIRECTORY_SEPARATOR . 'configuration.php';
            $result   = Bf_Filesystem::_write($filename, $newConfig);
            if ($result !== FALSE) {
                bfEncrypt::reply('success', array(
                    'prefix' => $prefix
                ));
            } else {
                bfEncrypt::reply(bfReply::ERROR, array(
                    'msg' => 'Could Not Save Config'
                ));
            }
        } catch (Exception $e) {
            bfEncrypt::reply(bfReply::ERROR, array(
                'msg' => $e->getMessage()
            ));
        }
    }

    /**
     * Validates a prefix.
     * The prefix must be 3-6 lowercase characters followed by
     * an underscore and must not alrady exist in the current database. It must
     * also not be jos_ or bak_.
     *
     * @package   AdminTools
     * @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
     *
     * @param $prefix string
     *                The prefix to check
     *
     * @throws exception
     * @return string bool validated prefix or false if the prefix is invalid
     */
    private function _validateDbPrefix($prefix)
    {
        $this->_initDb();


        // Check that the prefix is not jos_ or bak_
        if (($prefix == 'jos_') || ($prefix == 'bak_'))
            throw new exception ('Cannot be a standard prefix like jos_ or bak_');

        // Check that we're not trying to reuse the same prefix
        $config = JFactory::getConfig();
        if (version_compare(JVERSION, '3.0', 'ge')) {
            $oldprefix = $config->get('dbprefix', '');
        } else {
            $oldprefix = $config->getValue('config.dbprefix', '');
        }
        if ($prefix == $oldprefix)
            throw new exception ('Cannot be the same as existing prefix');

        // Check the length
        $pLen = strlen($prefix);
        if (($pLen < 4) || ($pLen > 6))
            throw new exception ('Prefix must be between 4 and 6 chars');

        // Check that the prefix ends with an underscore
        if (substr($prefix, -1) != '_')
            throw new exception ('Prefix must end with an underscore');

        // Check that the part before the underscore is lowercase letters
        $valid = preg_match('/[\w]_/i', $prefix);
        if ($valid === 0)
            throw new exception ('Prefix must be all lowercase');

        // Turn the prefix into lowercase
        $prefix = strtolower($prefix);

        // Check if the prefix already exists in the database
        $db = $this->_db;
        if (version_compare(JVERSION, '3.0', 'ge')) {
            $dbname = $config->get('db', '');
        } else {
            $dbname = $config->getValue('config.db', '');
        }
        $sql = "SHOW TABLES WHERE `Tables_in_{$dbname}` like '{$prefix}%'";
        $db->setQuery($sql);
        if (version_compare(JVERSION, '3.0', 'ge')) {
            $existing_tables = $db->loadColumn();
        } else {
            $existing_tables = $db->loadResultArray();
        }
        if (count($existing_tables)) {
            // Sometimes we have false alerts, e.g. a prefix of dev_ will match
            // tables starting with dev15_ or dev16_
            $realCount = 0;
            foreach ($existing_tables as $check) {
                if (substr($check, 0, $pLen) == $prefix) {
                    $realCount++;
                    break;
                }
            }
            if ($realCount)
                throw new exception ('Prefix already exists in the database');
        }

        return $prefix;
    }

    /**
     *
     */
    private function setUser()
    {
        $email    = $this->_dataObj->email;
        $pass     = $this->_dataObj->password;
        $username = $this->_dataObj->username;
        $where    = $this->_dataObj->where;

        if (!$email || !$pass || !$username || !$where) {
            bfEncrypt::reply('failure', array(
                'msg' => 'Not all required parts set'
            ));
        }

        $this->_initDb();

        $sql = 'UPDATE #__users SET username="%s", password="%s", email ="%s" WHERE %s';
        $sql = sprintf($sql, $username, $pass, $email, $where);
        $this->_db->setQuery($sql);
        $id = $this->_db->query();

        bfEncrypt::reply('success', array(
            'usersaved' => $id
        ));
    }

    /**
     * @param bool $internal
     *
     * @return array|mixed
     */
    private function getErrorLogs($internal = FALSE)
    {
        $this->_initDb();
        $limitstart = ( int )$this->_dataObj->ls;
        $limit      = ( int )$this->_dataObj->limit;
        if (!$limitstart)
            $limitstart = 0;
        if (!$limit)
            $limit = '9999999999999999';

        $sql = "SELECT * FROM bf_files WHERE filewithpath LIKE '%error_log' ORDER BY filemtime DESC LIMIT " . ( int )$limitstart . ', ' . $limit;
        $this->_db->setQuery($sql);
        $files = $this->_db->LoadObjectList();

        if ($internal === TRUE) {
            return $files;
        }

        $this->_db->setQuery("SELECT count(*) FROM bf_files WHERE filewithpath LIKE '%error_log'");
        $count = $this->_db->loadResult();

        bfEncrypt::reply('success', array(
            'files' => $files,
            'total' => $count
        ));
    }

    private function saveRobotsFile()
    {
        if (file_put_contents(JPATH_BASE . '/robots.txt', base64_decode($this->_dataObj->filecontents))) {
            bfEncrypt::reply('success', array(
                'msg' => 'File saved!'
            ));
        } else {
            bfEncrypt::reply('error', array(
                'msg' => 'File could not be saved!'
            ));
        }

    }

    private function getRobotsFile()
    {
        $this->_initDb();
        $this->_db->setQuery('SELECT id from bf_files WHERE filewithpath = "/robots.txt"');
        $id = $this->_db->loadResult();
        $this->downloadfile($id);
    }

    /**
     *
     */
    private function downloadfile($file_id = NULL)
    {
        $this->_initDb();
        if (NULL === $file_id) {
            $file_id = ( int )$this->_dataObj->f;
        }

        $this->_db->setQuery('SELECT filewithpath from bf_files WHERE id = ' . $file_id);
        $filename     = $this->_db->loadResult();
        $filewithpath = JPATH_BASE . $filename;
        if (file_exists($filewithpath)) {
            $contents              = file_get_contents($filewithpath);
            $contentsbase64_encode = base64_encode($contents);
            $obj                   = new stdclass ();
            $obj->filename         = $filename;
            $obj->filemd5          = md5($contents);
            $obj->filewithpath     = $filewithpath;
            $obj->filecontents     = $contentsbase64_encode;
            $obj->filesize         = filesize($filewithpath);
            $obj->basepath         = JPATH_BASE;
            $obj->writeable        = is_writable($filewithpath);

            bfEncrypt::reply('success', array(
                'file' => $obj
            ));
        } else {
            bfEncrypt::reply('error', array(
                'msg' => 'File No Longer Exists!'
            ));
        }
    }

    /**
     *
     */
    private function restorefile()
    {
        // Require more complex methods for dealing with files
        require 'bfFilesystem.php';

        // init mini-joomla
        $this->_initDb();

        // get the cached data on the file
        $this->_db->setQuery('SELECT filewithpath FROM bf_files WHERE id = ' . $this->_dataObj->fileid);
        $file_to_restore_nopath = $this->_db->loadResult();
        $file_to_restore        = JPATH_BASE . $file_to_restore_nopath;

        $new_file_contents = base64_decode($this->_dataObj->filecontents);
        $new_md5           = md5($new_file_contents);
        if ($new_md5 !== $this->_dataObj->md5) {
            bfEncrypt::reply('failure', 'MD5 Check 1 Failed');
        }

        $this->_db->setQuery('SELECT hash FROM bf_core_hashes WHERE filewithpath = "' . $file_to_restore_nopath . '"');
        $core_md5 = $this->_db->loadResult();
        if ($core_md5 !== $this->_dataObj->md5) {
            bfEncrypt::reply('failure', 'MD5 Check 2 Failed');
        }

        $backup = file_get_contents($file_to_restore);
        Bf_Filesystem::_write($file_to_restore, $new_file_contents);

        if (md5_file($file_to_restore) !== $this->_dataObj->md5) {
            Bf_Filesystem::_write($file_to_restore, $backup);
            bfEncrypt::reply('failure', 'MD5 Check 3 Failed');
        }

        $this->_db->setQuery("UPDATE bf_files SET suspectcontent = 0 , hashfailed = 0 where filewithpath = '" . $file_to_restore_nopath . "'");
        $this->_db->query();

        bfEncrypt::reply('success', 'Restored OK');
    }

    /**
     *
     */
    private function checkFTPLayer()
    {
        $this->_initDb();
        $config     = JFactory::getApplication();
        $ftp_pass   = $config->getCfg('ftp_pass', '');
        $ftp_user   = $config->getCfg('ftp_user', '');
        $ftp_enable = $config->getCfg('ftp_enable', '');
        $ftp_host   = $config->getCfg('ftp_host', '');
        $ftp_root   = $config->getCfg('ftp_root', '');
        if ($ftp_pass || $ftp_user || $ftp_enable == '1' || $ftp_host || $ftp_root) {
            bfEncrypt::reply('success', 1);
        } else {
            bfEncrypt::reply('success', 0);
        }
    }

    /**
     *
     */
    private function disableFTPLayer()
    {
        $this->_initDb();

        $config      = JFactory::getApplication();
        $config_file = JPATH_BASE . '/configuration.php';

        $ftp_pass   = $config->getCfg('ftp_pass', '');
        $ftp_user   = $config->getCfg('ftp_user', '');
        $ftp_enable = $config->getCfg('ftp_enable', '');
        $ftp_host   = $config->getCfg('ftp_host', '');
        $ftp_root   = $config->getCfg('ftp_root', '');

        $config_txt = file_get_contents(JPATH_BASE . '/configuration.php');
        $config_txt = str_replace("\$ftp_enable = '1';", "\$ftp_enable = '0';", $config_txt);
        $config_txt = str_replace("\$ftp_pass = '" . $ftp_pass . "';", "\$ftp_pass = '';", $config_txt);
        $config_txt = str_replace("\$ftp_user = '" . $ftp_user . "';", "\$ftp_user = '';", $config_txt);
        $config_txt = str_replace("\$ftp_host = '" . $ftp_host . "';", "\$ftp_host = '';", $config_txt);
        $config_txt = str_replace("\$ftp_root = '" . $ftp_root . "';", "\$ftp_root = '';", $config_txt);

        @chmod($config_file, 0777);
        if (file_put_contents($config_file, $config_txt)) {
            @chmod($config_file, 0644);
            bfEncrypt::reply('success', 1);
        } else {
            bfEncrypt::reply('failure', 'Could not write configuration.php to ' . $config_file);
        }
    }

    /**
     *
     */
    private function setFolderPermissions()
    {
        $fixed  = 0;
        $errors = 0;

        $this->_initDb();
        $this->_db->setQuery('SELECT id, folderwithpath from bf_folders WHERE folderinfo = "777"');
        $folders = $this->_db->loadObjectList();
        foreach ($folders as $folder) {
            if (@chmod(JPATH_BASE . $folder->folderwithpath, 0755)) {
                $fixed++;
                $this->_db->setQuery('UPDATE bf_folders SET folderinfo = "755" WHERE id = "' . ( int )$folder->id . '" AND folderinfo = "777"');
                $this->_db->query();
            } else {
                $errors++;
            }
        }

        $this->_db->setQuery('SELECT count(*) FROM bf_folders WHERE folderinfo LIKE "%777%"');
        $folders_777 = $this->_db->LoadResult();

        $res           = new stdClass ();
        $res->errors   = $errors;
        $res->fixed    = $fixed;
        $res->leftover = $folders_777;

        bfEncrypt::reply('success', $res);
    }

    /**
     * I do some sanity checks then enable .htaccess
     */
    private function setHtaccess()
    {
        // Require more complex methods for dealing with files
        require 'bfFilesystem.php';

        // init bfDatabase
        $this->_initDb();

        // To
        $htaccess = JPATH_BASE . DIRECTORY_SEPARATOR . '.htaccess';

        // From
        $htaccesstxt = JPATH_BASE . DIRECTORY_SEPARATOR . 'htaccess.txt';

        $res = new stdClass();
        if (file_exists($htaccess)) {
            $res->result = 'ERROR';
            $res->msg    = '.htaccess file already exists!';
            bfEncrypt::reply(bfReply::SUCCESS, $res);
        }

        if (!file_exists($htaccesstxt)) {
            $res->result = 'ERROR';
            $res->msg    = 'htaccess.txt file not found, cannot proceed';
            bfEncrypt::reply(bfReply::SUCCESS, $res);
        }

        // Test we are on apache
        if (!preg_match('/Apache/i', $_SERVER['SERVER_SOFTWARE'])) {
            $res->result = 'ERROR';
            $res->msg    = 'Server reported its not running Apache';
            bfEncrypt::reply(bfReply::SUCCESS, $res);
        }

        $didItWork = Bf_Filesystem::_write($htaccess, file_get_contents($htaccesstxt));

        if ($didItWork == FALSE) {
            $res->result = 'ERROR';
            $res->msg    = 'Could not copy htaccess.txt to .htaccess';
            bfEncrypt::reply(bfReply::SUCCESS, $res);
        }

        $res->result = 'SUCCESS';
        $res->msg    = '.htaccess enabled! - Go and test your site!';
        bfEncrypt::reply(bfReply::SUCCESS, $res);
    }

    /**
     * I set the new database credentials in /configuration.php after some testing
     */
    private function setDbCredentials()
    {
        // Require more complex methods for dealing with files
        require 'bfFilesystem.php';
        $this->_initDb();

        $password = $this->_dataObj->p;
        $user     = $this->_dataObj->u;

        $res = $this->testDbCredentials(TRUE);
        if ($res->result == 'error') {
            bfEncrypt::reply(bfReply::ERROR, $res);
        }
        /**
         * Updates the configuration.php file with the given prefix
         * (some code from below)
         *
         * @package   AdminTools
         * @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
         * @license   GNU General Public License version 3, or later
         *
         * @param $prefix string
         *                The prefix to write to the configuration.php file
         *
         * @return bool False if writing to the file was not possible
         */
        // Load the configuration and replace the db prefix
        $config = JFactory::getConfig();
        if (version_compare(JVERSION, '3.0', 'ge')) {
            $olduser     = $config->get('user');
            $oldpassword = $config->get('password');
            $host        = $config->get('host');
        } else {
            $olduser     = $config->getValue('config.user');
            $oldpassword = $config->getValue('configpassword');
            $host        = $config->getValue('host');
        }

        if (version_compare(JVERSION, '3.0', 'ge')) {
            $config->set('user', $user);
            $config->set('password', $password);
        } else {
            $config->setValue('config.user', $user);
            $config->setValue('config.password', $password);
        }

        $newConfig = $config->toString('PHP', 'config', array(
            'class' => 'JConfig'
        ));

        // On some occasions, Joomla! 1.6 ignores the configuration and
        // produces "class c". Let's fix this!
        $newConfig = str_replace('class c {', 'class JConfig {', $newConfig);

        // Try to write out the configuration.php
        $filename = JPATH_ROOT . DIRECTORY_SEPARATOR . 'configuration.php';
        $result   = Bf_Filesystem::_write($filename, $newConfig);

        // reconnect db! to use new credentials
        $newConnectionOptions['user']     = $user;
        $newConnectionOptions['password'] = $password;
        $newConnectionOptions['host']     = $host;

        // make new db connection
        $db = JDatabase::getInstance($newConnectionOptions);
        $db->setQuery('SHOW DATABASES  where `Database` NOT IN ("test", "information_schema", "mysql")');
        $dbs_visible = count($db->loadObjectList());

        if ($result !== FALSE) {
            bfEncrypt::reply('success', array(
                'msg'         => 'Config saved!',
                'dbs_visible' => $dbs_visible
            ));
        } else {
            bfEncrypt::reply(bfReply::ERROR, array(
                'msg' => 'Could Not Save Config'
            ));
        }
    }

    /**
     * @param bool $internal
     *
     * @return stdClass
     */
    private function testDbCredentials($internal = FALSE)
    {
        try {
            $this->_initDb();
            $config = JFactory::getApplication();
            $pass   = $this->_dataObj->p;
            $user   = $this->_dataObj->u;
            $host   = $config->getCfg('host', '');
            $db     = $config->getCfg('db', '');
            $link   = @mysql_connect($host, $user, $pass);
            $msg    = new stdClass ();
            if (!$link) {
                $msg->msg    = trim(mysql_error() . ' Could not connect to mysql server with supplied credentials');
                $msg->result = 'error';
                if ($internal === TRUE)
                    return $msg;
                bfEncrypt::reply('success', $msg);
            }
            if (!@mysql_select_db($db)) {
                $msg->msg    = trim(mysql_error() . ' Mysql User exists, but has no access to the database');
                $msg->result = 'error';
                if ($internal === TRUE)
                    return $msg;
                bfEncrypt::reply('success', $msg);
            }
            $msg->result = 'success';
            if ($internal === TRUE)
                return $msg;
            bfEncrypt::reply('success', $msg);
        } catch (Exception $e) {

            bfEncrypt::reply('error', 'exception: ' . $e->getMessage());
        }
    }

    /**
     *
     */
    private function getUpdatesCount()
    {
        require 'bfUpdates.php';

        $bfUpdates = new bfUpdates ();

        bfEncrypt::reply('success', array(
            'count' => $bfUpdates->getupdates(TRUE)
        ));

    }

    /**
     *
     */
    private function getUpdatesDetail()
    {
        require 'bfUpdates.php';

        $bfUpdates = new bfUpdates ();

        bfEncrypt::reply('success', array(
            'current_joomla_version' => JVERSION,
            'availableUpdates'       => $bfUpdates->getupdates()
        ));
    }

    /**
     * Fix Db Schema version in the db
     * @since 20130929
     */
    private function fixDbSchema()
    {
        require JPATH_ADMINISTRATOR . '/components/com_installer/models/database.php';
        $model = new InstallerModelDatabase();
        $model->fix();

        $changeSet = $model->getItems();
        bfEncrypt::reply('success', array(
            'latest'        => $changeSet->getSchema(),
            'current'       => $model->getSchemaVersion(),
            'schema_errors' => $model->getItems()
                                     ->check()
        ));
    }

    /**
     * Return the DB schema
     * @since 20130929
     */
    private function getDbSchemaVersion()
    {
        require JPATH_ADMINISTRATOR . '/components/com_installer/models/database.php';
        $model     = new InstallerModelDatabase();
        $changeSet = $model->getItems();
        bfEncrypt::reply('success', array(
            'latest'        => $changeSet->getSchema(),
            'current'       => $model->getSchemaVersion(),
            'schema_errors' => $model->getItems()
                                     ->check()
        ));
    }

    private function checkGoogleFile()
    {
        $found = FALSE;
        $files = scandir(JPATH_BASE);
        foreach ($files as $file) {
            if (preg_match('/google.*\.html/', $file))
                $found = TRUE;
        }
        bfEncrypt::reply('success', array(
            'found' => $found
        ));
    }

    private function toggleOnline()
    {
        // Require more complex methods for dealing with files
        require 'bfFilesystem.php';

        if ($this->_dataObj->status == "true") {
            $this->_dataObj->status = 0;
        } else if ($this->_dataObj->status == "false") {
            $this->_dataObj->status = 1;
        } else {
            $this->_dataObj->status = 0;
        }

        $config = JFactory::getConfig();

        if (version_compare(JVERSION, '3.0', 'ge')) {
            $config->set('offline', $this->_dataObj->status);
        } else {
            $config->setValue('config.offline', $this->_dataObj->status);
        }

        $newConfig = $config->toString('PHP', 'config', array(
            'class' => 'JConfig'
        ));
        // On some occasions, Joomla! 1.6 ignores the configuration and
        // produces "class c". Let's fix this!
        $newConfig = str_replace('class c {', 'class JConfig {', $newConfig);

        // Try to write out the configuration.php
        $filename = JPATH_ROOT . DIRECTORY_SEPARATOR . 'configuration.php';
        $result   = Bf_Filesystem::_write($filename, $newConfig);
        if ($result !== FALSE) {
            bfEncrypt::reply('success', array(
                'offline' => $this->_dataObj->status
            ));
        } else {
            bfEncrypt::reply(bfReply::ERROR, array(
                'msg' => 'Could Not Save Config'
            ));
        }

    }

    private function getOfflineStatus()
    {
        $config = JFactory::getApplication();
        bfEncrypt::reply('success', array(
            'offline' => $config->getCfg('offline')
        ));
    }
}

// init this class
$securityController = new bfTools ($dataObj);

// Run the tool method
$securityController->run();
