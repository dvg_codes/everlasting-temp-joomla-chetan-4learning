<?php
/**
 * @package Blue Flame Network (bfNetwork)
 * @copyright Copyright (C) 2011, 2012, 2013, 2014 Blue Flame IT Ltd. All rights reserved.
 * @license GNU General Public License version 3 or later
 * @link http://myJoomla.com/
 * @author Phil Taylor / Blue Flame IT Ltd.
 *
 * bfNetwork is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bfNetwork is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this package.  If not, see http://www.gnu.org/licenses/
 */
define ('_BF_IN_UPGRADE', 1);

try {
    require 'bfEncrypt.php';

    /**
     * If we have got here then we have already passed through decrypting
     * the encrypted header and so we are sure we are now secure and no one
     * else cannot run the code below......
     */

    // need bfZip to decompress
    require 'bfZip.php';

    // Read the version
    $oldVersion = file_get_contents('VERSION');

    // Check we have everything we need.
    if (!$dataObj->NOTENCRYPTED ['checksum'] || !$dataObj->NOTENCRYPTED ['zipfiledata']) {
        throw new Exception('Not all required elements received');
    }

    // Make sure that the checksum is right
    if (md5($dataObj->NOTENCRYPTED ['zipfiledata']) != $dataObj->NOTENCRYPTED ['checksum']) {
        throw new Exception('Checksum Mismatch');
    }

    // attempt to ensure our folder is writable
    if (!is_writeable('.')) {
        @chmod('.', 0755);
    }

    // Argh!
    if (!is_writeable('.')) {
        @chmod('.', 0777);
    }

    // Give Up!
    if (!is_writeable('.')) {
        throw new Exception('bfNetwork Folder not writeable');
    }

    // Save the Zip File
    if (!file_put_contents('upgrade', base64_decode($dataObj->NOTENCRYPTED ['zipfiledata']))) {
        throw new Exception('Could not auto upgrade (save upgrade file failed) - you need to install a new connector manually');
    }

    // Load the Zip file
    $zip = new Bf_Zip ('upgrade');

    // Extract the Zip file
    $ok = $zip->extract();

    if (!$ok) {
        throw new Exception('Could not auto upgrade (Extract Error) - you need to install a new connector manually');
    }
    // cleanup
    @unlink('upgrade');

    // cleanup old files
    if (file_exists('./bfViewLog.php')){
        unlink('./bfViewLog.php');
    }

    // cleanup old files
    if (file_exists('./bfDev.php')){
        unlink('./bfDev.php');
    }

    // Read the version
    $newVersion = file_get_contents('VERSION');

    if ($oldVersion === $newVersion) {
        // throw new Exception('Could not auto upgrade, version before upgrade was'.$oldVersion.' version after upgrade is '.$newVersion.' (Version file still the same after upgrade) - you need to install a new connector manually');
    }

    // Reply with a great big high five!
    bfEncrypt::reply(bfReply::SUCCESS, array(
                                            'version' => $newVersion
                                       ));

} catch (Exception $e) {
    bfEncrypt::reply(bfReply::ERROR, 'EXCEPTION: ' . $e->getMessage());
}
