<?php
/**
* @version 1.0.0
* @package RSFinder! 1.0.0
* @copyright (C) 2009-2010 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
define('RSFINDER','EXISTS');

$user = & JFactory::getUser();
if(isset($_GET['rsquick']) && !$user->id) 
{
	echo '<li><a href="index.php" id="result_0" class="rsInactive">Session Expired. Please log in.</a></li>'."\n";
	exit;
}

jimport( 'joomla.plugin.plugin' );

/**
 * RSFinder! system plugin
 */
class plgSystemRSFinder extends JPlugin
{
	
	function plgSystemRSFinder( &$subject, $config )
	{
		jimport('joomla.html.parameter');
		parent::__construct( $subject, $config ); 								//Call the parent constructor
		$this->_plugin =& JPluginHelper::getPlugin( 'system', 'rsfinder' ); 	//Get a reference to this plugin
		$this->_params = new JParameter( $this->_plugin->params ); 				//Get the plugin parameters
	}

	function is16()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return (version_compare('1.6.0', $current_version) <= 0);
	}
	
	function onAfterRender()
	{
		$db = &JFactory::getDBO();
		$lang =& JFactory::getLanguage();
		
		$app =& JFactory::getApplication();	
		if($app->getName() != 'site') 
		{
			$user = & JFactory::getUser();
			
			$search = JRequest::getVar('rsquick');
			
			if (isset($search) && strlen($search) > 1 && $user->id)
			{	
				$search = str_replace('&','',$search);
				$output = '';
				$index = 0;
				$limit = $this->_params->get( 'limit', 0);
				if($limit == 0) $limit == 50;
				
				//articles
				if ($this->_params->get( 'searchArticles', 0) && $index<$limit)
				{
					$db->setQuery("SELECT id as value, title as text FROM #__content WHERE title LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if (!empty($results))
						foreach($results as $result)
						{
							if ($index < $limit)
							{
								if ($this->is16())
									$output .= '<li><a href="index.php?option=com_content&task=article.edit&id='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit article)</em></a></li>'."\n";
								else
									$output .= '<li><a href="index.php?option=com_content&sectionid=-1&task=edit&cid[]='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit article)</em></a></li>'."\n";
							}
					
							$index ++;
						}
				}
				
				//components
				if ($this->_params->get( 'searchComponents', 0) && $index<$limit)
				{
					if ($this->is16())
					{
						$db->setQuery("SELECT name as text, element FROM #__extensions WHERE type = 'component' AND name LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
						$results = $db->loadObjectList();
						
						if (!empty($results))
						foreach($results as $result)
						{
							if ($index < $limit)
							{
								if ($this->is16())
									$output .= '<li><a href="index.php?option='.$result->element.'" id="result_'.$index.'" class="rsInactive">'.JText::_($result->text).' <em>(component)</em></a></li>'."\n";
							}
							$index ++;
						}
						
					} else 
					{
						$db->setQuery("SELECT `id` as value, `name` as text, `admin_menu_link`, `parent`, `option` FROM #__components WHERE admin_menu_link!='' AND name LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
						$results = $db->loadObjectList();
						
						if (!empty($results))
							foreach ($results as $result)
							{
								$lang->load($result->option.'.menu',JPATH_ADMINISTRATOR);
								
								if ($result->parent)
								{
									$db->setQuery("SELECT name FROM #__components WHERE id = '".$result->parent."'");
									$parent_name = $db->loadResult();
									$text = $result->text;
									if (!$lang->hasKey($result->text))
										$result->text = strtoupper($result->option).'.'.$result->text;
									if (!$lang->hasKey($result->text))
										$result->text = $text;
									$result->text = $parent_name . ' - ' . JText::_($result->text);
								}
								
								if($index<$limit)
									$output .= '<li><a href="index.php?'.$result->admin_menu_link.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(component)</em></a></li>'."\n";
							
							$index ++;
							}
					}
				}
				
				//categories
				if($this->_params->get( 'searchCategories', 0) && $index<$limit)
				{
					if ($this->is16())
						$db->setQuery("SELECT id as value, title as text, extension FROM #__categories WHERE title LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					else
						$db->setQuery("SELECT id as value, title as text FROM #__categories WHERE title LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if (!empty($results))
						foreach ($results as $result)
						{
							if($index<$limit)
							{
								if ($this->is16())
									$output .= '<li><a href="index.php?option=com_categories&task=category.edit&id='.$result->value.'&extension='.$result->extension.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit category)</em></a></li>'."\n";
								else
									$output .= '<li><a href="index.php?option=com_categories&section=com_content&task=edit&type=content&cid[]='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit category)</em></a></li>'."\n";
							}
							$index ++;
						}
				}
					
				//sections
				if($this->_params->get( 'searchSections', 0) && $index<$limit)
				{
					if (!$this->is16())
					{
						$db->setQuery("SELECT id as value, title as text FROM #__sections WHERE title LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
						$results = $db->loadObjectList();
						
						if (!empty($results))
							foreach ($results as $result)
							{
								if ($index<$limit)
									$output .= '<li><a href="index.php?option=com_sections&scope=content&task=edit&cid[]='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit section)</em></a></li>'."\n";
								$index ++;
							}
					}
				}
				
				
				//users
				if($this->_params->get( 'searchUsers', 0) && $index<$limit)
				{
					$db->setQuery("SELECT id as value, username as text FROM #__users WHERE username LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if (!empty($results))
						foreach($results as $result)
						{
							if ($index<$limit)
							{
								if ($this->is16())
									$output .= '<li><a href="index.php?option=com_users&task=user.edit&id='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit user)</em></a></li>'."\n";
								else
									$output .= '<li><a href="index.php?option=com_users&view=user&task=edit&cid[]='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit user)</em></a></li>'."\n";
							}
							$index ++;
						}
				}
					
				//menu items
				if ($this->_params->get( 'searchMenuItems', 0) && $index<$limit)
				{
					if ($this->is16())
						$db->setQuery("SELECT id as value, `title` as text FROM #__menu WHERE `menutype` != 'main' AND `menutype` != 'menu' AND title LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					else
						$db->setQuery("SELECT id as value, `name` as text FROM #__menu WHERE name LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if (!empty($results))
						foreach ($results as $result)
						{
							if($index<$limit)
							{
								if ($this->is16())
									$output .= '<li><a href="index.php?option=com_menus&task=item.edit&id='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.JText::_($result->text).' <em>(edit menu item)</em></a></li>'."\n";
								else
									$output .= '<li><a href="index.php?option=com_menus&menutype=mainmenu&task=edit&cid[]='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit menu item)</em></a></li>'."\n";
							}
							$index ++;
						}
				}
				
				//menus
				if ($this->_params->get( 'searchMenus', 0) && $index < $limit)
				{
					$db->setQuery("SELECT DISTINCT menutype as value, `title` as text FROM #__menu_types WHERE menutype LIKE '%".$db->getEscaped($search)."%' OR title LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if (!empty($results))
						foreach ($results as $result)
						{
							if($index<$limit)
							{
								$output .= '<li><a href="index.php?option=com_menus&task=view&menutype='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(list menu items)</em></a></li>'."\n";
							}
							$index ++;
						}
				}
					
				//modules
				if ($this->_params->get( 'searchModules', 0) && $index<$limit)
				{
					$db->setQuery("SELECT id as value, `title` as text FROM #__modules WHERE title LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if (!empty($results))
						foreach ($results as $result)
						{
							if($index<$limit)
							{
								if ($this->is16())
									$output .= '<li><a href="index.php?option=com_modules&task=module.edit&id='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit module)</em></a></li>'."\n";
								else
									$output .= '<li><a href="index.php?option=com_modules&client=0&task=edit&cid[]='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit module)</em></a></li>'."\n";
							}
							$index ++;
						}
				}
					
				//plugins
				if ($this->_params->get( 'searchPlugins', 0) && $index<$limit)
				{
					if ($this->is16())
						$db->setQuery("SELECT extension_id as value, `name` as text FROM #__extensions WHERE type = 'plugin' AND name LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					else
						$db->setQuery("SELECT id as value, `name` as text FROM #__plugins WHERE name LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if (!empty($results))
						foreach ($results as $result)
						{
							if($index<$limit)
							{
								if ($this->is16())
									$output .= '<li><a href="index.php?option=com_plugins&task=plugin.edit&extension_id='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit plugin)</em></a></li>'."\n";
								else
									$output .= '<li><a href="index.php?option=com_plugins&view=plugin&client=site&task=edit&cid[]='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit plugin)</em></a></li>'."\n";
							}
							$index ++;
						}
				}
				
				//RSEvents! events
				if($this->_params->get( 'searchRsevents', 0) && $index<$limit)
				{
					$db->setQuery("SELECT IdEvent as value, `EventName` as text FROM #__rsevents_events WHERE EventName LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if(!empty($results))
						foreach($results as $result){
							if($index<$limit)
								$output .= '<li><a href="index.php?option=com_rsevents&task=editevent&cid='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit event)</em></a></li>'."\n";
							$index ++;
						}
				}
				
				//Profiles
				if($this->_params->get( 'searchProfiles', 0) && $index<$limit)
				{
					$db->setQuery("SELECT `id`, `state`, `first_name`, `spouse_name`, `last_name` FROM #__profiles_families WHERE `first_name` LIKE '%".$db->getEscaped($search)."%' OR `spouse_name` LIKE '%".$db->getEscaped($search)."%' OR `last_name` LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if(!empty($results))
						foreach($results as $result){
							if($result->state === '1')
							{
								$text = $result->first_name;
								if($result->spouse_name)
								{
									$text .= ' &amp; '.$result->spouse_name;
								}
								$text .= ' '.$result->last_name;
								if($index<$limit)
									$output .= '<li><a href="index.php?option=com_profiles&view=family&layout=edit&id='.$result->id.'" id="result_'.$index.'" class="rsInactive">'.$text.' <em>(edit profile)</em></a></li>'."\n";
								$index ++;
							}
						}
				}

				//K2 articles
				if($this->_params->get( 'searchk2', 0) && $index<$limit)
				{
					$db->setQuery("SELECT id as value, `title` as text FROM #__k2_items WHERE title LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if(!empty($results))
						foreach($results as $result){
							if($index<$limit)
								$output .= '<li><a href="index.php?option=com_k2&view=item&cid='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit K2 article)</em></a></li>'."\n";
							$index ++;
						}
				}

				//Virtuemart products
				if($this->_params->get( 'searchVirtuemart', 0) && $index<$limit)
				{
					$db->setQuery("SELECT product_id as value, `product_name` as text FROM #__vm_product WHERE product_name LIKE '%".$db->getEscaped($search)."%' LIMIT ".$limit);
					$results = $db->loadObjectList();
					
					if(!empty($results))
						foreach($results as $result){
							if($index<$limit)
								$output .= '<li><a href="index.php?option=com_virtuemart&page=product.product_form&product_id='.$result->value.'" id="result_'.$index.'" class="rsInactive">'.$result->text.' <em>(edit Virtuemart product)</em></a></li>'."\n";
							$index ++;
						}
				}

				/*
				//default joomla links
				if ($this->is16())
				{
					$matches[] = array('text'=>'new article','value'=>'index.php?option=com_content&task=article.add');
					$matches[] = array('text'=>'new category','value'=>'index.php?option=com_categories&task=category.add&extension=com_content');
					$matches[] = array('text'=>'new menu','value'=>'index.php?option=com_menus&view=item&layout=edit&menutype=mainmenu');
					$matches[] = array('text'=>'new user','value'=>'index.php?option=com_users&task=user.add');
					$matches[] = array('text'=>'list categories','value'=>'index.php?option=com_categories&extension=com_content');
					$matches[] = array('text'=>'list users','value'=>'index.php?option=com_users&view=users');
					$matches[] = array('text'=>'menu manager','value'=>'index.php?option=com_menus&view=menus');
					$matches[] = array('text'=>'new group','value'=>'index.php?option=com_users&task=group.add');
					$matches[] = array('text'=>'new level','value'=>'index.php?option=com_users&task=level.add');
				
				} else 
				{
					$matches[] = array('text'=>'new article','value'=>'index.php?option=com_content&sectionid=-1&task=edit&cid[]=0');
					$matches[] = array('text'=>'new category','value'=>'index.php?option=com_categories&section=com_content&task=edit&type=content&cid[]=0');
					$matches[] = array('text'=>'new section','value'=>'index.php?option=com_sections&scope=content');
					$matches[] = array('text'=>'list sections','value'=>'index.php?option=com_sections&scope=content');
					$matches[] = array('text'=>'new menu','value'=>'index.php?option=com_menus&menutype=mainmenu&task=edit&cid[]=0');
					$matches[] = array('text'=>'new user','value'=>'index.php?option=com_users&view=user&task=edit&cid[]=0');
					$matches[] = array('text'=>'list categories','value'=>'index.php?option=com_categories&section=com_content');
					$matches[] = array('text'=>'list users','value'=>'index.php?option=com_users&task=view');
					$matches[] = array('text'=>'menu manager','value'=>'index.php?option=com_menus');
					$matches[] = array('text'=>'menu trash','value'=>'index.php?option=com_trash&task=viewMenu');
					$matches[] = array('text'=>'frontpage manager','value'=>'index.php?option=com_frontpage');
				}
				
				$matches[] = array('text'=>'templates manager','value'=>'index.php?option=com_templates');
				$matches[] = array('text'=>'languages manager','value'=>'index.php?option=com_languages');
				$matches[] = array('text'=>'modules manager','value'=>'index.php?option=com_modules');
				$matches[] = array('text'=>'plugins manager','value'=>'index.php?option=com_plugins');
				$matches[] = array('text'=>'install uninstall extensions','value'=>'index.php?option=com_installer');
				$matches[] = array('text'=>'global configuration','value'=>'index.php?option=com_config');
				$matches[] = array('text'=>'control panel','value'=>'index.php');
				$matches[] = array('text'=>'media manager','value'=>'index.php?option=com_media');
				$matches[] = array('text'=>'new event','value'=>'index.php?option=com_rsevents&task=editevent');
				$matches[] = array('text'=>'new k2 article','value'=>'index.php?option=com_k2&view=item');
				$matches[] = array('text'=>'new virtuemart product','value'=>'index.php?option=com_virtuemart&pshop_mode=admin&page=product.product_form');
				
				foreach($matches as $match)
				{
					if (stristr($match['text'],$search) && $index<$limit)
					{
						if($index<$limit)
							$output .= '<li><a href="'.$match['value'].'" id="result_'.$index.'" class="rsInactive">'.$match['text'].'</a></li>'."\n";
						$index ++;
					}
				}*/
					
				if($output == '') $output = '<li>not found</li>';
				
				echo $output;
				exit;
			}
		}
	}

}