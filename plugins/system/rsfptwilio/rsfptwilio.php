<?php
/**
 * @version 1.3.0
 * @package RSform!Pro Twilio Notifications
 * @copyright (C) 2014 www.electriceasel.com
 * @license GPL, http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Twilio SMS Notifications Plugin for RSForm! Pro
 */
class plgSystemRsfptwilio extends JPlugin
{
	/**
	 * @var JApplication
	 */
	protected $app;

	/**
	 * @var JDatabase
	 */
	protected $db;

	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	object	$subject The object to observe
	 * @param 	array   $config  An array that holds the plugin configuration
	 * @since	1.0
	 */
	public function __construct(&$subject, $config = array())
	{
		parent::__construct($subject, $config);
		$this->app = JFactory::getApplication();
		$this->db = JFactory::getDbo();

		$lang = JFactory::getLanguage();
		$lang->load('plg_system_rsfptwilio');
		$lang->load('plg_system_rsfptwilio', JPATH_ADMINISTRATOR);

		require __DIR__ . '/src/table.php';

		if (!class_exists('Services_Twilio'))
		{
			require __DIR__ . '/src/twilio-api/Twilio.php';
		}
	}

	public function rsfp_bk_onAfterShowConfigurationTabs($tabs)
	{
		$tabs->addTitle(JText::_('RSFP_TWILIO_LABEL'), 'form-twilio');
		$tabs->addContent($this->twilioConfigurationScreen());
	}

	/**
	 * Do the SMS Notification
	 *
	 * @param $args
	 */
	public function rsfp_f_onBeforeStoreSubmissions($args)
	{
		$formId = $args['formId'];

		$table = new TwilioTable;

		if ($table->load(array('form_id' => $formId)) && $table->published == 1)
		{
			$fields = (array) @unserialize($table->fields);
			$formName = $this->getFormName($formId);
			$sid = RSFormProHelper::getConfig('twilio.sid');
			$token = RSFormProHelper::getConfig('twilio.token');
			$sendingNumber = RSFormProHelper::getConfig('twilio.number');
			$numbersToNotify = array_filter(explode("\n", $table->numbers));

			$twilio = new Services_Twilio($sid, $token);

			$message = $formName . "\n\n";
			$message .= $this->buildMessage($args['post'], $fields);

			foreach ($numbersToNotify as $number)
			{
				try
				{
					$twilio->account->messages->sendMessage($sendingNumber, $number, $message);
				}
				catch (Services_Twilio_RestException $e)
				{
					continue;
				}
			}
		}
	}

	public function rsfp_onFormSave($form)
	{
		$data = array(
			'form_id' => $this->app->input->getInt('formId', null),
			'published' => $this->app->input->getInt('twilio_published', null),
			'numbers' => $this->app->input->getString('twilio_numbers', null),
			'fields' => serialize($this->app->input->get('twilio_fields', array(), 'array'))
		);

		$table = new TwilioTable;

		if ($data['form_id'] !== null)
		{
			$table->load(array('form_id' => $data['form_id']));
		}

		if (! $table->save($data))
		{
			JError::raiseWarning(500, $table->getError());
			return false;
		}

		return true;
	}

	public function rsfp_bk_onAfterShowFormEditTabsTab()
	{
		JFactory::getDocument()
			->addStyleDeclaration('li a#twilio span{background:url(/plugins/system/rsfptwilio/src/twilio_icon.png) no-repeat 10px center}');
		echo '<li><a href="javascript: void(0);" id="twilio"><span>'.JText::_('RSFP_TWILIO_INTEGRATION').'</span></a></li>';
	}

	public function rsfp_bk_onAfterShowFormEditTabs()
	{
		$formId = $this->app->input->getInt('formId', null);

		$lang = JFactory::getLanguage();
		$lang->load('plg_system_rsfptwilio');

		$table = new TwilioTable;

		$table->load(array('form_id' => $formId));

		$published = RSFormProHelper::renderHTML('select.booleanlist', 'twilio_published', 'class="inputbox"', $table->published);
		$checkboxes = $this->buildCheckboxInput($formId, unserialize($table->fields));

		$html = array();
		$html[] = '<div id="twiliodiv">';
		$html[] = '<table class="admintable">';
		$html[] = '<tr>';
		$html[] = '<td valign="top" align="left" width="30%">';
		$html[] = '<table>';
		$html[] = '<tr>';
		$html[] = '<td colspan="2"><div class="rsform_error">' . JText::_('RSFP_TWILIO_DESC') . '</div></td>';
		$html[] = '</tr>';
		$html[] = '<tr>';
		$html[] = '<td width="80" align="right" nowrap="nowrap" class="key">' . JText::_('RSFP_TWILIO_USE_INTEGRATION') . '</td>';
		$html[] = '<td>' . $published . '</td>';
		$html[] = '</tr>';
		$html[] = '<tr>';
		$html[] = '<td width="40" align="right" nowrap="nowrap" class="key">' . JText::_('RSFP_TWILIO_NUMBERS_LABEL') . '</td>';
		$html[] = '<td><textarea name="twilio_numbers" rows="5" cols="20">' . $table->numbers . '</textarea></td>';
		$html[] = '</tr>';
		$html[] = '<tr>';
		$html[] = '<td width="40" align="right" nowrap="nowrap" class="key">' . JText::_('RSFP_TWILIO_FIELDS_LABEL') . '</td>';
		$html[] = '<td>' . $checkboxes . '</td>';
		$html[] = '</tr>';
		$html[] = '</table>';
		$html[] = '</div>';

		echo implode("\n", $html);
	}

	protected function canRun()
	{
		if (class_exists('RSFormProHelper')) return true;

		$helper = JPATH_ADMINISTRATOR . '/components/com_rsform/helpers/rsform.php';

		if (file_exists($helper))
		{
			require_once $helper;
			RSFormProHelper::readConfig();
			return true;
		}

		return false;
	}

	protected function twilioConfigurationScreen()
	{
		ob_start();
		?>
		<div id="page-twilio">
			<table class="admintable">
				<tr>
					<td width="200" style="width: 200px;" align="right" class="key"><label for="public"><?php echo JText::_( 'RSFP_TWILIO_SID' ); ?></label></td>
					<td><input type="text" name="rsformConfig[twilio.sid]" value="<?php echo RSFormProHelper::htmlEscape(RSFormProHelper::getConfig('twilio.sid')); ?>" size="100" maxlength="100"></td>
				</tr>
				<tr>
					<td width="200" style="width: 200px;" align="right" class="key"><label for="private"><?php echo JText::_( 'RSFP_TWILIO_TOKEN' ); ?></label></td>
					<td><input type="text" name="rsformConfig[twilio.token]" value="<?php echo RSFormProHelper::htmlEscape(RSFormProHelper::getConfig('twilio.token'));  ?>" size="100" maxlength="100"></td>
				</tr>
				<tr>
					<td width="200" style="width: 200px;" align="right" class="key"><label for="private"><?php echo JText::_( 'RSFP_TWILIO_NUMBER' ); ?></label></td>
					<td><input type="text" name="rsformConfig[twilio.number]" value="<?php echo RSFormProHelper::htmlEscape(RSFormProHelper::getConfig('twilio.number'));  ?>" size="100" maxlength="100"></td>
				</tr>
			</table>
		</div>
		<?php

		return ob_get_clean();
	}

	protected function buildMessage($data, $fields)
	{
		$html = array();

		foreach ($data as $key => $value)
		{
			if (in_array($key, $fields) && ! empty($value))
			{
				if (is_array($value))
				{
					$value = implode(', ', $value);
				}

				$html[] = $key . ': ' . $value;
			}
		}

		return implode("\n", $html);
	}

	protected function buildCheckboxInput($formId, $selected = array())
	{
		$options = array_map(function($item) {
			return JHtml::_('select.option', $item['name'], $item['caption']);
		}, $this->getFormFields($formId));

		$selected = (array) $selected;

		$html = array();

		// Start the checkbox field output.
		$html[] = '<fieldset>';

		// Build the checkbox field output.
		$html[] = '<ul>';
		foreach ($options as $i => $option)
		{
			$checked = in_array($option->value, $selected) ? ' checked="checked"' : '';

			$html[] = '<li>';
			$html[] = '<input type="checkbox" id="twilio_fields_' . $i . '" name="twilio_fields[]"' . ' value="'
				. htmlspecialchars($option->value, ENT_COMPAT, 'UTF-8') . '"' . $checked . '/>';

			$html[] = '<label for="twilio_fields_' . $i . '">' . $option->text . '</label>';
			$html[] = '</li>';
		}
		$html[] = '</ul>';

		// End the checkbox field output.
		$html[] = '</fieldset>';

		return implode("\n", $html);
	}

	protected function getFormName($formId)
	{
		$query = $this->db->getQuery(true)
			->select('FormName')
			->from('#__rsform_forms')
			->where('FormId = ' . (int) $formId);

		return $this->db->setQuery($query)->loadResult();
	}

	protected function getFormFields($formId)
	{
		$query = $this->db->getQuery(true)
			->select('p.ComponentId, p.PropertyName, p.PropertyValue')
			->from('#__rsform_components AS c')
			->leftJoin('#__rsform_properties AS p ON c.ComponentId = p.ComponentId')
			->where('c.FormId = ' . (int) $formId)
			->where('(p.PropertyName = "NAME" OR p.PropertyName = "CAPTION")')
			->order('c.Order');

		$fields = (array) $this->db->setQuery($query)->loadObjectList();

		return $this->groupFields($fields);
	}

	protected function groupFields($fields)
	{
		$groupedFields = array();

		foreach ($fields as $field)
		{
			if (! array_key_exists($field->ComponentId, $groupedFields))
			{
				$groupedFields[$field->ComponentId] = array(
					'caption' => '',
					'name' => ''
				);
			}

			$groupedFields[$field->ComponentId][strtolower($field->PropertyName)] = $field->PropertyValue;
		}

		return $groupedFields;
	}
}
