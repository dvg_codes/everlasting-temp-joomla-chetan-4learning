<?php
/**
 * @version 1.0
 * @package RSform!Pro Twilio Notifications
 * @copyright (C) 2014 www.electriceasel.com
 * @license GPL, http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Twilio SMS Notifications Plugin for RSForm! Pro: Table class
 */
class TwilioTable extends JTable
{
	public $id;
	public $form_id;
	public $published = 0;
	public $numbers;
	public $fields;

	/**
	 * Constructor
	 *
	 * @param object JDatabase connector object
	 */
	public function __construct()
	{
		$db = JFactory::getDbo();

		parent::__construct('#__rsform_twilio', 'id', $db);
	}
}
