INSERT IGNORE INTO `#__rsform_config` (`SettingName`, `SettingValue`) VALUES ('twilio.sid', '');
INSERT IGNORE INTO `#__rsform_config` (`SettingName`, `SettingValue`) VALUES ('twilio.token', '');
INSERT IGNORE INTO `#__rsform_config` (`SettingName`, `SettingValue`) VALUES ('twilio.number', '');

CREATE TABLE IF NOT EXISTS `#__rsform_twilio` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `numbers` text NULL,
  `fields` text NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
