<?php

/**
 * @author Don Gilbert
 * @email don@electriceasel.com
 * @version $Id: com_profiles.php
 * @package Xmap
 * @license GNU/GPL
 * @description Xmap plugin for Joomla's web links component
 */
defined('_JEXEC') or die('Restricted access.');

class xmap_com_profiles
{
    
    static private $_initialized = false;
    /*
     * This function is called before a menu item is printed. We use it to set the
     * proper uniqueid for the item and indicate whether the node is expandible or not
     */

    static function prepareMenuItem($node, &$params)
    {
        $link_query = parse_url($node->link);
        parse_str(html_entity_decode($link_query['query']), $link_vars);
        $view = JArrayHelper::getValue($link_vars, 'view', '');
        if($view == 'families')
        {
	        $node->uid = 'com_profileslist';
	        $node->expandible = true;
        }
        if ($view == 'profile') {
            $id = intval(JArrayHelper::getValue($link_vars, 'id', 0));
            if ($id) {
                $node->uid = 'com_profilesi' . $id;
                $node->expandible = false;
            }
        }
    }

    static function getTree($xmap, $parent, &$params)
    {
        self::initialize($params);

        $link_query = parse_url($parent->link);
        parse_str(html_entity_decode($link_query['query']), $link_vars);
        $view = JArrayHelper::getValue($link_vars, 'view', 0);

        $priority = JArrayHelper::getValue($params, 'profile_priority', $parent->priority, '');
        $changefreq = JArrayHelper::getValue($params, 'profile_changefreq', $parent->changefreq, '');
        if ($priority == '-1')
            $priority = $parent->priority;
        if ($changefreq == '-1')
            $changefreq = $parent->changefreq;

        $params['profile_priority'] = $priority;
        $params['profile_changefreq'] = $changefreq;
        
        $params['menuitem'] = JArrayHelper::getValue($params, 'menuitem');
        
        self::getProfileTree($xmap, $parent, $params);
    }

    static function getProfileTree($xmap, $parent, &$params)
    {
    	$db		= JFactory::getDBO();
    	$query	= $db->getQuery(true);
    	
		$query->select('a.id');
		$query->from('#__profiles_families AS a');
        $query->where('a.state = 1');
        $query->order('a.ordering ASC');
    	
    	$rows = $db->setQuery($query)->loadObjectList();
    	
        $xmap->changeLevel(1);
        foreach ($rows as $profile) {
            $node = new stdclass;
            $node->id = $parent->id;
            $node->uid = $parent->uid . 'c' . $profile->id;
            $node->name = $profile->title;
            $node->link = "index.php?option=com_profiles&view=profile&id={$profile->id}&Itemid={$params['menuitem']}";
            $node->priority = $params['profile_priority'];
            $node->changefreq = $params['profile_changefreq'];
            $node->expandible = false;
            $xmap->printNode($node);
        }
        $xmap->changeLevel(-1);
    }
    
    static public function initialize(&$params)
    {
        if (self::$_initialized) {
            return;
        }
        
        self::$_initialized = true;
        require_once JPATH_SITE.DS.'components'.DS.'com_profiles'.DS.'models'.DS.'families.php';
        require_once JPATH_SITE.DS.'components'.DS.'com_profiles'.DS.'helpers'.DS.'profiles.php';

    }
}