<?php
/*
* @package		Profile Manager
* @copyright	2012 Electric Easel, Inc. www.electriceasel.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die ('Restricted access');

jimport('joomla.plugin.plugin');

class plgUserProfilemanager extends JPlugin {
	
	public function __construct(& $subject, $config) {
		parent::__construct($subject, $config);
		$api = JPATH_ADMINISTRATOR.'/components/com_profiles/api.php';
		if(file_exists($api))
		{
			require_once($api);
		}
	}

	// For Joomla! 1.6+ compat
	public function onUserAfterSave($user, $isnew, $succes, $msg) {
		self::onAfterStoreUser($user, $isnew, $succes, $msg);
	}

	public function onUserAfterDelete($user, $succes, $msg) {
		self::onAfterDeleteUser($user, $succes, $msg);
	}

	// For Joomla! 1.5 compat
	public function onAfterStoreUser($user, $isnew, $succes, $msg){
		if	(!$succes) {
			return false;
		}
		
		return ProfilesAPI::getInstance()->createProfileFromJ($user);
	}
	
	public function onAfterDeleteUser($user, $succes, $msg){
		if	(!$succes) {
			return false;
		}
		
		return ProfilesAPI::getInstance()->deleteProfileFromJ($user);
	}
}