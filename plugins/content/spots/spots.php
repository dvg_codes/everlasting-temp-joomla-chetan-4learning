<?php
/**
 * @package		Joomla.Plugin
 * @subpackage	Content.loadmodule
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class plgContentSpots extends JPlugin
{
	/**
	 * Plugin that loads module positions within content
	 *
	 * @param	string	The context of the content being passed to the plugin.
	 * @param	object	The article object.  Note $article->text is also available
	 * @param	object	The article params
	 * @param	int		The 'page' number
	 */
	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{
		// Don't run this plugin when the content is being indexed
		if ($context == 'com_finder.indexer') {
			return true;
		}

		// simple performance check to determine whether bot should process further
		if (strpos($article->text, '{spots}') === false) {
			return true;
		}

		// expression to search for (positions)
		$regex = '/{spots}/';

		// $matches[0] is full pattern match, $matches[1] is the position
		preg_match_all($regex, $article->text, $matches, PREG_SET_ORDER);
		// No matches, skip this
		if ($matches)
		{	
			foreach ($matches as $match)
			{
				$spots = (int) $this->params->get('spots_available', 2);
				$date  = date($this->params->get('date_format', 'F jS'));
				$replaceText = ($spots === 1) ? "1 spot" : $spots . ' spots';
				$output = "As of {$date} we currently have <span>{$replaceText}</span> available!";
				$article->text = preg_replace("|$match[0]|", addcslashes($output, '\\$'), $article->text);
				
				// Only run once.
				return;
			}
		}
	}
}
